package com.tbit.electrichelper.Bluetooth.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbit.electrichelper.R;


/**
 * Created by Kenny on 2016/3/26 11:36.
 * Desc：
 */
public class CustomProgressDialog extends ProgressDialog {

    private static final String TAG = CustomProgressDialog.class.getSimpleName();

    private AnimationDrawable mAnimation;
    //private Context mContext;
    private ImageView mImageView;
    private String mLoadingTip;
    private TextView mLoadingTv;
    private int mResid;

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, String content, int id) {
        super(context);
        //this.mContext = context;
        this.mLoadingTip = content;
        this.mResid = id;

        setCanceledOnTouchOutside(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    private void initData() {

        mImageView.setBackgroundResource(mResid);

        mAnimation = (AnimationDrawable) mImageView.getBackground();

        mImageView.post(new Runnable() {
            @Override
            public void run() {

                mAnimation.start();

            }
        });
        mLoadingTv.setText(mLoadingTip);

    }

    public void setContent(String str) {
        if (mLoadingTv == null)
            LogUtil.getInstance().info(TAG, "--mLoadingTv = NULL");
        mLoadingTv.setText(str);
    }

    private void initView() {
        //Set the screen content
        setContentView(R.layout.dialog_progress);
        mLoadingTv = (TextView) findViewById(R.id.loadingTv);
        LogUtil.getInstance().info(TAG, "--mLoadingTv = " + mLoadingTv);
        mImageView = (ImageView) findViewById(R.id.loadingIv);
    }

}
