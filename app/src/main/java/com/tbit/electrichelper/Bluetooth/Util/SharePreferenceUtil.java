package com.tbit.electrichelper.Bluetooth.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.Map;

/**
 * Created by Kenny on 2016/2/22 9:08.
 * Desc：Util for saving some local data.
 */
public class SharePreferenceUtil {
    private static final String TAG = SharePreferenceUtil.class.getSimpleName();
    private static SharedPreferences mSharedPreferences;
    private static SharePreferenceUtil instance;


    private SharePreferenceUtil(Context context) {
        mSharedPreferences = MyApplication.getInstance()._preferences;
        Log.i("Kenny", "--preference创建了" + mSharedPreferences);
    }

    /**
     * 初始化单例
     *
     * @param context
     */
    public static synchronized void init(Context context) {
        if (instance == null) {
            instance = new SharePreferenceUtil(context);
        }
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static SharePreferenceUtil getInstance() {
        if (instance == null) {
            throw new RuntimeException("class should init!");
        }
        return instance;
    }

    /**
     * 保存数据
     *
     * @param key
     * @param data
     */
    public void saveData(String key, Object data) {
        String type = data.getClass().getSimpleName();

        SharedPreferences.Editor editor = mSharedPreferences.edit();

        if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) data);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) data);
        } else if ("String".equals(type)) {
            editor.putString(key, (String) data);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) data);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) data);
        }

        editor.apply();
    }

    /**
     * 得到数据
     *
     * @param key
     * @param defValue
     * @return
     */
    public Object getData(String key, Object defValue) {

        String type = defValue.getClass().getSimpleName();
        Log.i(TAG, defValue + "--type--" + type);
        if ("Integer".equals(type)) {
            return mSharedPreferences.getInt(key, (Integer) defValue);
        } else if ("Boolean".equals(type)) {
            return mSharedPreferences.getBoolean(key, (Boolean) defValue);
        } else if ("String".equals(type)) {
            return mSharedPreferences.getString(key, (String) defValue);
        } else if ("Float".equals(type)) {
            return mSharedPreferences.getFloat(key, (Float) defValue);
        } else if ("Long".equals(type)) {
            return mSharedPreferences.getLong(key, (Long) defValue);
        }

        return null;
    }


    public Map<String, ?> getKeys() {
        return mSharedPreferences.getAll();
    }

    /**
     * @param key
     * @return true for yes , false for no
     */
    public boolean isContainKey(String key) {
        return mSharedPreferences.contains(key);
    }

    public void removeKey(String key) {
        mSharedPreferences.edit().remove(key).commit();
    }

}
