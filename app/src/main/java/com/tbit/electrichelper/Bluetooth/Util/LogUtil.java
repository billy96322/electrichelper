package com.tbit.electrichelper.Bluetooth.Util;

import android.util.Log;

/**
 * Created by Kenny on 2016/3/26 10:01.
 * Desc：
 */
public class LogUtil {
    public static final boolean DEBUG = true;
    public static final String TAG = "TAG";
    private static LogUtil sLogUtil;

    private LogUtil() {
    }

    public static LogUtil getInstance() {
        if (sLogUtil == null) {
            synchronized (LogUtil.class) {
                if (sLogUtil == null) {
                    sLogUtil = new LogUtil();
                }
            }
        }
        return sLogUtil;
    }

    public void debug(String tag, String msg) {
        if (DEBUG) {
            Log.d(tag, msg);
        }
    }

    public void info(String tag, String msg) {
        if (DEBUG) {
            Log.i(tag, msg);
        }
    }

    public void error(String tag, String msg) {
        if (DEBUG) {
            Log.e(tag, msg);
        }
    }

    public void warn(String tag, String msg) {
        if (DEBUG) {
            Log.w(tag, msg);
        }
    }
}
