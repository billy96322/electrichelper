package com.tbit.electrichelper.Bluetooth.Interface;

import com.tbit.electrichelper.Services.PacketParserService;
import com.tbit.electrichelper.Services.UartService;

/**
 * Created by Salmon on 2016/4/27 0027.
 */
public interface BluetoothParseServiceImpl {
    PacketParserService getPacketParserService();

    UartService getUartService();
}
