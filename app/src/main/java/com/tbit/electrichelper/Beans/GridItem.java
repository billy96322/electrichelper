package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/1/11.
 */
public class GridItem {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    private String name;
    private int iconRes;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status = false;

}
