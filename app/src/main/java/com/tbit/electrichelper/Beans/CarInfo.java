package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/3/1.
 */
public class CarInfo {
    /**
     * batteryNO :
     * brand :
     * businessPerson :
     * buyTime : 2016-01-21 11:31:57
     * carType :
     * color :
     * driver :
     * driver2 :
     * driverAddress :
     * driverCompany :
     * driverEmail :
     * driverFax :
     * driverMobile :
     * driverMobile2 :
     * driverRemark :
     * driverTel : ******
     * driverTel2 :
     * firmId : 0
     * iDNo :
     * id : 315735
     * installPerson :
     * installPlace :
     * insuranceNO :
     * invoiceNO :
     * joinTime : 2016-01-21 11:31:57
     * machineNO : 201169356
     * no : 201169356
     * overServiceTime : 2099-12-31 00:00:00
     * password : 123456
     * price : 0
     * remark :
     * serviceStatus : 0
     * sim : ******
     * specialRequest :
     * stoped : 1
     * teamId : 1001
     * type : 1
     */

    private String batteryNO;
    private String brand;
    private String businessPerson;
    private String buyTime;
    private String carType;
    private String color;
    private String driver;
    private String driver2;
    private String driverAddress;
    private String driverCompany;
    private String driverEmail;
    private String driverFax;
    private String driverMobile;
    private String driverMobile2;
    private String driverRemark;
    private String driverTel;
    private String driverTel2;
    private int firmId;
    private String iDNo;
    private int id;
    private String installPerson;
    private String installPlace;
    private String insuranceNO;
    private String invoiceNO;
    private String joinTime;
    private String machineNO;
    private String no;
    private String overServiceTime;
    private String password;
    private int price;
    private String remark;
    private int serviceStatus;
    private String sim;
    private String specialRequest;
    private int stoped;
    private int teamId;
    private int type;

    public void setBatteryNO(String batteryNO) {
        this.batteryNO = batteryNO;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setBusinessPerson(String businessPerson) {
        this.businessPerson = businessPerson;
    }

    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setDriver2(String driver2) {
        this.driver2 = driver2;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }

    public void setDriverCompany(String driverCompany) {
        this.driverCompany = driverCompany;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

    public void setDriverFax(String driverFax) {
        this.driverFax = driverFax;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public void setDriverMobile2(String driverMobile2) {
        this.driverMobile2 = driverMobile2;
    }

    public void setDriverRemark(String driverRemark) {
        this.driverRemark = driverRemark;
    }

    public void setDriverTel(String driverTel) {
        this.driverTel = driverTel;
    }

    public void setDriverTel2(String driverTel2) {
        this.driverTel2 = driverTel2;
    }

    public void setFirmId(int firmId) {
        this.firmId = firmId;
    }

    public void setIDNo(String iDNo) {
        this.iDNo = iDNo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInstallPerson(String installPerson) {
        this.installPerson = installPerson;
    }

    public void setInstallPlace(String installPlace) {
        this.installPlace = installPlace;
    }

    public void setInsuranceNO(String insuranceNO) {
        this.insuranceNO = insuranceNO;
    }

    public void setInvoiceNO(String invoiceNO) {
        this.invoiceNO = invoiceNO;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setOverServiceTime(String overServiceTime) {
        this.overServiceTime = overServiceTime;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setServiceStatus(int serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public void setStoped(int stoped) {
        this.stoped = stoped;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBatteryNO() {
        return batteryNO;
    }

    public String getBrand() {
        return brand;
    }

    public String getBusinessPerson() {
        return businessPerson;
    }

    public String getBuyTime() {
        return buyTime;
    }

    public String getCarType() {
        return carType;
    }

    public String getColor() {
        return color;
    }

    public String getDriver() {
        return driver;
    }

    public String getDriver2() {
        return driver2;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public String getDriverCompany() {
        return driverCompany;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public String getDriverFax() {
        return driverFax;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public String getDriverMobile2() {
        return driverMobile2;
    }

    public String getDriverRemark() {
        return driverRemark;
    }

    public String getDriverTel() {
        return driverTel;
    }

    public String getDriverTel2() {
        return driverTel2;
    }

    public int getFirmId() {
        return firmId;
    }

    public String getIDNo() {
        return iDNo;
    }

    public int getId() {
        return id;
    }

    public String getInstallPerson() {
        return installPerson;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public String getInsuranceNO() {
        return insuranceNO;
    }

    public String getInvoiceNO() {
        return invoiceNO;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public String getNo() {
        return no;
    }

    public String getOverServiceTime() {
        return overServiceTime;
    }

    public String getPassword() {
        return password;
    }

    public int getPrice() {
        return price;
    }

    public String getRemark() {
        return remark;
    }

    public int getServiceStatus() {
        return serviceStatus;
    }

    public String getSim() {
        return sim;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public int getStoped() {
        return stoped;
    }

    public int getTeamId() {
        return teamId;
    }

    public int getType() {
        return type;
    }
}
