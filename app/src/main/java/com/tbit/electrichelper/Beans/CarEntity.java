package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/1/19.
 */
public class CarEntity {

    /**
     * carIcon : 1
     * carId : 315735
     * carNO : 201169356
     * machineNO : 201169356
     * serviceStatus : 0
     * serviceTime : 2099-12-31
     * teamId : 1001
     */

    private int carIcon;
    private int carId;
    private String carNO;
    private String machineNO;
    private int serviceStatus;
    private String serviceTime;
    private int teamId;

    @Override
    public String toString() {
        return "CarEntity{" +
                "carIcon=" + carIcon +
                ", carId=" + carId +
                ", carNO='" + carNO + '\'' +
                ", machineNO='" + machineNO + '\'' +
                ", serviceStatus=" + serviceStatus +
                ", serviceTime='" + serviceTime + '\'' +
                ", teamId=" + teamId +
                '}';
    }

    public void setCarIcon(int carIcon) {
        this.carIcon = carIcon;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public void setCarNO(String carNO) {
        this.carNO = carNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public void setServiceStatus(int serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getCarIcon() {
        return carIcon;
    }

    public int getCarId() {
        return carId;
    }

    public String getCarNO() {
        return carNO;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public int getServiceStatus() {
        return serviceStatus;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public int getTeamId() {
        return teamId;
    }
}
