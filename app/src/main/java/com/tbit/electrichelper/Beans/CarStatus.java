package com.tbit.electrichelper.Beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Salmon on 2016/1/19.
 */
public class CarStatus implements Parcelable {

    public static final Creator<CarStatus> CREATOR = new Creator<CarStatus>() {
        @Override
        public CarStatus createFromParcel(Parcel source) {
            return new CarStatus(source);
        }

        @Override
        public CarStatus[] newArray(int size) {
            return new CarStatus[size];
        }
    };
    /**
     * acode :
     * carId : 315735
     * direction : 0
     * gpstime : 2016-01-19 15:23:02
     * la : 22.553546
     * lat : 22.556199
     * lng : 113.954056
     * lo : 113.942625
     * mileage : 1
     * pointed : 0
     * scode :
     * speed : 0
     * stopTime : 0
     * strGGPV : 066:YDGJ=3,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     */

    private String acode;
    private String carId;
    private int direction;
    private String gpstime;
    private double la;
    private double lat;
    private double lng;
    private double lo;
    private int mileage;
    private int pointed;
    private String scode;
    private int speed;
    private int stopTime;
    private String strGGPV;

    public CarStatus() {
    }

    protected CarStatus(Parcel in) {
        this.acode = in.readString();
        this.carId = in.readString();
        this.direction = in.readInt();
        this.gpstime = in.readString();
        this.la = in.readDouble();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.lo = in.readDouble();
        this.mileage = in.readInt();
        this.pointed = in.readInt();
        this.scode = in.readString();
        this.speed = in.readInt();
        this.stopTime = in.readInt();
        this.strGGPV = in.readString();
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getGpstime() {
        return gpstime;
    }

    public void setGpstime(String gpstime) {
        this.gpstime = gpstime;
    }

    public double getLa() {
        return la;
    }

    public void setLa(double la) {
        this.la = la;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLo() {
        return lo;
    }

    public void setLo(double lo) {
        this.lo = lo;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public int getPointed() {
        return pointed;
    }

    public void setPointed(int pointed) {
        this.pointed = pointed;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStopTime() {
        return stopTime;
    }

    public void setStopTime(int stopTime) {
        this.stopTime = stopTime;
    }

    public String getStrGGPV() {
        return strGGPV;
    }

    public void setStrGGPV(String strGGPV) {
        this.strGGPV = strGGPV;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.acode);
        dest.writeString(this.carId);
        dest.writeInt(this.direction);
        dest.writeString(this.gpstime);
        dest.writeDouble(this.la);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
        dest.writeDouble(this.lo);
        dest.writeInt(this.mileage);
        dest.writeInt(this.pointed);
        dest.writeString(this.scode);
        dest.writeInt(this.speed);
        dest.writeInt(this.stopTime);
        dest.writeString(this.strGGPV);
    }
}
