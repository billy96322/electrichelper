package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/1/30.
 */
public class TbitCommonResponse {
    private int ret = -1;
    private String msg = "未知错误";
    private String data;

    @Override
    public String toString() {
        return "TbitResponse{" +
                "ret=" + ret +
                ", msg='" + msg + '\'' +
                ", data='" + data + '\'' +
                '}';
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
