package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/2/1.
 */
public class Image {
    String url;
    String id;

    public Image() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
