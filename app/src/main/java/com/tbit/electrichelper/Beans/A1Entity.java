package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/6/12 0012.
 */
public class A1Entity {
    /**
     * boundTime : 2016-06-12 10:52:17
     * machineNO : 135790246
     * platformUserId : 15
     * remark : remark
     */

    private String boundTime;
    private String machineNO;
    private int platformUserId;
    private String remark;

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public String getBoundTime() {
        return boundTime;
    }

    public void setBoundTime(String boundTime) {
        this.boundTime = boundTime;
    }

    public int getPlatformUserId() {
        return platformUserId;
    }

    public void setPlatformUserId(int platformUserId) {
        this.platformUserId = platformUserId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
