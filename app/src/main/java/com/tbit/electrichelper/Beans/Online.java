package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/3/14.
 */
public class Online {

    /**
     * carId : 542476
     * online : true
     * speed : 0
     */

    private String carId;
    private boolean online;
    private int speed;

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
