package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/6/30 0030.
 */
public class ShareResult {
    private long timeMile;
    private String tid;
    private int status;

    public ShareResult(int status) {
        this.status = status;
    }

    public ShareResult(long timeMile, String tid, int status) {
        this.timeMile = timeMile;
        this.tid = tid;
        this.status = status;
    }

    public ShareResult() {
    }

    public long getTimeMile() {
        return timeMile;
    }

    public void setTimeMile(long timeMile) {
        this.timeMile = timeMile;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
