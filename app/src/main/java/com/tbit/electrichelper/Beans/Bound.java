package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/2/25.
 */
public class Bound {
    public int getPlatformUserId() {
        return platformUserId;
    }

    public void setPlatformUserId(int platformUserId) {
        this.platformUserId = platformUserId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getBoundTime() {
        return boundTime;
    }

    public void setBoundTime(String boundTime) {
        this.boundTime = boundTime;
    }

    private int platformUserId;
    private int carId;
    private String boundTime;
}
