package com.tbit.electrichelper.Beans;

import java.util.List;

/**
 * Created by Salmon on 2016/1/19.
 */
public class CarData {

    /**
     * carNO : 201169356
     * carIcon : 1
     * carId : 315735
     * teamId : 1001
     * serviceStatus : 0
     * machineNO : 201169356
     * serviceTime : 2099-12-31
     */

    private List<CarsEntity> cars;

    public List<CarsEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarsEntity> cars) {
        this.cars = cars;
    }

    public static class CarsEntity {
        private String carNO;
        private int carIcon;
        private String carId;
        private int teamId;
        private int serviceStatus;
        private String machineNO;
        private String serviceTime;

        public String getCarId() {
            return carId;
        }

        public void setCarId(String carId) {
            this.carId = carId;
        }

        @Override
        public String toString() {
            return "CarsEntity{" +
                    "carNO='" + carNO + '\'' +
                    ", carIcon=" + carIcon +
                    ", carId=" + carId +
                    ", teamId=" + teamId +
                    ", serviceStatus=" + serviceStatus +
                    ", machineNO='" + machineNO + '\'' +
                    ", serviceTime='" + serviceTime + '\'' +
                    '}';
        }

        public String getCarNO() {
            return carNO;
        }

        public void setCarNO(String carNO) {
            this.carNO = carNO;
        }

        public int getCarIcon() {
            return carIcon;
        }

        public void setCarIcon(int carIcon) {
            this.carIcon = carIcon;
        }

        public int getTeamId() {
            return teamId;
        }

        public void setTeamId(int teamId) {
            this.teamId = teamId;
        }

        public int getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(int serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public String getMachineNO() {
            return machineNO;
        }

        public void setMachineNO(String machineNO) {
            this.machineNO = machineNO;
        }

        public String getServiceTime() {
            return serviceTime;
        }

        public void setServiceTime(String serviceTime) {
            this.serviceTime = serviceTime;
        }
    }
}
