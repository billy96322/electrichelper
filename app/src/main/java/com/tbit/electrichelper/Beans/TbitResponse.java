package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2015/11/12.
 */
public class TbitResponse {
    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    private boolean res = false;
    private String desc = "未知错误";
    private String result;

    @Override
    public String toString() {
        return "TbitResponse{" +
                "res=" + res +
                ", desc='" + desc + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
