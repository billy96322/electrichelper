package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/6/7 0007.
 */
public class Car {
    String carId;
    int type;

    public Car() {
    }
    public Car(String carId, int type) {
        this.carId = carId;
        this.type = type;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
