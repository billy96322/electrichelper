package com.tbit.electrichelper.Beans;

import java.util.List;

/**
 * Created by Salmon on 2016/1/19.
 */
public class Message {

    public List<CarStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<CarStatus> statuses) {
        this.statuses = statuses;
    }

    private List<CarStatus> statuses;

    private List<String> respCommands;

    public List<String> getRespCommands() {
        return respCommands;
    }

    public void setRespCommands(List<String> respCommands) {
        this.respCommands = respCommands;
    }
}
