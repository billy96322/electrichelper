package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/1/29.
 */
public class ServicePoint {

    /**
     * address : 深圳市南山区
     * introduce : 介绍
     * lat : 22.565702
     * lon : 113.954593
     * name : 雅迪维修点1
     * phone : 13435792203
     * remark : 备注
     * sellPointId : 3
     */

    private String address;
    private String introduce;
    private double lat;
    private double lon;
    private String name;
    private String phone;
    private String remark;
    private int sellPointId;

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setSellPointId(int sellPointId) {
        this.sellPointId = sellPointId;
    }

    public String getAddress() {
        return address;
    }

    public String getIntroduce() {
        return introduce;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getRemark() {
        return remark;
    }

    public int getSellPointId() {
        return sellPointId;
    }
}
