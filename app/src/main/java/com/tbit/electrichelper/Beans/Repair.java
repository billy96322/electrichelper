package com.tbit.electrichelper.Beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Salmon on 2016/2/1.
 */
public class Repair implements Parcelable {

    /**
     * faultCode : 故障码
     * faultDescribe : 故障描述
     * finishTime : 2016-01-29 11:43:54
     * handleTime : 2016-01-30 15:48:22
     * lat : 22.123456
     * lon : 113.123456
     * phone : 13435792203
     * remark : 备注
     * repairMan : 张三
     * repairRecordId : 1
     * repairTime : 2016-01-12 00:00:00
     * sellPointId : 1
     * status : 1
     * userId : 1
     */

    private String faultCode;
    private String faultDescribe;
    private String finishTime;
    private String handleTime;
    private double lat;
    private double lon;
    private String phone;
    private String remark;
    private String repairMan;
    private int repairRecordId;
    private String repairTime;
    private int sellPointId;
    private int status;
    private int userId;

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public void setFaultDescribe(String faultDescribe) {
        this.faultDescribe = faultDescribe;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public void setHandleTime(String handleTime) {
        this.handleTime = handleTime;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setRepairMan(String repairMan) {
        this.repairMan = repairMan;
    }

    public void setRepairRecordId(int repairRecordId) {
        this.repairRecordId = repairRecordId;
    }

    public void setRepairTime(String repairTime) {
        this.repairTime = repairTime;
    }

    public void setSellPointId(int sellPointId) {
        this.sellPointId = sellPointId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public String getFaultDescribe() {
        return faultDescribe;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public String getHandleTime() {
        return handleTime;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getPhone() {
        return phone;
    }

    public String getRemark() {
        return remark;
    }

    public String getRepairMan() {
        return repairMan;
    }

    public int getRepairRecordId() {
        return repairRecordId;
    }

    public String getRepairTime() {
        return repairTime;
    }

    public int getSellPointId() {
        return sellPointId;
    }

    public int getStatus() {
        return status;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.faultCode);
        dest.writeString(this.faultDescribe);
        dest.writeString(this.finishTime);
        dest.writeString(this.handleTime);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
        dest.writeString(this.phone);
        dest.writeString(this.remark);
        dest.writeString(this.repairMan);
        dest.writeInt(this.repairRecordId);
        dest.writeString(this.repairTime);
        dest.writeInt(this.sellPointId);
        dest.writeInt(this.status);
        dest.writeInt(this.userId);
    }

    public Repair() {
    }

    protected Repair(Parcel in) {
        this.faultCode = in.readString();
        this.faultDescribe = in.readString();
        this.finishTime = in.readString();
        this.handleTime = in.readString();
        this.lat = in.readDouble();
        this.lon = in.readDouble();
        this.phone = in.readString();
        this.remark = in.readString();
        this.repairMan = in.readString();
        this.repairRecordId = in.readInt();
        this.repairTime = in.readString();
        this.sellPointId = in.readInt();
        this.status = in.readInt();
        this.userId = in.readInt();
    }

    public static final Creator<Repair> CREATOR = new Creator<Repair>() {
        public Repair createFromParcel(Parcel source) {
            return new Repair(source);
        }

        public Repair[] newArray(int size) {
            return new Repair[size];
        }
    };
}
