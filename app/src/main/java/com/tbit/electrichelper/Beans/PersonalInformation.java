package com.tbit.electrichelper.Beans;

/**
 * Created by Salmon on 2016/2/25.
 */
public class PersonalInformation {

    /**
     * birthday :
     * email :
     * joinTime : 2016-02-25 15:19:10
     * name :
     * password : E10ADC3949BA59ABBE56E057F20F883E
     * phone : 13723769269
     * platformType : 0
     * platformUserId : 5
     * sex : 1
     */

    private String birthday;
    private String email;
    private String joinTime;
    private String name;
    private String password;
    private String phone;
    private int platformType;
    private int platformUserId;
    private int sex;

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPlatformType(int platformType) {
        this.platformType = platformType;
    }

    public void setPlatformUserId(int platformUserId) {
        this.platformUserId = platformUserId;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public int getPlatformType() {
        return platformType;
    }

    public int getPlatformUserId() {
        return platformUserId;
    }

    public int getSex() {
        return sex;
    }

    @Override
    public String toString() {
        return "PersonalInformation{" +
                "birthday='" + birthday + '\'' +
                ", email='" + email + '\'' +
                ", joinTime='" + joinTime + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", platformType=" + platformType +
                ", platformUserId=" + platformUserId +
                ", sex=" + sex +
                '}';
    }
}
