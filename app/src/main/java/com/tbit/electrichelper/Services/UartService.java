package com.tbit.electrichelper.Services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Constant;
import com.tbit.electrichelper.Bluetooth.Util.SharePreferenceUtil;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Kenny on 2016/2/2 13:35.
 * Desc：
 */
public class UartService extends Service {
    public final static String ACTION_GATT_CONNECTED =
            "com.tbit.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.tbit.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.tbit.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.tbit.ACTION_DATA_AVAILABLE";
    public final static String ACTION_NOTIFICATION =
            "com.tbit.ACTION_NOTIFICATION";
    public final static String EXTRA_DATA =
            "com.tbit.EXTRA_DATA";
    public final static String DEVICE_DOES_NOT_SUPPORT_UART =
            "com.tbit.DEVICE_DOES_NOT_SUPPORT_UART";
    public final static String ACTION_RSSI_CHANGE =
            "com.tbit.ACTION_RSSI_CHANGE";
    public static final UUID SPS_SERVICE_UUID = UUID.fromString("000056ef-0000-1000-8000-00805f9b34fb");
    public static final UUID SPS_TX_UUID = UUID.fromString("000034e1-0000-1000-8000-00805f9b34fb");
    public static final UUID SPS_RX_UUID = UUID.fromString("000034e2-0000-1000-8000-00805f9b34fb");
    public static final UUID SPS_NOTIFY_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    private final static String TAG = UartService.class.getSimpleName();
    private final static int FAST_RECONNECT_COUNT = 5;
    private final static int FAST_RECONNECT_DURATION = 5000;
    private final static int SLOW_RECONNECT_DURATION = 5000;
    private static final int ARRAY_SIZE = 10;
    //    private static final int HANDLE_FAST_RECONNECT = 0;
    private static final int HANDLE_SLOW_RECONNECT = 1;
    private static int mArrayCursor = 0;
    private final IBinder mBinder = new LocalBinder();
    private boolean needReconnect = true;
    private String mDeviceAddr;
    private String mDeviceTid;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothDevice device;
    private int mConnectionState = STATE_DISCONNECTED;
    private boolean isServiceAlive;
    private int[] arrayRssi = new int[ARRAY_SIZE];
    private SharePreferenceUtil mSharePreferenceUtil;
    private int mReconnectCount = 0;
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            gatt.readRemoteRssi();
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mReconnectCount = 0;
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "--Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "--Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "--Disconnected from GATT server.");
                if (needReconnect) {
                    broadcastUpdate(intentAction);
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //将已经连接过的设备保存至本地，做历史记录

                if (!mSharePreferenceUtil.isContainKey(device.getAddress()))
                    mSharePreferenceUtil.saveData(device.getAddress(), device.getName());

                if (!mSharePreferenceUtil.isContainKey(Constant.SP_HAS_A1_DEVICE)) {
                    mSharePreferenceUtil.saveData(Constant.SP_HAS_A1_DEVICE, true);
                }

                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                //When app connected to device,send the broadcast to update the connected device name in ControlFragment.
                broadcastUpdate(Constant.ACTION_DEVICE_CONN_OK, gatt.getDevice().getName());
            } else {
                Log.w(TAG, "--onServicesDiscovered received: " + status);
            }

            List<BluetoothGattService> list = getSupportedGattServices();
            for (BluetoothGattService service : list) {
                Log.i(TAG, "--!!" + service.getUuid() + "--" + service.toString());
                List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();
                for (BluetoothGattCharacteristic characteristic : gattCharacteristics) {
                    Log.i(TAG, "--@@" + characteristic.getUuid() + "--" + characteristic.toString());
                    List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();
                    for (BluetoothGattDescriptor descriptor : descriptors) {
                        Log.i(TAG, "--##" + descriptor.getUuid() + "--" + descriptor.toString());
                    }
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "--onCharacteristicRead");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.i(TAG, "--读取操作成功");
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            if (mArrayCursor == ARRAY_SIZE)
                mArrayCursor = 0;
            //以绝对值形式存储
            arrayRssi[mArrayCursor] = Math.abs(rssi);
            mArrayCursor++;
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic,
                                          int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
//                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                Log.i(TAG, "--onCharacteristicWrite  成功  " + characteristic.getUuid() + "--" + characteristic.getValue());
//                broadcastUpdate(Constant.ACTION_WRITE_OK);
//                ((MyApplication) getApplication()).sendThread.updateStatus(true);
                EventBus.getDefault().post(new Event.UpdateStatus(true));
            } else {
                Log.i(TAG, "--onCharacteristicWrite  失败  ");
                MobclickAgent.reportError(getApplicationContext(), TAG + "--onCharacteristicWrite  failed  ");
                broadcastUpdate(Constant.ACTION_SEND_FAIL);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            Log.i(TAG, "--onCharacteristicChanged--" + characteristic.getUuid() + "--" + characteristic.getValue());
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
//                case HANDLE_FAST_RECONNECT:
//                    if (mReconnectCount < FAST_RECONNECT_COUNT) {
//                        Log.d(TAG, "handleMessage: 尝试重连 - 快速 " + mReconnectCount);
//                        connectGatt();
//                        mReconnectCount++;
//                        sendEmptyMessageDelayed(HANDLE_FAST_RECONNECT, FAST_RECONNECT_DURATION);
//                    }
//                    break;
                case HANDLE_SLOW_RECONNECT:
                    connectGatt();
                    Log.d(TAG, "handleMessage: 尝试重连 - 慢 ");
                    sendEmptyMessageDelayed(HANDLE_SLOW_RECONNECT, SLOW_RECONNECT_DURATION);
                    break;
            }
        }
    };

    public int getRssiAvg() {
        //去掉一个最大值，去掉一个最小值
        Arrays.sort(arrayRssi);
        int sum = 0;

        for (int i = 1; i < arrayRssi.length - 1; i++) {
            sum += arrayRssi[i];
        }
        Log.i(TAG, "avgRssi: " + (sum / (ARRAY_SIZE - 2)));
        return sum / (ARRAY_SIZE - 2);
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//        Log.i(TAG, "--broadcastUpdate" + intent.getAction());
    }

    private void broadcastUpdate(final String action, final String data) {
        final Intent intent = new Intent(action);
        intent.putExtra(Constant.EXTRA_DATA, data);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        Log.i(TAG, "--broadcastUpdate" + intent.getAction());
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        if (SPS_RX_UUID.equals(characteristic.getUuid())) {
            Log.i(TAG, "--broadcastUpdate  characteristic" + characteristic.getUuid() + "--" + characteristic.getValue());
            // Log.d(TAG, String.format("Received TX: %d",characteristic.getValue() ));
            intent.putExtra(EXTRA_DATA, characteristic.getValue());
        } else {

        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        //当蓝牙服务绑定的时候，接着绑定数据包解析的服务
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind: ");
        return super.onUnbind(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "--Unable to initialize BluetoothManager.");
                MobclickAgent.reportError(getApplicationContext(), TAG + "--Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "--Unable to obtain a BluetoothAdapter.");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address, String tid) {
        mDeviceAddr = address;
        mDeviceTid = tid;
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "--BluetoothAdapter not initialized or unspecified address.");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            Log.d(TAG, "--Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }
        device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "--Device not found.  Unable to connect.");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "--Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    public void reConnect() {
//        if (mBluetoothGatt != null) {
//            boolean result = mBluetoothGatt.connect();
//            Log.i(TAG, "-->>reConnect--" + result);
//
//            Toast.makeText(getApplication(), "reConnect", Toast.LENGTH_LONG).show();
//        } else {
//            //If mBluetoothGatt = null ,try to create a new connection
//            connect(mDeviceAddr);
//        }
        if (checkIsSamsung()) {
            //这里最好 Sleep 300毫秒，测试发现有时候三星手机断线之后立马调用connect会容易蓝牙奔溃
//            if (mBluetoothGatt != null) {
//                mBluetoothGatt.connect();
//            }
//            mHandler.sendEmptyMessageDelayed(HANDLE_FAST_RECONNECT, 500);
            mHandler.sendEmptyMessageDelayed(HANDLE_SLOW_RECONNECT, SLOW_RECONNECT_DURATION);
        } else {
//            mHandler.sendEmptyMessage(HANDLE_FAST_RECONNECT);
            mHandler.sendEmptyMessageDelayed(HANDLE_SLOW_RECONNECT, SLOW_RECONNECT_DURATION);
        }
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        mConnectionState = STATE_DISCONNECTED;
        needReconnect = false;
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "--BluetoothAdapter not initialized");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        Log.w(TAG, "--mBluetoothGatt closed");
        mBluetoothDeviceAddress = null;
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "--BluetoothAdapter not initialized");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }


    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    /*
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);


        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }*/

    /**
     * 改造一下
     */
    public void readCharacteristic(UUID RXUUID) {
        BluetoothGattService RxService = mBluetoothGatt.getService(SPS_SERVICE_UUID);
        showMessage("mBluetoothGatt null" + mBluetoothGatt);
        if (RxService == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            Log.i(TAG, "--Rx service not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Rx service not found!");
            return;
        }
        BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(SPS_RX_UUID);
        if (RxChar == null) {
            Log.i(TAG, "--Rx Characteristic not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Rx Characteristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        if (mBluetoothGatt == null)
            Log.i(TAG, "--mBluetoothGatt=null");
        boolean status = mBluetoothGatt.readCharacteristic(RxChar);

        Log.d(TAG, "--read RXchar - status=" + status);
    }

    /**
     * Enable TXNotification
     *
     * @return
     */
    public void enableTXNotification() {
        if (mBluetoothGatt == null) {
            showMessage("enableTXNotification mBluetoothGatt null" + mBluetoothGatt);
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        BluetoothGattService RxService = mBluetoothGatt.getService(SPS_SERVICE_UUID);
        if (RxService == null) {
            showMessage("--Rx service not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Rx service not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(SPS_RX_UUID);
        if (RxChar == null) {
            showMessage("--Tx charateristic not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Tx charateristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(RxChar, true);

        BluetoothGattDescriptor descriptor = RxChar.getDescriptor(SPS_NOTIFY_DESCRIPTOR);
        if (descriptor == null) {
            Log.i(TAG, "--找不到descriptor");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--descriptor not found");
            return;
        }
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);

    }

    public void writeRXCharacteristic(byte[] value) {

        Log.d("asd", "writeRXCharacteristic: " + mBluetoothGatt);
        BluetoothGattService Service = mBluetoothGatt.getService(SPS_SERVICE_UUID);
        if (mBluetoothGatt == null) {
            showMessage("writeRXCharacteristic mBluetoothGatt null " + mBluetoothGatt);
        }
        if (Service == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            Log.i(TAG, "--service not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--service not found!  DEVICE_DOES_NOT_SUPPORT_UART");
            return;
        }
        BluetoothGattCharacteristic TxChar = Service.getCharacteristic(SPS_TX_UUID);
        if (TxChar == null) {
            Log.i(TAG, "--Tx Characteristic not found!");
            MobclickAgent.reportError(getApplicationContext(), TAG + "--Tx Characteristic not found!  DEVICE_DOES_NOT_SUPPORT_UART");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        TxChar.setValue(value);
        if (mBluetoothGatt == null)
            Log.i(TAG, "--mBluetoothGatt=null");
        boolean status = mBluetoothGatt.writeCharacteristic(TxChar);
        if (status) {
//            Toast.makeText(getApplicationContext(),"指令下发成功！",Toast.LENGTH_LONG).show();
            Log.d(TAG, "--指令下发成功！");
        } else {
//            Toast.makeText(getApplicationContext(),"指令下发失败！",Toast.LENGTH_LONG).show();
            Log.d(TAG, "--指令下发失败！");
        }
    }

    private boolean checkIsSamsung() { //此方法是我自行使用众多三星手机总结出来，不一定很准确
        String brand = android.os.Build.BRAND;
        if (brand.toLowerCase().equals("samsung")) {
            return true;
        }
        return false;
    }

    public void connectGatt() {
        Log.d(TAG, "connectGatt: " + mConnectionState);
        if (mConnectionState == STATE_CONNECTED || !needReconnect) {
//            mHandler.removeMessages(HANDLE_FAST_RECONNECT);
            return;
        }
        if (device != null) {
            if (mBluetoothGatt != null) {
                mBluetoothGatt.disconnect();
                mBluetoothGatt.close();
                mBluetoothGatt = null;
            }
            mBluetoothGatt = device.connectGatt(this, false,
                    mGattCallback);
        } else {
            Log.e(TAG, "the bluetoothDevice is null, please reset the bluetoothDevice");
            MobclickAgent.reportError(getApplicationContext(), TAG + "the bluetoothDevice is null, please reset the bluetoothDevice");
        }
    }

    private void showMessage(String msg) {
        Log.e(TAG, msg);
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public BluetoothGatt getGatt() {
        return this.mBluetoothGatt;
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onWriteRXCharacteristic(Event.WriteRXCharacteristic event) {
        writeRXCharacteristic(event.sendData);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isServiceAlive = true;
        EventBus.getDefault().register(this);
        mSharePreferenceUtil = SharePreferenceUtil.getInstance();
//        new RefreshRssiThread().start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceAlive = false;
        disconnect();
        EventBus.getDefault().unregister(this);
    }

    public void setNeedReconnect(boolean needReconnect) {
        this.needReconnect = needReconnect;
    }

    public String getDeviceTid() {
        return mDeviceTid;
    }

    public int getConnectionState() {
        return mConnectionState;
    }

    public class LocalBinder extends Binder {
        public UartService getService() {
            return UartService.this;
        }
    }

    class RefreshRssiThread extends Thread {
        @Override
        public void run() {
            super.run();
            while (isServiceAlive) {
                if (mBluetoothGatt != null) {
                    mBluetoothGatt.readRemoteRssi();
                    SystemClock.sleep(300);
                }
            }
        }
    }
}
