package com.tbit.electrichelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.baidu.mapapi.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igexin.sdk.PushManager;
import com.tbit.electrichelper.Beans.A1Entity;
import com.tbit.electrichelper.Beans.Car;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.PersonalInformation;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Util.SharePreferenceUtil;
import com.tbit.electrichelper.Util.TbitProtocol;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Salmon on 2015/11/11.
 */
public class MyApplication extends BaseApplication {

    private static MyApplication myApplication = null;
    public SharedPreferences _preferences;
    public String clientId = "";
    public LatLng myPosition = null;
    public String myPositionDesc = "";
    public float myPositionRadius = 0;
    public List<Activity> activities = new ArrayList<>();
    public Map<String, ConcurrentHashMap<String, Object>> globalData = null;
    public List<CarData.CarsEntity> a2Cars = new ArrayList<>();
    public List<A1Entity> a1Cars = new ArrayList<>();
    public Map<String, Object> noBeloningData = null;
    public String common_token = "";
    public int connectionFailTime;
    /**
     * 蓝牙相关
     */
    public String deviceName;
    public String deviceAddr;
    // 标识是否有绑定过蓝牙设备，如果没绑定过则不会出现蓝牙模式弹框
    public boolean hasA1Device = false;
    // 标识是否切换至蓝牙模式
    public boolean isOfflineMode = false;
    private String localCookie = "";
    private int loginStatus;
    private Car curCar = new Car();
    private Gson mGson;

    public String shareSecretCreateByLocal = "";

    public static MyApplication getInstance() {
        return myApplication;
    }

    public int getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(int loginStatus) {
        this.loginStatus = loginStatus;
    }

    public Car getCurCar() {
        return curCar;
    }

    public void setCurCar(String carId, int type) {
        EventBus.getDefault().post(new Event.ChangeDevice(new Car(carId, type)));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _preferences = getSharedPreferences(TbitProtocol.SP_NAME, MODE_PRIVATE);
        myApplication = this;
        PushManager.getInstance().initialize(this.getApplicationContext());
        SharePreferenceUtil.init(getApplicationContext());
        mGson = new Gson();
        loginStatus = TbitProtocol.LOGGED_OUT;
        curCar.setCarId("");
        globalData = new ConcurrentHashMap<>();
        noBeloningData = new HashMap<>();
        connectionFailTime = 0;
        try {
            File file = new File(TbitProtocol.EXTERNAL_STORAGE, TbitProtocol.BAIDU_NAVI);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 读取A1设备的本地数据
        a1Cars.addAll(readA1Data());

        // 读取最后一次登录类型
        int lastType = _preferences.getInt(TbitProtocol.SP_LOW_LAST_CHOSEN_TYPE,
                TbitProtocol.TYPE_A2);
        curCar.setType(lastType);

        // 读取本地分享码，防止分享给自己
        shareSecretCreateByLocal = _preferences.getString(TbitProtocol.SP_A1_SHARE_LOCAL, "");
    }

    public void setCurGlobal(String key, Object object) {
        setGlobal(curCar.getCarId(), key, object);
    }

    public Object getCurGlobal(String key) {
        return getGlobal(curCar.getCarId(), key);
    }

    public void setGlobal(String carId, String key, Object object) {
        if (globalData.get(carId) == null) {
            initCar(carId);
        }
        globalData.get(carId).put(key, object);
    }

    public Object getGlobal(String carId, String key) {
        if (globalData.get(carId) == null) {
            return null;
        }
        return globalData.get(carId).get(key);
    }

    public void initCar(String carId) {
        globalData.put(carId, new ConcurrentHashMap<String, Object>());
    }

    public String getLocalCookie() {
        return localCookie;
    }

    public void setLocalCookie(String cookie) {
        localCookie = cookie;
    }

    public Object getNobeloning(String key) {
        return noBeloningData.get(key);
    }

    public void putNobeloning(String key, Object object) {
        noBeloningData.put(key, object);
    }

    public boolean hasA2Bound() {
        if (a2Cars.size() == 0) {
            return false;
        }
        return true;
    }

    public boolean hasA1Bound() {
        if (a1Cars.size() == 0) {
            return false;
        }
        return true;
    }

    public boolean hasAnyCarBound() {
        return hasA1Bound() || hasA2Bound();
    }

    public int getUserId() {
        int result = 0;
        PersonalInformation info = (PersonalInformation) getNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO);
        if (info != null) {
            result = info.getPlatformUserId();
        }
        return result;
    }

    public boolean isNetworkConnected() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI ||
                    activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                result = true;
            }
        } else {
            result = false;
        }
        return result;
    }

    public void saveA1Data(List<A1Entity> a1Cars) {
        if (a1Cars == null) {
            return;
        }
        String s = mGson.toJson(a1Cars);
        _preferences.edit().putString(TbitProtocol.SP_A1_OFFLINE_DATA, s).apply();
    }

    public List<A1Entity> readA1Data() {
        List<A1Entity> list = new ArrayList<>();
        String s = _preferences.getString(TbitProtocol.SP_A1_OFFLINE_DATA, "");
        if (!TextUtils.isEmpty(s)) {
            list = mGson.fromJson(s, new TypeToken<List<A1Entity>>(){}.getType());
        }
        return list;
    }

    public void saveA1StateData(String tid, Map<Integer, String> map) {
        if (map == null)
            return;
        String s = mGson.toJson(map);
        _preferences.edit().putString(tid + TbitProtocol.SP_A1_STATE_DATA, s).apply();
    }

    public Map<Integer, String> readA1StateData(String tid) {
        Map<Integer, String> map = new HashMap<>();
        String s = _preferences.getString(tid + TbitProtocol.SP_A1_STATE_DATA, "");
        if (!TextUtils.isEmpty(s)) {
            map = mGson.fromJson(s, new TypeToken<Map<Integer, String>>(){}.getType());
        }
        return map;
    }
}
