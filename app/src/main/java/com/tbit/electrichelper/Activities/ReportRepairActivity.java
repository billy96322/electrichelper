package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.baidu.mapapi.model.LatLng;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.RequestParams;
import com.tbit.electrichelper.Adapter.GalleryAdapter;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.Image;
import com.tbit.electrichelper.Beans.TbitCommonResponse;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.UploadActivity;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ReportRepairActivity extends UploadActivity implements ISimpleDialogListener, UploadActivity.UpLoadFile {
    private static final String BUNDLE_KEY_SELL_POINT_ID = "KEY_sellPointId";
    private static final String BUNDLE_KEY_LATLNG = "KEY_latlng";
    private final int TAG_SUBMIT = 0;

    @Bind(R.id.textInput_name)
    TextInputLayout textInputName;
    @Bind(R.id.textInput_phone)
    TextInputLayout textInputPhone;
//    @Bind(R.id.textInput_errCode)
//    TextInputLayout textInputErrCode;
    @Bind(R.id.textInput_remark)
    TextInputLayout textInputRemark;
    @Bind(R.id.btn_gallery)
    Button btnGallery;
    @Bind(R.id.btn_camera)
    Button btnCamera;
    @Bind(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    @Bind(R.id.textInput_feedBack)
    TextInputLayout textInputFeedBack;

    private Handler mHandler;
    private int sellPointId;
    private LatLng position;
    private List<Image> images = new ArrayList<>();
    private GalleryAdapter adapter;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_camera:
                    if (images.size() >= 10) {
                        showCrouton("最多只能上传10张图片");
                        break;
                    }
                    snapShot();
                    break;
                case R.id.btn_gallery:
                    if (images.size() >= 10) {
                        showCrouton("最多只能上传10张图片");
                        break;
                    }
                    loadImg();
                    break;
                case R.id.btn_submit: {
                    if (varifyInput()) {
                        String name = getStringFromTextInputLayout(textInputName);
                        String phone = getStringFromTextInputLayout(textInputPhone);
//                        String errCode = getStringFromTextInputLayout(textInputErrCode);
                        String feedBack = getStringFromTextInputLayout(textInputFeedBack);
                        String remark = getStringFromTextInputLayout(textInputRemark);

                        CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);

                        Map<String, String> map = new HashMap<String, String>();
                        map.put(NetworkProtocol.PARAMKEY_USERID, String.valueOf(application.getUserId()));
                        map.put(NetworkProtocol.PARAMKEY_SELLPOINTID, String.valueOf(sellPointId));
                        map.put(NetworkProtocol.PARAMKEY_REPAIRMAN, name);
                        map.put(NetworkProtocol.PARAMKEY_PHONE, phone);
                        try {
                            map.put(NetworkProtocol.PARAMKEY_FAULT_CODE, status.getStrGGPV().split("=")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        map.put(NetworkProtocol.PARAMKEY_FAULT_DESCRIBE, feedBack);
                        map.put(NetworkProtocol.PARAMKEY_LON, String.valueOf(position.longitude));
                        map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(position.latitude));
                        if (remark.length() != 0) {
                            map.put(NetworkProtocol.PARAMKEY_REMARK, remark);
                        }
                        postNetworkJson(TAG_SUBMIT, NetworkProtocol.URL_REPAIR_REPORT, map, true);
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };

    public static void startActivity(Context context, int sellPointId, LatLng latLng) {
        Intent intent = new Intent();
        intent.putExtra(BUNDLE_KEY_SELL_POINT_ID, sellPointId);
        intent.putExtra(BUNDLE_KEY_LATLNG, latLng);
        intent.setClass(context, ReportRepairActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_report_repair);
        ButterKnife.bind(this);

        initToolBar();

        setOnUploadListener(this);

        sellPointId = getIntent().getIntExtra(BUNDLE_KEY_SELL_POINT_ID, 0);
        position = getIntent().getParcelableExtra(BUNDLE_KEY_LATLNG);

        createHandler();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        idRecyclerview.setLayoutManager(linearLayoutManager);
        adapter = new GalleryAdapter(this, images, true);
        idRecyclerview.setAdapter(adapter);

        textInputName.setErrorEnabled(false);
        textInputPhone.setErrorEnabled(false);
//        textInputErrCode.setErrorEnabled(false);
        textInputRemark.setErrorEnabled(false);
        textInputFeedBack.setErrorEnabled(false);

        btnGallery.setOnClickListener(onClickListener);

        btnCamera.setOnClickListener(onClickListener);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (varifyInput()) {
                    String name = getStringFromTextInputLayout(textInputName);
                    String phone = getStringFromTextInputLayout(textInputPhone);
//                    String errCode = getStringFromTextInputLayout(textInputErrCode);
                    String feedBack = getStringFromTextInputLayout(textInputFeedBack);
                    String remark = getStringFromTextInputLayout(textInputRemark);

                    CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);

                    Map<String, String> map = new HashMap<String, String>();
                    map.put(NetworkProtocol.PARAMKEY_USERID, String.valueOf(application.getUserId()));
                    map.put(NetworkProtocol.PARAMKEY_SELLPOINTID, String.valueOf(sellPointId));
                    map.put(NetworkProtocol.PARAMKEY_REPAIRMAN, name);
                    map.put(NetworkProtocol.PARAMKEY_PHONE, phone);
                    try {
                        map.put(NetworkProtocol.PARAMKEY_FAULT_CODE, status.getStrGGPV().split("=")[1]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    map.put(NetworkProtocol.PARAMKEY_FAULT_DESCRIBE, feedBack);
                    map.put(NetworkProtocol.PARAMKEY_LON, String.valueOf(position.longitude));
                    map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(position.latitude));
                    if (images.size() > 0) {
                        map.put(NetworkProtocol.PARAMKEY_IMAGEIDS, getImageIds());
                    }
                    if (remark.length() != 0) {
                        map.put(NetworkProtocol.PARAMKEY_REMARK, remark);
                    }
                    postNetworkJson(TAG_SUBMIT, NetworkProtocol.URL_REPAIR_REPORT, map, true);
                }
            }
        });

    }

    private Toolbar mToolBar;
    private TextView mTextMenu;
    private void initToolBar() {
        mToolBar = (Toolbar) findViewById(R.id.toolbar_common);
        mTextMenu = (TextView) findViewById(R.id.text_menu);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.title)).setText("报修");

        mTextMenu.setVisibility(View.VISIBLE);
        mTextMenu.setText("报修记录");
        mTextMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterActivity(RepairListActivity.class);
            }
        });
    }

    private void createHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_SUBMIT:
                if (jsonObject.isRes()) {
                    showCrouton(R.string.dialog_report_succeed, Style.CONFIRM);
                    enterActivity(RepairListActivity.class);
                    finish();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
    }

    private String getStringFromTextInputLayout(TextInputLayout textInputLayout) {
        return textInputLayout.getEditText().getText().toString();
    }

    private boolean varifyInput() {
        String name = getStringFromTextInputLayout(textInputName);
        String phone = getStringFromTextInputLayout(textInputPhone);
//        String errCode = getStringFromTextInputLayout(textInputErrCode);
        String feedBack = getStringFromTextInputLayout(textInputFeedBack);

        if (name.length() == 0) {
            showCrouton(getString(R.string.pls_input) + "姓名");
            return false;
        }

        if (phone.length() == 0) {
            showCrouton(getString(R.string.pls_input) + "电话号码");
            return false;
        }

        if (feedBack.length() == 0) {
            showCrouton(getString(R.string.pls_input) + "错误反馈");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        showQuidDialog();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        finish();
    }

    public void showQuidDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage(R.string.dialog_quit_report_confirm)
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(0)
                .setNegativeButtonText(R.string.cancel)
                .setTitle(R.string.quit).show();
    }

    @Override
    public void loadFinished(String url) {
        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("image", getFile());
            requestParams.put(NetworkProtocol.PARAMKEY_TOKEN, application.common_token);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        /*GalleryWithShortCut bean = new GalleryWithShortCut();
        bean.setBitmap(ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(filePath), 80, 80));
        bean.setUrl(filePath);
        galleryBeans.add(bean);*/
        Image image = new Image();
        image.setUrl(url);
        images.add(image);
        uploadBitmap(requestParams, NetworkProtocol.URL_UPLOAD_IMG);
    }

    @Override
    public void onUpLoadSuccess(String result) {
        super.onUpLoadSuccess(result);
        TbitCommonResponse response = null;
        try {
            response = gson.fromJson(result, TbitCommonResponse.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        if (response != null && response.getRet() == 1) {
            showCrouton("上传成功", Style.CONFIRM);
            Image image = images.get(images.size() - 1);
            image.setId(response.getData());
//            image.setBitmap(ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(image.getUrl()), 80, 80));
            adapter.notifyDataSetChanged();
        } else {
            showCrouton("上传失败" + response.getData());
            if (images.size() != 0) {
                images.remove(images.size() - 1);
            }
        }
    }
    
    private String getImageIds() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        boolean isCommas = false;
        for (Image image : images) {
            if (isCommas) {
                sb.append(",");
            }
            sb.append(image.getId());
            isCommas = true;
        }
        return sb.toString();
    }

    @Override
    public void onUpLoadFailure(String result) {
        super.onUpLoadFailure(result);
        if (images.size() != 0) {
            images.remove(images.size() - 1);
        }
    }

    @Override
    protected void showCrouton(String s) {
        showCrouton(s, Style.ALERT);
    }

    @Override
    protected void showCrouton(String s, Style style) {
        Crouton.cancelAllCroutons();
        Crouton.makeText(this, s, style, R.id.rl_report_repair_container).show();
    }
}
