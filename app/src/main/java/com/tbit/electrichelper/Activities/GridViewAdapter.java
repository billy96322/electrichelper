package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbit.electrichelper.Beans.GridItem;
import com.tbit.electrichelper.R;

import java.util.List;

/**
 * Created by Salmon on 2016/1/11.
 */
public class GridViewAdapter extends BaseAdapter {

    private List<GridItem> datas;
    private LayoutInflater layoutInflater;

    public GridViewAdapter(Context context, List<GridItem> datas) {
        this.datas = datas;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_gridview, null);
            viewHolder = new ViewHolder();
            viewHolder.icon = (ImageView) view.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView) view.findViewById(R.id.gridview_text);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.icon.setImageResource(datas.get(position).getIconRes());
        viewHolder.title.setText(datas.get(position).getName());
        return view;
    }

    private class ViewHolder {
        TextView title;
        ImageView icon;
    }
}
