package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Beans.Repair;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RepairListActivity extends CommonToolbarActivity {
    private final int TAG_GET_DATA = 0;
    private final String ITEM_DESC = "desc";
    private final String ITEM_DATE = "date";
    private final String ITEM_STATUS = "status";

    @Bind(R.id.list_repair)
    ListView listRepair;

    private List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
    private List<Repair> repairList = new ArrayList<Repair>();
    private SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText("报修记录");

        adapter = new SimpleAdapter(this, data, R.layout.item_repair,
                new String[]{ITEM_DESC, ITEM_DATE, ITEM_STATUS},
                new int[]{R.id.text_repair_desc, R.id.text_repair_date, R.id.text_repair_status});
        listRepair.setAdapter(adapter);
        listRepair.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RepairDetail.startActivity(RepairListActivity.this, repairList.get(position));
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_USERID, String.valueOf(application.getUserId()));
        postNetworkJson(TAG_GET_DATA, NetworkProtocol.URL_REPAIR_RECORDS, map, true);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_repair_list;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_GET_DATA:
                if (jsonObject.isRes()) {
                    repairList = gson.fromJson(jsonObject.getResult(), new TypeToken<List<Repair>>(){}.getType());
                    initData();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
        }
    }

    private void initData() {
        if (repairList != null) {
            for (Repair repair:repairList) {
                HashMap<String, String> map = new HashMap<>();
                map.put(ITEM_DESC, repair.getFaultDescribe());
                map.put(ITEM_DATE, translateTime(repair.getRepairTime()));
                map.put(ITEM_STATUS, repair.getStatus()==1?"已处理":"未处理");
                data.add(map);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private String translateTime(String s) {
        String[] temp = s.split(" ");
        return temp[0];
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
