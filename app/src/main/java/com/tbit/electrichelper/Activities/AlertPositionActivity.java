package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.DataUtil;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AlertPositionActivity extends CommonToolbarActivity implements BaiduMap.OnMapClickListener, BaiduMap.OnMarkerClickListener {
    private static final String BUNDLE_KEY_CAR_STATUS = "car_status";
    private static final int TAG_ADDRESS = 0;
    @Bind(R.id.map_alert)
    TextureMapView mapAlert;
    BaiduMap mBaiduMap;

    TextView tipTime, tipDesc, tvSpeed, tvCarno, tvType, tvScode;
    private View infoWindowView;
    private Marker carMarker;
    private CarStatus status;

    public static void enterActivity(Context context, CarStatus status) {
        Intent intent = new Intent(context, AlertPositionActivity.class);
        intent.putExtra(BUNDLE_KEY_CAR_STATUS, status);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbar.setTitle("告警");

        status = getIntent().getParcelableExtra(BUNDLE_KEY_CAR_STATUS);

        mBaiduMap = mapAlert.getMap();
        TbitUtil.hideZoomControl(mapAlert);

        mBaiduMap.setOnMapClickListener(this);
        mBaiduMap.setOnMarkerClickListener(this);

        LatLng latLng = new LatLng(status.getLat(), status.getLng());
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).zoom(16).build()));
        carMarker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(TbitUtil.getMarkerBitmap(this, true))).title("child").anchor(0.5f, 1.0f));

        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(status.getLat()));
        map.put(NetworkProtocol.PARAMKEY_LNG, String.valueOf(status.getLng()));
        postNetworkJson(TAG_ADDRESS, NetworkProtocol.URL_GET_ADDRESS_DESC, map, false);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_alert_position;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_ADDRESS:
                if (jsonObject.isRes()) {
                    mBaiduMap.showInfoWindow(new InfoWindow(getInfowindow(jsonObject.getResult()), carMarker.getPosition(), -TbitUtil.dip2px(this, 40)));
                } else {
                    mBaiduMap.showInfoWindow(new InfoWindow(getInfowindow(getString(R.string.position_desc_load_fail)), carMarker.getPosition(), -TbitUtil.dip2px(this, 40)));
                }
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        if (tag == TAG_ADDRESS) {
            mBaiduMap.showInfoWindow(new InfoWindow(getInfowindow(getString(R.string.position_desc_load_fail)), carMarker.getPosition(), -TbitUtil.dip2px(this, 40)));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mBaiduMap.hideInfoWindow();
    }

    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mBaiduMap.showInfoWindow(new InfoWindow(getInfowindow(null), marker.getPosition(), -TbitUtil.dip2px(this, 40)));
        return true;
    }

    private void initInfoWindow() {
        infoWindowView = getLayoutInflater().inflate(R.layout.popwindown_user_info, null);
        // 显示当前电量
//        tipPower = (ImageView) infoWindow.findViewById(R.id.iv_displayElectricQuantity_mapTip);
        // 显示当前时间
        tvCarno = (TextView) infoWindowView.findViewById(R.id.tv_carNo);
        tvSpeed = (TextView) infoWindowView.findViewById(R.id.tv_Speed);
        tipTime = (TextView) infoWindowView.findViewById(R.id.tv_time_info);
        tipDesc = (TextView) infoWindowView.findViewById(R.id.tv_address_maptip);
        // 显示定位方式
        tvType = (TextView) infoWindowView.findViewById(R.id.tv_locateType);
        tvScode = (TextView) infoWindowView.findViewById(R.id.tv_sCode);
    }

    private View getInfowindow(String content) {
        if (infoWindowView == null) {
            initInfoWindow();
        }
        // 显示位置描述
        String temp = tipDesc.getText().toString();
        if (content != null && content.length() != 0) {
            temp = content;
        }
        tipTime.setText(status.getGpstime());
        tvSpeed.setText(String.valueOf(translateSpeed(status.getStrGGPV())) + "Km/h");
        String machineNo = DataUtil.getMachineNoByCarId(application.getCurCar().getCarId());
        tvCarno.setText(machineNo);
        tipDesc.setText(temp);
        tvType.setText(getPositionTypeResouce(status.getPointed()));
        tvScode.setText(getSCodeString(status.getScode()));
        return infoWindowView;
    }

    private int translateSpeed(String s) {
        try {
            if (s == null || s.equals("")) {
                return 0;
            }
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            float result = 0;

            result = Integer.parseInt(temp[26]);
            return (int) result;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String getPositionTypeResouce(int type) {
        String result = "";
        switch (type) {
            case 1:
                result = "卫星定位";
                break;
            case 2:
                result = "基站定位";
                break;
            default:
                break;
        }
        return result;
    }

    private String getSCodeString(String status) {
        String result = "";
        if (status == null || status.length() == 0) {
            return result;
        }
        if (status.split(":").length < 2) {
            return result;
        }
        String[] temp = status.split(":");

        boolean needCommas = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < temp.length; i++) {
            if (needCommas) {
                sb.append(",");
            }
            try {
                sb.append(translateSCode(Integer.parseInt(status.split(":")[i])));
                needCommas = true;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        result = sb.toString();
        return result;
    }

    private String translateSCode(int sCode) {
        String result = "";
        switch (sCode) {
            case 0:
                result = "已设防";
                break;
            case 1:
                result = "未设防";
                break;
            case 2:
                result = "休眠";
                break;
            case 3:
                result = "断电";
                break;
            case 4:
                result = "运动";
                break;
            case 5:
                result = "ACC开";
                break;
            case 6:
                result = "车门开";
                break;
            case 7:
                result = "油门断";
                break;
            case 8:
                result = "电门断";
                break;
            case 9:
                result = "电机锁";
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapAlert.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapAlert.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapAlert.onPause();
    }
}
