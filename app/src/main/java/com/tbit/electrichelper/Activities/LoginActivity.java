package com.tbit.electrichelper.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Beans.A1Entity;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.PersonalInformation;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class LoginActivity extends CommonToolbarActivity implements ISimpleDialogListener{
    public static final String BUNDLE_KEY_LOGIN_PHONE = "phone_login";
    public static final String BUNDLE_KEY_LOGIN_PASSWORDS = "psw_login";
    public static final String BUNDLE_KEY_LOGIN_STATE = "state_login";
    public static final short BUNDLE_VALUE_LOGIN_DO_NOTHING = -1;
    public static final short BUNDLE_VALUE_LOGIN_INPUT_PHONE = 0;
    public static final short BUNDLE_VALUE_LOGIN_AUTO_LOGIN = 1;

    private static final int REQUEST_CODE_REGISTER = 0;

    private static final int DIALOG_REQUEST_BAD_NETWORK = 0;

    private static final int NETWORK_TAG_LOGIN = 0;
    private static final int NETWORK_TAG_BOUND_DATA = 1;
    private static final int NETWORK_TAG_GET_CAR_STATUSES = 2;
    private static final int NETWORK_TAG_A1_BIND_DATA = 3;
    @Bind(R.id.edit_login_phone)
    EditText editLoginPhone;
    @Bind(R.id.edit_login_passwords)
    EditText editLoginPasswords;
    @Bind(R.id.button_login_login)
    Button buttonLogin;
    @Bind(R.id.remember)
    CheckBox remember;
    @Bind(R.id.experience_login)
    TextView experienceLogin;
    @Bind(R.id.text_login_register)
    TextView textLoginRegister;
    @Bind(R.id.text_login_forgot_password)
    TextView textLoginForgotPassword;

    private MyApplication myApplication;
    private SharedPreferences sp;
    private CarStatus curStatus;

    // 如果连续登陆失败三次，弹出提示框提示切换成蓝牙设置模式（无需登录）
    private int loginFailCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        myApplication = MyApplication.getInstance();
        sp = myApplication._preferences;

        mToolbarTitle.setText(R.string.login);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (varifyInput(editLoginPhone, editLoginPasswords)) {
                    Map<String, String> parms = new HashMap<String, String>();
                    parms.put(NetworkProtocol.PARAMKEY_PHONE, editLoginPhone.getText().toString());
                    parms.put(NetworkProtocol.PARAMKEY_PASSWORD, editLoginPasswords.getText().toString());
                    mProgressDialogUtils.showProgressDialog(true);
                    postNetWorkForLogin(NETWORK_TAG_LOGIN, NetworkProtocol.URL_LOGIN, parms, false);
                }
            }
        });

        experienceLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                editLoginPhone.setText("13530662960");
//                editLoginPasswords.setText("123456789");
//                remember.setChecked(false);
//                buttonLogin.performClick();

                Map<String, String> parms = new HashMap<String, String>();
                parms.put(NetworkProtocol.PARAMKEY_PHONE, "18589028104");
                parms.put(NetworkProtocol.PARAMKEY_PASSWORD, "123456");
                mProgressDialogUtils.showProgressDialog(true);
                postNetWorkForLogin(NETWORK_TAG_LOGIN, NetworkProtocol.URL_LOGIN, parms, false);
            }
        });

        if (sp.contains(TbitProtocol.SP_LOGIN_NAME) && sp.contains(TbitProtocol.SP_LOGIN_PASSWORD)) {
            editLoginPhone.setText(sp.getString(TbitProtocol.SP_LOGIN_NAME, ""));
            editLoginPasswords.setText(sp.getString(TbitProtocol.SP_LOGIN_PASSWORD, ""));
            remember.setChecked(true);
        }

        textLoginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), REQUEST_CODE_REGISTER);
            }
        });

        textLoginForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                enterActivity(ForgotPasswordActivity.class);
                ForgotPasswordActivity.startActivity(LoginActivity.this, editLoginPhone.getText().toString());
            }
        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        if (!jsonObject.isRes()) {
            mProgressDialogUtils.showProgressDialog(false);
            showCrouton(jsonObject.getDesc());
            return;
        }
        switch (tag) {
            case NETWORK_TAG_LOGIN:
                if (remember.isChecked()) {
                    sp.edit().putString(TbitProtocol.SP_LOGIN_NAME, editLoginPhone.getText().toString()).apply();
                    sp.edit().putString(TbitProtocol.SP_LOGIN_PASSWORD, editLoginPasswords.getText().toString()).apply();
                } else {
                    sp.edit().remove(TbitProtocol.SP_LOGIN_NAME).apply();
                    sp.edit().remove(TbitProtocol.SP_LOGIN_PASSWORD).apply();
                }
                PersonalInformation info = gson.fromJson(jsonObject.getResult(), PersonalInformation.class);
                application.putNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO, info);
                postNetworkJson(NETWORK_TAG_BOUND_DATA, NetworkProtocol.URL_GET_CAR_DATA, null, false);
                break;
            case NETWORK_TAG_BOUND_DATA:
                CarData carData = gson.fromJson(jsonObject.getResult(), CarData.class);
                application.a2Cars.clear();
                if (carData.getCars().size() != 0) {
                    application.a2Cars.addAll(carData.getCars());
                }
                postNetworkJson(NETWORK_TAG_A1_BIND_DATA, NetworkProtocol.URL_A1_BINDING_QUERY, null, false);
                break;
            case NETWORK_TAG_A1_BIND_DATA:
                List<A1Entity> a1s = gson.fromJson(jsonObject.getResult(),
                        new TypeToken<List<A1Entity>>(){}.getType());
                application.a1Cars.clear();
                application.a1Cars.addAll(a1s);

                // 保存A1离线数据
                application._preferences.edit().putString(TbitProtocol.SP_A1_OFFLINE_DATA,
                        jsonObject.getResult()).apply();

                // 无绑定跳转至绑定界面
                if (application.a2Cars.size() == 0 &&
                        application.a1Cars.size() == 0) {
                    mProgressDialogUtils.showProgressDialog(false);
                    showCrouton("尚未绑定车辆，请先绑定", Style.INFO);
                    application.setLoginStatus(TbitProtocol.LOGGED_IN);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setAction(TbitProtocol.ACTION_NOTHING_BIND);
                    startActivity(intent);
                    return;
                }

                // 恢复默认选项到最后一次选择的选项
                boolean hasLast = false;
                String lastCarId = sp.getString(TbitProtocol.SP_LOW_LAST_CHOSEN_MACHINE, "");
                int lastType = sp.getInt(TbitProtocol.SP_LOW_LAST_CHOSEN_TYPE, -1);
                if (!TextUtils.isEmpty(lastCarId) && lastType != -1) {
                    for (CarData.CarsEntity e : application.a2Cars) {
                        if (e.getCarId().equals(lastCarId)) {
                            hasLast = true;
                            application.setCurCar(lastCarId, lastType);
                            break;
                        }
                    }
                    if (!hasLast) {
                        for (A1Entity e : application.a1Cars) {
                            if (e.getMachineNO().equals(lastCarId)) {
                                hasLast = true;
                                application.setCurCar(lastCarId, lastType);
                                EventBus.getDefault().post(new Event.BleUIUpdate());
                                break;
                            }
                        }
                    }
                }
                if (!hasLast) {
                    if (application.hasA1Bound()) {
                        application.setCurCar(application.a1Cars.get(0).getMachineNO(), TbitProtocol.TYPE_A1);
                        EventBus.getDefault().post(new Event.BleUIUpdate());
                    } else {
                        application.setCurCar(application.a2Cars.get(0).getCarId(), TbitProtocol.TYPE_A2);
                    }
                }

                // 如果有a2设备，获得更新
                if (application.a2Cars.size() != 0) {
                    Map<String, String> map = new HashMap<>();
                    map.put(NetworkProtocol.PARAMKEY_CARID, getCarIdString());
                    postNetworkJson(NETWORK_TAG_GET_CAR_STATUSES, NetworkProtocol.URL_POSITION, map, false);
                } else {
                    LocalBroadcastManager.getInstance(LoginActivity.this)
                            .sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_LOGGED_IN));
                    finish();
                }
                break;
            case NETWORK_TAG_GET_CAR_STATUSES:
                List<CarStatus> temp = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                }.getType());
                for (CarStatus status : temp) {
                    application.setGlobal(status.getCarId(), TbitProtocol.KEY_GLOBAL_STATUS, status);
                }
                mProgressDialogUtils.showProgressDialog(false);
                LocalBroadcastManager.getInstance(LoginActivity.this)
                        .sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_LOGGED_IN));
                application.putNobeloning(TbitProtocol.KEY_NOBELONING_PHONE, editLoginPhone.getText().toString());
                application.putNobeloning(TbitProtocol.KEY_NOBELONING_PASSWORD, editLoginPasswords.getText().toString());
                finish();
                break;
            default:
                break;
        }
        super.parseResults(jsonObject, tag);
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        if (tag == NETWORK_TAG_LOGIN) {
            loginFailCount++;
            if (loginFailCount %3 == 0 &&
                    !application.isOfflineMode && application.hasA1Device) {
                showDialogWhenBadNetwork();
            }
        }
    }

    private boolean varifyInput(EditText etAccount, EditText etPassword) {
        if (etAccount.getText().toString().length() == 0) {
            showCrouton("请输入帐号");
            return false;
        }

        if (etPassword.getText().toString().length() == 0) {
            showCrouton("请输入密码");
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_REGISTER:
                if (resultCode != RESULT_OK) {
                    return;
                }
                short loginState = data.getShortExtra(LoginActivity.BUNDLE_KEY_LOGIN_STATE, LoginActivity.BUNDLE_VALUE_LOGIN_DO_NOTHING);
                String phone;
                String passwords;
                if (loginState == LoginActivity.BUNDLE_VALUE_LOGIN_DO_NOTHING) {
                    return;
                } else if (loginState == LoginActivity.BUNDLE_VALUE_LOGIN_INPUT_PHONE) {
                    phone = data.getStringExtra(LoginActivity.BUNDLE_KEY_LOGIN_PHONE);
                    editLoginPhone.setText(phone);
                    return;
                } else if (loginState == LoginActivity.BUNDLE_VALUE_LOGIN_AUTO_LOGIN) {
                    phone = data.getStringExtra(LoginActivity.BUNDLE_KEY_LOGIN_PHONE);
                    passwords = data.getStringExtra(LoginActivity.BUNDLE_KEY_LOGIN_PASSWORDS);
                    editLoginPhone.setText(phone);
                    editLoginPasswords.setText(passwords);
                    buttonLogin.performClick();
                }
                break;
            default:
                break;
        }
    }

    private String getCarIdString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if (application.a2Cars != null && application.a2Cars.size() != 0) {
            boolean needComas = false;
            for (CarData.CarsEntity c : application.a2Cars) {
                if (needComas) {
                    sb.append(",");
                }
                sb.append(c.getCarId());
                needComas = true;
            }
        }
        return sb.toString();
    }

    private void showDialogWhenBadNetwork () {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("网络")
                .setMessage("当前网络状况不良，是否切换至蓝牙控制车辆模式")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_BAD_NETWORK)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_BAD_NETWORK:
                loginFailCount = 0;
                break;
        }
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_BAD_NETWORK:
                EventBus.getDefault().post(new Event.OfflineMode());
                finish();
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }
}
