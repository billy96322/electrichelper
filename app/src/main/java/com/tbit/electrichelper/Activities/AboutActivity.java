package com.tbit.electrichelper.Activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.tbit.electrichelper.Beans.PersonalInformation;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.Fragments.Dialogs.EditTextDialog;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;
import com.tbit.electrichelper.Util.TextViewPlus;
import com.tbit.electrichelper.Util.UpdateManager;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.OnekeyShareTheme;

public class AboutActivity extends CommonToolbarActivity implements ISimpleDialogListener, UpdateManager.OnUpdateListener {
    public static final int PLATFORM_ID = 10;
    public static final int USER_TYPE = 3;
    public static final String HELP_URL = NetworkProtocol.URL_COMMON + "/cn/help.html";
    //    public static final String TERMS_OF_SERVICE_URL = NetworkProtocol.URL_COMMON + "fwtk.html";
    public static final String FEEDBACK_URL = NetworkProtocol.URL_COMMON + "feedback.html?platformId=%1$s&userType=%2$s&userId=%3$s&token=%4$s";
    public static final String INTRODUCE_URL = NetworkProtocol.URL_COMMON + "updateIntroduce.html?language=zh-CN&client=android&version=%1$s&platformId=%2$s";
//    private static final String DEVELOPER_URL = "http://192.168.1.216:9000/";
//    private static final String DEVELOPER_URL_COMMON = "http://192.168.1.215:30000/";
//    private static final String REGULAR_URL = "http://tbitgps.com/";
//    private static final String REGULAR_URL_COMMON = "http://common.tbit.com.cn/";

    private static final int DIALOG_REQUEST_UPDATE = 0;
    private static final int DIALOG_REQUEST_WIFI = 1;

    private static final int TAG_NOTIFICATION_UPDATE = 0;

    @Bind(R.id.button_about_quit)
    Button buttonQuit;
    @Bind(R.id.text_about_feedback)
    TextViewPlus textFeedback;
    @Bind(R.id.text_about_introduce)
    TextViewPlus textAboutIndroduce;
    @Bind(R.id.text_about_help)
    TextViewPlus textAboutHelp;
    @Bind(R.id.text_about_update)
    TextViewPlus textAboutUpdate;
    @Bind(R.id.image_about_logo)
    ImageView imageAboutLogo;

    private int developModeCounter = 0;
    private UpdateManager updateManager;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private int downloadProgress = 1;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.text_about_introduce: {
                    String url = String.format(INTRODUCE_URL, getString(R.string.app_version), String.valueOf(PLATFORM_ID));
                    BrowserActivity.startActivity(AboutActivity.this, url);
                    break;
                }
                case R.id.text_about_help:
                    BrowserActivity.startActivity(AboutActivity.this, HELP_URL);
                    break;
                case R.id.text_about_update:
                    mProgressDialogUtils.showProgressDialog(true);
                    updateManager.checkUpdate();
                    break;
                case R.id.text_about_feedback:
                    if (application.getLoginStatus() != TbitProtocol.LOGGED_IN) {
                        enterActivity(LoginActivity.class);
                        break;
                    }
                    PersonalInformation info = (PersonalInformation) application.getNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO);
                    if (info == null) {
                        break;
                    }
                    String url = String.format(FEEDBACK_URL, String.valueOf(PLATFORM_ID), String.valueOf(USER_TYPE),
                            info.getPlatformUserId(), application.common_token);
                    BrowserActivity.startActivity(AboutActivity.this, url);
                    break;
                case R.id.button_about_quit:
                    LocalBroadcastManager.getInstance(AboutActivity.this).
                            sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_LOGGED_OUT));
                    finish();
                    break;
                case R.id.image_about_logo:
//                    if (application.getLoginStatus() != TbitProtocol.LOGGED_IN) {
//                        developModeCounter++;
//                        if (developModeCounter > 2 && (developModeCounter % 3 == 0)) {
//
//                        }
//                    }
                    final EditTextDialog editTextDialog = new EditTextDialog();
                    editTextDialog.setTitle("WeChat");
                    editTextDialog.setEditTextListener(new EditTextDialog.EditTextListener() {
                        @Override
                        public void onConfirm(String editString) {
                            showShare(AboutActivity.this, null, false, editString);
                            editTextDialog.dismiss();
                        }

                        @Override
                        public void onCancel() {
                            editTextDialog.dismiss();
                        }

                        @Override
                        public void onNeutral() {
                            editTextDialog.dismiss();
                        }
                    });
                    editTextDialog.show(getSupportFragmentManager(), null);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText("关于");

        updateManager = new UpdateManager(this, this);

        developModeCounter = 0;

        if (application.getLoginStatus() == TbitProtocol.LOGGED_OUT) {
            buttonQuit.setVisibility(View.GONE);
        }
        textAboutIndroduce.setOnClickListener(onClickListener);
        textAboutHelp.setOnClickListener(onClickListener);
        textAboutUpdate.setOnClickListener(onClickListener);
        textFeedback.setOnClickListener(onClickListener);
        buttonQuit.setOnClickListener(onClickListener);
        imageAboutLogo.setOnClickListener(onClickListener);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_about;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            default:
                break;
        }
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
        if (requestCode == DIALOG_REQUEST_UPDATE) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_url = Uri.parse(NetworkProtocol.URL_APP_UPDATE_DOWNLOAD);
            intent.setData(content_url);
            startActivity(intent);
        }
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == DIALOG_REQUEST_UPDATE) {
            if (TbitUtil.isWIFI()) {
                startDownload();
            } else {
                SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                        .setTitle(R.string.update)
                        .setMessage("未连接无线网络，下载将可能产生流量费用，是否继续")
                        .setPositiveButtonText(R.string.confirm)
                        .setNegativeButtonText(R.string.cancel)
                        .setRequestCode(DIALOG_REQUEST_WIFI)
                        .show();
            }
        } else if (requestCode == DIALOG_REQUEST_WIFI) {
            startDownload();
        }
    }

    private void startDownload() {
        showLongToast("后台下载中...");
        updateManager.download();
    }

    private void showUpdateDialog(String desc) {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle(R.string.update)
                .setMessage(desc)
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setNeutralButtonText("浏览器下载")
                .setRequestCode(DIALOG_REQUEST_UPDATE)
                .setCancelable(false)
                .show();
    }

    private void installApk(String fileUrl) {
        File apkfile = new File(fileUrl);
        if (!apkfile.exists()) {
            return;
        }
        // 通过Intent安装APK文件
        Intent i = new Intent(Intent.ACTION_VIEW);
        // 安装完成后提示是否打开app
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
                "application/vnd.android.package-archive");
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onNewVersionFound(boolean hasNewVersion, String describe) {
        mProgressDialogUtils.showProgressDialog(false);
        if (isRunning && visible) {
            if (hasNewVersion) {
                showUpdateDialog(describe);
            } else {
                showToast("已经是最新版本");
            }
        }
    }

    @Override
    public void onDownloading(int progress) {
        notifyDownloadProgress(progress);
    }

    @Override
    public void onDownloadFinished(String fileUrl) {
        installApk(fileUrl);
    }

    @Override
    public void onDownloadFailed() {
        showToast("下载失败");
    }

    private void initDownloadNotification() {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_yadea)
                .setContentTitle("韩田管家更新下载中")
                .setTicker("韩田管家更新下载中...");
    }

    private void notifyDownloadProgress(int progress) {
        if (progress % 5 == 0 && progress != downloadProgress) {
            if (notificationManager == null || notificationBuilder == null) {
                initDownloadNotification();
            }
            downloadProgress = progress;
            if (downloadProgress == 100) {
                notificationManager.cancel(TAG_NOTIFICATION_UPDATE);
                return;
            }
            notificationBuilder.setProgress(100, downloadProgress, false);
            notificationManager.notify(TAG_NOTIFICATION_UPDATE, notificationBuilder.build());
        }
    }

    public void showShare(Context context, String platformToShare, boolean showContentEdit, String text) {
        OnekeyShare oks = new OnekeyShare();
        oks.setSilent(!showContentEdit);
        if (platformToShare != null) {
            oks.setPlatform(platformToShare);
        }
        //ShareSDK快捷分享提供两个界面第一个是九宫格 CLASSIC  第二个是SKYBLUE
        oks.setTheme(OnekeyShareTheme.CLASSIC);
        // 令编辑页面显示为Dialog模式
        oks.setDialogMode();
        // 在自动授权时可以禁用SSO方式
        oks.disableSSOWhenAuthorize();
//        oks.setTitleUrl("http://tbitgps.com/");
//        oks.setTitle("this is title");
        oks.setText(text);
        //oks.setImagePath("/sdcard/test-pic.jpg");  //分享sdcard目录下的图片
//        oks.setImageUrl("http://i.stack.imgur.com/m6J5G.png");
//        oks.setUrl("http://tbitgps.com/"); //微信不绕过审核分享链接
//        oks.setVenueName("ShareSDK");
//        oks.setVenueDescription("This is a beautiful place!");
        // 将快捷分享的操作结果将通过OneKeyShareCallback回调
        //oks.setCallback(new OneKeyShareCallback());
        // 去自定义不同平台的字段内容
        //oks.setShareContentCustomizeCallback(new ShareContentCustomizeDemo());
        // 在九宫格设置自定义的图标
        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        String label = "ShareSDK";
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {

            }
        };
        oks.setCustomerLogo(logo, label, listener);

        // 为EditPage设置一个背景的View
        //oks.setEditPageBackground(getPage());

        // 启动分享
        oks.show(context);
    }


}
