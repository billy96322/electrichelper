package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.xiaopan.switchbutton.SwitchButton;

public class SettingActivity extends CommonToolbarActivity {
    private static final int TAG_NETWORK_SET_PUSH = 0;
    private static final int TAG_NETWORK_GET_PUSH = 1;

    @Bind(R.id.switch_battery_low)
    SwitchButton switchBatteryLowWarning;
    @Bind(R.id.rl_battery_low)
    RelativeLayout rlLowPowerWarn;
    @Bind(R.id.switch_my_position)
    SwitchButton switchMyPosition;
    @Bind(R.id.rl_my_position)
    RelativeLayout rlMyPosition;
    @Bind(R.id.switch_push_service)
    SwitchButton switchPushService;
    @Bind(R.id.rl_push_service)
    RelativeLayout rlPushService;

    private boolean isSwitchByHand = true;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rl_battery_low:
                    switchBatteryLowWarning.performClick();
                    break;
                case R.id.rl_my_position:
                    switchMyPosition.performClick();
                    break;
                case R.id.rl_push_service:
                    switchPushService.performClick();
                    break;
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.switch_battery_low: {
                    application._preferences.edit().putBoolean(TbitProtocol.SP_LOW_POWER_WARN, isChecked).apply();
                    break;
                }
                case R.id.switch_my_position:
                    application._preferences.edit().putBoolean(TbitProtocol.SP_SHOW_MY_POSITION, isChecked).apply();
                    break;
                case R.id.switch_push_service:
                    if (!isSwitchByHand) {
                        isSwitchByHand = true;
                        break;
                    }
                    switchPushService.setEnabled(false);
                    Map<String, String> map = new HashMap<>();
                    map.put(NetworkProtocol.PARAMKEY_AUTOPUSH, String.valueOf(switchPushService.isChecked()?1:0));
                    postNetworkJson(TAG_NETWORK_SET_PUSH, NetworkProtocol.URL_PUSH_SET, map, false);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText(R.string.action_settings);

        switchPushService.setEnabled(false);
        postNetworkJson(TAG_NETWORK_GET_PUSH, NetworkProtocol.URL_PUSH_QUERY, null, false);

        initView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    private void initView() {
        rlLowPowerWarn.setOnClickListener(onClickListener);
        rlMyPosition.setOnClickListener(onClickListener);
        rlPushService.setOnClickListener(onClickListener);

        switchBatteryLowWarning.setOnCheckedChangeListener(onCheckedChangeListener);
        switchMyPosition.setOnCheckedChangeListener(onCheckedChangeListener);
        switchPushService.setOnCheckedChangeListener(onCheckedChangeListener);

        switchMyPosition.setChecked(application._preferences.getBoolean(TbitProtocol.SP_SHOW_MY_POSITION, true));
        switchBatteryLowWarning.setChecked(application._preferences.getBoolean(TbitProtocol.SP_LOW_POWER_WARN, true));
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switchPushService.setEnabled(true);
        switch (tag) {
            case TAG_NETWORK_GET_PUSH:
                try {
                    switchPushService.setChecked(Integer.parseInt(jsonObject.getResult()) == 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case TAG_NETWORK_SET_PUSH:
                if (!jsonObject.isRes()) {
                    showCrouton(jsonObject.getDesc());
                    isSwitchByHand = false;
                    switchPushService.setChecked(!switchPushService.isChecked());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        switchPushService.setEnabled(true);
        switch (tag) {
            case TAG_NETWORK_SET_PUSH:
                isSwitchByHand = false;
                switchPushService.setChecked(!switchPushService.isChecked());
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
