package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ForgotPasswordActivity extends CommonToolbarActivity {
    private static final int TAG_VARIFICATION_CODE = 0;
    private static final int TAG_RESET_PASSWORD = 1;

    private static final String BUNDLE_KEY_PHONE = "bundle_key_phone";

    @Bind(R.id.edit_forgotPsw_phone_number)
    EditText editForgotPswPhoneNumber;
    @Bind(R.id.edit_forgotPsw_verification_code)
    EditText editForgotPswVerificationCode;
    @Bind(R.id.text_forgotPsw_get_verification_code)
    TextView textForgotPswGetVerificationCode;
    @Bind(R.id.edit_forgotPsw_passwords)
    EditText editForgotPswPasswords;
    @Bind(R.id.button_forgotPsw_phone_next)
    Button buttonForgotPswPhoneNext;

    private String phoneFromLogin = "";

    private Handler mHandler = new Handler();
    int i = 61;// 倒计时的整个时间数

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText("忘记密码");

        phoneFromLogin = getIntent().getStringExtra(BUNDLE_KEY_PHONE);

        editForgotPswPhoneNumber.setText(phoneFromLogin);

        textForgotPswGetVerificationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editForgotPswPhoneNumber.getText().toString();
                if (!varifyPhone(phone)) {
                    return;
                }

                mProgressDialogUtils.showProgressDialog(true);
                // 获取短信验证码
//                getIdentificationCode(((RegisterActivity)parentActivity).getPhone());
                Map<String, String> map = new HashMap<>();
                map.put(NetworkProtocol.PARAMKEY_PHONE, phone);
                map.put(NetworkProtocol.PARAMKEY_OPERATE_CODE, String.valueOf(1));
                postNetWorkForLogin(TAG_VARIFICATION_CODE, NetworkProtocol.URL_REGISTER_VARIFICATION_CODE, map, true);

            }
        });

        buttonForgotPswPhoneNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editForgotPswPhoneNumber.getText().toString();
                String authCode = editForgotPswVerificationCode.getText().toString();
                String password = editForgotPswPasswords.getText().toString();
                if (!varifySubmit(phone, authCode, password)) {
                    return;
                }
                Map<String, String> map = new HashMap<>();
                map.put(NetworkProtocol.PARAMKEY_PLATFORM_SMS_AUTH_CODE, authCode);
                map.put(NetworkProtocol.PARAMKEY_PASSWORD, password);
                postNetworkJson(TAG_RESET_PASSWORD, NetworkProtocol.URL_FORGOT_PASSWORD, map, true);
            }
        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_forgot_password;
    }

    public static void startActivity(Context context, String phone) {
        Intent intent = new Intent();
        intent.setClass(context, ForgotPasswordActivity.class);
        intent.putExtra(BUNDLE_KEY_PHONE, phone);
        context.startActivity(intent);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        if (tag == TAG_VARIFICATION_CODE) {
            if (jsonObject.isRes()) {
                textForgotPswGetVerificationCode.setClickable(false);
                showCrouton("获得验证码成功", Style.CONFIRM);
                new Thread(new ClassCut()).start();// 开启倒计时
                completeLayout();
            } else {
                showCrouton(jsonObject.getDesc());
            }
        } else if (tag == TAG_RESET_PASSWORD) {
            if (jsonObject.isRes()) {
                showLongToast("修改密码成功");
                finish();
            } else {
                showCrouton(jsonObject.getDesc());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private boolean varifyPhone(String phone) {
        if (phone.length() == 0) {
            showCrouton("请输入手机号码");
            return false;
        } else if (phone.length() != 11) {
            showCrouton("手机号位数为11位");
            return false;
        }
        return true;
    }

    private boolean varifySubmit(String phone, String authCode, String password) {
        if (!varifyPhone(phone)) {
            return false;
        }
        if (authCode.length() != 6) {
            showCrouton("请正确输入6位验证码");
            return false;
        }
        if (password.length() == 0) {
            showCrouton("密码不能为空");
            return false;
        }
        if (password.length() < 6 || password.length() > 12) {
            showCrouton("密码长度有误，请输入6-12位密码");
            return false;
        }
        return true;
    }

    private void completeLayout() {
        buttonForgotPswPhoneNext.setVisibility(View.VISIBLE);
        editForgotPswPasswords.setVisibility(View.VISIBLE);
    }

    class ClassCut implements Runnable {// 倒计时逻辑子线程

        @Override
        public void run() {
            while (i > 0) {// 整个倒计时执行的循环
                if (!isRunning) {
                    return;
                }
                i--;
                mHandler.post(new Runnable() {// 通过它在UI主线程中修改显示的剩余时间
                    @Override
                    public void run() {
                        if (!isRunning) {
                            return;
                        }
                        textForgotPswGetVerificationCode.setText(i + "");// 显示剩余时间
                    }
                });
                try {
                    Thread.sleep(1000);// 线程休眠一秒钟 这个就是倒计时的间隔时间
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 下面是倒计时结束逻辑
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!isRunning) {
                        return;
                    }
                    textForgotPswGetVerificationCode.setText("点击获取");// 一轮倒计时结束,修改剩余时间为一分钟
                    textForgotPswGetVerificationCode.setClickable(true);
                    if (visible) {
                        showCrouton("若未收到验证码，请重新获取", Style.INFO);
                    }
                }
            });
            i = 61;// 修改倒计时剩余时间变量为60秒
        }
    }
}
