package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.util.Log;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.HashMap;
import java.util.Map;

public class TestActivity extends BaseNetworkActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Map<String, String> parms = new HashMap<String, String>();
        parms.put(NetworkProtocol.PARAMKEY_LANG, "zh-CN");
        parms.put(NetworkProtocol.PARAMKEY_NAME, "201169356");
        parms.put(NetworkProtocol.PARAMKEY_PASSWORD, "123456");
        parms.put(NetworkProtocol.PARAMKEY_TYPE, "3");
        postNetWorkForLogin(0, NetworkProtocol.URL_LOGIN, parms, true);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        if (tag == 0) {
            Map<String, String> map = new HashMap<>();
            postNetworkJson(1, NetworkProtocol.URL_GET_CAR_DATA, map, false);
        } else if (tag == 1) {
            Map<String, String> map = new HashMap<>();
            map.put(NetworkProtocol.PARAMKEY_CARID, String.valueOf(315735));
            Log.d("asd", "gonna get position");
            postNetworkJson(2, NetworkProtocol.URL_POSITION, map, false);
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        Log.d("asd", "onFail");
    }
}
