package com.tbit.electrichelper.Activities;


import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.tbit.electrichelper.Adapter.CommonAdapter;
import com.tbit.electrichelper.Adapter.ViewHolder;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Util.SharePreferenceUtil;
import com.tbit.electrichelper.Fragments.Dialogs.EditTextDialog;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.UartService;
import com.tbit.electrichelper.Util.CommonToolbarActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;

/**
 * Created by Kenny on 2016/1/19.
 * Desc：Record devices user had connected.
 */
public class HistoricalConnectionsActivity extends CommonToolbarActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = "asd";

    private ListView mHistories;
    private TextView mEmpty;
    private CommonAdapter<Map<String, Object>> mAdapter;
    private List<String> temp = new ArrayList<>();
    private List<Map<String, Object>> deviceList = new ArrayList<>();
    private BottomSheetDialog mBottomSheetDialog;

    private EditTextDialog mEditDialog;
    private UartService mUartService;
    private Handler mHandler;

    private ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mUartService = ((UartService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();
        initViews();
        initListener();

        mToolbarTitle.setText("连接记录");

        getAllList();
        mAdapter = new CommonAdapter<Map<String, Object>>(this, deviceList, R.layout.history_device_item) {
            @Override
            public void convert(ViewHolder holder, Map<String, Object> item) {
                TextView name = holder.getView(R.id.tv_deviceName);
                for (Map.Entry<String, Object> entry : item.entrySet()) {
                    String nameString = null;
                    try {
                        nameString = (String) entry.getValue();
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                    if (TextUtils.isEmpty(nameString)) {
                        nameString = entry.getKey();
                    }
                    name.setText(nameString);
                }
            }
        };
        mHistories.setAdapter(mAdapter);
        mHistories.setEmptyView(mEmpty);

        bindService();

        EventBus.getDefault().register(this);
    }

    private void bindService() {
        Intent intent = new Intent(this, UartService.class);
        bindService(intent, mServiceConn, BIND_AUTO_CREATE);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_historical_connections;
    }

    /**
     * Get every item from local data
     */
    private void getAllList() {
        Iterator<String> iterator = SharePreferenceUtil.getInstance().getKeys().keySet().iterator();
        while (iterator.hasNext()) {
//            Log.i(TAG, "--iterator" + iterator.next());
            Map<String, Object> map = new HashMap<>();
            String key = iterator.next();
            if (key.contains(":") && key.length() == 17) {
//                FF:FF:FF:FF:FF:FF
                Object value = SharePreferenceUtil.getInstance().getData(key, "");
                temp.add(key);
                map.put(key, value);
                deviceList.add(map);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConn);
        EventBus.getDefault().unregister(this);
        Crouton.cancelAllCroutons();
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void initListener() {
        mHistories.setOnItemClickListener(this);
    }

    private void initViews() {
        mHistories = (ListView) findViewById(R.id.lv_history);
        mEmpty = (TextView) findViewById(R.id.tv_empty);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        showBottomSheet(position);
    }

    private SheepListener mSheepListener;
    private void showBottomSheet(int position) {
        if (mBottomSheetDialog == null) {
            mBottomSheetDialog = new BottomSheetDialog(this);
            mSheepListener = new SheepListener(mBottomSheetDialog, position);

            View view = getLayoutInflater().inflate(R.layout.sheet_history, null);
            Button connect = (Button) view.findViewById(R.id.btn_connect);
            Button rename = (Button) view.findViewById(R.id.btn_rename);
            Button delete = (Button) view.findViewById(R.id.btn_delete);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.setContentView(view, params);
            mBottomSheetDialog.setCancelable(true);

            connect.setOnClickListener(mSheepListener);
            rename.setOnClickListener(mSheepListener);
            delete.setOnClickListener(mSheepListener);
        }
        mSheepListener.setPosition(position);
        mBottomSheetDialog.show();
    }


    private void actionRename(final int position) {
        if (mEditDialog == null) {
            mEditDialog = new EditTextDialog();
            mEditDialog.setTitle("重命名");
            mEditDialog.setEditTextHint("十个字符以内");
            mEditDialog.setCancelable(false);
            mEditDialog.setEditTextListener(new EditTextDialog.EditTextListener() {
                @Override
                public void onConfirm(String editString) {
                    if (editString.length() > 10 || TextUtils.isEmpty(editString)) {
                        showToast("请正确重命名！");
                        return;
                    }
                    Map<String, Object> map = deviceList.get(position);
                    map.put(temp.get(position), editString.trim());
                    mAdapter.notifyDataSetChanged();
                    //更新本地数据
                    SharePreferenceUtil.getInstance().saveData(temp.get(position), editString);
                    showToast("重命名成功: " + editString);
                    mEditDialog.dismiss();
                }

                @Override
                public void onCancel() {
                    mEditDialog.dismiss();
                }

                @Override
                public void onNeutral() {

                }
            });
        }
        final String oldName = deviceList.get(position).get(temp.get(position)).toString();
        mEditDialog.setEditTextContent(oldName);
        if (!mEditDialog.isVisible()) {
            mEditDialog.show(getSupportFragmentManager(), null);
        }
    }

    /**
     * delete the device from sharePreference and update the adapter.
     *
     * @param
     */
    private void actionDelete(int position) {
        //删除本地记录
        SharePreferenceUtil.getInstance().removeKey(temp.get(position));
        //更新删除后的ui
        deviceList.remove(position);
        temp.remove(position);
        mAdapter.notifyDataSetChanged();
        showToast("删除成功!");
    }

    private void actionConnect(int position) {
        Map<String, Object> map = deviceList.get(position);
        String name = null;
        String address = null;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            name = (String) entry.getValue();
            address = entry.getKey();
        }
        if (TextUtils.isEmpty(address)) {
            return;
        }
        mProgressDialogUtils.setCancelable(false);
        mProgressDialogUtils.showProgressDialog(true, "正在连接中...");
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isRunning && mProgressDialogUtils != null) {
                    mProgressDialogUtils.showProgressDialog(false);
                }
            }
        }, 10*1000);
        mUartService.disconnect();
        application.deviceName = name;
        application.deviceAddr = address;
        mUartService.connect(address, null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectSucc(Event.BleConnectSucc event) {
        mProgressDialogUtils.showProgressDialog(false);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnFail(Event.BleConnectFail event) {
        mProgressDialogUtils.showProgressDialog(false);
    }

    public class SheepListener implements View.OnClickListener {

        private BottomSheetDialog sheep;
        private int position;

        public SheepListener(BottomSheetDialog sheep, int postion) {
            this.sheep = sheep;
            this.position = postion;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_connect:
                    sheep.dismiss();
                    actionConnect(position);
                    break;
                case R.id.btn_rename:
                    actionRename(position);
                    sheep.dismiss();
                    break;
                case R.id.btn_delete:
                    actionDelete(position);
                    sheep.dismiss();
                    break;
            }
        }
    }
}
