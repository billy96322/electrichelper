package com.tbit.electrichelper.Activities;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.FileterAngleUtils;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Style;

public class HistoryActivity extends CommonToolbarActivity implements OnMarkerClickListener, com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener {

    /* 常量 */
    private static final String DATEPICKER_TAG = "datepicker";
    /**
     * 每次播放轨迹的间隔，毫秒
     */
    private final int HISTORY_PLAY_INTERVAL = 400;
    private final int TAG_GET_HISTORY = 0;

	/* 组件 */
    /**
     * 进行搜索
     */
    private ImageButton doSearch;
    /**
     * 进行播放
     */
    private ImageButton doPlay;
    /**
     * 播放图标
     */
    private ImageView iconPlay;
    /**
     * 播放进度条
     */
    private SeekBar seekBar;
    /**
     * 只显示GPS定位与显示全部轨迹
     */
    private CheckBox isShowBaseStation;

    /* 地图相关 */
    private TextureMapView mapView;
    private BaiduMap aMap;
    /**
     * 轨迹点
     */
    private Marker trackMarker;
    /**
     * 地图弹出框内容，显示手表当前定位时间
     */
    private TextView tipTime;
    /**
     * 轨迹线
     */
    private Polyline trackLine;
    /**
     * 轨迹点的弹出框
     */
    private View infoWindow;
    /**
     * 设置轨迹线
     */
    private PolylineOptions options;

	/* 轨迹数据 */
    /**
     * 存放所有的轨迹点
     */
    private List<CarStatus> positionList;
    /**
     * 存放gps轨迹点
     */
    private List<CarStatus> gpsPositionList;
    /**
     * 含有基站定位
     */
    private List<LatLng> pointListNoBS;
    /**
     * 不包含基站定位
     */
    private List<LatLng> pointListHasBS;
    /**
     * 播放轨迹时的数据
     */
    private List<LatLng> pointList = new ArrayList<LatLng>();

	/* 其他 */

    private MyApplication application;
    /**
     * 日期选择弹出框
     */
//    private DatePickerDialog datePickerDialog;

    private com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog;
    private Handler handler;
    /**
     * 轨迹播放线程
     */
    private Runnable historyPlay = null;
    /**
     * 播放进度标示
     */
    private int playIndex = 1;
    /**
     * 当前是显示的是否是播放按钮
     */
    private boolean isPlayBtn = true;

    /**
     * 获取是否需要过滤 默认开启
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mToolbarTitle.setText("历史轨迹");

        mapView = (TextureMapView) findViewById(R.id.mv_map_history);

        if (aMap == null) {
            aMap = mapView.getMap();
        }
        aMap.setOnMarkerClickListener(this);

        application = MyApplication.getInstance();
        /* 判断数据是否被回收 */
       /* if (application.getOnGlobal() == null) {
            this.finish();
            return;
        }
        AppManager.getAppManager().addActivity(this);*/


        handler = createHandler();
        /* 初始化日期选择的默认时间，当前手机日期 */
        Calendar calendar = new GregorianCalendar();
        datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog = new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setCancelable(true);
        // 显示年的范围
//        datePickerDialog.setYearRange(calendar.get(Calendar.YEAR) - 1, calendar.get(Calendar.YEAR));
        initMap();
        initView();

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_history;
    }

    @Override
    protected String inflateCustomTextMenu() {
        return "历史轨迹";
    }

    @Override
    protected void onCustomMenuClick() {
        enterActivity(AlertHistoryActivity.class);
    }

    @SuppressLint("HandlerLeak")
    private Handler createHandler() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        // 判断是否初始化及清除上一次的数据
                        if (pointListHasBS == null) {
                            pointListHasBS = new ArrayList<LatLng>();
                        } else {
                            pointListHasBS.clear();
                        }
                        if (pointListNoBS == null) {
                            pointListNoBS = new ArrayList<LatLng>();
                        } else {
                            pointListNoBS.clear();
                        }
                            /* 判断是否有轨迹点 */
                        if (positionList.size() == 0 || positionList == null) {
//                            Toast.makeText(HistoryActivity.this, R.string.history_noTrack, Toast.LENGTH_SHORT).show();
                            showCrouton(R.string.history_noTrack, Style.INFO);
                        } else {
                            options = new PolylineOptions();
                            options.color(Color.argb(180, 1, 200, 1));
                            options.width(10);
                            if (gpsPositionList != null) {
                                gpsPositionList.clear();
                            }
                                /* 遍历所有轨迹点 */
                            if (positionList.size() == 1) {
                                positionList.add(positionList.get(0));
                            }

                            for (CarStatus p : positionList) {
                                pointListHasBS.add(new LatLng(p.getLat(), p.getLng()));
                                // 过滤出gps轨迹点
                                if (p.getPointed() == 1) {
                                    if (gpsPositionList == null) {
                                        gpsPositionList = new ArrayList<>();
                                    }
                                    gpsPositionList.add(p);
                                    pointListNoBS.add(new LatLng(p.getLat(), p.getLng()));
                                }
                            }

                            if (gpsPositionList != null && gpsPositionList.size() != 0) {
                                gpsPositionList.add(gpsPositionList.get(0));
                            }

                            if (!isShowBaseStation.isChecked()) {
                                if (pointListNoBS.size() == 0) {
//                                    Toast.makeText(History_Activity_Baidu.this, R.string.history_noGps, Toast.LENGTH_SHORT).show();
                                    showCrouton(R.string.history_noGps, Style.INFO);
                                    pointList.addAll(pointListHasBS);
                                    if (pointListHasBS.size() == 1) {
                                        pointListHasBS.add(pointListHasBS.get(0));
                                        positionList.add(positionList.get(0));
                                    }
                                    if (options.getPoints() == null) {
                                        List<LatLng> temp = new ArrayList<>();
                                        temp.addAll(pointListHasBS);
                                        options.points(temp);
                                    } else {
                                        options.getPoints().clear();
                                        options.getPoints().addAll(pointListHasBS);
                                    }
                                    return;
                                }
                            }

                            if (pointListHasBS.size() == 1) {
                                pointListHasBS.add(pointListHasBS.get(0));
                                positionList.add(positionList.get(0));
                            }

                            if (pointListNoBS.size() == 1) {
                                pointListNoBS.add(pointListNoBS.get(0));
                            }
                            if (options.getPoints() == null) {
                                List<LatLng> temp = new ArrayList<>();
                                temp.addAll(pointListHasBS);
                                options.points(temp);
                            } else {
                                options.getPoints().clear();
                                options.getPoints().addAll(pointListHasBS);
                            }

                            if (options.getPoints().size() > 0) {
                                // 查询到历史轨迹
//                                    Toast.makeText(HistoryActivity.this, R.string.tip_querySuc, Toast.LENGTH_SHORT).show();
                                showCrouton(R.string.tip_querySuc, Style.CONFIRM);
                                // 默认显示所有轨迹
                                pointList.clear();
                                // 判断没有基站定位时是否有数据
                                if (pointListNoBS.size() > 0) {
                                    if (isShowBaseStation.isChecked()) {
                                        pointList.addAll(pointListHasBS);
                                    } else {
                                        pointList.addAll(pointListNoBS);
                                    }
                                } else {
                                    isShowBaseStation.setChecked(true);
                                    aMap.clear();
                                    pointList.addAll(pointListHasBS);
                                }
                                addTrackLine();
                                // 设置进度条最大值
                                seekBar.setMax(pointList.size() - 1);
                                fitHistoryView();
                            }
                        }
                        break;
                    case 2:
                        if (playIndex >= pointList.size()) {
                            playIndex = pointList.size() - 1;
                        }
                        if (playIndex == pointList.size() - 1) {
                            doSearch.setEnabled(true);
                            // 播放结束
                            /* 显示隐藏播放、暂停按钮和播放速度 */
                            iconPlay.setBackgroundResource(R.drawable.ic_play_history);
                            isPlayBtn = true;
                            seekBar.setVisibility(View.VISIBLE);
                            seekBar.setProgress(playIndex);
                            seekBar.setEnabled(true);
//                            trackMarker.setPosition(pointList.get(playIndex));
//                            aMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(pointList.get(0)).zoom(17).build()));
                            isShowBaseStation.setEnabled(true);
                        } else {
                            // 更新精度条
                            seekBar.setProgress(playIndex);
                            // 更新地图图标位置
                            trackMarker.setPosition(pointList.get(playIndex));
                            aMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(pointList.get(playIndex)).zoom(17).build()));
                        }
                        setIndex(playIndex + 1);
                        break;
                    default:
                        break;
                }
            }
        };
    }


    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_GET_HISTORY:
                if (jsonObject.isRes()) {
                    positionList = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                    }.getType());
                    removeDuplicate(positionList);
                    FileterAngleUtils.filterAngle(positionList);
                    handler.sendEmptyMessage(1);
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
            default:
                break;
        }
    }

    /**
     * 加入轨迹线和图标
     */
    private void addTrackLine() {
        seekBar.setVisibility(View.VISIBLE);
        seekBar.setOnSeekBarChangeListener(new strackLineSeekBarListner());

        options.getPoints().clear();
        options.getPoints().addAll(pointList);
        // 添加轨迹线
        try {
            doPlay.setEnabled(true);
            if (trackLine != null) {
                trackLine.remove();
            }
            trackLine = (Polyline) aMap.addOverlay(options);
            trackLine.setVisible(true);
        } catch (Exception e) {
            System.out.println("add track line exception \n" + e);
        }
        // 添加marker
//        if (trackMarker == null) {
//
//        } else {
//            trackMarker.setPosition(pointList.get(0));
//        }
        if (trackMarker != null) {
            trackMarker.remove();
        }
        trackMarker = (Marker) aMap.addOverlay(new MarkerOptions().position(pointList.get(0)).title("history").icon(BitmapDescriptorFactory.fromBitmap(TbitUtil.getMarkerBitmap(HistoryActivity.this, true))).anchor(0.5f, 1.0f));

        // 进度条从0开始
        seekBar.setProgress(0);
        // 地图移动到第一个轨迹点
        aMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(pointList.get(0)).zoom(17).build()));
    }

    /**
     * 初始化地图组件
     */
    private void initMap() {
        /* 将当前地图显示位置移动到当前手表所在的位置 */
        aMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().zoom(16).build()));
        CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        if (status == null) {
            enterActivity(MainActivity.class);
            finish();
            return;
        }
        LatLng latLng = new LatLng(status.getLat(), status.getLng());
        if (latLng != null) {
            MapStatus ms = new MapStatus.Builder().target(latLng).zoom(16).build();
            MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
            aMap.animateMapStatus(u);
        }
        /* 取消地图缩放图标 */

        TbitUtil.hideZoomControl(mapView);
    }

    private void fitHistoryView() {
        if (positionList.size() > 0) {
            List<LatLng> latLngs = new ArrayList<>();
            for (CarStatus status : positionList) {
                latLngs.add(new LatLng(status.getLat(), status.getLng()));
            }
            LatLng[] fitPoint = TbitUtil.getFitPoint(latLngs);
            MapStatusUpdate msu = MapStatusUpdateFactory.newLatLngBounds(new LatLngBounds.Builder().include(fitPoint[0]).include(fitPoint[1]).build());
            aMap.setMapStatus(msu);
        }
    }

    /**
     * 初始化其他组件
     */
    private void initView() {

        // 搜索历史轨迹
        doSearch = (ImageButton) findViewById(R.id.btn_doSearch_history);
        doSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 显示选择日历弹出框
                if (!datePickerDialog.isVisible()) {
//                    datePickerDialog.show(getSupportFragmentManager(), "select date");
                    datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
                }
            }
        });
        // 切换只显示gps轨迹或显示全部轨迹
        isShowBaseStation = (CheckBox) findViewById(R.id.btn_isShowBaseStation_history);
        isShowBaseStation.setEnabled(true);
        isShowBaseStation.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // 防止第一次勾选时提示没有数据
                if (pointListNoBS != null) {
                    if (isChecked) {
                        // 显示全部定位轨迹
                        if (pointListHasBS.size() > 0) {
                            aMap.clear();
                            // 显示包含基站定位
                            // 默认显示所有轨迹
                            pointList.clear();
                            // 判断没有基站定位时是否有数据
                            pointList.addAll(pointListHasBS);
                            addTrackLine();
                            // 设置进度条最大值
                            seekBar.setMax(pointList.size() - 1);
                        }
                    } else {
                        // 只显示gps定位轨迹
                        if (!(pointListNoBS.size() > 0)) {
                            // 没有gps定位
//                            Toast.makeText(HistoryActivity.this, R.string.history_noGps, Toast.LENGTH_SHORT).show();
                            showCrouton(R.string.history_noGps, Style.INFO);
                            isShowBaseStation.setChecked(true);
                        } else {
                            aMap.clear();
                            // 默认显示所有轨迹
                            pointList.clear();
                            // 判断没有基站定位时是否有数据
                            pointList.addAll(pointListNoBS);
                            addTrackLine();
                            // 设置进度条最大值
                            seekBar.setMax(pointList.size() - 1);
                        }
                    }
                }
            }
        });
		/* 播放暂停按钮上的图标，模式显示播放图标 */
        iconPlay = (ImageView) findViewById(R.id.iv_trackIcon_history);
        iconPlay.setBackgroundResource(R.drawable.ic_play_history);
		/* 播放轨迹按钮 */
        doPlay = (ImageButton) findViewById(R.id.btn_doPlay_history);
        doPlay.setEnabled(false);
        doPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlayBtn) {
					/* 播放状态 */
                    // 不可搜索
                    doSearch.setEnabled(false);
                    // 当前为播放按钮,切换为暂停按钮
                    iconPlay.setBackgroundResource(R.drawable.ic_stop_history);
                    isPlayBtn = false;
                    seekBar.setEnabled(false);
					/* 是否是从头播放 */
                    if (playIndex == 0 || playIndex > pointList.size() - 1) {
                        setIndex(0);
                    }
                    historyPlay = new Runnable() {
                        @Override
                        public void run() {
                            Message msg = new Message();
                            msg.what = 2;
                            handler.sendMessage(msg);
                            if (playIndex < pointList.size())
                                handler.postDelayed(this, HISTORY_PLAY_INTERVAL);
                        }
                    };
                    handler.post(historyPlay);
                } else {
					/* 暂停状态 */
                    // 搜索可用
                    doSearch.setEnabled(true);
                    // 进度条可用
                    seekBar.setEnabled(true);
                    // 设置图片为播放图片
                    iconPlay.setBackgroundResource(R.drawable.ic_play_history);
                    isPlayBtn = true;
                    handler.removeCallbacks(historyPlay);
                }
            }
        });

        // 播放进度条
        seekBar = (SeekBar) findViewById(R.id.sb_selectPoint_history);
        seekBar.setVisibility(View.GONE);
        seekBar.setOnSeekBarChangeListener(new strackLineSeekBarListner());
        // 进度条单击事件，当进度条被点击的时候不允许进行搜索操作
        seekBar.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    doSearch.setEnabled(false);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    doSearch.setEnabled(true);
                }
                return false;
            }
        });

    }


    /**
     * 获取头像
     */
/*    private Bitmap getChildIcon() {
        int newWidth = (int) (51 * getResources().getDisplayMetrics().density);
        int newHeight = (int) (75 * getResources().getDisplayMetrics().density);
        Matrix m = new Matrix();
        String iconPath = SBProtocol.TBIT_FILE_ICON + "w" + application.getWristbandId() + "_" + 1 + ".jpg";
        Bitmap b = null;
        if (new File(iconPath).exists()) {
            b = BitmapFactory.decodeFile(iconPath);
        } else {
            b = BitmapFactory.decodeResource(getResources(), R.drawable.icon_online);
        }
        int width = b.getWidth();
        int height = b.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        m.postScale(scaleWidth, scaleHeight);
        b = Bitmap.createBitmap(b, 0, 0, width, height, m, true);
        return b;
    }*/
//    @Override
//    public void onDateSet(DatePicker view, int year, int month, int day) {
//        // 移除播放线程
//        handler.removeCallbacks(historyPlay);
//        // 设置播放标示为1
//        setIndex(0);
//        isPlayBtn = true;
//        pointList.clear();
//        showProgressDialog(true, getString(R.string.history_queringTrack));
//        aMap.clear();
//        seekBar.setVisibility(View.GONE);
//        doPlay.setEnabled(false);
//        // 显示所搜索的日期
//        String date = year + "-" + String.valueOf(month + 1) + "-" + day + "";
//        actionBar.setTitle(date);
//        // 查询轨迹
////        new Thread(getHistoryTrack(year + "-" + String.valueOf(month + 1) + "-" + day + "")).start();
//        getHistories(date);
//    }
    private void getHistories(String date) {
        String startTime = date + " 00:00:00";
        String endTime = date + " 23:59:59";
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
        map.put(NetworkProtocol.PARAMKEY_STARTTIME, startTime);
        map.put(NetworkProtocol.PARAMKEY_ENDTIME, endTime);
        map.put(NetworkProtocol.PARAMKEY_MAPTYPE, String.valueOf(0));
        postNetworkJson(TAG_GET_HISTORY, NetworkProtocol.URL_HISTORY, map, true);
    }

    /**
     * 设置进度条当前位置，当处于轨迹开始位置的时候可以进行gps与全部轨迹的切换
     */
    private void setIndex(int i) {
        this.playIndex = i;
        Log.i("history_playIndex", i + "");
        // 当进度条的当前位置为0或者播放完毕后，允许进行选择是否显示基站定位
        if (playIndex == 0 || playIndex > pointList.size()) {
            this.isShowBaseStation.setEnabled(true);
        } else {
            this.isShowBaseStation.setEnabled(false);
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

        // 结束Activity&从堆栈中移除
//        AppManager.getAppManager().finishActivity(this);

    }

    @Override
    public void finish() {
        isRunning = false;
        showProgressDialog(false);
        try {
            handler.removeCallbacks(historyPlay);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.finish();
    }


    private void initInfoWindow() {
        infoWindow = getLayoutInflater().inflate(R.layout.popwindown_track_info, null);
        tipTime = (TextView) infoWindow.findViewById(R.id.tv_time_track);
    }

    public View getInfoWindow(String content) {
        if (infoWindow == null || tipTime == null) {
            initInfoWindow();
        }
        if (content == null) {
            content = tipTime != null ? tipTime.getText().toString() : "";
        }
        tipTime.setText(content);
        return infoWindow;
    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        return false;
    }

    private void removeDuplicate(List<CarStatus> data) {
        int i = 0;
        while (i < data.size() - 1) {
            CarStatus pCur = data.get(i);
            CarStatus pNext = data.get(i + 1);
            if (pCur.getLat() == pNext.getLat() && pCur.getLng() == pNext.getLng()) {
                if (pCur.getAcode() == null || pCur.getAcode().equals("")) {
                    data.remove(i);
                } else {
                    data.remove(i + 1);
                }
            } else {
                i++;
            }
        }
    }

    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
        // 移除播放线程
        handler.removeCallbacks(historyPlay);
        // 设置播放标示为1
        setIndex(0);
        isPlayBtn = true;
        pointList.clear();
        showProgressDialog(true, getString(R.string.history_queringTrack));
        aMap.clear();
        seekBar.setVisibility(View.GONE);
        doPlay.setEnabled(false);
        // 显示所搜索的日期
        String date = year + "-" + String.valueOf(month + 1) + "-" + day + "";
        mToolbarTitle.setText(date);
        // 查询轨迹
//        new Thread(getHistoryTrack(year + "-" + String.valueOf(month + 1) + "-" + day + "")).start();
        getHistories(date);
    }


    /**
     * 进度条拖动事件
     */
    class strackLineSeekBarListner implements OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar arg0, int progress, boolean fromUser) {
            // 播放的位置
            int i = (int) ((pointList.size() * (progress)) / 100);
            // 防止数组越界
            if (i >= pointList.size()) {
                i = pointList.size() - 1;
            }
            // 更新进度条
            if (seekBar.isEnabled()) {
                setIndex(progress);
            }
            try {
                // 显示当前轨迹点的时间
                String time = isShowBaseStation.isChecked() ? positionList.get(progress).getGpstime() : gpsPositionList.get(progress).getGpstime();
                time = time.split(" ")[1];
                // 移动轨迹
                trackMarker.setPosition(pointList.get(progress));
                aMap.showInfoWindow(new InfoWindow(getInfoWindow(time), trackMarker.getPosition(), -TbitUtil.dip2px(HistoryActivity.this, 40)));
                MapStatus ms = new MapStatus.Builder().target(pointList.get(progress)).zoom(17).build();
                MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
                aMap.animateMapStatus(u);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
