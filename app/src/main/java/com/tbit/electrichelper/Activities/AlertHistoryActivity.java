package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Adapter.CommonAdapter;
import com.tbit.electrichelper.Adapter.ViewHolder;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.DataUtil;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class AlertHistoryActivity extends CommonToolbarActivity implements com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener {
    private static final String DATEPICKER_TAG = "datepicker";
    private static final int TAG_UPDATE = 0;
    public static String[] ALARM_ARRAY = {"电量不足", "车辆被移动", "SOS报警", "车门打开报警",
            "主电池被断开", "震动报警", "失窃报警", "偏离路线", "进入区域", "离开区域", "停车超时", "超速报警",
            "ACC异常接通", "进入点", "离开点", "进入线", "离开线", "进入区域", "离开区域", "胶带接触异常",
            "胶带损毁", "普益报警", "离线超时告警", "设备拆除告警"};
    @Bind(R.id.list_alert_history)
    ListView listAlert;
    //    private DatePickerDialog datePickerDialog;
    private com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog;
    private CommonAdapter<CarStatus> mAdapter;
    private List<CarStatus> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbar.setTitle("历史告警");

        mAdapter = new CommonAdapter<CarStatus>(this, data, R.layout.item_alert_history) {
            @Override
            public void convert(ViewHolder holder, CarStatus item) {
                String machineNo = DataUtil.getMachineNoByCarId(item.getCarId());
                ((TextView) holder.getView(R.id.text_alert_item_car_id)).setText(machineNo);
                ((TextView) holder.getView(R.id.text_alert_item_time)).setText(item.getGpstime());
                ((TextView) holder.getView(R.id.text_alert_item_status)).setText(getSCodeString(item.getScode()));
                ((TextView) holder.getView(R.id.text_alert_item_alert_info)).setText(getALarmStats(item.getAcode()));
            }
        };
        listAlert.setAdapter(mAdapter);
        listAlert.setEmptyView(getListEmptyView());
        listAlert.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertPositionActivity.enterActivity(AlertHistoryActivity.this, data.get(position));
            }
        });

        Calendar calendar = new GregorianCalendar();
//        datePickerDialog = new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setCancelable(true);
        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_alert_history;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_alert_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_alert_history:
                if (!datePickerDialog.isVisible()) {
                    datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
                }
                break;
        }
        return true;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_UPDATE:
                if (jsonObject.isRes()) {
                    data.clear();
                    List<CarStatus> temp = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                    }.getType());
                    if (temp == null || temp.size() == 0) {
                        showCrouton("此日期没有告警", Style.INFO);
                        mAdapter.notifyDataSetChanged();
                        break;
                    }
                    data.addAll(temp);
                    mAdapter.notifyDataSetChanged();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
        }
    }

    private String getSCodeString(String status) {
        String result = "";
        if (status == null || status.length() == 0) {
            return result;
        }
        if (status.split(":").length < 2) {
            return result;
        }
        String[] temp = status.split(":");

        boolean needCommas = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < temp.length; i++) {
            if (needCommas) {
                sb.append(",");
            }
            try {
                sb.append(translateSCode(Integer.parseInt(status.split(":")[i])));
                needCommas = true;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        result = sb.toString();
        return result;
    }

    private String translateSCode(int sCode) {
        String result = "";
        switch (sCode) {
            case 0:
                result = "已设防";
                break;
            case 1:
                result = "未设防";
                break;
            case 2:
                result = "休眠";
                break;
            case 3:
                result = "断电";
                break;
            case 4:
                result = "运动";
                break;
            case 5:
                result = "ACC开";
                break;
            case 6:
                result = "车门开";
                break;
            case 7:
                result = "油门断";
                break;
            case 8:
                result = "电门断";
                break;
            case 9:
                result = "电机锁";
                break;
            default:
                break;
        }
        return result;
    }

    private String getALarmStats(String acode) {
        if (TextUtils.isEmpty(acode))
            return "";
        String[] alarms = acode.split(":");
        int len = alarms.length;
        if (len < 2)
            return "";
        if (alarms[1].contains(",")) {
            String[] alarmArea = alarms[1].split(",");
            return ALARM_ARRAY[Integer.valueOf(alarmArea[0])] + ":" + alarmArea[1];
        } else {
            return ALARM_ARRAY[Integer.valueOf(alarms[1])];
        }

    }

    private View getListEmptyView() {
        TextView textView = new TextView(this);
        ((ViewGroup) listAlert.getParent()).addView(textView);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(ContextCompat.getColor(this, R.color.material_blue_grey_600));
        textView.setBackgroundColor(ContextCompat.getColor(this, R.color.grey_200));
        textView.setTextSize(16);
        textView.setText("请点击右上角按钮，选择日期查询告警");
        return textView;
    }

    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
        String date = year + "-" + String.valueOf(month + 1) + "-" + day + "";
        mToolbar.setTitle(date);
        String startTime = date + " 00:00:00";
        String endTime = date + " 23:59:59";
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
        map.put(NetworkProtocol.PARAMKEY_STARTTIME, startTime);
        map.put(NetworkProtocol.PARAMKEY_ENDTIME, endTime);
//        map.put(NetworkProtocol.PARAMKEY_MAPTYPE, String.valueOf(0));
        postNetworkJson(TAG_UPDATE, NetworkProtocol.URL_HISTORY_ALERT, map, true);
    }
}
