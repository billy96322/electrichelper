package com.tbit.electrichelper.Activities;

import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.tbit.electrichelper.Beans.A1Entity;
import com.tbit.electrichelper.Fragments.Dialogs.Y2HPickDialog;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.UartService;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.ShareCodeGenerator;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShareActivity extends CommonToolbarActivity implements ISimpleDialogListener{
    private static final String TAG = "asd";
    private static final int SHARE_REQUEST_WX = 0;
    private static final int SHARE_REQUEST_QQ = 1;
    private static final int SHARE_REQUEST_SMS = 2;
    private static final int DIALOG_REQUEST_DISCONNECT = 3;
    private static final String SHARE_CONTENT = "复制这条信息，打开“智能管家”，即可自动弹出连接窗口进行连接。#%s#";
    @Bind(R.id.text_secret)
    TextView textSecret;
    @Bind(R.id.radio_long_time_validation)
    AppCompatRadioButton radioLongTimeValidation;
    @Bind(R.id.radio_limit_validation)
    AppCompatRadioButton radioLimitValidation;
    @Bind(R.id.radioGroup_validation)
    RadioGroup radioGroupValidation;
    @Bind(R.id.share_wechat)
    LinearLayout shareWechat;
    @Bind(R.id.share_qq)
    LinearLayout shareQq;
    @Bind(R.id.share_sms)
    LinearLayout shareSms;

    private String secret = "";
    private String curCarId;

    private Y2HPickDialog timeDialog;
    private UartService mService;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((UartService.LocalBinder) service).getService();

            if (mService.getConnectionState() == UartService.STATE_CONNECTED) {
                if (application.getCurCar().getCarId().equals(mService.getDeviceTid())) {
                    Log.d(TAG, "onServiceConnected: cur " + application.getCurCar().getCarId());
                    Log.d(TAG, "onServiceConnected: con " + mService.getDeviceTid());
                    disconnectRemind();
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mToolbarTitle.setText("分享");

        if (!application.hasA1Bound()) {
            finish();
            return;
        }
        boolean isCurA1 = false;
        curCarId = application.getCurCar().getCarId();
        for (A1Entity e : application.a1Cars) {
            if (curCarId.equals(e.getMachineNO()))
                isCurA1 = true;
        }

        if (!isCurA1) {
            showToast("当前不是A1设备，请先切换至A1设备");
            finish();
            return;
        }
        secret = ShareCodeGenerator.gen(curCarId);

        refreshSecret();

        initView();

        timeDialog = new Y2HPickDialog();
        timeDialog.setOnTimeSelectListener(new Y2HPickDialog.OnTimeSelectListener() {
            @Override
            public void onTimeSelected(String timeString) {
                String result = ShareCodeGenerator.gen(timeString, curCarId);
                if (!TextUtils.isEmpty(result)) {
                    secret = result;
                    refreshSecret();
                } else {
                    showToast("密码生成异常");
                    radioLongTimeValidation.performClick();
                }
            }

            @Override
            public void onCanceled() {
                radioLongTimeValidation.setChecked(true);
            }
        });

        textSecret.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                copySecret();
                showToast("已复制密钥");
                return false;
            }
        });

        radioGroupValidation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_limit_validation:
                        if (!timeDialog.isVisible())
                            timeDialog.show(getSupportFragmentManager(), null);
                        break;
                    case R.id.radio_long_time_validation:
                        secret = ShareCodeGenerator.gen(curCarId);
                        refreshSecret();
                        break;
                }
            }
        });

        Intent intent = new Intent(this, UartService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    private void initView() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.share_qq:
                        share(SHARE_REQUEST_QQ);
                        break;
                    case R.id.share_wechat:
                        share(SHARE_REQUEST_WX);
                        break;
                    case R.id.share_sms:
                        share(SHARE_REQUEST_SMS);
                        break;
                }
            }
        };

        shareQq.setOnClickListener(listener);
        shareWechat.setOnClickListener(listener);
        shareSms.setOnClickListener(listener);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_share;
    }

    private void refreshSecret() {
        StringBuilder sb = new StringBuilder();
        sb.append(secret.substring(0,3));
        sb.append("  ");
        sb.append(secret.substring(3,6));
        sb.append("  ");
        sb.append(secret.substring(6,10));

        textSecret.setText(sb.toString());

        int shakeOffset = TbitUtil.dip2px(ShareActivity.this, 3);
        ObjectAnimator shakeAnim = ObjectAnimator.ofFloat(textSecret, "translationX", -shakeOffset,
                shakeOffset, 0);
        shakeAnim.setDuration(100);
        shakeAnim.setRepeatCount(6);
        shakeAnim.start();
    }

    public void copySecret() {
        application.shareSecretCreateByLocal = secret;
        application._preferences.edit().putString(TbitProtocol.SP_A1_SHARE_LOCAL, secret).apply();
        String message = String.format(SHARE_CONTENT, secret);
        ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = android.content.ClipData.newPlainText("text label", message);
        clipboard.setPrimaryClip(clip);
    }

    private void jumpTo(String app, String launcher) {
        Intent intent = new Intent();
        ComponentName cmp = new ComponentName(app,launcher);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(cmp);
        startActivity(intent);
    }

    private void jumpToWx() {
        try {
            jumpTo("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
        } catch (Exception e) {
            e.printStackTrace();
            showLongToast("请确认是否安装微信");
        }
    }

    private void jumpToQQ() {
        try {
            jumpTo("com.tencent.mobileqq", "com.tencent.mobileqq.activity.SplashActivity");
        } catch (Exception e) {
            e.printStackTrace();
            showLongToast("请确认是否安装微信");
        }
    }
    private void jumpToSms() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType("vnd.android-dir/mms-sms");
        startActivity(intent);
    }

    private void share(int requestCode) {
        copySecret();
        String targetDesc = getTargetDescribe(requestCode);
        String message = String.format(SHARE_CONTENT, String.valueOf(secret));
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("已复制下列信息")
                .setMessage(message)
                .setPositiveButtonText(targetDesc)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(requestCode)
                .show();
    }

    private String getTargetDescribe(int requestCode) {
        String result = "";
        switch (requestCode) {
            case SHARE_REQUEST_WX:
                result = "去微信粘贴";
                break;
            case SHARE_REQUEST_QQ:
                result = "去QQ粘贴";
                break;
            case SHARE_REQUEST_SMS:
                result = "去发短信粘贴";
                break;
        }
        return result;
    }

    private void disconnectRemind() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("分享")
                .setMessage("你当前要分享的设备处于连接状态，分享接收者将无法连接，是否先断开当前连接")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_DISCONNECT)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case SHARE_REQUEST_WX:
                jumpToWx();
                break;
            case SHARE_REQUEST_QQ:
                jumpToQQ();
                break;
            case SHARE_REQUEST_SMS:
                jumpToSms();
                break;
            case DIALOG_REQUEST_DISCONNECT:
                LocalBroadcastManager.getInstance(ShareActivity.this).sendBroadcast(
                        new Intent(TbitProtocol.BROADCAST_RECEIVER_BLUETOOTH_DISCONNECT));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mConnection);
    }
}
