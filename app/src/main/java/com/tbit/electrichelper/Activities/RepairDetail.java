package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Adapter.GalleryAdapter;
import com.tbit.electrichelper.Beans.Image;
import com.tbit.electrichelper.Beans.Repair;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RepairDetail extends CommonToolbarActivity {
    private static final String BUNDLE_KEY_REPAIR_DETAIL = "bundle_key_repair";
    private final int TAG_GET_IMAGE_IDS = 0;
    @Bind(R.id.text_repair_detail_name)
    TextView textRepairDetailName;
    @Bind(R.id.text_repair_detail_date_start)
    TextView textRepairDetailDateStart;
    @Bind(R.id.text_repair_detail_date_deal)
    TextView textRepairDetailDateDeal;
    @Bind(R.id.text_repair_detail_date_finish)
    TextView textRepairDetailDateFinish;
    @Bind(R.id.text_repair_detail_phone)
    TextView textRepairDetailPhone;
    @Bind(R.id.text_repair_detail_err_desc)
    TextView textRepairDetailErrDesc;
    @Bind(R.id.text_repair_detail_err_code)
    TextView textRepairDetailErrCode;
    @Bind(R.id.text_repair_detail_remark)
    TextView textRepairDetailRemark;
    @Bind(R.id.id_recyclerView)
    RecyclerView idRecyclerView;

    private Repair repairDetail;
    private GalleryAdapter adapter;
    private List<Image> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mToolbarTitle.setText("报修详情");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        idRecyclerView.setLayoutManager(linearLayoutManager);
        adapter = new GalleryAdapter(this, images, false);
        idRecyclerView.setAdapter(adapter);

        repairDetail = getIntent().getParcelableExtra(BUNDLE_KEY_REPAIR_DETAIL);

        Map<String,String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_REPAIR_RECORD_ID, String.valueOf(repairDetail.getRepairRecordId()));
        postNetworkJson(TAG_GET_IMAGE_IDS, NetworkProtocol.URL_REPAIR_IMAGEIDS, map, false);

        initView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_repair_detail;
    }

    private void initView() {
        textRepairDetailName.setText(repairDetail.getRepairMan());
        textRepairDetailDateStart.setText(repairDetail.getRepairTime());
        textRepairDetailDateDeal.setText(repairDetail.getHandleTime());
        textRepairDetailDateFinish.setText(repairDetail.getFinishTime());
        textRepairDetailPhone.setText(repairDetail.getPhone());
        textRepairDetailErrDesc.setText(repairDetail.getFaultDescribe());
        textRepairDetailErrCode.setText(repairDetail.getFaultCode());
        textRepairDetailRemark.setText(repairDetail.getRemark());
    }

    public static void startActivity(Context context, Repair repair) {
        Intent intent = new Intent();
        intent.setClass(context, RepairDetail.class);
        intent.putExtra(BUNDLE_KEY_REPAIR_DETAIL, repair);
        context.startActivity(intent);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_GET_IMAGE_IDS:
                if (jsonObject.isRes()) {
                    List<ImageId> ids = gson.fromJson(jsonObject.getResult(), new TypeToken<List<ImageId>>(){}.getType());
                    if (ids != null && ids.size() > 0) {
                        for (ImageId id:ids) {
                            Image image = new Image();
                            image.setUrl(String.format(NetworkProtocol.URL_GET_IMG, id.getImageId(), application.common_token));
                            images.add(image);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_back_to_map:
                Intent intent = new Intent(this, ServicePointMapActivity.class);
                intent.putExtra(ServicePointMapActivity.BUNDLE_KEY_SELL_POINT, repairDetail.getSellPointId());
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sellpoint_detail, menu);
        return true;
    }

    private class ImageId {
        public int getRepairRecordId() {
            return repairRecordId;
        }

        public void setRepairRecordId(int repairRecordId) {
            this.repairRecordId = repairRecordId;
        }

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        private int repairRecordId;
        private String imageId;
    }
}
