package com.tbit.electrichelper.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.tbit.electrichelper.Fragments.Register.RegisterFragmentOne;
import com.tbit.electrichelper.Fragments.Register.RegisterFragmentTwo;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;

public class RegisterActivity extends CommonToolbarActivity implements ISimpleDialogListener {
    private int totalCountOfFragment;
    private String phoneToRegister = null;
    private String passwords = null;
    private int currentStep = 0;
    private Fragment[] fragments;

    public String getPhone() {
        return phoneToRegister;
    }

    public void setPhone(String phoneToRegister) {
        this.phoneToRegister = phoneToRegister;
    }

    public String getPasswords() {
        return passwords;
    }

    public void setPasswords(String passwords) {
        this.passwords = passwords;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbarTitle.setText(R.string.register);

        initFragment();
        setFragment(currentStep);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void initFragment() {
        fragments = new Fragment[]{new RegisterFragmentOne(), new RegisterFragmentTwo()};
        totalCountOfFragment = fragments.length;
    }

    private void setFragment(int index) {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.content, fragments[index]);
        if (fragments[0] != fragments[index]) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void toNext() {
        if (currentStep < totalCountOfFragment - 1) {
            setFragment(++currentStep);
        }
    }

    public void toPre() {
        if (currentStep > 0) {
            setFragment(--currentStep);
        }
    }

    public void quitDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage("是否退出注册")
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(0)
                .setNegativeButtonText(R.string.cancel)
                .setTitle(R.string.register).show();
    }

    public void registerDoneDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage("注册完成！是否自动登录？")
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(1)
                .setNegativeButtonText(R.string.cancel)
                .setCancelable(false)
                .setTitle(R.string.register).show();
    }

    public void alreadyRegisteredDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage("该手机号码已经注册！是否登录？")
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(2)
                .setNegativeButtonText(R.string.cancel)
                .setTitle(R.string.register).show();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        switch (requestCode) {
            case 0:

                break;
            case 1:
                finish();
                break;
            case 2:

                break;
            default:
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case 0:
                finish();
                break;
            case 1: {
                Intent intent = new Intent();
                intent.putExtra(LoginActivity.BUNDLE_KEY_LOGIN_STATE, LoginActivity.BUNDLE_VALUE_LOGIN_AUTO_LOGIN);
                intent.putExtra(LoginActivity.BUNDLE_KEY_LOGIN_PHONE, getPhone());
                intent.putExtra(LoginActivity.BUNDLE_KEY_LOGIN_PASSWORDS, getPasswords());
                setResult(RESULT_OK, intent);
                finish();
                break;
            }
            case 2:

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        quitDialog();
    }
}
