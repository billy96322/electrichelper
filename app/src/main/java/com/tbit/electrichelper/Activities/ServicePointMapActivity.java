package com.tbit.electrichelper.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BaiduNaviManager;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Beans.ServicePoint;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.Fragments.MoreFragment;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ServicePointMapActivity extends CommonToolbarActivity implements BaiduMap.OnMarkerClickListener, BaiduMap.OnMapClickListener {
    private static final int DIALOG_REQUEST_GPS = 0;
    private final int TAG_UPDATE_SELL_POINT_BEGIN = 0;
    private final int TAG_UPDATE_SELL_POINT = 1;
    private final int HANDLE_ZOOM_MAP = 0;
    private final int HANDLE_SET_MARKER = 1;
    private final int HANDLE_UPDATE_SELLS_POINT_BEGIN = 2;
    private static final int TAG_UPDATE_WHILE_COMMING_BACK = 2;
    public static final String BUNDLE_KEY_SELL_POINT = "sellpoint_id";

    @Bind(R.id.bmapView)
    TextureMapView mMapView;
    BaiduMap mBaiduMap;
    TextView myPositionInfoWindow;
    private List<LatLng> positionList = new ArrayList<>();
    private List<ServicePoint> dataList = new ArrayList<>();
    private List<Marker> markerList = new ArrayList<>();
    private MyApplication application;
    private Handler mHandhler;
    private View infoWindowView;
    private TextView tvShopName, tvPhone, tvAddress;
    private TextView btnCall, btnNavi, btnReport;
    private Marker myMarker;
    private int currentZoomLevel = 14;
    private boolean isShowAllSellPoint = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText("售后网点");

        application = MyApplication.getInstance();
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setOnMapClickListener(this);
        mBaiduMap.setOnMarkerClickListener(this);
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus) {

            }

            @Override
            public void onMapStatusChange(MapStatus mapStatus) {

            }

            @Override
            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                if (isShowAllSellPoint) {
                    int zoomLevel = (int) mapStatus.zoom;
                    if (zoomLevel > 17 || zoomLevel < 11 || zoomLevel == currentZoomLevel) {
                        return;
                    }
                    currentZoomLevel = zoomLevel;
                    int radius = zoom2radius(zoomLevel);
                    updateServicePoint(radius);
                }
            }
        });

        if (application.myPosition == null) {
            enterActivity(MainActivity.class);
            finish();
            return;
        }

        myPositionInfoWindow = new TextView(this);
        myPositionInfoWindow.setTextColor(0xFF000000);
        myPositionInfoWindow.setBackgroundResource(R.drawable.popup);
        myPositionInfoWindow.setText(" 我的位置");
        myMarker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(application.myPosition).icon(BitmapDescriptorFactory.fromBitmap(TbitUtil.getMyPosMarkerBitmap(this))).title("me").anchor(0.5f, 1.0f));

        TbitUtil.hideZoomControl(mMapView);
        initInfoWindow();

        if (application.myPosition != null) {
            MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(application.myPosition);
            mBaiduMap.setMapStatus(msu);
        }

        createHandler();

        getData();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_service_point_map;
    }

    private void createHandler() {
        mHandhler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case HANDLE_ZOOM_MAP:
                        fitHistoryView();
                        sendEmptyMessageDelayed(HANDLE_SET_MARKER, 600);
                        break;
                    case HANDLE_SET_MARKER:
                        initOverlay();
                        break;
                    case HANDLE_UPDATE_SELLS_POINT_BEGIN:
                        if (application.myPosition == null) {
                            showCrouton(R.string.location_fail);
                            break;
                        }
                        Map<String, String> map = new HashMap<>();
                        LatLng latLng = application.myPosition;
                        map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(latLng.latitude));
                        map.put(NetworkProtocol.PARAMKEY_LON, String.valueOf(latLng.longitude));
                        map.put(NetworkProtocol.PARAMKEY_RADIUS, String.valueOf(5));
                        map.put(NetworkProtocol.PARAMKEY_PLATEFORM_TYPE, String.valueOf(10));
                        postNetworkJson(TAG_UPDATE_SELL_POINT_BEGIN, NetworkProtocol.URL_SERVICE_OUTLET, map, true);
                        break;
                }
            }
        };
    }

    private void getData() {
        if (application.myPosition == null) {
            showCrouton(R.string.pls_wait, Style.INFO);
//            MainActivity.setLocationScanDuration(MainActivity.LOCATION_SCAN_DURATION_SHORT);
            mHandhler.sendEmptyMessageDelayed(HANDLE_UPDATE_SELLS_POINT_BEGIN, 5000);
        }
        mHandhler.sendEmptyMessage(HANDLE_UPDATE_SELLS_POINT_BEGIN);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        if (!jsonObject.isRes()) {
            showToast(jsonObject.getDesc());
            return;
        }
        switch (tag) {
            case TAG_UPDATE_SELL_POINT_BEGIN:
                dataList = gson.fromJson(jsonObject.getResult(), new TypeToken<List<ServicePoint>>() {
                }.getType());
                for (ServicePoint point : dataList) {
                    positionList.add(new LatLng(point.getLat(), point.getLon()));
                }
                mHandhler.sendEmptyMessageDelayed(HANDLE_ZOOM_MAP, 400);
                break;
            case TAG_UPDATE_SELL_POINT:
                dataList = gson.fromJson(jsonObject.getResult(), new TypeToken<List<ServicePoint>>() {
                }.getType());
                positionList.clear();
                for (ServicePoint point : dataList) {
                    positionList.add(new LatLng(point.getLat(), point.getLon()));
                }
                mHandhler.sendEmptyMessage(HANDLE_SET_MARKER);
                break;
            case TAG_UPDATE_WHILE_COMMING_BACK:
                dataList.clear();
                ServicePoint servicePoint = gson.fromJson(jsonObject.getResult(), ServicePoint.class);
                dataList.add(servicePoint);
                positionList.clear();
                positionList.add(new LatLng(servicePoint.getLat(), servicePoint.getLon()));
                mHandhler.sendEmptyMessageDelayed(HANDLE_ZOOM_MAP, 400);
                break;
            default:
                break;
        }
    }

    private void fitHistoryView() {
        if (positionList.size() > 0) {
            positionList.add(application.myPosition);
            LatLng[] fitPoint = TbitUtil.getFitPoint(positionList);
            fitPoint = TbitUtil.stretchLines(fitPoint, 1.2f);
            MapStatusUpdate msu = MapStatusUpdateFactory.newLatLngBounds(new LatLngBounds.Builder().include(fitPoint[0]).include(fitPoint[1]).build());
            positionList.remove(application.myPosition);
            mBaiduMap.setMapStatus(msu);
        }
    }

    private void initOverlay() {
        for (Marker m : markerList) {
            m.remove();
        }
        if (myMarker != null) {
            myMarker.remove();
        }
        myMarker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(application.myPosition).icon(BitmapDescriptorFactory.fromBitmap(TbitUtil.getMyPosMarkerBitmap(this))).title("me").anchor(0.5f, 1.0f));
        markerList.clear();
        BitmapDescriptor bd = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_orange_36dp);
        for (LatLng latlng : positionList) {
            MarkerOptions option = new MarkerOptions().position(latlng).icon(bd).animateType(MarkerOptions.MarkerAnimateType.grow);
            markerList.add((Marker) mBaiduMap.addOverlay(option));
        }

        if (!isShowAllSellPoint) {
            mBaiduMap.showInfoWindow(new InfoWindow(updateInfoWindow(0), markerList.get(0).getPosition(), -TbitUtil.dip2px(ServicePointMapActivity.this, 43)));
        }
    }

    private void moveMarkerToCenter(LatLng latLng) {
        MapStatus ms = new MapStatus.Builder().target(latLng).build();
        MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
        mBaiduMap.animateMapStatus(u);
    }

    @Override
    public void onFailure(int tag) {
        if (!isRunning) {
            return;
        }
        application.connectionFailTime++;
        showProgressDialog(false);
        if (tag == TAG_UPDATE_SELL_POINT_BEGIN) {
            showToast(getResources().getString(R.string.internet_error));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (marker.equals(myMarker)) {
            mBaiduMap.showInfoWindow(new InfoWindow(myPositionInfoWindow, marker.getPosition(), -TbitUtil.dip2px(this, 20)));
            return true;
        }
        final int pos = markerList.indexOf(marker);
        moveMarkerToCenter(positionList.get(pos));
        mHandhler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBaiduMap.showInfoWindow(new InfoWindow(updateInfoWindow(pos), marker.getPosition(), -TbitUtil.dip2px(ServicePointMapActivity.this, 43)));
            }
        }, 500);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void initInfoWindow() {
        infoWindowView = getLayoutInflater().inflate(R.layout.popwindown_service_map, null);
        // 显示当前电量
//        tipPower = (ImageView) infoWindow.findViewById(R.id.iv_displayElectricQuantity_mapTip);
        // 显示当前时间
        tvShopName = (TextView) infoWindowView.findViewById(R.id.tv_shopName);
        tvPhone = (TextView) infoWindowView.findViewById(R.id.tv_phone);
        tvAddress = (TextView) infoWindowView.findViewById(R.id.tv_address);
        btnCall = (TextView) infoWindowView.findViewById(R.id.btn_call);
        btnNavi = (TextView) infoWindowView.findViewById(R.id.btn_navi);
        btnReport = (TextView) infoWindowView.findViewById(R.id.btn_report);
    }

    private View updateInfoWindow(final int pos) {
        final ServicePoint point = dataList.get(pos);
        tvShopName.setText(point.getName());
        tvPhone.setText(point.getPhone());
        tvAddress.setText(point.getAddress());
        btnNavi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGpsOn()) {
                    mProgressDialogUtils.showProgressDialog(true, getString(R.string.pls_wait));
                    LatLng start = application.myPosition;
                    LatLng end = positionList.get(pos);
                    if (BaiduNaviManager.isNaviInited()) {
                        routeplanToNavi(start, end);
                    } else {
                        initNavi(start, end);
                    }
                } else {
                    showGpsDialog();
                }
            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = point.getSellPointId();
                LatLng latLng = new LatLng(point.getLat(), point.getLon());
                ReportRepairActivity.startActivity(ServicePointMapActivity.this, id, latLng);
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + point.getPhone()));
                if (ActivityCompat.checkSelfPermission(ServicePointMapActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                ServicePointMapActivity.this.startActivity(intent);
            }
        });
        return infoWindowView;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mBaiduMap.hideInfoWindow();
    }

    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }

    private void initNavi(final LatLng start, final LatLng end) {
        BaiduNaviManager.getInstance().init(ServicePointMapActivity.this, TbitProtocol.EXTERNAL_STORAGE, TbitProtocol.BAIDU_NAVI, new BaiduNaviManager.NaviInitListener() {
            @Override
            public void onAuthResult(int i, final String s) {

                if (0 != i) {
                    ServicePointMapActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(ServicePointMapActivity.this, "key校验失败, " + s, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            @Override
            public void initStart() {
            }

            @Override
            public void initSuccess() {
                routeplanToNavi(start, end);
            }

            @Override
            public void initFailed() {
                mProgressDialogUtils.showProgressDialog(false);
            }
        }, null);
    }

    private void routeplanToNavi(LatLng start, LatLng end) {
        BNRoutePlanNode sNode = new BNRoutePlanNode(start.longitude,
                start.latitude, null, null, BNRoutePlanNode.CoordinateType.BD09LL);
        BNRoutePlanNode eNode = new BNRoutePlanNode(end.longitude,
                end.latitude, null, null, BNRoutePlanNode.CoordinateType.BD09LL);
        double distance = TbitUtil.GetShortDistance(start, end);
        if (distance < 50) {
            mProgressDialogUtils.showProgressDialog(false);
            showLongToast("导航距离小于50米，请直接查看地图");
            return;
        }
        if (sNode != null && eNode != null) {
            List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
            list.add(sNode);
            list.add(eNode);
            BaiduNaviManager.getInstance().launchNavigator(this, list, 1, true, new DemoRoutePlanListener(sNode));
        }
    }

    private int zoom2radius(int zoom) {
        int scale = 1000;
        switch (zoom) {
            case 17:
                scale = 100;
                break;
            case 16:
                scale = 200;
                break;
            case 15:
                scale = 500;
                break;
            case 14:
                scale = 1000;
                break;
            case 13:
                scale = 2000;
                break;
            case 12:
                scale = 5000;
                break;
            case 11:
                scale = 10000;
                break;
            default:
                scale = 1000;
                break;
        }
        return (scale * 10) / 1000;
    }

    private void updateServicePoint(int radius) {
        Map<String, String> map = new HashMap<>();
        LatLng latLng = application.myPosition;
        map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(latLng.latitude));
        map.put(NetworkProtocol.PARAMKEY_LON, String.valueOf(latLng.longitude));
        //  半径
        map.put(NetworkProtocol.PARAMKEY_RADIUS, String.valueOf(radius));
        map.put(NetworkProtocol.PARAMKEY_PLATEFORM_TYPE, String.valueOf(10));
        postNetworkJson(TAG_UPDATE_SELL_POINT, NetworkProtocol.URL_SERVICE_OUTLET, map, false);
    }

    public class DemoRoutePlanListener implements BaiduNaviManager.RoutePlanListener {

        private BNRoutePlanNode mBNRoutePlanNode = null;

        public DemoRoutePlanListener(BNRoutePlanNode node) {
            mBNRoutePlanNode = node;
        }

        @Override
        public void onJumpToNavigator() {
            mProgressDialogUtils.showProgressDialog(false);
            for (Activity ac : application.activities) {

                if (ac.getClass().getName().endsWith("NavigationGuideActivity")) {
                    return;
                }
            }
            Intent intent = new Intent(ServicePointMapActivity.this, NavigationGuideActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(MoreFragment.ROUTE_PLAN_NODE, (BNRoutePlanNode) mBNRoutePlanNode);
            intent.putExtras(bundle);
            startActivity(intent);
        }

        @Override
        public void onRoutePlanFailed() {
            // TODO Auto-generated method stub
            mProgressDialogUtils.showProgressDialog(false);
            showCrouton("路线规划失败，请检查当前网络或定位状态");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int sellpointId = intent.getIntExtra(BUNDLE_KEY_SELL_POINT, -1);
        if (sellpointId != -1) {
            isShowAllSellPoint = false;
            mBaiduMap.hideInfoWindow();
            Map<String, String> map = new HashMap<>();
            map.put(NetworkProtocol.PARAMKEY_SELLPOINTID, String.valueOf(sellpointId));
            postNetworkJson(TAG_UPDATE_WHILE_COMMING_BACK, NetworkProtocol.URL_GET_SELLPOINT_INFO_BY_ID, map, true);
        }
    }

    private boolean isGpsOn() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showGpsDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle(R.string.navigate)
                .setMessage("导航需要开启GPS定位,请开启GPS定位")
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(DIALOG_REQUEST_GPS)
                .show();
    }
}
