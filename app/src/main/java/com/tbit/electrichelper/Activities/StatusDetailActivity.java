package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Adapter.ExpandableAdapter;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StatusDetailActivity extends CommonToolbarActivity {

    public static final String ITEM_NAME = "item_name";
    public static final String ITEM_STATUS = "item_status";
    public static final String ITEM_CHILD = "item_child";
    private static final int NETWORK_UPDATE_STATUS = 0;
    private static final String STRING_POSITIVE = "正常";
    private static final String STRING_NEGATIVE = "异常";
    @Bind(R.id.listView)
    ExpandableListView listView;

    private CarStatus status;
    private ExpandableAdapter adapter;
    private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
    private MyApplication application;
    private boolean isRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        isRunning = true;

        mToolbarTitle.setText("详情");
        application = MyApplication.getInstance();

        adapter = new ExpandableAdapter(this, data);
        listView.setAdapter(adapter);
        listView.setGroupIndicator(null);

        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
        postNetworkJson(NETWORK_UPDATE_STATUS, NetworkProtocol.URL_POSITION, map, true);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_status_detail;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (!isRunning) {
            return;
        }
        super.parseResults(jsonObject, tag);
        if (tag == NETWORK_UPDATE_STATUS) {
            if (jsonObject.isRes()) {
                List<CarStatus> temp = new Gson().fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                }.getType());
                if (temp != null && temp.size() != 0) {
                    status = temp.get(0);
                    application.setCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS, status);
                }
            }
            initData();
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        initData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void initData() {
        if (status == null) {
            status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        }
        if (status == null) {
            enterActivity(MainActivity.class);
            finish();
            return;
        }
        String[] fatherItems = new String[]{"动力系统", "供电系统", "显示系统", "定位系统"};
        String[] motivationItems = new String[]{"电机缺相", "控制器故障", "转把故障", "霍尔故障", "巡航状态", "电子刹车", "侧架状态"};
        String[] powerItems = new String[]{"电量", "欠压保护"};
        String[] displayItems = new String[]{"仪表盘"};
        String[] locateItem = new String[]{"GPS定位", "GSM信号强度"};
        data.clear();

        String[] childData = null;
        try {
            String[] temp = status.getStrGGPV().split("=");
            childData = temp[1].split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Map<String, String>> children = new ArrayList<>();

        Map<String, String> child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[0]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[1]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[1]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[5]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[2]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[6]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[3]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[7]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[4]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[2]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[5]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[17]));
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, motivationItems[6]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[24]));
        children.add(child);

        Map<String, Object> motivationSystem = new HashMap<>();
        motivationSystem.put(ITEM_NAME, fatherItems[0]);
        motivationSystem.put(ITEM_STATUS, isMotivationSystemFunction(status.getStrGGPV()) ? "正常" : "故障");
        motivationSystem.put(ITEM_CHILD, children);

        children = new ArrayList<>();
        child = new HashMap<>();
        child.put(ITEM_NAME, powerItems[0]);
        child.put(ITEM_STATUS, (translatePower(status.getStrGGPV()) * 25) + "%");
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, powerItems[1]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[3]));
        children.add(child);


        Map<String, Object> powerSupplySystem = new HashMap<>();
        powerSupplySystem.put(ITEM_NAME, fatherItems[1]);
        powerSupplySystem.put(ITEM_STATUS, isPowerSystemFunction(translatePower(status.getStrGGPV())));
        powerSupplySystem.put(ITEM_CHILD, children);


        children = new ArrayList<>();
        child = new HashMap<>();
        child.put(ITEM_NAME, displayItems[0]);
        child.put(ITEM_STATUS, childData == null ? "正常" : translateStatus(childData[25]));
        children.add(child);

        Map<String, Object> displaySystem = new HashMap<>();
        displaySystem.put(ITEM_NAME, fatherItems[2]);
        displaySystem.put(ITEM_STATUS, "正常");
        displaySystem.put(ITEM_CHILD, children);

        children = new ArrayList<>();
        child = new HashMap<>();
        child.put(ITEM_NAME, locateItem[0]);
        child.put(ITEM_STATUS, "正常");
        children.add(child);

        child = new HashMap<>();
        child.put(ITEM_NAME, locateItem[1]);
        child.put(ITEM_STATUS, "正常");
        children.add(child);

        Map<String, Object> locationSystem = new HashMap<>();
        locationSystem.put(ITEM_NAME, fatherItems[3]);
        locationSystem.put(ITEM_STATUS, "正常");
        locationSystem.put(ITEM_CHILD, children);

        data.add(motivationSystem);
        data.add(powerSupplySystem);
        data.add(displaySystem);
        data.add(locationSystem);

        adapter.notifyDataSetChanged();
    }

    private boolean isMotivationSystemFunction(String s) {
        int result = 0;
        try {
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            result = 0;

            result = stringToInt(temp[1] + temp[5] + temp[6] + temp[7]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result == 0;
    }

    private String isPowerSystemFunction(int i) {
        return i > 1 ? "正常" : "低电";
    }

    private String translateStatus(String i) {
        return i.equals("0") ? "正常" : "故障";
    }

    private int translatePower(String s) {
        int result = 0;
        try {
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            result = stringToInt(temp[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    private int stringToInt(String s) {
        return Integer.parseInt(s);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }
}
