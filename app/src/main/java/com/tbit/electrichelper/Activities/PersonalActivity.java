package com.tbit.electrichelper.Activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.tbit.electrichelper.Beans.CarInfo;
import com.tbit.electrichelper.Beans.PersonalInformation;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class PersonalActivity extends CommonToolbarActivity implements com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener, ISimpleDialogListener {
    private static final int TAG_NETWORK_UPDATE_CAR_INFO = 0;
    private static final int TAG_NETWORK_QUERY_CAR_INFO = 1;
    private static final int TAG_NETWORK_UPDATE_PERSONAL_INFO = 2;
    private static final String DATEPICKER_TAG = "datepicker";
    private final int DIALOG_REQUEST_SEX = 0;
    private final int DIALOG_REQUEST_QUIT = 1;
    @Bind(R.id.edit_personal_info_name)
    EditText editPersonalInfoName;
    @Bind(R.id.text_personal_info_phone)
    TextView textPersonalInfoPhone;
    @Bind(R.id.text_personal_info_sex)
    TextView textPersonalInfoSex;
    @Bind(R.id.text_personal_birthday)
    TextView textPersonalBirthday;
    @Bind(R.id.edit_personal_info_email)
    EditText editPersonalInfoEmail;
    @Bind(R.id.edit_car_info_plate_number)
    EditText editCarInfoPlateNumber;
    @Bind(R.id.text_car_info_sim_number)
    TextView textCarInfoSimNumber;
    @Bind(R.id.text_car_info_machine_number)
    TextView textCarInfoMachineNumber;
    @Bind(R.id.edit_car_info_car_owner)
    EditText editCarInfoCarOwner;
    @Bind(R.id.text_car_info_owner_phone)
    TextView textCarInfoOwnerPhone;

    //    private String personalPhone,personalName,personalBirthday,personalSex,personalEmail;
//    private String carPlate,carSimNum,carMachineNum,carOwnerName,carPhone;
    private PersonalInformation personalInfo = new PersonalInformation();
    private CarInfo carInfo = new CarInfo();
    private com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.text_personal_birthday:
                    resetTimePicker();
                    if (!datePickerDialog.isVisible()) {
                        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
                    }
                    break;
                case R.id.text_personal_info_sex:
                    SimpleDialogFragment.createBuilder(PersonalActivity.this, getSupportFragmentManager())
                            .setTitle("性别")
                            .setNegativeButtonText("男")
                            .setPositiveButtonText("女")
                            .setRequestCode(DIALOG_REQUEST_SEX)
                            .show();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);

        mToolbarTitle.setText("个人信息");

        Calendar calendar = new GregorianCalendar();
//        datePickerDialog = new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle(R.string.personal_centre);

        personalInfo = (PersonalInformation) application.getNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO);
        carInfo = (CarInfo) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_CAR_INFO);

        if (personalInfo == null || carInfo == null) {
            enterActivity(MainActivity.class);
            finish();
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
        postNetworkJson(TAG_NETWORK_QUERY_CAR_INFO, NetworkProtocol.URL_CAR_INFO, map, true);
//        initView();

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_personal;
    }

    @Override
    protected String inflateCustomTextMenu() {
        return "提交";
    }

    @Override
    protected void onCustomMenuClick() {
        if (!preVerify()) {
            return;
        }
        boolean isPersonalInfoModified = personalVerify();
        boolean isCarInfoModified = carVerify();
        if (!isCarInfoModified && !isPersonalInfoModified) {
            showCrouton("没有改变", Style.INFO);
            return;
        }
        if (isPersonalInfoModified) {
            submitPersonalInfo();
        }
        if (isCarInfoModified) {
            submitCarInfo();
        }
    }

    private void initView() {
        editPersonalInfoName.setText(personalInfo.getName());
        textPersonalInfoPhone.setText(personalInfo.getPhone());
        textPersonalBirthday.setText(personalInfo.getBirthday());
        textPersonalInfoSex.setText(getSex(personalInfo.getSex()));
        editPersonalInfoEmail.setText(personalInfo.getEmail());
        textPersonalBirthday.setOnClickListener(onClickListener);
        textPersonalInfoSex.setOnClickListener(onClickListener);

        editCarInfoPlateNumber.setText(carInfo.getNo());
        textCarInfoSimNumber.setText(carInfo.getSim());
        textCarInfoMachineNumber.setText(carInfo.getMachineNO());
        editCarInfoCarOwner.setText(carInfo.getDriver());
        textCarInfoOwnerPhone.setText(carInfo.getDriverTel());
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_NETWORK_QUERY_CAR_INFO:
                if (jsonObject.isRes()) {
                    carInfo = gson.fromJson(jsonObject.getResult(), CarInfo.class);
                    application.setCurGlobal(TbitProtocol.KEY_GLOBAL_CAR_INFO, carInfo);
                    initView();
                } else {
                    if (carInfo != null) {
                        initView();
                    } else {
                        showCrouton("获得数据失败");
                        finish();
                    }
                }
                break;
            case TAG_NETWORK_UPDATE_CAR_INFO:
                if (jsonObject.isRes()) {
                    showToast("修改成功");
                    finish();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
            case TAG_NETWORK_UPDATE_PERSONAL_INFO:
                if (jsonObject.isRes()) {
                    showToast("修改成功");
                    PersonalInformation info = (PersonalInformation) application.getNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO);
                    info.setName(getEditString(editPersonalInfoName));
                    info.setSex(textPersonalInfoSex.getText().toString().equals("男") ? 1 : 0);
                    info.setBirthday(textPersonalBirthday.getText().toString());
                    info.setEmail(getEditString(editPersonalInfoEmail));
//                    application.putNobeloning(TbitProtocol.KEY_NOBELONING_PERSONAL_INFO, info);
                    finish();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
        }
    }

    private boolean preVerify() {
        if (getEditString(editPersonalInfoName).equals("")) {
            showCrouton("请输入姓名");
            return false;
        }
        if (getEditString(editPersonalInfoEmail).equals("")) {
            showCrouton("请输入邮箱");
            return false;
        }
        if (!TbitUtil.isEmailLegal(getEditString(editPersonalInfoEmail))) {
            showCrouton("输入的邮箱不合法");
            return false;
        }
//        else if (getEditString(textPersonalInfoPhone).equals("")) {
//            showCrouton("请输入电话");
//            return false;
//        }
//        else if (getEditString(editPersonalInfoEmail).equals("")) {
//            return false;
//        }
        if (getEditString(editCarInfoPlateNumber).equals("")) {
            showCrouton("请输入车牌号码");
            return false;
        }
        if (getEditString(editCarInfoCarOwner).equals("")) {
            showCrouton("请输入车主姓名");
            return false;
        }
//        else if (getEditString(textCarInfoMachineNumber).equals("")) {
//            showCrouton("请输入机器编号");
//            return false;
//        }

//        else if (getEditString(textCarInfoSimNumber).equals("")) {
//            showCrouton("请输入sim卡号");
//            return false;
//        }
//        else if (getEditString(textCarInfoOwnerPhone).equals("")) {
//            showCrouton("请输入车主电话");
//            return false;
//        }
        return true;
    }

    private boolean personalVerify() {
        if (!getEditString(editPersonalInfoEmail).equals(personalInfo.getEmail())) {
            return true;
        } else if (!getEditString(editPersonalInfoName).equals(personalInfo.getName())) {
            return true;
        } else if (!textPersonalBirthday.getText().toString().equals(personalInfo.getBirthday())) {
            return true;
        } else if (!getSex(personalInfo.getSex()).equals(textPersonalInfoSex.getText().toString())) {
            return true;
        }
        return false;
    }

    private boolean carVerify() {
        if (!getEditString(editCarInfoPlateNumber).equals(carInfo.getNo())) {
            return true;
        } else if (!getEditString(editCarInfoCarOwner).equals(carInfo.getDriver())) {
            return true;
        }
        return false;
    }

    private void init() {
//        carPlate = editCarInfoPlateNumber.getText().toString();
//        carSimNum = textCarInfoSimNumber.getText().toString();
//        carMachineNum = textCarInfoMachineNumber.getText().toString();
//        carPhone = textCarInfoOwnerPhone.getText().toString();
//        carOwnerName = editCarInfoCarOwner.getText().toString();
    }

    private String getEditString(EditText editText) {
        return editText.getText().toString();
    }


    private String getSex(int i) {
        return i == 1 ? "男" : "女";
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                finish();
//                break;
//        }
//        return true;
//    }

//    @Override
//    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//        String date = year + "-" + String.valueOf(monthOfYear + 1) + "-" + dayOfMonth + "";
//        textPersonalBirthday.setText(date);
//    }

    private void resetTimePicker() {
        String s = textPersonalBirthday.getText().toString();
        Calendar calendar = new GregorianCalendar();
        if (s.length() != 0) {
            calendar = TbitUtil.getGregorianCalendarByString(s);
        }
//        datePickerDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.onDayOfMonthSelected(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.initialize(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_QUIT:

                break;
            case DIALOG_REQUEST_SEX:
                textPersonalInfoSex.setText(getSex(1));
                break;
            default:
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_SEX:
                textPersonalInfoSex.setText(getSex(0));
                break;
            case DIALOG_REQUEST_QUIT:
                finish();
                break;
        }
    }

    private void submitCarInfo() {
        Map<String, String> map = new HashMap<>();
        map.put("id", application.getCurCar().getCarId());
        map.put(NetworkProtocol.PARAMKEY_NO, getEditString(editCarInfoPlateNumber));
//        map.put(NetworkProtocol.PARAMKEY_SIM, getEditString(textCarInfoSimNumber));
        map.put(NetworkProtocol.PARAMKEY_MACHINENO, textCarInfoMachineNumber.getText().toString());
        map.put(NetworkProtocol.PARAMKEY_DRIVER, getEditString(editCarInfoCarOwner));
//        map.put(NetworkProtocol.PARAMKEY_DRIVERTEL, getEditString(textCarInfoOwnerPhone));
        postNetworkJson(TAG_NETWORK_UPDATE_CAR_INFO, NetworkProtocol.URL_CAR_INFO_UPDATE, map, true);
    }

    private void submitPersonalInfo() {
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_NAME, getEditString(editPersonalInfoName));
        map.put(NetworkProtocol.PARAMKEY_SEX, (textPersonalInfoSex.getText().toString().equals("男")) ? "1" : "0");
        map.put(NetworkProtocol.PARAMKEY_BIRTHDAY, textPersonalBirthday.getText().toString());
        map.put(NetworkProtocol.PARAMKEY_EMAIL, getEditString(editPersonalInfoEmail));
        postNetworkJson(TAG_NETWORK_UPDATE_PERSONAL_INFO, NetworkProtocol.URL_PERSONAL_INFO_UPDATE, map, true);
    }

    @Override
    protected void showCrouton(String s) {
        showCrouton(s, Style.ALERT);
    }

    @Override
    protected void showCrouton(String s, Style style) {
        Crouton.cancelAllCroutons();
        Crouton.makeText(this, s, style, R.id.crouton_frame).show();
    }

    @Override
    public void onBackPressed() {
        if (personalVerify() || carVerify()) {
            SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                    .setTitle("退出")
                    .setMessage("是否放弃当前修改?")
                    .setPositiveButtonText("确定")
                    .setNegativeButtonText("取消")
                    .setRequestCode(DIALOG_REQUEST_QUIT)
                    .show();
        } else {
            finish();
        }
    }

    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
        String date = year + "-" + String.valueOf(month + 1) + "-" + day + "";
        textPersonalBirthday.setText(date);
    }
}
