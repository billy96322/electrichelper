package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BrowserActivity extends BaseActivity {

    private static final String KEY_PUTEXTRA_URL = "URL";
    private static final int VIBRATE_LEVERL = 30;

    @Bind(R.id.wv_webPage_web)
    WebView webView;
    @Bind(R.id.tv_pre_webview)
    ImageView ivPre;
    @Bind(R.id.tv_next_webview)
    ImageView ivNext;
    @Bind(R.id.tv_open_in_browser_webview)
    ImageView ivOpenInBrowser;
    @Bind(R.id.back)
    ImageView back;

    /**
     * 震动
     */
    private Vibrator vibrator;
    private String url;

    public static void startActivity(Context context, String url) {
        Intent intent = new Intent();
        intent.setClass(context, BrowserActivity.class);
        intent.putExtra(KEY_PUTEXTRA_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        ButterKnife.bind(this);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        url = getIntent().getStringExtra(KEY_PUTEXTRA_URL);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* 设置javascript */
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        // 不使用缓存
        // webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        /* URL */
        webView.loadUrl(url);
		/* 视图 */
        webView.setWebViewClient(new HelloWebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
				/* 加载完成 */
                // progressDialog.dismiss();
                super.onPageFinished(view, url);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }
        });

        ivPre.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                vibrator.vibrate(VIBRATE_LEVERL);
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
        });
        ivNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                vibrator.vibrate(VIBRATE_LEVERL);
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }
        });
        ivOpenInBrowser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(webView.getUrl());
                intent.setData(content_url);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (webView != null) {
            webView.destroy();
        }
        vibrator.cancel();
        super.onDestroy();
    }

    /**
     * 返回按键 结束activity
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            finish();
        }
        return true;
    }

    /**
     * Web视图
     */
    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
