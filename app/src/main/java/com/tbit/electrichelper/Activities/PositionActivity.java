package com.tbit.electrichelper.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.Online;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.DataUtil;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class PositionActivity extends CommonToolbarActivity implements BaiduMap.OnMapClickListener, BaiduMap.OnMarkerClickListener {

    private static final int TAG_GET_POSITION_DATA = 0;
    private static final int TAG_GET_POSITION_DESC = 1;
    private static final int TAG_GET_POSITION_IMMEDIATELY = 2;
    private static final int TAG_SET_LOCK_ON = 3;
    private static final int TAG_SET_LOCK_OFF = 4;
    private static final int HANDLE_REFRESH_MAP = 0;
    @Bind(R.id.bmapView)
    TextureMapView mMapView;
    BaiduMap mBaiduMap;
    TextView tipTime, tipDesc, tvSpeed, tvCarno, tvType, tvScode;
    //    ImageView tipPower;
    @Bind(R.id.btn_locate)
    ImageView btnLocate;
    @Bind(R.id.btn_history)
    ImageView btnHistory;
    @Bind(R.id.btn_lock)
    ImageView btnLock;
    @Bind(R.id.btn_unlock)
    ImageView btnUnlock;
    private CarStatus status;
    private Marker carMarker;
    private Marker myMarker;
    private Handler mHandler;
    private View infoWindowView;
    private TextView myPositionInfoWindow;
    private InfoWindow curInfoWindow;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (application.getLoginStatus() != TbitProtocol.LOGGED_IN) {
                showCrouton(getString(R.string.tip_only_logged_in_user_positive), Style.INFO);
                return;
            }
            if (!application.hasA2Bound()) {
                toBoundActivity();
                return;
            }
            switch (v.getId()) {
                case R.id.btn_locate: {
                    btnLocate.setClickable(false);
                    Map<String, String> map = new HashMap<>();
                    map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(2));
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, String.valueOf(3));
                    postNetworkJson(TAG_GET_POSITION_IMMEDIATELY, NetworkProtocol.URL_SEND_COMMAND, map, false);
                    break;
                }
                case R.id.btn_history:
                    enterActivity(HistoryActivity.class);
                    break;
                case R.id.btn_lock: {
                    Map<String, String> map = new HashMap<>();
                    map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(2));
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, String.valueOf(0));
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(1));
                    postNetworkJson(TAG_SET_LOCK_ON, NetworkProtocol.URL_SEND_COMMAND, map, false);
                    break;
                }
                case R.id.btn_unlock: {
                    Map<String, String> map = new HashMap<>();
                    map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(2));
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, String.valueOf(0));
                    map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(0));
                    postNetworkJson(TAG_SET_LOCK_OFF, NetworkProtocol.URL_SEND_COMMAND, map, false);
                    break;
                }
                default:
                    break;
            }
        }
    };

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (jsonObject == null || !isRunning) {
            return;
        }
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_GET_POSITION_DATA:
                if (jsonObject.isRes()) {
                    List<CarStatus> temp = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                    }.getType());
                    for (CarStatus status : temp) {
                        application.setGlobal(status.getCarId(), TbitProtocol.KEY_GLOBAL_STATUS, status);
                    }
                }
                showStatus();
                break;
            case TAG_GET_POSITION_DESC:
                if (jsonObject.isRes()) {
                    curInfoWindow = new InfoWindow(getInfowindow(jsonObject.getResult()), carMarker.getPosition(), -TbitUtil.dip2px(this, 40));
                    mBaiduMap.showInfoWindow(curInfoWindow);
                }
                break;
            case TAG_GET_POSITION_IMMEDIATELY:
                btnLocate.setClickable(true);
                if (jsonObject.isRes()) {
                    showCrouton("指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton(jsonObject.getDesc(), Style.INFO);
                }
                break;
            case TAG_SET_LOCK_ON:
                btnLock.setClickable(true);
                if (jsonObject.isRes()) {
                    showCrouton("指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton(jsonObject.getDesc(), Style.INFO);
                }
                break;
            case TAG_SET_LOCK_OFF:
                btnUnlock.setClickable(true);
                if (jsonObject.isRes()) {
                    showCrouton("指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton(jsonObject.getDesc(), Style.INFO);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        switch (tag) {
            case TAG_GET_POSITION_DATA:
                showStatus();
                break;
            case TAG_SET_LOCK_ON:
                btnLock.setClickable(true);
                break;
            case TAG_SET_LOCK_OFF:
                btnUnlock.setClickable(true);
                break;
            case TAG_GET_POSITION_IMMEDIATELY:
                btnLocate.setClickable(true);
                break;
        }
    }

    private void showStatus() {
        status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        if (status == null) {
            mHandler.removeMessages(HANDLE_REFRESH_MAP);
            return;
        }
        final LatLng latLng = new LatLng(status.getLat(), status.getLng());

        //设置marker
        if (carMarker == null) {
            carMarker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(latLng).icon(getMarker()).title("child").anchor(0.5f, 1.0f));
        } else {
            carMarker.setPosition(latLng);
            carMarker.setIcon(getMarker());
        }

        if (application._preferences.getBoolean(TbitProtocol.SP_SHOW_MY_POSITION, true) && application.myPosition != null) {
            if (myPositionInfoWindow == null) {
                myPositionInfoWindow = new TextView(this);
                myPositionInfoWindow.setTextColor(0xFF000000);
                myPositionInfoWindow.setBackgroundResource(R.drawable.popup);
                myPositionInfoWindow.setText(" 我的位置");
            }
            if (myMarker == null) {
                myMarker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(application.myPosition).icon(BitmapDescriptorFactory.fromBitmap(TbitUtil.getMyPosMarkerBitmap(this))).title("me").anchor(0.5f, 1.0f));
            } else {
                myMarker.setPosition(application.myPosition);
            }

            LatLng[] fitPoints = TbitUtil.getFitPoint(new ArrayList<>(Arrays.asList(latLng, application.myPosition)));
            fitPoints = TbitUtil.stretchLines(fitPoints, 1.2f);
            final MapStatusUpdate msu = MapStatusUpdateFactory.newLatLngBounds(new LatLngBounds.Builder().include(fitPoints[0]).include(fitPoints[1]).build());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        mBaiduMap.animateMapStatus(msu);
                        if (curInfoWindow != null) {
                            mBaiduMap.showInfoWindow(curInfoWindow);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);

        } else {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).zoom(16).build()));
                        if (curInfoWindow != null) {
                            mBaiduMap.showInfoWindow(curInfoWindow);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);
        }
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_LAT, String.valueOf(status.getLat()));
        map.put(NetworkProtocol.PARAMKEY_LNG, String.valueOf(status.getLng()));
        postNetworkJson(TAG_GET_POSITION_DESC, NetworkProtocol.URL_GET_ADDRESS_DESC, map, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isRunning) {
                    return;
                }
                if (msg.what == HANDLE_REFRESH_MAP) {
                    if (visible) {
                        showStatus();
                    }
                    mHandler.sendEmptyMessageDelayed(HANDLE_REFRESH_MAP, 10 * 1000);
                }
                super.handleMessage(msg);
            }
        };

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle("定位");
//        status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);

        mToolbarTitle.setText("定位");

        mBaiduMap = mMapView.getMap();
        TbitUtil.hideZoomControl(mMapView);

        mBaiduMap.setOnMapClickListener(this);
        mBaiduMap.setOnMarkerClickListener(this);

        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
        if (application.getLoginStatus() == TbitProtocol.GUEST) {
            showStatus();
        } else {
            postNetworkJson(TAG_GET_POSITION_DATA, NetworkProtocol.URL_POSITION, map, true);
        }


        btnLocate.setOnClickListener(onClickListener);
        btnHistory.setOnClickListener(onClickListener);
        btnLock.setOnClickListener(onClickListener);
        btnUnlock.setOnClickListener(onClickListener);
    }

    @Override
    protected String inflateCustomTextMenu() {
        return "绑定";
    }

    @Override
    protected void onCustomMenuClick() {
        toBoundActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_position, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_position_bound:
                break;
        }
        return true;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_position;
    }

    private void initInfoWindow() {
        infoWindowView = getLayoutInflater().inflate(R.layout.popwindown_user_info, null);
        // 显示当前电量
//        tipPower = (ImageView) infoWindow.findViewById(R.id.iv_displayElectricQuantity_mapTip);
        // 显示当前时间
        tvCarno = (TextView) infoWindowView.findViewById(R.id.tv_carNo);
        tvSpeed = (TextView) infoWindowView.findViewById(R.id.tv_Speed);
        tipTime = (TextView) infoWindowView.findViewById(R.id.tv_time_info);
        tipDesc = (TextView) infoWindowView.findViewById(R.id.tv_address_maptip);
        // 显示定位方式
        tvType = (TextView) infoWindowView.findViewById(R.id.tv_locateType);
        tvScode = (TextView) infoWindowView.findViewById(R.id.tv_sCode);
    }

    private View getInfowindow(String content) {
        if (infoWindowView == null) {
            initInfoWindow();
        }
        // 显示位置描述
        String temp = tipDesc.getText().toString();
        if (content != null && content.length() != 0) {
//            temp = String.format(getString(R.string.location_describe), status.getCarId(), content, status.getSpeed());
            temp = content;
        }
        tipTime.setText(status.getGpstime());
//        if (Math.abs(System.currentTimeMillis() - TbitUtil.getTimeMilesByString(status.getGpstime())) > 3600000) {
//            tvSpeed.setText("0Km/h");
//        } else {
//            tvSpeed.setText(String.valueOf(translateSpeed(status.getStrGGPV())) + "Km/h");
//        }
        tvSpeed.setText(String.valueOf(translateSpeed(status.getStrGGPV())) + "Km/h");
        String machineNo = DataUtil.getMachineNoByCarId(application.getCurCar().getCarId());
        tvCarno.setText(machineNo);
        tipDesc.setText(temp);
        tvType.setText(getPositionTypeResouce(status.getPointed()));
        tvScode.setText(getSCodeString(status.getScode()));
//        setView();
        return infoWindowView;
    }

    private int translateSpeed(String s) {
        try {
            if (s == null || s.equals("")) {
                return 0;
            }
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            float result = 0;

            result = Integer.parseInt(temp[26]);
            return (int) result;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private String getPositionTypeResouce(int type) {
        String result = "";
        switch (type) {
            case 1:
                result = "卫星定位";
                break;
            case 2:
                result = "基站定位";
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        mHandler.removeMessages(HANDLE_REFRESH_MAP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
        mHandler.sendEmptyMessage(HANDLE_REFRESH_MAP);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeMessages(HANDLE_REFRESH_MAP);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mBaiduMap.hideInfoWindow();
    }

    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.equals(this.carMarker)) {
            mBaiduMap.showInfoWindow(new InfoWindow(getInfowindow(null), marker.getPosition(), -TbitUtil.dip2px(this, 40)));
        } else if (marker.equals(this.myMarker)) {
            mBaiduMap.showInfoWindow(new InfoWindow(myPositionInfoWindow, marker.getPosition(), -TbitUtil.dip2px(this, 20)));
        }
        return true;
    }

//    @Override
//    protected void showCrouton(String s) {
//        showCrouton(s, Style.ALERT);
//    }
//
//    @Override
//    protected void showCrouton(String s, Style style) {
//        Crouton.cancelAllCroutons();
//        Crouton.makeText(this, s, style, R.id.frame).show();
//    }

    private void toBoundActivity() {
        Intent intent = new Intent(PositionActivity.this, MainActivity.class);
        intent.setAction(TbitProtocol.ACTION_NOTHING_BIND);
        startActivity(intent);
    }

    private String getSCodeString(String status) {
        String result = "";
        if (status == null || status.length() == 0) {
            return result;
        }
        if (status.split(":").length < 2) {
            return result;
        }
        String[] temp = status.split(":");

        boolean needCommas = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < temp.length; i++) {
            if (needCommas) {
                sb.append(",");
            }
            try {
                sb.append(translateSCode(Integer.parseInt(status.split(":")[i])));
                needCommas = true;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        result = sb.toString();
        return result;
    }

    private String translateSCode(int sCode) {
        String result = "";
        switch (sCode) {
            case 0:
                result = "已设防";
                break;
            case 1:
                result = "未设防";
                break;
            case 2:
                result = "休眠";
                break;
            case 3:
                result = "断电";
                break;
            case 4:
                result = "运动";
                break;
            case 5:
                result = "ACC开";
                break;
            case 6:
                result = "车门开";
                break;
            case 7:
                result = "油门断";
                break;
            case 8:
                result = "电门断";
                break;
            case 9:
                result = "电机锁";
                break;
            default:
                break;
        }
        return result;
    }

    private BitmapDescriptor getMarker() {
        return BitmapDescriptorFactory.fromBitmap(TbitUtil.getMarkerBitmap(this, isOnline()));
    }

    private boolean isOnline() {
        try {
            Online online = (Online) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_ONLINE);
            return online.isOnline();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return true;
    }
}
