package com.tbit.electrichelper.Activities;

import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.LatLng;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Adapter.MainPageAdapter;
import com.tbit.electrichelper.Beans.Car;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.CarInfo;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.Message;
import com.tbit.electrichelper.Beans.Online;
import com.tbit.electrichelper.Beans.ShareResult;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Constant;
import com.tbit.electrichelper.Bluetooth.Interface.BluetoothParseServiceImpl;
import com.tbit.electrichelper.Bluetooth.Util.ByteUtil;
import com.tbit.electrichelper.Bluetooth.Util.CustomProgressDialog;
import com.tbit.electrichelper.Bluetooth.Util.SharePreferenceUtil;
import com.tbit.electrichelper.Fragments.BluetoothControlFragment;
import com.tbit.electrichelper.Fragments.BluetoothHomeFragment;
import com.tbit.electrichelper.Fragments.DetectFragment;
import com.tbit.electrichelper.Fragments.Dialogs.AddBindDialog;
import com.tbit.electrichelper.Fragments.Dialogs.BindingManageDialogFragment;
import com.tbit.electrichelper.Fragments.HomeFragment;
import com.tbit.electrichelper.Fragments.MoreFragment;
import com.tbit.electrichelper.Fragments.ServiceFragment;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.PacketParserService;
import com.tbit.electrichelper.Services.UartService;
import com.tbit.electrichelper.Util.BaseNetworkActivity;
import com.tbit.electrichelper.Util.DataUtil;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.ShareCodeGenerator;
import com.tbit.electrichelper.Util.TabChanger;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;
import com.tbit.electrichelper.Util.UpdateManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseNetworkActivity implements
        ISimpleDialogListener, UpdateManager.OnUpdateListener,
        BindingManageDialogFragment.BindListener, BluetoothParseServiceImpl {
    public static final int LOCATION_SCAN_DURATION_SHORT = 1000;
    public static final int LOCATION_SCAN_DURATION_LONG = 10 * 1000;

    private static final int DIALOG_REQUEST_QUIT = 0;
    private static final int DIALOG_REQUEST_LOW_POWER = 1;
    private static final int DIALOG_REQUEST_UPDATE = 2;
    private static final int DIALOG_REQUEST_NOT_WIFI = 3;
    private static final int DIALOG_REQUEST_NO_NETWORK = 4;
    private static final int DIALOG_REQUEST_CHOOSE_BIND_TYPE = 5;
    private static final int DIALOG_REQUEST_SHARE_CONNECT = 6;

    private static final int HANDLE_UPDATE_STATUS_AND_CARID = 0;
    private static final int HANDLE_GET_MESSAGE = 1;
    private static final int HANDLE_SET_CLIENT_ID = 2;
    private static final int HANDLE_UPDATE_STATUS_ONLY = 3;
    private static final int HANDLE_UPDATE_STATUS_ONLY_AUTO = 4;
    private static final int HANDLE_TOKEN = 5;
    private static final int HANDLE_CAR_INFO = 6;
    private static final int HANDLE_LOGIN_RETRY_AUTO = 7;
    private static final int HANDLE_STOP_REFRESHING_DATA = 8;
    private static final int HANDLE_CHANGE_CAR = 9;
    private static final int HANDLE_LOG_OUT = 10;
    private static final int HANDLE_HEARTBEAT = 11;

    private static final int TAG_GET_POSITION_DATA = 1;
    private static final int TAG_GET_CAR_DATA = 0;
    private static final int TAG_SET_CLIENT_ID = 2;
    private static final int TAG_GET_MESSAGE = 3;
    private static final int TAG_TOKEN = 4;
    private static final int TAG_CAR_INFO = 5;
    private static final int TAG_LOG_IN_RETRY = 6;
    private static final int TAG_ONLINE = 7;
    private static final int TAG_HEARTBEAT = 8;

    private static final int FLAG_HOME = 0;
    private static final int FLAG_DETECT = 1;
    private static final int FLAG_SERVICE = 2;
    private static final int FLAG_MORE = 3;

    // 以下三个 蓝牙相关
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;

    private static final int TAG_NOTIFICATION_UPDATE = 0;
    public static boolean isAlive = false;
    private final String TAG = "asd";
    @Bind(R.id.viewpager)
    ViewPager mViewPager;
    @Bind(R.id.id_indicator_home)
    TabChanger tabHome;
    @Bind(R.id.id_indicator_detect)
    TabChanger tabDetect;
    @Bind(R.id.id_indicator_service)
    TabChanger tabService;
    @Bind(R.id.id_indicator_more)
    TabChanger tabMore;
    NotificationCompat.Builder notificationBuilder;
    Map<String, String> positionRequestParams;
    Map loginParams;
    @Bind(R.id.image_bind_menu)
    ImageView imageBindMenu;
    private LocationClient mLocationClient = null;
    private List<Fragment> mFragments = new ArrayList<>();
    private MainPageAdapter mAdapter;
    private List<TabChanger> mTabs = new ArrayList<>();
    private int tabFlag = 0;
    private Handler mHandler;
    private boolean isLowBatteryShowed = false;
    private boolean needUpdateDialogShow = false;
    private String updateDesc = "";
    private UpdateManager updateManager;
    private NotificationManager notificationManager;
    private BindingManageDialogFragment mBindingDialog;
    private AddBindDialog mA2AddBindingDialog;
    /**
     * 蓝牙相关
     */
    private PacketParserService mParserService;
    private boolean successed = false;//暂时用于处理自动设防撤防
    private BluetoothAdapter mBtAdapter;
    private byte[] receiveData = null;
    private byte[] head = new byte[8];
    private byte[] temp1 = null;
    private UartService mService;
    private int mState = UART_PROFILE_DISCONNECTED;
    //    private AutoDefenceThread autoDefenceThread;
    private ReadDataThread readDataThread;
    private MediaPlayer mediaPlayer;
    private CustomProgressDialog mAlarmDialog;

    private AddBindDialog.AddBindListener mAddBindListener = new AddBindDialog.AddBindListener() {
        @Override
        public void onAddBind() {
            LocalBroadcastManager.getInstance(MainActivity.this).
                    sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_BIND_CHANGE));
        }
    };
    private BDLocationListener myListener = new BDLocationListener() {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            if (!isRunning && application == null) {
                if (mLocationClient != null) {
                    mLocationClient.stop();
                }
                return;
            }
            if (bdLocation == null) {
                return;
            }
            if (bdLocation.getAddrStr() != null) {
                application.myPosition = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());
                application.myPositionDesc = bdLocation.getAddrStr();
                application.myPositionRadius = bdLocation.getRadius();
            }
        }
    };
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            resetTabsAlpha();

            switch (v.getId()) {
                case R.id.id_indicator_home:
//                    mTabs.get(FLAG_HOME).setIconAlpha(1);
                    mTabs.get(FLAG_HOME).setBackgroundResource(R.drawable.tab_background);
                    mViewPager.setCurrentItem(FLAG_HOME, false);
                    break;
                case R.id.id_indicator_detect:
                    mTabs.get(FLAG_DETECT).setBackgroundResource(R.drawable.tab_background);
                    mViewPager.setCurrentItem(FLAG_DETECT, false);
                    break;
                case R.id.id_indicator_service:
                    mTabs.get(FLAG_SERVICE).setBackgroundResource(R.drawable.tab_background);
                    mViewPager.setCurrentItem(FLAG_SERVICE, false);
                    break;
                case R.id.id_indicator_more:
                    mTabs.get(FLAG_MORE).setBackgroundResource(R.drawable.tab_background);
                    mViewPager.setCurrentItem(FLAG_MORE, false);
                    break;
            }
        }
    };
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isRunning) {
                return;
            }
            String action = intent.getAction();
            Log.d("asd", "onReceived: " + action);
            if (action.equals(TbitProtocol.BROADCAST_RECEIVER_LOGGED_IN)) {
                application.setLoginStatus(TbitProtocol.LOGGED_IN);
                application.connectionFailTime = 0;
                mHandler.sendEmptyMessage(HANDLE_UPDATE_STATUS_ONLY_AUTO);
                mHandler.sendEmptyMessage(HANDLE_GET_MESSAGE);
                mHandler.sendEmptyMessage(HANDLE_SET_CLIENT_ID);
                mHandler.sendEmptyMessage(HANDLE_TOKEN);
                mHandler.sendEmptyMessage(HANDLE_CAR_INFO);
                mHandler.sendEmptyMessage(HANDLE_HEARTBEAT);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_LOGGED_OUT)) {
                mHandler.sendEmptyMessage(HANDLE_LOG_OUT);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_UPDATE_STATUS)) {
                mHandler.sendEmptyMessage(HANDLE_UPDATE_STATUS_ONLY);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_ALL_UNBOUNDED)) {
                mHandler.sendEmptyMessage(HANDLE_STOP_REFRESHING_DATA);// 相当于退出
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_CHANGE_CAR)) {
                mHandler.sendEmptyMessage(HANDLE_CHANGE_CAR);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_LOG_IN_RETRY)) {
                mHandler.sendEmptyMessage(HANDLE_LOGIN_RETRY_AUTO);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_BIND_CHANGE)) {
                mHandler.sendEmptyMessage(HANDLE_UPDATE_STATUS_AND_CARID);
            } else if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                if (readDataThread != null) {
                    readDataThread.setWait();
                }
//                BluetoothControlFragment.isStateRefreshNeeded = true;
//                BluetoothControlFragment.isUIRefreshNeeded = true;
                mService.setNeedReconnect(true);
                mState = UART_PROFILE_CONNECTED;
            } else if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                mState = UART_PROFILE_DISCONNECTED;
                mService.reConnect();
                showToast("OH NO！连接断开，正尝试重新建立连接...");
//                if (reconnectCount < 0) {
//                    showToast("尝试连接失败，请重新选择设备。");
//                    mService.disconnect();
//                }
            } else if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
                //发送连接指令
                try {
                    mParserService.getConnect(mService.getDeviceTid());
                } catch (Exception e) {
                    showToast("请选择泰比特A1设备进行连接!");
                }
            } else if (action.equals(UartService.ACTION_DATA_AVAILABLE) ||
                    action.equals(UartService.ACTION_NOTIFICATION)) {
                final byte[] rxValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                temp1 = ByteUtil.byteMerger(temp1, rxValue);//拼接缓存
                Log.d(TAG, "onReceive: " + temp1);
            } else if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                showToast("请正确连接泰比特A1设备");
                EventBus.getDefault().post(new Event.BleConnectFail());
                mService.disconnect();
            } else if (action.equals(Constant.ACTION_SEND_FAIL)) {
                Log.i(TAG, "-->>下发指令失败，提示用户失败");
                showToast("指令发送失败！");
            } else if (action.equals(Constant.ACTION_OTA_UPDATE)) {
                //启动服务，检测是否下载，需要下载，下载，下载完成自动发送文件，不需要下载，提示用户固件版本为最新
            } else if (action.equals(Constant.ACTION_DEVICE_CONN_PASS)) {
                showToast("连接成功");
                EventBus.getDefault().post(new Event.BleConnectSucc());
                //将最近一次连接的设备保存在本地，方便下一次直接连接
                SharePreferenceUtil.getInstance().saveData(Constant.SP_RECENTDEVICE, application.deviceAddr);
//                if (autoDefenceThread == null) {
//                    autoDefenceThread = new AutoDefenceThread();
//                    autoDefenceThread.start();
//                }
            } else if (action.equals(Constant.ACTION_DEVICE_CONN_FAIL)) {
                showToast("连接失败");
                EventBus.getDefault().post(new Event.BleConnectFail());
            } else if (action.equals(Constant.ACTION_WRITE_OK)) {
                if (mParserService.mSendThread != null) {
                    mParserService.mSendThread.updateStatus(true);
                }
            } else if (action.equals(Constant.ACTION_ALARM)) {
                String alarm = intent.getStringExtra(Constant.EXTRA_DATA);
                AlarmDialogShow(alarm);
            } else if (action.equals(TbitProtocol.BROADCAST_RECEIVER_BLUETOOTH_DISCONNECT)) {
                if (mService != null)
                    mService.disconnect();
            }
        }
    };
    private boolean isLoginRetryStarted = false;
    private int downloadProgress = 1;
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mParserService = ((PacketParserService.MyBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mParserService = null;
        }
    };
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            if (!mService.initialize()) {
                finish();
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
            mService = null;
        }
    };
    private String mShareConnectCode;

    private void createHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                super.handleMessage(msg);
                if (!isRunning) {
                    return;
                }
                switch (msg.what) {
                    case HANDLE_UPDATE_STATUS_AND_CARID:
                        if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                            getAllBinds();
                        }
                        break;
                    case HANDLE_GET_MESSAGE:
                        if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                            postNetworkJson(TAG_GET_MESSAGE, NetworkProtocol.URL_GET_MESSAGE, null, false);
                            postNetworkJson(TAG_ONLINE, NetworkProtocol.URL_ONLINE, null, false);
                            sendEmptyMessageDelayed(HANDLE_GET_MESSAGE, 10 * 1000);
                        }
                        break;
                    case HANDLE_SET_CLIENT_ID:
                        boundClientId();
                        break;
                    case HANDLE_UPDATE_STATUS_ONLY:
                        updateStatus();
                        break;
                    case HANDLE_UPDATE_STATUS_ONLY_AUTO:
                        updateStatus();
                        sendEmptyMessageDelayed(HANDLE_UPDATE_STATUS_ONLY_AUTO, 10 * 1000);
                        break;
                    case HANDLE_TOKEN:
                        getToken();
                        break;
                    case HANDLE_CAR_INFO:
                        getCarInfo();
                        break;
                    case HANDLE_LOGIN_RETRY_AUTO:
                        loginRetry();
                        sendEmptyMessageDelayed(HANDLE_LOGIN_RETRY_AUTO, 10 * 1000);
                        break;
                    case HANDLE_STOP_REFRESHING_DATA:
                        stopRefreshing();
                        break;
                    case HANDLE_CHANGE_CAR:
                        if (application.getCurCar().getType() == TbitProtocol.TYPE_A2) {
                            Fragment fragment = getFragment(FLAG_HOME);
                            if (fragment instanceof HomeFragment) {
                                ((HomeFragment) fragment).updateStatusBar();
                            }
                        }
                        mHandler.sendEmptyMessage(HANDLE_CAR_INFO);
                        mHandler.sendEmptyMessage(HANDLE_UPDATE_STATUS_ONLY);
                        // TODO: 2016/4/23 0023 要根据是否是蓝牙设备操作
                        detectHideResult();
                        break;
                    case HANDLE_LOG_OUT:
                        stopRefreshing();
                        mHandler.removeMessages(HANDLE_HEARTBEAT);
                        MyApplication.getInstance().setLoginStatus(TbitProtocol.LOGGED_OUT);
                        showToast("已退出当前账户");
                        break;
                    case HANDLE_HEARTBEAT:
                        postNetworkJson(TAG_HEARTBEAT, NetworkProtocol.URL_HEATBEAT, null,
                                false);
                        sendEmptyMessageDelayed(HANDLE_HEARTBEAT, 60 * 1000);
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ButterKnife.bind(this);

        createHandler();

        isAlive = true;

        LocalBroadcastManager.getInstance(this).
                registerReceiver(mReceiver, makeIntentFilter());
        EventBus.getDefault().register(this);

        initView();

        initFragment();

        initLocation();

        initBluetoothRelevant();

        mLocationClient.start();

        updateManager = new UpdateManager(this, this);
        updateManager.checkUpdate();

        if (!application.isNetworkConnected() && application.hasA1Device) {
            showDialogWhenNoNetwork();
        }
    }

    private void initBluetoothRelevant() {
        service_init();
        initMedia();
        initAlarmDialog();
        initBLE();

        if (readDataThread == null) {
            readDataThread = new ReadDataThread();
            readDataThread.start();
        }

        application.hasA1Device = application._preferences.getBoolean(Constant.SP_HAS_A1_DEVICE, false);
    }

    private void initView() {
        mTabs.add(tabHome);
        mTabs.add(tabDetect);
        mTabs.add(tabService);
        mTabs.add(tabMore);

        mBindingDialog = new BindingManageDialogFragment();
        mBindingDialog.setCancelable(false);
        mBindingDialog.setBindListener(this);
        imageBindMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                    if (application.hasAnyCarBound()) {
                        if (mBindingDialog != null && !mBindingDialog.isAdded()) {
                            mBindingDialog.show(getSupportFragmentManager(), "");
                        }
                    } else {
                        onAddBind(new Event.AddBindEvent());
                    }
                } else {
                    enterActivity(LoginActivity.class);
                }
            }
        });

        mA2AddBindingDialog = new AddBindDialog();
        mA2AddBindingDialog.setAddBindListener(mAddBindListener);

        tabHome.setOnClickListener(listener);
        tabDetect.setOnClickListener(listener);
        tabService.setOnClickListener(listener);
        tabMore.setOnClickListener(listener);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case TAG_GET_POSITION_DATA:
                if (jsonObject.isRes()) {
                    List<CarStatus> temp = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                    }.getType());
                    if (temp == null || temp.size() == 0) {
                        break;
                    }
                    for (CarStatus status : temp) {
                        application.setGlobal(status.getCarId(), TbitProtocol.KEY_GLOBAL_STATUS, status);
                    }
                    CarStatus status = null;
                    try {
                        status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!isLowBatteryShowed && application._preferences.getBoolean(TbitProtocol.SP_LOW_POWER_WARN, true)
                            && status != null && isLowBattery(status.getStrGGPV())) {
                        showLowBatteryWarning();
                        isLowBatteryShowed = true;
                    }
                    Fragment fragment = getFragment(FLAG_HOME);
                    // TODO: 2016/4/23 0023 要根据是否是蓝牙设备操作
                    if (fragment instanceof HomeFragment) {
                        ((HomeFragment) fragment).updateStatusBar();
                    }
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
            case TAG_GET_CAR_DATA:
                if (jsonObject.isRes()) {
                    CarData carData = gson.fromJson(jsonObject.getResult(), CarData.class);
                    List<CarData.CarsEntity> temp = carData.getCars();
                    if (temp == null && temp.size() != 0) {
                        return;
                    }
                    application.a2Cars.clear();
                    application.a2Cars.addAll(temp);
                    updateStatus();
                } else {
                    showCrouton(jsonObject.getDesc());
                }
                break;
            case TAG_SET_CLIENT_ID:
                if (jsonObject.isRes()) {
                    Log.d("LOG", jsonObject.toString());
                }
                break;
            case TAG_GET_MESSAGE:
                if (jsonObject.isRes()) {
                    Message message = gson.fromJson(jsonObject.getResult(), Message.class);
                    if (message.getRespCommands() == null || message.getRespCommands().size() == 0) {
                        break;
                    }
                    StringBuilder sb = new StringBuilder();
                    boolean needLineFeed = false;
                    for (String s : message.getRespCommands()) {
                        if (needLineFeed) {
                            sb.append("\n");
                        }
                        sb.append(s);
                        needLineFeed = true;
                    }
                    showLongToast(sb.toString());
                }
                break;
            case TAG_TOKEN:
                if (jsonObject.isRes()) {
                    application.common_token = jsonObject.getResult();
                }
                break;
            case TAG_CAR_INFO:
                if (jsonObject.isRes()) {
                    CarInfo info = gson.fromJson(jsonObject.getResult(), CarInfo.class);
                    application.setCurGlobal(TbitProtocol.KEY_GLOBAL_CAR_INFO, info);
                }
                break;
            case TAG_LOG_IN_RETRY:
                mHandler.removeMessages(HANDLE_LOGIN_RETRY_AUTO);
                break;
            case TAG_ONLINE:
                if (jsonObject.isRes()) {
                    List<Online> onlines = gson.fromJson(jsonObject.getResult(), new TypeToken<List<Online>>() {
                    }.getType());
                    if (onlines == null || onlines.size() == 0) {
                        break;
                    }
                    for (Online online : onlines) {
                        application.setGlobal(online.getCarId(), TbitProtocol.KEY_GLOBAL_ONLINE, online);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void getAllBinds() {
        Map<String, String> map = new HashMap<>();
        postNetworkJson(TAG_GET_CAR_DATA, NetworkProtocol.URL_GET_CAR_DATA, map, false);
    }

    public void updateStatus() {
        if (DataUtil.getEntityByCarId(application.getCurCar().getCarId()) == null) {
            return;
        }
        if (positionRequestParams == null) {
            positionRequestParams = new ConcurrentHashMap<>();
        } else {
            positionRequestParams.clear();
        }
        positionRequestParams.put(NetworkProtocol.PARAMKEY_CARID, getCarIdString());
        postNetworkJson(TAG_GET_POSITION_DATA, NetworkProtocol.URL_POSITION, positionRequestParams, false);
    }

    /**
     * 初始化各个fragment
     */
    private void initFragment() {

        Fragment f1;
        Fragment f2;
        Fragment serviceFragment = new ServiceFragment();
        Fragment moreFragment = new MoreFragment();

        // 最后一次切换的状态
        int type = application.getCurCar().getType();
        if (type == TbitProtocol.TYPE_A2) {
            f1 = new HomeFragment();
            f2 = new DetectFragment();
        } else {
            f1 = new BluetoothHomeFragment();
            f2 = new BluetoothControlFragment();
        }

        mFragments.add(f1);
        mFragments.add(f2);
        mFragments.add(serviceFragment);
        mFragments.add(moreFragment);

        mAdapter = new MainPageAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        };

        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(4);

        //viewpager滑动事件监听器
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.d("MainActivity","position: "+position+" positionOffset: "+positionOffset);
                /*mTabs.get(position).setIconAlpha(1 - positionOffset);
                if (position < mTabs.size() - 1) {
                    mTabs.get(position + 1).setIconAlpha(positionOffset);
                }*/
            }

            @Override
            public void onPageSelected(int position) {
                resetTabsAlpha();
                mTabs.get(position).setBackgroundResource(R.drawable.tab_background);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
               /* if (state == 2 || state == 0) {
                    for (int i=0; i<mTabs.size(); i++) {
                        if (i != tabFlag) {
                            mTabs.get(i).setIconAlpha(0);
                        }
                    }
                }*/
            }
        });

        mTabs.get(0).setBackgroundResource(R.drawable.tab_background);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //此处重写为空，不可删除
//        super.onSaveInstanceState(outState);
    }

    /**
     * 恢复tab按钮颜色
     */
    private void resetTabsAlpha() {
        for (int i = 0; i < mTabs.size(); i++) {
            mTabs.get(i).setBackgroundColor(0xff404754);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void finish() {
        if (mReceiver != null) {
            LocalBroadcastManager.getInstance(this).
                    unregisterReceiver(mReceiver);
        }
        if (mLocationClient != null && mLocationClient.isStarted()) {
            mLocationClient.stop();
        }
        if (mService != null) {
            mService.close();
            unbindService(mServiceConnection);
            mService.stopSelf();
        }
        if (mParserService != null) {
            unbindService(conn);
            mParserService.stopSelf();
        }
        EventBus.getDefault().unregister(this);

        isAlive = false;
        super.finish();
    }

    @Override
    public void onBackPressed() {
        showQuidDialog();
    }

    public void showQuidDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage(R.string.dialog_quit_confirm)
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(DIALOG_REQUEST_QUIT)
                .setNegativeButtonText(R.string.cancel)
                .setTitle(R.string.quit).show();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_CHOOSE_BIND_TYPE:
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_CHOOSE_BIND_TYPE:
                break;
        }
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case DIALOG_REQUEST_QUIT:
                application.setLoginStatus(TbitProtocol.LOGGED_OUT);
                mHandler.sendEmptyMessage(HANDLE_LOG_OUT);
                finish();
                break;
            case DIALOG_REQUEST_UPDATE:
                if (TbitUtil.isWIFI()) {
                    // TODO: 2016/3/12 下载实现
                    showLongToast("后台下载中...");
                    updateManager.download();
                } else {
                    SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                            .setTitle(R.string.update)
                            .setMessage("未连接无线网络，下载将可能产生流量费用，是否继续")
                            .setPositiveButtonText(R.string.confirm)
                            .setNegativeButtonText(R.string.cancel)
                            .setRequestCode(DIALOG_REQUEST_NOT_WIFI)
                            .show();
                }
                break;
            case DIALOG_REQUEST_NOT_WIFI:
                // TODO: 2016/3/12 下载实现
                showLongToast("后台下载中...");
                updateManager.download();
                break;
            case DIALOG_REQUEST_NO_NETWORK:
                onOfflineModeActivated(new Event.OfflineMode());
                break;
            case DIALOG_REQUEST_CHOOSE_BIND_TYPE:
                showA2BindingDialog();
                break;
            case DIALOG_REQUEST_SHARE_CONNECT:
                shareConnect();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // TODO: 2016/4/23 0023 要根据是否是蓝牙设备操作
        if (application.getLoginStatus() == TbitProtocol.LOGGED_IN && application.hasA2Bound()) {
            Fragment fragment = getFragment(FLAG_HOME);
            if (fragment instanceof HomeFragment) {
                ((HomeFragment) fragment).updateStatusBar();
            }
        }
        if (needUpdateDialogShow) {
            showUpdateDialog(updateDesc);
        }

        readClipBoard();
    }

    private void initLogoutMode() {
        // TODO: 2016/4/23 0023 要根据是否是蓝牙设备操作
        Fragment fragment = getFragment(FLAG_HOME);
        if (fragment instanceof HomeFragment) {
            ((HomeFragment) fragment).setLogoutStatus();
        }
        detectHideResult();
    }

    private void detectHideResult() {
        Fragment fragment = getFragment(FLAG_DETECT);
        if (fragment instanceof DetectFragment) {
            ((DetectFragment) fragment).hideResult();
        }
    }

    private IntentFilter makeIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_LOGGED_IN);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_LOGGED_OUT);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_UPDATE_STATUS);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_ALL_UNBOUNDED);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_CHANGE_CAR);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_LOG_IN_RETRY);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_BIND_CHANGE);
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        intentFilter.addAction(Constant.ACTION_ALARM);
        intentFilter.addAction(Constant.ACTION_SEND_FAIL);
        intentFilter.addAction(Constant.ACTION_WRITE_OK);
        intentFilter.addAction(Constant.ACTION_OTA_UPDATE);
        intentFilter.addAction(Constant.ACTION_DEVICE_CONN_PASS);
        intentFilter.addAction(Constant.ACTION_DEVICE_CONN_FAIL);
        intentFilter.addAction(TbitProtocol.BROADCAST_RECEIVER_BLUETOOTH_DISCONNECT);

        return intentFilter;
    }

    private void boundClientId() {
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CLIENTTYPE, String.valueOf(0));
        map.put(NetworkProtocol.PARAMKEY_CLIENTID, application.clientId);
        postNetworkJson(TAG_SET_CLIENT_ID, NetworkProtocol.URL_PUSH_BOUND, map, false);
    }

    private void getToken() {
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_APPID, TbitProtocol.VALUE_TBIT_COMMON_APPID);
        map.put(NetworkProtocol.PARAMKEY_SECRET, TbitProtocol.VALUE_TBIT_COMMON_SECRET);
        postNetworkJsonCommon(TAG_TOKEN, NetworkProtocol.URL_TOKEN, map, false);
    }

    private void getCarInfo() {
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, String.valueOf(application.getCurCar().getCarId()));
        postNetworkJson(TAG_CAR_INFO, NetworkProtocol.URL_CAR_INFO, map, false);
    }

    private void loginRetry() {
        String phone = (String) application.getNobeloning(TbitProtocol.KEY_NOBELONING_PHONE);
        String password = (String) application.getNobeloning(TbitProtocol.KEY_NOBELONING_PASSWORD);
        if (phone == null && password == null) {
            return;
        }
        if (loginParams == null) {
            loginParams = new HashMap();
        }
        loginParams.put(NetworkProtocol.PARAMKEY_PHONE, phone);
        loginParams.put(NetworkProtocol.PARAMKEY_PASSWORD, password);
        postNetWorkForLogin(TAG_LOG_IN_RETRY, NetworkProtocol.URL_LOGIN, loginParams, false);
    }

    private void initLocation() {
        mLocationClient = new LocationClient(getApplicationContext());
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);// 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");// 可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
        option.setScanSpan(LOCATION_SCAN_DURATION_LONG);// 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);// 可选，设置是否需要地址信息，默认不需要
        option.setIsNeedLocationDescribe(true);// 可选，设置是否需要地址描述
        mLocationClient.setLocOption(option);
        mLocationClient.registerLocationListener(myListener);
    }

    private void showLowBatteryWarning() {
        if (visible) {
            SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                    .setMessage(R.string.dialog_low_battery_warning)
                    .setPositiveButtonText(R.string.confirm)
                    .setRequestCode(DIALOG_REQUEST_LOW_POWER)
                    .setTitle(R.string.battery).show();
        }
    }

    private boolean isLowBattery(String s) {
        try {
            String[] temp = s.split("=");
            temp = temp[1].split(",");

            int result = Integer.parseInt(temp[0]);
            return result < 2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onFailure(int tag) {
        if (!isRunning) {
            return;
        }
        application.connectionFailTime++;
        showProgressDialog(false);
        // 每连接失败十次尝试重新登录一次
        if (!isLoginRetryStarted && application.connectionFailTime > 90) {
            isLoginRetryStarted = true;
            LocalBroadcastManager.getInstance(this).
                    sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_LOG_IN_RETRY));
        }
    }

    @Override
    public void onNewVersionFound(boolean hasNewVersion, String describe) {
        if (hasNewVersion) {
            if (visible) {
                showUpdateDialog(describe);
            } else {
                needUpdateDialogShow = true;
                updateDesc = describe;
            }
        }
    }

    @Override
    public void onDownloading(int progress) {
        notifyDownloadProgress(progress);
    }

    @Override
    public void onDownloadFinished(String fileUrl) {
        File apkfile = new File(fileUrl);
        if (!apkfile.exists()) {
            return;
        }
        // 通过Intent安装APK文件
        Intent i = new Intent(Intent.ACTION_VIEW);
        // 安装完成后提示是否打开app
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setDataAndType(Uri.parse("file://" + apkfile.toString()),
                "application/vnd.android.package-archive");
        startActivity(i);
    }

    @Override
    public void onDownloadFailed() {
        cancelUpdateNotification();
        showToast("下载失败");
    }

    private void showUpdateDialog(String desc) {
        SimpleDialogFragment.createBuilder(MainActivity.this, getSupportFragmentManager())
                .setTitle(R.string.update)
                .setMessage(desc)
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_UPDATE)
                .setCancelable(false)
                .show();
        needUpdateDialogShow = false;
    }

    private void initDownloadNotification() {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_yadea)
                .setContentTitle("雅迪管家更新下载中")
                .setTicker("雅迪管家更新下载中...");
    }

    private void notifyDownloadProgress(int progress) {
        if (progress % 5 == 0 && progress != downloadProgress) {
            if (notificationManager == null || notificationBuilder == null) {
                initDownloadNotification();
            }
            downloadProgress = progress;
            if (downloadProgress == 100) {
                notificationManager.cancel(TAG_NOTIFICATION_UPDATE);
                return;
            }
            notificationBuilder.setProgress(100, downloadProgress, false);
            notificationManager.notify(TAG_NOTIFICATION_UPDATE, notificationBuilder.build());
        }
    }

    private void cancelUpdateNotification() {
        if (notificationManager != null) {
            notificationManager.cancel(TAG_NOTIFICATION_UPDATE);
        }
    }

    private String getCarIdString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if (application.a2Cars != null && application.a2Cars.size() != 0) {
            boolean needComas = false;
            for (CarData.CarsEntity carId : application.a2Cars) {
                if (needComas) {
                    sb.append(",");
                }
                sb.append(carId.getCarId());
                needComas = true;
            }
        }
        return sb.toString();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        if (action.equals(TbitProtocol.ACTION_NOTHING_BIND)) {
            showChooseBindDialog();
        }
    }

    @Override
    public void onBindChange(Car car) {
        if (car.getCarId().equals(application.getCurCar().getCarId())) {
            return;
        }
        boolean isBluetooth = car.getType() == TbitProtocol.TYPE_A1;
        application.setCurCar(car.getCarId(), car.getType());
        if (isBluetooth) {
            // 如果切换至其他A1设备，断开当前连接
            EventBus.getDefault().post(new Event.BleUIUpdate());
            if (mService.getConnectionState() == UartService.STATE_CONNECTED &&
                    car.getCarId().equals(mService.getDeviceTid())) {
                mService.disconnect();
            }
            String machineNo = DataUtil.getMachineNoByCarId(application.getCurCar().getCarId());
            showLongToast("已选择车辆" + machineNo);
        } else
            showLongToast("已选择车辆" + application.getCurCar().getCarId());
    }

    private void toBluetoothMode() {
        Fragment f1 = new BluetoothHomeFragment();
        Fragment f2 = new BluetoothControlFragment();
        mAdapter.replaceFragment(FLAG_HOME, f1);
        mAdapter.replaceFragment(FLAG_DETECT, f2);
        mAdapter.notifyDataSetChanged();
    }

    private void toNormalMode() {
        Fragment f1 = new HomeFragment();
        Fragment f2 = new DetectFragment();
        mAdapter.replaceFragment(FLAG_HOME, f1);
        mAdapter.replaceFragment(FLAG_DETECT, f2);
        mAdapter.notifyDataSetChanged();
    }

    private void showDialogWhenNoNetwork() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("无网络")
                .setMessage("当前无网络，是否切换至蓝牙模式直接控制A1设备")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_NO_NETWORK)
                .setCancelable(false)
                .show();
    }

    private void showChooseBindDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage("请选择要绑定的设备类型")
                .setPositiveButtonText("A2")
                .setNegativeButtonText("A1")
                .setNeutralButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_CHOOSE_BIND_TYPE)
                .setCancelable(false)
                .show();
    }

    private void showA2BindingDialog() {
        if (!mA2AddBindingDialog.isAdded()) {
            mA2AddBindingDialog = new AddBindDialog();
            mA2AddBindingDialog.setAddBindListener(mAddBindListener);
            mA2AddBindingDialog.show(getSupportFragmentManager(), null);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddBind(Event.AddBindEvent event) {
        showChooseBindDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeDevice(Event.ChangeDevice event) {
        Car car = event.car;
        if (application.getCurCar().getType() != car.getType()) {
            if (car.getType() == TbitProtocol.TYPE_A2) {
                toNormalMode();
            } else {
                toBluetoothMode();
            }
        }
        if (!car.getCarId().equals(application.getCurCar().getCarId())) {
            application.getCurCar().setCarId(car.getCarId());
            application.getCurCar().setType(car.getType());
            application._preferences.edit().putString(TbitProtocol.SP_LOW_LAST_CHOSEN_MACHINE,
                    car.getCarId()).apply();
            application._preferences.edit().putInt(TbitProtocol.SP_LOW_LAST_CHOSEN_TYPE,
                    car.getType()).apply();
            CarData.CarsEntity e = DataUtil.getEntityByCarId(application.getCurCar().getCarId());
            if (e != null) {
                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_CHANGE_CAR));
            } else {
                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_BIND_CHANGE));
            }
        } else {
            application.getCurCar().setCarId(car.getCarId());
            application.getCurCar().setType(car.getType());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOfflineModeActivated(Event.OfflineMode event) {
        application.isOfflineMode = true;
        application.getCurCar().setType(TbitProtocol.TYPE_A1);
        toBluetoothMode();
    }

    private Fragment getFragment(int flag) {
        return getSupportFragmentManager().findFragmentByTag(MainPageAdapter.
                makeFragmentName(mViewPager.getId(), flag));
    }

    @Override
    public PacketParserService getPacketParserService() {
        return mParserService;
    }

    @Override
    public UartService getUartService() {
        return mService;
    }

    /**
     * 弹出告警提示并播放告警声音
     *
     * @param alarm
     */
    private void AlarmDialogShow(String alarm) {
        mAlarmDialog.show();
        mAlarmDialog.setContent(alarm);
        if (mediaPlayer == null) {
            initMedia();
        }
        mediaPlayer.start();
    }

    /**
     * 初始化播放媒体
     */
    private void initMedia() {
        mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                    mp.start();
                    mp.setLooping(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Ensures Bluetooth is available on the device and it is enabled. If not,
     * displays a dialog requesting user permission to enable Bluetooth.
     */
    private void initBLE() {
        mBtAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if (mBtAdapter == null || !mBtAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        String addr = (String) SharePreferenceUtil.getInstance().getData(Constant.SP_RECENTDEVICE, "");
        if (!TextUtils.isEmpty(addr)) {
        }
    }

    private void initAlarmDialog() {
        mAlarmDialog = new CustomProgressDialog(this, "告警信息", R.drawable.frame_alarm);
        mAlarmDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //停止并释放资源
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        });
    }

    private void service_init() {
        Intent bindIntent = new Intent(this, PacketParserService.class);
        startService(bindIntent);
        boolean flag = bindService(bindIntent, conn, Context.BIND_AUTO_CREATE);

        Intent bindIntent2 = new Intent(this, UartService.class);
        startService(bindIntent);
        boolean flag2 = bindService(bindIntent2, mServiceConnection, Context.BIND_AUTO_CREATE);

        if (flag && flag2) {

        }
    }

    /**
     * 控制自动模式下，设防撤防线程
     */
//    class AutoDefenceThread extends Thread {
//        boolean auto = false;
//        int localRssi = 0;
//        int realTimeRssi = 0;
//        boolean guard = false;
//
//        @Override
//        public void run() {
//            super.run();
//            while (isRunning) {
//                Log.d(TAG, "run: AutoDefenceThread");
//                //判断当前模式是否是自动撤防设防模式
//                auto = (boolean) SharePreferenceUtil.getInstance().getData(Constant.SP_MODE_LOCK, false);
//                localRssi = (int) SharePreferenceUtil.getInstance().getData(Constant.SP_RSSI, 0);
//                guard = (boolean) SharePreferenceUtil.getInstance().getData(Constant.SP_GUARD, false);
//                //是
//                if (auto) {
//                    //获取用户所设置的rssi值与APP读取的rssi值的平均值作比较，大于设防，小于，撤防
//                    //这里还需控制一下循环
//                    try {
//                        realTimeRssi = mService.getRssiAvg();
//                        if (localRssi > realTimeRssi) {
//                            if (!guard) {
//                                do {
//                                    //设防指令
//                                    BluetoothControlFragment.isStateRefreshNeeded = true;
//                                    mParserService.send(Constant.REQUEST_IGNORE, Constant.COMMAND_SETTING, Constant.SEND_KEY_DEFENCE,
//                                            new Byte[]{Constant.VALUE_ON});
//                                    SystemClock.sleep(3000);
//                                } while ((!(boolean) SharePreferenceUtil.getInstance().getData(Constant.SP_GUARD, false)) &&
//                                            getUartService().getGatt() != null);
//                            }
//                        } else {
//                            if (guard) {
//                                do {
//                                    //撤防指令
//                                    BluetoothControlFragment.isStateRefreshNeeded = true;
//                                    mParserService.send(Constant.REQUEST_IGNORE, Constant.COMMAND_SETTING, Constant.SEND_KEY_DEFENCE,
//                                            new Byte[]{Constant.VALUE_OFF});
//                                    SystemClock.sleep(3000);
//                                } while ((boolean) SharePreferenceUtil.getInstance().getData(Constant.SP_GUARD, false));
//                                SharePreferenceUtil.getInstance().saveData(Constant.SP_GUARD, false &&
//                                        getUartService().getGatt() != null);
//                            }
//
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                //否，边儿玩去~
//                SystemClock.sleep(2000);
//            }
//        }
//    }
    private void stopRefreshing() {
        isLowBatteryShowed = false;
        mHandler.removeMessages(HANDLE_GET_MESSAGE);
        mHandler.removeMessages(HANDLE_UPDATE_STATUS_ONLY_AUTO);
        mHandler.removeMessages(HANDLE_UPDATE_STATUS_ONLY);
        initLogoutMode();
    }

    private void readClipBoard() {
        ClipboardManager myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData abc = myClipboard.getPrimaryClip();
        if (abc == null)
            return;
        ClipData.Item item = abc.getItemAt(0);
        String text = item.getText().toString();
        if (text.length() > 100)
            return;
        if (!text.contains("智能管家"))
            return;
        String regEx = "#\\d{10}#";
        Pattern pattern = Pattern.compile(regEx);
        Matcher m = pattern.matcher(text);
        if (!m.find())
            return;
        String res = m.group(0);
        String code = res.substring(1, res.length() - 1);
        if (code.equals(application.shareSecretCreateByLocal))
            return;
        if (code.equals(mShareConnectCode))
            return;
        mShareConnectCode = code;
        showA1ShareDialog();
    }

    private void showA1ShareDialog() {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("分享")
                .setMessage("检测到您通过分享获得连接密码，是否尝试连接蓝牙设备")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setRequestCode(DIALOG_REQUEST_SHARE_CONNECT)
                .setCancelable(false)
                .show();
    }

    private void shareConnect() {
        ShareResult result = ShareCodeGenerator.resoleCode(mShareConnectCode);
        if (result.getStatus() == ShareCodeGenerator.CODE_ERR_ILLEGAL) {
            showToast("您的分享密码有误，请联系分享者");
        } else if (result.getStatus() == ShareCodeGenerator.CODE_ERR_EXPIRED) {
            showToast("您的分享密码已过期，请联系分享者");
        } else if (result.getStatus() == ShareCodeGenerator.CODE_OK) {
            application.getCurCar().setType(TbitProtocol.TYPE_A1);
            toBluetoothMode();
            Intent intent = new Intent(MainActivity.this, ScanActivity.class);
            intent.putExtra(ScanActivity.BUNDLE_KEY_SHARE_CONNECT_TID, result.getTid());
            startActivity(intent);
        }
    }

    class ReadDataThread extends Thread {
        boolean wait = true;
        boolean wait2 = true;

        public void setWait() {
            wait = true;
            wait2 = true;
        }

        @Override
        public void run() {
            super.run();
            while (isRunning) {
                try {
//                    Log.d(TAG, "run: reading data...");
                    if (temp1 != null && temp1.length != 0) {
                        Log.d(TAG, "temp1: " + ByteUtil.bytesToHexString(temp1));
                        StringBuilder builder = new StringBuilder();
                        for (byte b : temp1) {
                            builder.append(String.format("%02X ", b));
                        }
                        Log.i("dataComeGo", "--receiveData= " + builder.toString());
                        for (int i = 0; i < temp1.length; i++) {
                            if (temp1[i] == (byte) 0xAA) {
                                Log.i(TAG, "--找到头");
                                if (temp1.length - i >= 8) {
                                    Log.i(TAG, "--头的长度够了");
                                    //可以拼接头
                                    System.arraycopy(temp1, i, head, 0, 8);//把数据复制到head
                                    int len = head[5] & 0xFF;  //4 5角标为数据长度  这里存在小问题，后面研究
                                    if (len <= temp1.length - 8) {
                                        //后面接着的数据达到len的长度，直接取出来
                                        receiveData = ByteUtil.subBytes(temp1, i, i + 8 + len);//将完整的数据包截取出来
                                        Log.d(TAG, "=======================================");
                                        for (byte b : receiveData) {
                                            Log.i(TAG, "--receiveData: " + b);
                                        }
                                        mParserService.parseReceivedPacket(receiveData);//发送指令
                                        Log.i(TAG, "--temp1 length" + temp1.length);
                                        temp1 = ByteUtil.subBytes(temp1, i + 8 + len, temp1.length - (i + 8 + len));//清除已经发送的部分
                                        Log.i(TAG, "--temp1 length" + temp1.length);
                                        break;
                                    } else {
                                        //后面缓存的数据不够len的长度，等待
                                        Log.d(TAG, "--temp1 等待数据包");
                                        if (!wait) {
                                            //不等待了，把前面的头和数据丢掉
                                            temp1 = null;
                                            wait = true;
                                        }
                                        SystemClock.sleep(3000);
                                        wait = false;
                                    }

                                } else {
                                    Log.d(TAG, "--temp1 等待数据包");
                                    //头不够长，等待头
                                    if (!wait2) {
                                        //不等待了，把前面的头
                                        temp1 = null;
                                        wait2 = true;
                                    }
                                    SystemClock.sleep(3000);
                                    wait2 = false;
                                }
                            }
                        }
                    }
                    SystemClock.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
