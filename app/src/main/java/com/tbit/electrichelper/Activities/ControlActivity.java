package com.tbit.electrichelper.Activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ControlActivity extends CommonToolbarActivity {
    private final int ITEM_ID_SET_DEFENCE = 0;
    private final int ITEM_ID_LOCK = 1;
    private final int ITEM_ID_CANCEL_DEFENCE = 2;
    private final int ITEM_ID_UNLOCK = 3;

    @Bind(R.id.iv_panel)
    ImageView ivPanel;
    @Bind(R.id.btn_setDefence)
    TextView btnSetDefence;
    @Bind(R.id.btn_cancelDefence)
    TextView btnCancelDefence;
    @Bind(R.id.btn_lock)
    TextView btnLock;
    @Bind(R.id.btn_locate)
    TextView btnLocate;

    private List<ControlItem> datas = new ArrayList<>();
    private Bitmap bitmapOff;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                ivPanel.setImageBitmap(bitmapOff);
                return true;
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                switch (v.getId()) {
                    case R.id.btn_setDefence:
                        setSwitch(ITEM_ID_SET_DEFENCE);
                        break;
                    case R.id.btn_cancelDefence:
                        setSwitch(ITEM_ID_CANCEL_DEFENCE);
                        break;
                    case R.id.btn_lock:
                        setSwitch(ITEM_ID_UNLOCK);
                        break;
                    case R.id.btn_locate:
                        setSwitch(ITEM_ID_LOCK);
                        break;
                }
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mToolbarTitle.setText(R.string.control);

        initData();

        btnSetDefence.setClickable(true);
        btnCancelDefence.setClickable(true);
        btnLock.setClickable(true);
        btnLocate.setClickable(true);

        btnSetDefence.setOnTouchListener(onTouchListener);
        btnCancelDefence.setOnTouchListener(onTouchListener);
        btnLocate.setOnTouchListener(onTouchListener);
        btnLock.setOnTouchListener(onTouchListener);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_control;
    }

    private void initData() {
        datas.clear();
        bitmapOff = BitmapFactory.decodeResource(getResources(), R.drawable.image_control_off);

        ControlItem item1 = new ControlItem();
        item1.setId(ITEM_ID_SET_DEFENCE);
        item1.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.image_control_set_defence));

        ControlItem item2 = new ControlItem();
        item2.setId(ITEM_ID_LOCK);
        item2.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.image_control_locate));

        ControlItem item3 = new ControlItem();
        item3.setId(ITEM_ID_CANCEL_DEFENCE);
        item3.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.image_control_cancel_defence));

        ControlItem item4 = new ControlItem();
        item4.setId(ITEM_ID_UNLOCK);
        item4.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.image_control_lock));

        datas.add(item1);
        datas.add(item2);
        datas.add(item3);
        datas.add(item4);
    }

    private void setSwitch(int i) {
        ivPanel.setImageBitmap(datas.get(i).getBitmap());
        Map<String, String> map = new HashMap<>();
        switch (i) {
            case ITEM_ID_SET_DEFENCE: {
                map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(2));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, String.valueOf(0));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(1));

                break;
            }
            case ITEM_ID_CANCEL_DEFENCE: {
                map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(2));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, String.valueOf(0));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(0));
                break;
            }
            case ITEM_ID_LOCK: {
                map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(1));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, "SC");
                map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(1));
                break;
            }
            case ITEM_ID_UNLOCK: {
                map.put(NetworkProtocol.PARAMKEY_CARID, application.getCurCar().getCarId());
                map.put(NetworkProtocol.PARAMKEY_COMMAND_TYPE, String.valueOf(1));
                map.put(NetworkProtocol.PARAMKEY_COMMAND_NAME, "SC");
                map.put(NetworkProtocol.PARAMKEY_COMMAND_VALUE, String.valueOf(0));
                break;
            }
        }
        postNetworkJson(i, NetworkProtocol.URL_SEND_COMMAND, map, false);

    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case ITEM_ID_SET_DEFENCE:
                if (jsonObject.isRes()) {
                    showCrouton("设防指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton("设防:" + jsonObject.getDesc());
                }
                break;
            case ITEM_ID_CANCEL_DEFENCE:
                if (jsonObject.isRes()) {
                    showCrouton("撤防指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton("撤防:" + jsonObject.getDesc());
                }
                break;
            case ITEM_ID_LOCK:
                if (jsonObject.isRes()) {
                    showCrouton("锁车指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton("锁车:" + jsonObject.getDesc());
                }
                break;
            case ITEM_ID_UNLOCK:
                if (jsonObject.isRes()) {
                    showCrouton("解锁指令发送成功", Style.CONFIRM);
                } else {
                    showCrouton("解锁:" + jsonObject.getDesc());
                }
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (datas != null) {
            for (ControlItem item : datas) {
                item.getBitmap().recycle();
            }
        }
    }

    private class ControlItem {
        private int id;
        private Bitmap bitmap;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }
    }
}
