package com.tbit.electrichelper.Activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * 欢迎界面
 */
public class Welcome_Activity extends BaseActivity {

    private final int HANDLER_TO_NEXT = 1;
    @Bind(R.id.welcome_skip)
    Button skipButton;
    /**
     * 停留时间
     */
    private int STAY_TIME = 3000;
    /**
     * 版本升级管理器
     */
    private Handler handler;

    /**
     * 启动页图片
     */
    private ImageView welcomeImage;
    private boolean isNeedJump = true;


    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 定义全屏参数
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        // 获得当前窗体对象
        Window window = this.getWindow();
        // 设置当前窗体为全屏显示
        window.setFlags(flag, flag);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HANDLER_TO_NEXT && isNeedJump) {
                    toNextActivity();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                }
            }
        };

        if (!MainActivity.isAlive) {
            /* 是否已开启App，防止重复开启登录界面，引起界面间混乱 */
            // 2秒后跳转至下一个界面
            welcomeImage = (ImageView) findViewById(R.id.welcome_image);
            handler.sendMessageDelayed(handler.obtainMessage(HANDLER_TO_NEXT), STAY_TIME);
            skipButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isNeedJump = false;
                    toNextActivity();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                }
            });

            ObjectAnimator zoomAnimX = ObjectAnimator.ofFloat(welcomeImage, "scaleX", 1.0f, 1.2f);
            ObjectAnimator zoomAnimY = ObjectAnimator.ofFloat(welcomeImage, "scaleY", 1.0f, 1.2f);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setDuration(STAY_TIME + 800);
            animatorSet.play(zoomAnimX).with(zoomAnimY);
            animatorSet.start();
        } else {
            finish();
        }
    }


    /**
     * 进入下一界面
     */
    public void toNextActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

}
