package com.tbit.electrichelper.Activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Adapter.CommonAdapter;
import com.tbit.electrichelper.Adapter.ViewHolder;
import com.tbit.electrichelper.Beans.A1Entity;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Util.ByteUtil;
import com.tbit.electrichelper.Fragments.Dialogs.EditTextDialog;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.UartService;
import com.tbit.electrichelper.Util.CommonToolbarActivity;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.RadarView;
import com.tbit.electrichelper.Util.TbitProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kenny on 2016/1/19.
 * Desc：To scan the ble devices around the smart phone;
 */
public class ScanActivity extends CommonToolbarActivity implements
        AdapterView.OnItemClickListener, ISimpleDialogListener {

    public static final String TAG = "ScanActivity";
    public static final String BUNDLE_KEY_SHARE_CONNECT_TID = "share_connect_id";
    private static final int DIALOG_REQUEST_SHARE_DEVICE_NOT_FOUNT = 0;
    private static final int NETWORK_TAG_BIND_A1 = 0;
    private static final int NETWORK_TAG_GET_BINDING_A1 = 1;
    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;
    private static final int HANDLE_STOP_SCAN = 0;
    private static final int HANDLE_CANCELABLE = 1;
    public static long SCAN_PERIOD = 10000;
    public static ScanActivity instance = null;
    private static long CONNECT_TIME_OUT = 10 * 1000;
    private List<Map<String, Object>> list = new ArrayList<>();
    private CommonAdapter<Map<String, Object>> mAdapter;
    private ListView lv_devices;
    private RadarView mBleRadar;
    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    private BluetoothAdapter mBtAdapter = null;
    private List<BluetoothDevice> deviceList;
    private Handler mHandler;
    private boolean isShareMode;
    private String sharedId;

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String resolvedId = resolveTID(scanRecord);

                            addDevice(device, rssi, resolvedId, ByteUtil.bytesToHexString(scanRecord));

                            if (isShareMode) {
                                if (resolvedId != null && resolvedId.length() > 3) {
                                    String part = resolvedId.substring(resolvedId.length()-3,
                                            resolvedId.length());
                                    if (part.equals(sharedId)) {
                                        stopScanAndConnect();
                                    }
                                }
                            }
                        }
                    });
                }
            };
    private boolean isScanning = false;
    private boolean isConnecting = true;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((UartService.LocalBinder) service).getService();
            isConnecting = false;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private int mPosCurSelected = 0;

    private String resolveTID(byte[] scanRecord) {
        String result = null;
        try {
            byte[] menuData = ByteUtil.adv_report_parse((short) 0xff, scanRecord);
            result = ByteUtil.bytesToHexString(menuData);
            result = result.substring(4);
            result = result.toUpperCase();
            result = result.replace("F", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        mToolbarTitle.setText("检测蓝牙");
        mProgressDialogUtils.setCancelable(false);

        String tid = getIntent().getStringExtra(BUNDLE_KEY_SHARE_CONNECT_TID);

        isShareMode = !TextUtils.isEmpty(tid);
        sharedId = tid;

        if (isShareMode) {
            showProgressDialog(true, getString(R.string.pls_wait));
        }

        getService();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isRunning) {
                    return;
                }
                int what = msg.what;
                switch (what) {
                    case HANDLE_STOP_SCAN:
                        if (isShareMode) {
                            stopScanAndConnect();
                            break;
                        }
                        stopScan();
                        break;
                    case HANDLE_CANCELABLE:
                        mProgressDialogUtils.showProgressDialog(false);
                        break;
                }
            }
        };

        initViews();
        initListener();

        initBLE();
        mAdapter = new CommonAdapter<Map<String, Object>>(this, list, R.layout.device_item) {
            @Override
            public void convert(ViewHolder holder, Map<String, Object> item) {
                TextView deviceName = holder.getView(R.id.tv_device);
                ImageView signal = holder.getView(R.id.iv_signal);

                int rssi = 0;
                try {
                    deviceName.setText((String) (item.get("name")));
                    rssi = -Integer.valueOf(item.get("rssi").toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (rssi <= 70) {
                    signal.setImageResource(R.drawable.signal1);
                } else if (rssi <= 80) {
                    signal.setImageResource(R.drawable.signal2);
                } else if (rssi <= 90) {
                    signal.setImageResource(R.drawable.signal3);
                } else if (rssi <= 100) {
                    signal.setImageResource(R.drawable.signal4);
                } else {
                    signal.setImageResource(R.drawable.signal5);
                }
            }
        };
        lv_devices.setAdapter(mAdapter);
        lv_devices.setOnItemClickListener(this);

        //初次进来自动扫描
        mBleRadar.performClick();
        EventBus.getDefault().register(this);
    }

    private void getService() {
        Intent intent = new Intent(this, UartService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_scan;
    }

    @Override
    protected int inflateCustomImageMenu() {
        return R.drawable.ic_history_white_36dp;
    }

    @Override
    protected void onCustomMenuClick() {
        if (isConnecting) {
            showToast("正在连接请稍等");
            return;
        }
        enterActivity(HistoricalConnectionsActivity.class);
    }

    /**
     * Ensures Bluetooth is available on the device and it is enabled. If not,
     * displays a dialog requesting user permission to enable Bluetooth.
     */
    private void initBLE() {
        mBtAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if (mBtAdapter == null || !mBtAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        deviceList = new ArrayList<>();
    }

    private void initListener() {
        mBleRadar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isScanning) {
                    startScan();
                    mHandler.sendEmptyMessageDelayed(HANDLE_STOP_SCAN, SCAN_PERIOD);
                } else {
                    mHandler.removeMessages(HANDLE_STOP_SCAN);
                    stopScan();
                }
            }
        });
    }

    private void initViews() {
        lv_devices = (ListView) findViewById(R.id.lv_devices);
        mBleRadar = (RadarView) findViewById(R.id.scanview);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mService.disconnect();
        stopScan();
        mPosCurSelected = position;
        Map<String, Object> map = list.get(position);
        String tid = (String) map.get("tid");
        if (TextUtils.isEmpty(tid)) {
            showToast("广播数据中未发现tid号");
            return;
        }
        boolean hasConnectBefore = false;
        for (A1Entity e : application.a1Cars) {
            if (String.valueOf(e.getMachineNO()).equals(tid)) {
                hasConnectBefore = true;
            }
        }
        if (hasConnectBefore) {
            if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                connect();
            } else {
                if (application.isNetworkConnected() && !application.isOfflineMode) {
                    enterActivity(LoginActivity.class);
                } else {
                    connect();
                }
            }
        } else {
            if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                showInputRemarkDialog();
            } else {
                enterActivity(LoginActivity.class);
            }
        }
    }

    private void connect() {
        mProgressDialogUtils.showProgressDialog(true, "正在连接中...");
        mHandler.sendEmptyMessageDelayed(HANDLE_CANCELABLE, CONNECT_TIME_OUT);
        mDevice = deviceList.get(mPosCurSelected);
        String tid = "";
        try {
             tid = (String) list.get(mPosCurSelected).get("tid");
        } catch (Exception e) {
            e.printStackTrace();
        }
        application.deviceName = mDevice.getName();
        application.deviceAddr = mDevice.getAddress();
        mService.connect(mDevice.getAddress(), tid);
    }

    private void showInputRemarkDialog() {
        final EditTextDialog dialog = new EditTextDialog();
        dialog.setTitle("设置备注名");
        dialog.setCancelable(false);
        dialog.setEditTextHint("请输入10个字位以内的设备备注名");
        dialog.setEditTextListener(new EditTextDialog.EditTextListener() {
            @Override
            public void onConfirm(String editString) {
                if (editString.length() > 10) {
                    showToast("请输入10个字位以内的设备备注名");
                    return;
                }
                mProgressDialogUtils.showProgressDialog(true, "正在绑定中...");
                String tid = (String) list.get(mPosCurSelected).get("tid");
                Map<String, String> param = new HashMap<>();
                param.put(NetworkProtocol.PARAMKEY_MACHINENO, tid);
                param.put(NetworkProtocol.PARAMKEY_REMARK, editString);
                postNetworkJson(NETWORK_TAG_BIND_A1, NetworkProtocol.URL_A1_BIND, param, false);
                dialog.dismiss();
            }

            @Override
            public void onCancel() {
                dialog.dismiss();
            }

            @Override
            public void onNeutral() {

            }
        });
        dialog.show(getSupportFragmentManager(), null);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        if (!jsonObject.isRes()) {
            showToast(jsonObject.getDesc());
            mProgressDialogUtils.showProgressDialog(false);
            return;
        }
        switch (tag) {
            case NETWORK_TAG_BIND_A1:
                postNetworkJson(NETWORK_TAG_GET_BINDING_A1, NetworkProtocol.URL_A1_BINDING_QUERY, null,
                        false);
                break;
            case NETWORK_TAG_GET_BINDING_A1:
                mProgressDialogUtils.showProgressDialog(false);
                List<A1Entity> a1s = null;
                try {
                    a1s = gson.fromJson(jsonObject.getResult(),
                            new TypeToken<List<A1Entity>>() {
                            }.getType());
                    application.a1Cars.clear();
                    application.a1Cars.addAll(a1s);
                    application.saveA1Data(a1s);
                    connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        super.onFailure(tag);
        switch (tag) {
            case NETWORK_TAG_BIND_A1:
            case NETWORK_TAG_GET_BINDING_A1:
                mProgressDialogUtils.showProgressDialog(false);
                break;
        }
    }

    /******************************
     * The following is about ble *
     ******************************/
    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mBtAdapter.startLeScan(mLeScanCallback);
        } else {
            mBtAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private void addDevice(BluetoothDevice device, int rssi, String tid, String extraData) {
        boolean deviceFound = false;

        for (BluetoothDevice listDev : deviceList) {
            if (listDev.getAddress().equals(device.getAddress())) {
                deviceFound = true;
                break;
            }
        }

        if (!device.getName().contains("TBIT_A1")) {
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("name", device.getName());
        map.put("address", device.getAddress());
        map.put("rssi", rssi);
        map.put("tid", tid);

        if (!deviceFound) {
//            showDialoge(device.getName(), extraData);
            Log.d(TAG, tid + " : " + extraData);
            deviceList.add(device);
            list.add(map);
            lv_devices.smoothScrollToPosition(mAdapter.getCount() - 1);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
    }

    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        mBtAdapter.stopLeScan(mLeScanCallback);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBtAdapter.stopLeScan(mLeScanCallback);
        unbindService(mConnection);
        mService = null;
        EventBus.getDefault().unregister(this);
    }

    private void startScan() {
        if (isScanning) {
            return;
        }
        isScanning = true;
        mBleRadar.startScan();
        deviceList.clear();
        mAdapter.notifyDataSetChanged();
        list.clear();
        scanLeDevice(true);
    }

    private void stopScan() {
        isScanning = false;
        mBleRadar.dismiss();
        scanLeDevice(false);
    }

    private void stopScanAndConnect() {
        showProgressDialog(false);
        stopScan();
        if (list.isEmpty()) {
            Log.d(TAG, "stopScanAndConnect: empty list");
            showShareDeviceNotFoundDialog();
            return;
        }
        boolean hasFound = false;

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            String tid = (String) map.get("tid");
            if (!TextUtils.isEmpty(tid) && tid.length() > 3) {
                String part = tid.substring(tid.length()-3, tid.length());
                if (part.equals(sharedId)) {
                    hasFound = true;
                    mPosCurSelected = i;
                    break;
                }
            }
        }

        if (!hasFound) {
            Log.d(TAG, "stopScanAndConnect: hasn't found");
            showShareDeviceNotFoundDialog();
            return;
        }

        connect();
    }

    private void showShareDeviceNotFoundDialog() {
        showProgressDialog(false);
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setTitle("分享")
                .setMessage("未找到分享的设备，请确认分享的设备是否在附近")
                .setPositiveButtonText(R.string.confirm)
                .setRequestCode(DIALOG_REQUEST_SHARE_DEVICE_NOT_FOUNT)
                .setCancelable(false)
                .show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectSucc(Event.BleConnectSucc event) {
        mProgressDialogUtils.showProgressDialog(false);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectFail(Event.BleConnectFail event) {
        mProgressDialogUtils.showProgressDialog(false);
    }

    private void showDialoge(String title, String message) {
        String content = "Name:\n" + title + "\nContent:\n" + message;
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                .setMessage(content)
                .setCancelable(true)
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .show();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {

    }
}
