package com.tbit.electrichelper.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.TbitUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class NetworkPhotoViewActivity extends AppCompatActivity {

    static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %% ID: %d";
    static final String SCALE_TOAST_STRING = "Scaled to: %.2ff";
    private static final String BUNDLE_KEY_URL = "bundle_url";
    @Bind(R.id.iv_photo)
    PhotoView ivPhoto;
    private PhotoViewAttacher mAttacher;
    private Callback mCallback = new Callback() {
        @Override
        public void onSuccess() {
            if (mAttacher == null) {
                mAttacher = new PhotoViewAttacher(ivPhoto);
                mAttacher.setOnPhotoTapListener(new PhotoTapListener());
            } else {
                mAttacher.update();
            }
        }

        @Override
        public void onError() {

        }
    };

    public static void startActivity(Context context, String url) {
        Intent intent = new Intent(context, NetworkPhotoViewActivity.class);
        intent.putExtra(BUNDLE_KEY_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //定义全屏参数
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        //获得当前窗体对象
        Window window = NetworkPhotoViewActivity.this.getWindow();
        //设置当前窗体为全屏显示
        window.setFlags(flag, flag);

        setContentView(R.layout.activity_photo_view);
        ButterKnife.bind(this);

        String url = getIntent().getStringExtra(BUNDLE_KEY_URL);

        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.ic_photo_primarydark_48dp)
                .error(R.drawable.ic_broken_image_red_400_48dp)
                .resize(TbitUtil.dip2px(this,250),TbitUtil.dip2px(this,250))
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .centerInside()
                .into(ivPhoto, mCallback);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAttacher != null) {
            mAttacher.cleanup();
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    private class PhotoTapListener implements PhotoViewAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
            float xPercentage = x * 100f;
            float yPercentage = y * 100f;

        }
    }
}
