package com.tbit.electrichelper.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BaiduNaviManager;
import com.tbit.electrichelper.Activities.AboutActivity;
import com.tbit.electrichelper.Activities.ControlActivity;
import com.tbit.electrichelper.Activities.LoginActivity;
import com.tbit.electrichelper.Activities.NavigationGuideActivity;
import com.tbit.electrichelper.Activities.PersonalActivity;
import com.tbit.electrichelper.Activities.SettingActivity;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;
import com.tbit.electrichelper.Util.TextViewPlus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends BaseFragment implements ISimpleDialogListener {
    public static final String ROUTE_PLAN_NODE = "routePlanNode";
    private final int HANDLE_START_NAVIGATION = 0;
    private final int DIALOG_REQUEST_NAVI = 11;
    private final int DIALOG_REQUEST_GPS = 12;

    @Bind(R.id.text_about_update)
    TextViewPlus setting;
    @Bind(R.id.image_about_logo)
    ImageView avatar;
    @Bind(R.id.text_about_introduce)
    TextViewPlus control;
    @Bind(R.id.text_about_help)
    TextViewPlus navifation;
    @Bind(R.id.about)
    TextViewPlus about;

    private MyApplication application;
    private CarStatus status;
    private Handler mHandler;
    public static final String TAG = MoreFragment.class.getSimpleName();

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, view);
        application = MyApplication.getInstance();
        createHandler();
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getInstance().getLoginStatus() != TbitProtocol.LOGGED_OUT) {
                    if (application.hasA2Bound()) {
                        enterActivity(SettingActivity.class);
                    } else {
                        addBinding();
                    }
                } else {
                    enterActivity(LoginActivity.class);
                }

            }
        });

//        initLocation();

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getInstance().getLoginStatus() != TbitProtocol.LOGGED_OUT) {
                    if (application.hasA2Bound()) {
                        enterActivity(PersonalActivity.class);
                    } else {
                        addBinding();
                    }
                } else {
                    enterActivity(LoginActivity.class);
                }

            }
        });

        navifation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (application.getLoginStatus() != TbitProtocol.LOGGED_OUT) {
                    if (application.hasA2Bound()) {
                        if (application.myPosition == null) {
                            showCroupton(R.string.location_fail);
//                            MainActivity.setLocationScanDuration(MainActivity.LOCATION_SCAN_DURATION_SHORT);
                            return;
                        }
                        navigationConfirmDialog();
                    } else {
                        addBinding();
                    }
                } else {
                    enterActivity(LoginActivity.class);
                }
            }
        });

        control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (application.getLoginStatus() == TbitProtocol.LOGGED_OUT) {
                    enterActivity(LoginActivity.class);
                } else if (application.getLoginStatus() == TbitProtocol.GUEST) {
                    showCroupton(R.string.tip_only_logged_in_user_positive);
                } else if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                    if (application.hasA2Bound()) {
                        enterActivity(ControlActivity.class);
                    } else {
                        addBinding();
                    }
                }
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterActivity(AboutActivity.class);
            }
        });
        /*about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterActivity(ControlTestActivity.class);
            }
        });*/
        return view;
    }

    private void createHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case HANDLE_START_NAVIGATION:
                        LatLng latLng = new LatLng(status.getLat(), status.getLng());
                        routeplanToNavi(application.myPosition, latLng);
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (BaiduNaviManager.getInstance().isNaviInited()) {
            BaiduNaviManager.getInstance().uninit();
        }
        ButterKnife.unbind(this);
    }

    private void initNavi() {
        BaiduNaviManager.getInstance().init(parentActivity, TbitProtocol.EXTERNAL_STORAGE, TbitProtocol.BAIDU_NAVI, new BaiduNaviManager.NaviInitListener() {
            @Override
            public void onAuthResult(int i, final String s) {

                if (0 != i) {
                    parentActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(parentActivity, "key校验失败, " + s, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            @Override
            public void initStart() {
            }

            @Override
            public void initSuccess() {
                if (status != null) {
                    mHandler.sendEmptyMessage(HANDLE_START_NAVIGATION);
                } else {
                    showCroupton("status in fragment is null");
                    mProgressDialogUtils.showProgressDialog(false);
                }
            }

            @Override
            public void initFailed() {

            }
        }, null);
    }

    private void routeplanToNavi(LatLng start, LatLng end) {
        BNRoutePlanNode sNode = new BNRoutePlanNode(start.longitude,
                start.latitude, null, null, BNRoutePlanNode.CoordinateType.BD09LL);
        BNRoutePlanNode eNode = new BNRoutePlanNode(end.longitude,
                end.latitude, null, null, BNRoutePlanNode.CoordinateType.BD09LL);
        double distance = TbitUtil.GetShortDistance(start, end);
        if (distance < 50) {
            mProgressDialogUtils.showProgressDialog(false);
            showLongToast("导航距离小于50米，建议到首页中的地图界面中查看地图");
            return;
        }
        if (sNode != null && eNode != null) {
            List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
            list.add(sNode);
            list.add(eNode);
            BaiduNaviManager.getInstance().launchNavigator(parentActivity, list, 1, true, new DemoRoutePlanListener(sNode));
        }
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == DIALOG_REQUEST_NAVI) {
            if (isGpsOn()) {
                startNavigation();
            } else {
                showGpsDialog();
            }
        }
    }

    private void startNavigation() {
        status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        mProgressDialogUtils.showProgressDialog(true, getString(R.string.pls_wait));
        if (BaiduNaviManager.getInstance().isNaviInited()) {
            mHandler.sendEmptyMessage(HANDLE_START_NAVIGATION);
        } else {
            initNavi();
        }
    }

    private void navigationConfirmDialog() {
        SimpleDialogFragment.createBuilder(parentActivity, getChildFragmentManager())
                .setMessage(R.string.dialog_navigation_start_confirm)
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setTargetFragment(this, DIALOG_REQUEST_NAVI)
                .setTitle(R.string.navigate).show();
    }

    public class DemoRoutePlanListener implements BaiduNaviManager.RoutePlanListener {

        private BNRoutePlanNode mBNRoutePlanNode = null;

        public DemoRoutePlanListener(BNRoutePlanNode node) {
            mBNRoutePlanNode = node;
        }

        @Override
        public void onJumpToNavigator() {
            mProgressDialogUtils.showProgressDialog(false);
            for (Activity ac : application.activities) {

                if (ac.getClass().getName().endsWith("NavigationGuideActivity")) {
                    return;
                }
            }
            Intent intent = new Intent(parentActivity, NavigationGuideActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ROUTE_PLAN_NODE, (BNRoutePlanNode) mBNRoutePlanNode);
            intent.putExtras(bundle);
            parentActivity.startActivity(intent);
        }

        @Override
        public void onRoutePlanFailed() {
            // TODO Auto-generated method stub
            mProgressDialogUtils.showProgressDialog(false);
            showCroupton("路线规划失败，请检查当前网络或定位状态");
        }
    }

    private boolean isGpsOn() {
        LocationManager locationManager = (LocationManager) parentActivity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showGpsDialog() {
        SimpleDialogFragment.createBuilder(parentActivity, getChildFragmentManager())
                .setTitle(R.string.navigate)
                .setMessage("导航需要开启GPS定位,请开启GPS定位")
                .setPositiveButtonText(R.string.confirm)
                .setTargetFragment(this, DIALOG_REQUEST_GPS)
                .show();
    }
}
