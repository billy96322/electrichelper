package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tbit.electrichelper.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salmon on 2016/4/26 0026.
 */
public class RadioGroupDialog extends DialogFragment {

    private TextView mText;
    private RadioGroup mRadioGroup;
    private TextView mConfirm;
    private TextView mCancel;
    private TextView mTitle;

    private OnConfirmListener mOnConfirmListener;
    private OnCancelListener mOnCancelListener;

    private int mCheckIndex = -1;

    private List<RadioButton> radioButtons = new ArrayList<>();
    private String mTitleContent = "";
    private int mDefaultSelect = 0;

    private int mGroupOrientation = RadioGroup.HORIZONTAL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.radio_dialog, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        mText = (TextView) view.findViewById(R.id.text_radioDialog_content);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.radioGroup_radioDialog);
        mConfirm = (TextView) view.findViewById(R.id.text_dialog_positive);
        mCancel = (TextView) view.findViewById(R.id.text_dialog_negative);
        mTitle = (TextView) view.findViewById(R.id.title);

        mTitle.setText(mTitleContent);
        mRadioGroup.setOrientation(mGroupOrientation);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        int count = radioButtons.size();
        for (int i = 0; i < count; i++) {
            RadioButton rb = radioButtons.get(i);
            if (rb.getParent() != null) {
                ViewGroup parent = (ViewGroup) rb.getParent();
                parent.removeView(rb);
            }
            rb.setLayoutParams(params);
            rb.setId(i);
            if (i == mDefaultSelect)
                rb.setChecked(true);
            mRadioGroup.addView(rb);
        }


        if (mCheckIndex > 0 && mCheckIndex < count) {
            radioButtons.get(mCheckIndex).setChecked(true);
        }

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnConfirmListener != null) {
                    mOnConfirmListener.onConfirm(mRadioGroup.getCheckedRadioButtonId());
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCancelListener != null) {
                    mOnCancelListener.onCancel();
                } else {
                    dismiss();
                }
            }
        });
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        mOnConfirmListener = onConfirmListener;
    }

    public void setOnCancelListener(OnCancelListener onCancelListener) {
        mOnCancelListener = onCancelListener;
    }

    public void setTitle(String title) {
        mTitleContent = title;
    }

    public void addRadioButton(RadioButton radioButton) {
        radioButtons.add(radioButton);
    }

    public void setRadioList(List<RadioButton> radioButtons) {
        this.radioButtons = radioButtons;
    }

    public void setCheck(int index) {
        mCheckIndex = index;
    }

    public void setRadioGroupOrientation(int orientation) {
        mGroupOrientation = orientation;
    }

    public interface OnConfirmListener {
        void onConfirm(int id);
    }

    public interface OnCancelListener {
        void onCancel();
    }

    public void setDefaultSelect(int defaultSelect) {
        mDefaultSelect = defaultSelect;
    }
}
