package com.tbit.electrichelper.Fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.squareup.picasso.Picasso;
import com.tbit.electrichelper.Activities.LoginActivity;
import com.tbit.electrichelper.Activities.PositionActivity;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.Online;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BannerAdapter;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;


import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements BaiduMap.OnMarkerClickListener {
    public static final String TAG = HomeFragment.class.getSimpleName();
    private static final int HANDLE_UPDATE_MAP_DEFAULT = 0;
    private static final int HANDLE_UPDATE_MAP = 1;
    private static final int HANDLE_LOOP_BANNER = 3;
    private static final int HANDLE_UPDATE_BANNER_DATA = 4;
    private static final int BANNER_LOOP_DURATION = 10000;
    /*@Bind(R.id.convenientBanner)
    ConvenientBanner convenientBanner;*/
    @Bind(R.id.bmapView)
    TextureMapView mMapView;
    /*@Bind(R.id.pos_desc)
    TextView tvPosDesc;
    @Bind(R.id.btn_getPos)
    Button btnGetPos;*/
    BaiduMap mBaiduMap;
    @Bind(R.id.etMilestone)
    TextView etMilestone;
    @Bind(R.id.etPower)
    TextView etPower;
    @Bind(R.id.etVelocity)
    TextView etVelocity;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.linear_home_status_bar)
    LinearLayout linearHomeStatusBar;
    private List<Integer> localImages = new ArrayList<>();
    private Handler mHandler;
    private MyApplication application;
    private Marker marker;
    private Bitmap markerBitmap;
    private ArrayList<ImageView> imageViews = new ArrayList<>();
    private BannerAdapter bannerAdapter;
    private List<String> urls = new ArrayList<>();
    private boolean canLoop = true;

    public HomeFragment() {
    }

    public void updateStatusBar() {
        if (etMilestone == null) {
            return;
        }
        CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        if (status == null) {
            return;
        }
        etMilestone.setText(translateMillestone(status.getStrGGPV()) + "Km");
        etPower.setText(translatePower(status.getStrGGPV()) * 25 + "%");
        etVelocity.setText(String.valueOf(translateSpeed(status.getStrGGPV())) + "Km/h");
        updateMarkerStatus();
        mHandler.sendEmptyMessage(HANDLE_UPDATE_MAP);
    }

    public void setLogoutStatus() {
        if (etMilestone == null) {
            showCroupton("homefragment view recycled");
            return;
        }
        etMilestone.setText("里程");
        etPower.setText("电量");
        etVelocity.setText("速度");
        mHandler.sendEmptyMessage(HANDLE_UPDATE_MAP_DEFAULT);
    }

    private int translatePower(String s) {
        float result = 0;
        try {
            if (s == null || s.equals("")) {
                return 0;
            }
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            result = 0;
            result = Integer.parseInt(temp[0]);
            result = (result / 5) * 100;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int) result;
    }

    private int translateMillestone(String s) {
        float result = 0;
        try {
            if (s == null || s.equals("")) {
                return 0;
            }
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            result = 0;
            result = Integer.parseInt(temp[27]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int) result;
    }

    private int translateSpeed(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }
        float result = 0;
        try {
            String[] temp = s.split("=");
            temp = temp[1].split(",");
            result = 0;
            result = Integer.parseInt(temp[26]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int) result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        application = MyApplication.getInstance();
        createHandler();

        Online online = null;
        try {
            online = (Online) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_ONLINE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        mBaiduMap = mMapView.getMap();
        markerBitmap = TbitUtil.getMarkerBitmap(parentActivity, online != null ? online.isOnline() : true);
        mBaiduMap.setOnMarkerClickListener(this);
        TbitUtil.hideZoomControl(mMapView);

        localImages.add(R.drawable.pic_ad_test);

        /*convenientBanner.setPages(new CBViewHolderCreator() {
            @Override
            public Object createHolder() {
                return new LocalImageHolderView();
            }
        }, localImages);*/
//                .setPageIndicator(new int[]{R.drawable.ic_page_indicator, R.drawable.ic_page_indicator_focused});

        for (int i = 0; i < 3; i++) {
            ImageView iv = new ImageView(parentActivity);
            iv.setBackgroundResource(R.drawable.pic_ad_test);
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageViews.add(iv);
        }
        bannerAdapter = new BannerAdapter(imageViews);
        viewpager.setAdapter(bannerAdapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
               /* switch (state) {
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        bannerHandler.sendEmptyMessage(ImageHandler.MSG_KEEP_SILENT);
                        break;
                    case ViewPager.SCROLL_STATE_IDLE:
                        bannerHandler.sendEmptyMessageDelayed(ImageHandler.MSG_UPDATE_IMAGE, ImageHandler.MSG_DELAY);
                        break;
                    default:
                        break;
                }*/
            }
        });

//        mHandler.sendEmptyMessageDelayed(HANDLE_UPDATE_BANNER_DATA, 1000);

        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                toPositionActivity();
            }

            @Override
            public boolean onMapPoiClick(MapPoi mapPoi) {
                return false;
            }
        });

        linearHomeStatusBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                    if (application.hasA2Bound()) {
                        LocalBroadcastManager.getInstance(parentActivity)
                                .sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_UPDATE_STATUS));
                    } else {
                        addBinding();
                    }
                } else {
                    enterActivity(LoginActivity.class);
                }
            }
        });

        mHandler.sendEmptyMessage(HANDLE_UPDATE_MAP_DEFAULT);
        return view;
    }



    private void testBanner() {
        imageViews.clear();
        urls.add("http://cdn.pcbeta.attachment.inimc.com/data/attachment/forum/201406/15/131126annc7pgpzya6yeac.jpg");
        urls.add("http://cdn.pcbeta.attachment.inimc.com/data/attachment/forum/201406/15/131241rk6qvedqq3e8mghq.jpg");
        urls.add("http://bbs.pcbeta.com/data/attachment/forum/201406/15/131531hz8w83mfzy98429r.png");
        urls.add("http://bbs.pcbeta.com/data/attachment/forum/201406/15/131542s18my0zqq8qbz22j.jpg");
        LayoutInflater inflater = LayoutInflater.from(parentActivity);
        for (String url : urls) {
//            ImageView iv = (ImageView) inflater.inflate(R.layout.item_banner, null);
            ImageView iv = new ImageView(parentActivity);
            Picasso.with(parentActivity)
                    .load(url)
                    .placeholder(R.drawable.pic_ad_test)
                    .error(R.drawable.pic_ad_test)
                    .fit()
                    .into(iv);
            imageViews.add(iv);
        }
        bannerAdapter.notifyDataSetChanged();
        viewpager.setCurrentItem(0);
        mHandler.sendEmptyMessage(HANDLE_LOOP_BANNER);
    }

    private void toPositionActivity() {
        if (MyApplication.getInstance().getLoginStatus() != TbitProtocol.LOGGED_OUT) {
            if (application.hasA2Bound()) {
                enterActivity(PositionActivity.class);
            } else {
                addBinding();
            }
        } else {
            enterActivity(LoginActivity.class);
        }
    }

    private void createHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isRunning) {
                    return;
                }
                super.handleMessage(msg);
                switch (msg.what) {
                    case HANDLE_UPDATE_MAP_DEFAULT: {
                        MapStatus ms = new MapStatus.Builder().target(TbitProtocol.DEFAULT_LATLNG).zoom(17).build();
                        MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
                        mBaiduMap.animateMapStatus(u);
                        if (marker != null) {
                            marker.remove();
                        }
                        marker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(TbitProtocol.DEFAULT_LATLNG).icon(BitmapDescriptorFactory.fromBitmap(markerBitmap)).title("child").anchor(0.5f, 1.0f));
                        break;
                    }
                    case HANDLE_UPDATE_MAP: {
                        CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
                        if (status == null) {
                            break;
                        }
                        LatLng latLng = new LatLng(status.getLat(), status.getLng());
                        MapStatus ms = new MapStatus.Builder().target(latLng).zoom(16).build();
                        MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
                        mBaiduMap.animateMapStatus(u);
                        if (marker == null) {
                            marker = (Marker) mBaiduMap.addOverlay(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(markerBitmap)).title("child").anchor(0.5f, 1.0f));
                        } else {
                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
                            marker.setPosition(latLng);
                        }
                        break;
                    }
                    case HANDLE_LOOP_BANNER: {
                        if (canLoop) {
                            if (viewpager.getCurrentItem() > bannerAdapter.getCount() - 1) {
                                viewpager.setCurrentItem(0, true);
                            } else {
                                viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
                            }
                            sendEmptyMessageDelayed(HANDLE_LOOP_BANNER, BANNER_LOOP_DURATION);
                        }
                        break;
                    }
                    case HANDLE_UPDATE_BANNER_DATA: {
                        testBanner();
                        break;
                    }
                    default:
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onStop() {
        super.onStop();
//        convenientBanner.stopTurning();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        toPositionActivity();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }

    /*class LocalImageHolderView implements Holder<Integer> {
        private ImageView imageView;

        @Override
        public View createView(Context context) {
            imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            return imageView;
        }

        @Override
        public void UpdateUI(Context context, int position, Integer data) {
            imageView.setImageResource(data);
        }
    }*/
    private void updateMarkerStatus() {
        Online online = null;
        try {
            online = (Online) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_ONLINE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        markerBitmap = TbitUtil.getMarkerBitmap(parentActivity, online != null ? online.isOnline() : true);
    }


}


