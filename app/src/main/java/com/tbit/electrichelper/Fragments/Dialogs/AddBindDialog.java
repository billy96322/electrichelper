package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.NetworkCallback;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.NetworkUtils;

import java.util.HashMap;
import java.util.Map;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Salmon on 2016/4/28 0028.
 */
public class AddBindDialog extends EditTextDialog implements NetworkCallback {

    private static final int NETWORK_TAG_A2_BIND = 0;
    MaterialProgressBar mProgressBar;
    private NetworkUtils mNetworkUtil;
    private Gson gson;
    private MyApplication application;
    private AddBindListener mAddBindListener;

    public AddBindDialog() {
        gson = new Gson();
        application = MyApplication.getInstance();
        mNetworkUtil = new NetworkUtils(this, application);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.add_bind));
        setEditTextHint("请输入设备编号");
        setInputType(InputType.TYPE_CLASS_NUMBER);
        super.onViewCreated(view, savedInstanceState);

        mPositiveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptBind();
            }
        });
    }

    private void attemptBind() {
        String input = mEditText.getText().toString();
        if (input.length() == 0) {
            showToast("请输入设备编号");
            return;
        }
        setLoading(true);
        Map<String, String> map = new HashMap<String, String>();
        map.put(NetworkProtocol.PARAMKEY_MACHINENO, input);
        map.put(NetworkProtocol.PARAMKEY_PLATEFORM_TYPE, String.valueOf(10));
        mNetworkUtil.loadJsonData(NETWORK_TAG_A2_BIND, NetworkProtocol.URL_BOUND, map);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (!isVisible()) {
            return;
        }
        setLoading(false);
        if (!jsonObject.isRes()) {
            showToast(jsonObject.getDesc());
            return;
        }
        showToast("绑定成功");
        if (mAddBindListener != null) {
            mAddBindListener.onAddBind();
            dismiss();
        }
    }

    @Override
    public void onFailure(int tag) {
        if (!isVisible()) {
            return;
        }
        setLoading(false);
        showToast("当前网络不给力");
    }

    @Override
    public void onProgress(long bytesWritten, long totalSize) {

    }

    private void setLoading(boolean loading) {
        if (!isVisible()) {
            return;
        }
        if (mProgressBar == null) {
            mProgressBar = new MaterialProgressBar(getContext());
            ViewGroup.LayoutParams params = mEditText.getLayoutParams();
            mProgressBar.setLayoutParams(params);
            mInputLayout.addView(mProgressBar);
        }
        if (loading) {
            mEditText.setEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mEditText.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);
        }
    }


    public void setAddBindListener(AddBindListener listener) {
        mAddBindListener = listener;
    }

    public interface AddBindListener {
        void onAddBind();
    }

}
