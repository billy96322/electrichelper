package com.tbit.electrichelper.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.tbit.electrichelper.Activities.ScanActivity;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Constant;
import com.tbit.electrichelper.Bluetooth.Interface.BluetoothParseServiceImpl;
import com.tbit.electrichelper.Bluetooth.Util.SharePreferenceUtil;
import com.tbit.electrichelper.Bluetooth.Util.VibratorUtil;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.PacketParserService;
import com.tbit.electrichelper.Util.BannerAdapter;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Views.DashboardView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;


/**
 * Created by Kenny on 2016/1/19.
 * Desc：
 */
public class BluetoothHomeFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = BluetoothHomeFragment.class.getSimpleName();
    /**
     * 轮播图片的数组
     */
    final List<Integer> imgres = new ArrayList<Integer>();
    private DashboardView dashboard;
    private ImageButton ib_sf;
    private ImageButton ib_cf;
    private ImageButton ib_xc;
    private ImageButton ib_battery;
    private ImageButton ib_dashboard;
    //    private Vibrator vibrator;
    //true for press ,false for normal
    private boolean cf_state = true;
    private boolean sf_state;
    private boolean xc_state;
    private ViewPager mBanner;
    private BannerAdapter mBannerAdapter;
    private BluetoothParseServiceImpl mListener;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    int pos = msg.arg1;
                    ib_battery.setImageResource(pos);
                    break;
                case 2:
                    int progress = msg.arg1;
                    dashboard.setProgress(progress);
                    break;
            }
        }
    };


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVoltageUpdate(Event.BleVoltageUpdate event) {
        updateBattery(event.voltage);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSpeedUpdate(Event.BleSpeedUpdate event) {
        updateSpeed(event.speed);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMileageUpdate(Event.BleMileageUpdate event) {
//        updateMileage(event.mileage);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_ble, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initBanner(view);
        initViews(view);
        initUIState();
        initListener();

        new BatteryAnimation().start();
        new DashboardAnimation().start();
    }

    private PacketParserService getParserService() {
        return mListener.getPacketParserService();
    }

    private void initBanner(View view) {
        mBanner = (ViewPager) view.findViewById(R.id.viewpager);
        ArrayList<ImageView> imageViews = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            ImageView iv = new ImageView(parentActivity);
            iv.setBackgroundResource(R.drawable.pic_ad_test);
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageViews.add(iv);
        }
        mBannerAdapter = new BannerAdapter(imageViews);
        mBanner.setAdapter(mBannerAdapter);
    }

    private void initListener() {
        ib_sf.setOnClickListener(this);
        ib_cf.setOnClickListener(this);
        ib_xc.setOnClickListener(this);
    }

    private void initViews(View view) {
        ib_sf = (ImageButton) view.findViewById(R.id.ib_sf);
        ib_cf = (ImageButton) view.findViewById(R.id.ib_cf);
        ib_xc = (ImageButton) view.findViewById(R.id.ib_xc);
        ib_battery = (ImageButton) view.findViewById(R.id.ib_battery);
        dashboard = (DashboardView) view.findViewById(R.id.dashboard);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        Crouton.cancelAllCroutons();
    }

    @Override
    public void onClick(View v) {
        //暂时注释
        if (mListener.getUartService().getGatt() == null) {
            startActivity(new Intent(getActivity(), ScanActivity.class));
            return;
        }
        //暂时注释
        VibratorUtil.Vibrate(getActivity(), new long[]{100, 10, 100, 10}, false);
        switch (v.getId()) {

            case R.id.ib_sf:
                if (!sf_state) {
                    ib_sf.setImageResource(R.drawable.sf_press);
                    ib_cf.setImageResource(R.drawable.cf_normal);
                    cf_state = false;
                    sf_state = true;
                    getParserService().send(Constant.REQUEST_SET_DEFENCE, Constant.COMMAND_SETTING, Constant.SEND_KEY_DEFENCE,
                            new Byte[]{Constant.VALUE_ON});
                    save(Constant.REQUEST_SET_DEFENCE, "1");
                }
                break;
            case R.id.ib_cf:
                if (!cf_state) {
                    ib_sf.setImageResource(R.drawable.sf_normal);
                    ib_cf.setImageResource(R.drawable.cf_press);
                    cf_state = true;
                    sf_state = false;
                    getParserService().send(Constant.REQUEST_SET_DEFENCE, Constant.COMMAND_SETTING, Constant.SEND_KEY_DEFENCE,
                            new Byte[]{Constant.VALUE_OFF});
                    save(Constant.REQUEST_SET_DEFENCE, "0");
                }
                break;
            case R.id.ib_xc:
                xc_state = !xc_state;
                ib_xc.setImageResource(xc_state ? R.drawable.xc_press : R.drawable.xc_normal);
                getParserService().send(Constant.REQUEST_FIND_CAR, Constant.COMMAND_SETTING, Constant.SEND_KEY_FIND,
                        new Byte[]{xc_state ? Constant.VALUE_ON : Constant.VALUE_OFF});
//                SharePreferenceUtil.getInstance().saveData(Constant.SP_FIND_CAR, xc_state);
                break;
        }
    }


    private void save(int key, String value) {
        Map<Integer, String> map = application.readA1StateData(application.getCurCar().getCarId());
        map.put(key, value);
        application.saveA1StateData(application.getCurCar().getCarId(), map);
    }

    /**
     * When user open the APP ,restore the system state
     *
     * @return init successfully
     */
    public void initUIState() {
        Map<Integer, String> map = application.readA1StateData(application.getCurCar().getCarId());

        boolean isDefence = "1".equals(map.get(Constant.REQUEST_SET_DEFENCE));

        if (isDefence) {
            ib_sf.setImageResource(R.drawable.sf_press);
            ib_cf.setImageResource(R.drawable.cf_normal);
            cf_state = false;
            sf_state = true;
        } else {
            ib_sf.setImageResource(R.drawable.sf_normal);
            ib_cf.setImageResource(R.drawable.cf_press);
            cf_state = true;
            sf_state = false;
        }
        ib_xc.setImageResource(R.drawable.xc_normal);
        xc_state = false;
    }

    /**
     * update the image witch showed on BluetoothHomeFragment according to percentage of charge.
     *
     * @param percentage
     */
    private void updateBattery(byte percentage) {
        int p = Integer.valueOf(percentage);
        Log.i(TAG, "--<<>>updateBattery=" + p);
        if (p < 20) {
            ib_battery.setImageResource(R.drawable.battery_1);
        } else if (p <= 40) {
            ib_battery.setImageResource(R.drawable.battery_2);
        } else if (p <= 60) {
            ib_battery.setImageResource(R.drawable.battery_3);
        } else if (p <= 80) {
            ib_battery.setImageResource(R.drawable.battery_4);
        } else {
            ib_battery.setImageResource(R.drawable.battery_5);
        }
    }

    /**
     * 更新HomeFragment的速度与里程的仪表盘显示
     *
     * @param data
     */
    private void updateSpeed(byte data) {
        Log.i(TAG, "--更新里程" + data);
        dashboard.setProgress(data);
    }

    /**
     * 更新里程
     *
     * @param data
     */
    private void updateMileage(byte data) {
        Log.i(TAG, "--更新里程");
        dashboard.setMileage(data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BluetoothParseServiceImpl) {
            mListener = (BluetoothParseServiceImpl) context;
        } else {
            throw new RuntimeException("host activity must " +
                    "implement BluetoothParseServiceImpl");
        }
    }


    /**
     * 电池动画，APP打开初次进来时电池闪动一下，然后回到一个状态
     */
    class BatteryAnimation extends Thread {
        @Override
        public void run() {
            super.run();
            SystemClock.sleep(2000);
            int[] res = new int[]{R.drawable.battery_1, R.drawable.battery_2, R.drawable.battery_3, R.drawable.battery_4, R.drawable.battery_5};
            for (int i = 0; i < res.length * 2; i++) {
                Message msg = Message.obtain();
                msg.what = 1;
                msg.arg1 = res[i % res.length];
                mHandler.sendMessageDelayed(msg, 1000);
                SystemClock.sleep(1000);
            }
        }
    }

    class DashboardAnimation extends Thread {
        @Override
        public void run() {
            super.run();
            SystemClock.sleep(2000);
            for (int i = 0; i < 240 * 2; i++) {
                Message msg = Message.obtain();
                SystemClock.sleep(20);
                msg.what = 2;
                if (i < 240) {
                    msg.arg1 = i;
                } else {
                    msg.arg1 = Math.abs(i - 240 * 2);
                }
//                mHandler.sendMessageDelayed(msg, 50);
                mHandler.sendMessage(msg);
            }
        }
    }
}

