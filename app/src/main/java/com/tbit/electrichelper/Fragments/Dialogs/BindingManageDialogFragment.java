package com.tbit.electrichelper.Fragments.Dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.tbit.electrichelper.Activities.ScanActivity;
import com.tbit.electrichelper.Adapter.CommonAdapter;
import com.tbit.electrichelper.Adapter.ViewHolder;
import com.tbit.electrichelper.Beans.A1Entity;
import com.tbit.electrichelper.Beans.Car;
import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.NetworkCallback;
import com.tbit.electrichelper.Util.NetworkProtocol;
import com.tbit.electrichelper.Util.NetworkUtils;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Salmon on 2016/4/23 0023.
 */
public class BindingManageDialogFragment extends DialogFragment implements NetworkCallback {
    private static final int NETWORK_TAG_A2_CAR_DATA = 0;
    private static final int NETWORK_A2_UNBIND = 1;
    private static final int NETWORK_TAG_CAR_POSITION = 2;
    private static final int NETWORK_TAG_A1_CAT_DATA = 3;
    private static final int NETWORK_A1_UNBIND = 4;

    @Bind(R.id.list_bind)
    ListView mBindList;
    @Bind(R.id.text_dialog_neutral)
    TextView textDialogNeutral;
    @Bind(R.id.text_dialog_positive)
    TextView textDialogPositive;
    @Bind(R.id.text_dialog_negative)
    TextView textDialogNegative;
    @Bind(R.id.progressbar)
    MaterialProgressBar progressbar;
    @Bind(R.id.holder)
    TextView holder;
    @Bind(R.id.title)
    TextView mTitle;

    private AddBindDialog addDialog;
    private TextDialog delDialog;
    private TextDialog chooseDeviceDialog;

    private int curSelected = 0;
    private int curDeleteSelected;

    private NetworkUtils mNetworkUtil;
    private Gson gson;
    private List<Binding> mData = new ArrayList<>();
    private CommonAdapter<Binding> mAdapter;
    private BindListener mListener;
    private MyApplication application;
    private boolean isLoading = false;
    private View.OnClickListener mOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.text_dialog_positive:
                    if (mListener != null) {
                        if (mData.size() != 0) {
                            Binding b = mData.get(curSelected);
                            mListener.onBindChange(new Car(b.carId, b.type));
                        }
                    }
                    dismiss();
                    break;
                case R.id.text_dialog_negative:
                    dismiss();
                    break;
                case R.id.text_dialog_neutral:
                    if (!chooseDeviceDialog.isAdded()) {
                        chooseDeviceDialog.show(getFragmentManager(), null);
                    }
                    break;
            }
        }
    };
    private TextDialog.OnConfirmListener mDelListener = new TextDialog.OnConfirmListener() {
        @Override
        public void onConfirm() {
            Binding b = mData.get(curDeleteSelected);
            if (b.type == TbitProtocol.TYPE_A2)
                a2Delete();
            else
                a1Delete();
        }
    };

    private AddBindDialog.AddBindListener mAddListener = new AddBindDialog.AddBindListener() {
        @Override
        public void onAddBind() {
            loadA2();
        }
    };
    private boolean isA1FirstBound;
    private Toast mToast;

    public BindingManageDialogFragment() {
        super();

        gson = new Gson();
        application = MyApplication.getInstance();
        mNetworkUtil = new NetworkUtils(this, application);

        delDialog = new TextDialog();
        delDialog.setTitle("解绑设备");
        delDialog.setOnConfirmListener(mDelListener);

        chooseDeviceDialog = new TextDialog();
        chooseDeviceDialog.setTitle("绑定");
        chooseDeviceDialog.setText("请选择要绑定的设备类型");
        chooseDeviceDialog.setPositiveText("A2");
        chooseDeviceDialog.setNegativeText("A1");
        chooseDeviceDialog.setNeutralText("取消");
        chooseDeviceDialog.setOnConfirmListener(new TextDialog.OnConfirmListener() {
            @Override
            public void onConfirm() {
                addDialog = new AddBindDialog();
                addDialog.setAddBindListener(mAddListener);
                addDialog.show(getFragmentManager(), null);
                chooseDeviceDialog.dismiss();
            }
        });
        chooseDeviceDialog.setOnCancelListener(new TextDialog.OnCancelListener() {
            @Override
            public void onCancel() {
                startActivity(new Intent(getContext(), ScanActivity.class));
                chooseDeviceDialog.dismiss();
                dismiss();
            }
        });
        chooseDeviceDialog.setOnNeutralListener(new TextDialog.OnNeutralListener() {
            @Override
            public void onNeutral() {
                chooseDeviceDialog.dismiss();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_binding, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textDialogPositive.setOnClickListener(mOnclickListener);
        textDialogNegative.setOnClickListener(mOnclickListener);
        textDialogNeutral.setOnClickListener(mOnclickListener);

        mTitle.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

        mData.clear();
        initView();

        loadFromLocal();
    }

    private void initView() {
        mAdapter = new CommonAdapter<Binding>(getContext(), mData, R.layout.item_bind) {
            @Override
            public void convert(ViewHolder holder, Binding item) {
                TextView textView = holder.getView(R.id.text_machine_id);
                ImageView imageView = holder.getView(R.id.image_bind_tick);

                if (item.isSelected) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.GONE);
                }
                textView.setText(item.machineNo);
            }
        };
        mBindList.setAdapter(mAdapter);
        mBindList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mData.get(curSelected).isSelected = false;
                mData.get(position).isSelected = true;
                curSelected = position;
                mAdapter.notifyDataSetChanged();
            }
        });

        mBindList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                curDeleteSelected = position;
                String machineId = mData.get(position).machineNo;
                delDialog.setText("是否解绑设备: " + machineId);
                if (!delDialog.isVisible()) {
                    delDialog.show(getFragmentManager(), null);
                }
                return true;
            }
        });

    }

    private void loadFromLocal() {
        int count = application.a2Cars.size();
        boolean isCurCarRemoved = true;
        mData.clear();
        for (int i = 0; i < count; i++) {
            CarData.CarsEntity e = application.a2Cars.get(i);
            Binding b;
            if (e.getCarId().equals(application.getCurCar().getCarId())) {
                isCurCarRemoved = false;
                b = BindingFactory.A2Creator(e.getCarId(), e.getMachineNO(), true);
                curSelected = i;
            } else {
                b = BindingFactory.A2Creator(e.getCarId(), e.getMachineNO());
            }
            mData.add(b);
        }
        for (A1Entity e : application.a1Cars) {
            Binding b;
            if (e.getMachineNO().equals(application.getCurCar().getCarId())) {
                isCurCarRemoved = false;
                b = BindingFactory.A1Creator(e.getMachineNO(), true);
                curSelected = count;
            } else {
                b = BindingFactory.A1Creator(e.getMachineNO());
            }
            mData.add(b);
            count ++;
        }
        if (count == 0) {
            isA1FirstBound = true;
        } else {
            if (holder.getVisibility() == View.VISIBLE)
                holder.setVisibility(View.GONE);
        }
        if (isCurCarRemoved) {
            autoChangeBinding();
        }
        mAdapter.notifyDataSetChanged();
    }

    private void autoChangeBinding() {
        if (!application.hasAnyCarBound()) {
            return;
        }
        if (application.hasA2Bound()) {
            application.setCurCar(application.a2Cars.get(0).getCarId(), TbitProtocol.TYPE_A2);
        } else {
            application.setCurCar(application.a1Cars.get(0).getMachineNO(), TbitProtocol.TYPE_A1);
        }
        loadFromLocal();
    }

    private void loadA2() {
        showLoading();
        mNetworkUtil.loadJsonData(NETWORK_TAG_A2_CAR_DATA, NetworkProtocol.URL_GET_CAR_DATA,
                null);
    }

    private void a2Delete() {
        String selected = mData.get(curDeleteSelected).carId;
        String using = application.getCurCar().getCarId();
        if (selected.equals(using)) {
            // 如果是接触当前绑定的设备是A1设备并且正在使用，断开蓝牙连接
            sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_BLUETOOTH_DISCONNECT));
        }
        showLoading();
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, String.valueOf(mData.get(curDeleteSelected).carId));
        mNetworkUtil.loadJsonData(NETWORK_A2_UNBIND, NetworkProtocol.URL_UNBOUND, map);
    }

    private void a1Delete() {
        showLoading();
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_MACHINENO, String.valueOf(mData.get(curDeleteSelected).carId));
        mNetworkUtil.loadJsonData(NETWORK_A1_UNBIND, NetworkProtocol.URL_A1_UNBIND, map);
    }

    /**
     * 第一次绑定时，需要刷新数据
     */
    private void attemptUpdatePosition() {
        Map<String, String> map = new HashMap<>();
        map.put(NetworkProtocol.PARAMKEY_CARID, String.valueOf(application.a2Cars.get(0)));
        mNetworkUtil.loadJsonData(NETWORK_TAG_CAR_POSITION, NetworkProtocol.URL_POSITION, map);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (isLoading) {
            Intent intent = new Intent(TbitProtocol.BROADCAST_RECEIVER_BIND_CHANGE);
            sendBroadcast(intent);
        }
        ButterKnife.unbind(this);
    }

    public void setBindListener(BindListener listener) {
        this.mListener = listener;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (!isVisible()) {
            return;
        }
        hideLoading();
        if (!jsonObject.isRes()) {
            showToast(jsonObject.getDesc());
            return;
        }
        switch (tag) {
            case NETWORK_TAG_A2_CAR_DATA:
                CarData carData = gson.fromJson(jsonObject.getResult(), CarData.class);
                List<CarData.CarsEntity> temp = carData.getCars();
                application.a2Cars.clear();
                application.a2Cars.addAll(temp);
                if (temp.size() == 0) {
                    if (!application.hasA1Bound()) {
                        holder.setVisibility(View.VISIBLE);
                    }
                    isA1FirstBound = true;
                    application.globalData.clear();
                    sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_ALL_UNBOUNDED));
                    loadFromLocal();
                    break;
                }
                if (isA1FirstBound) {
                    holder.setVisibility(View.GONE);
                    attemptUpdatePosition();
                }
                loadFromLocal();
                break;
            case NETWORK_A2_UNBIND:
                mData.remove(curDeleteSelected);
                if (mData.size() == 0) {
                    sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_ALL_UNBOUNDED));
                }
                loadA2();
                break;
            case NETWORK_TAG_CAR_POSITION:
                List<CarStatus> statuses = gson.fromJson(jsonObject.getResult(), new TypeToken<List<CarStatus>>() {
                }.getType());
                for (CarStatus status : statuses) {
                    application.setGlobal(status.getCarId(), TbitProtocol.KEY_GLOBAL_STATUS, status);
                }
                sendBroadcast(new Intent(TbitProtocol.BROADCAST_RECEIVER_LOGGED_IN));
                isA1FirstBound = false;
                break;
            case NETWORK_A1_UNBIND:
                mNetworkUtil.loadJsonData(NETWORK_TAG_A1_CAT_DATA, NetworkProtocol.URL_A1_BINDING_QUERY, null);
                showLoading();
                break;
            case NETWORK_TAG_A1_CAT_DATA:
                try {
                    List<A1Entity> a1s = gson.fromJson(jsonObject.getResult(), new TypeToken<List<A1Entity>>(){}.getType());
                    application.a1Cars.clear();
                    application.a1Cars.addAll(a1s);
                    application.saveA1Data(a1s);
                    if (!application.hasAnyCarBound()) {
                        holder.setVisibility(View.VISIBLE);
                    }
                } catch (JsonSyntaxException e) {
                } catch (NullPointerException e) {
                }
                loadFromLocal();
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        if (!isVisible()) {
            return;
        }
        hideLoading();
        showToast("当前网络不给力");
    }

    @Override
    public void onProgress(long bytesWritten, long totalSize) {

    }

    private void hideLoading() {
        isLoading = false;
        progressbar.setVisibility(View.GONE);
    }

    private void showLoading() {
        isLoading = true;
        progressbar.setVisibility(View.VISIBLE);
    }

    private void sendBroadcast(Intent intent) {
        LocalBroadcastManager.getInstance(getContext())
                .sendBroadcast(intent);
    }

    private void showToast(String s) {
        if (mToast == null) {
            mToast = Toast.makeText(MyApplication.getInstance(), "", Toast.LENGTH_SHORT);
        }
        mToast.setText(s);
        mToast.show();
    }

    public interface BindListener {
        void onBindChange(Car car);
    }

    static class Binding {
        String carId;
        String machineNo;
        int type;
        boolean isSelected;

        public Binding(String carId, String machineNo, int type, boolean isSelected) {
            this.carId = carId;
            this.machineNo = machineNo;
            this.type = type;
            this.isSelected = isSelected;
        }
    }

    static class BindingFactory {
        public static Binding creator(String carId, String machineNo, int type, boolean isSelected) {
            return new Binding(carId, machineNo, type, isSelected);
        }

        public static Binding A2Creator(String carId, String machineNo, boolean isSelected) {
            return creator(carId, machineNo, TbitProtocol.TYPE_A2, isSelected);
        }

        public static Binding A2Creator(String carId, String machineNo) {
            return A2Creator(carId, machineNo, false);
        }

        public static Binding A1Creator(String carId, boolean isSelected) {
            return creator(carId, String.valueOf(carId), TbitProtocol.TYPE_A1, isSelected);
        }

        public static Binding A1Creator(String carId) {
            return A1Creator(carId, false);
        }
    }
}
