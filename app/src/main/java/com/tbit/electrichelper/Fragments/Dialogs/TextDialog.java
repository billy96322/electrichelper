package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tbit.electrichelper.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Salmon on 2016/4/28 0028.
 */
public class TextDialog extends DialogFragment {
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.text)
    TextView text;
    @Bind(R.id.text_dialog_neutral)
    TextView textDialogNeutral;
    @Bind(R.id.text_dialog_positive)
    TextView textDialogPositive;
    @Bind(R.id.text_dialog_negative)
    TextView textDialogNegative;

    private OnConfirmListener onConfirmListener;
    private OnCancelListener onCancelListener;
    private OnNeutralListener onNeutralListener;
    private String mTitle = "";
    private String mContent = "";
    private String mTextPositive;
    private String mTextNegative;
    private String mTextNeutral;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.text_dialog_fragment, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        text.setText(mContent);
        title.setText(mTitle);

        if (!TextUtils.isEmpty(mTextPositive)) {
            textDialogPositive.setText(mTextPositive);
        }
        if (!TextUtils.isEmpty(mTextNegative)) {
            textDialogNegative.setText(mTextNegative);
        }
        if (!TextUtils.isEmpty(mTextNeutral)) {
            textDialogNeutral.setText(mTextNeutral);
        }

        textDialogPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onConfirmListener != null) {
                    onConfirmListener.onConfirm();
                    dismiss();
                }
            }
        });

        textDialogNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCancelListener != null) {
                    onCancelListener.onCancel();
                } else {
                    dismiss();
                }
            }
        });

        textDialogNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNeutralListener != null) {
                    onNeutralListener.onNeutral();
                }
            }
        });
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setText(String content) {
        mContent = content;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        onConfirmListener = listener;
    }

    public void setOnCancelListener(OnCancelListener listener) {
        onCancelListener = listener;
    }

    public void setOnNeutralListener(OnNeutralListener listener) {
        onNeutralListener = listener;
    }

    public void setPositiveText(String textPositive) {
        mTextPositive = textPositive;
    }

    public void setNegativeText(String textNegative) {
        mTextNegative = textNegative;
    }

    public void setNeutralText(String textNeutral) {
        mTextNeutral = textNeutral;
    }

    public interface OnConfirmListener {
        void onConfirm();
    }

    public interface OnCancelListener {
        void onCancel();
    }

    public interface OnNeutralListener {
        void onNeutral();
    }
}
