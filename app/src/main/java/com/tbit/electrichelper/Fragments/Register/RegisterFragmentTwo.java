package com.tbit.electrichelper.Fragments.Register;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tbit.electrichelper.Activities.RegisterActivity;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkFragment;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;


public class RegisterFragmentTwo extends BaseNetworkFragment {
    private static final int NETWORK_TAG_VERIFICATION_CODE = 0;
    private static final int NETWORK_TAG_REGISTER = 1;

    private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    @Bind(R.id.edit_register_verification_code)
    EditText editRegisterVerificationCode;
    @Bind(R.id.text_register_get_verification_code)
    TextView textRegisterGetVerificationCode;
    @Bind(R.id.button_register_verification_next)
    Button buttonRegisterVerificationNext;
    int i = 61;// 倒计时的整个时间数
    @Bind(R.id.edit_passwords)
    EditText editPasswords;
    private String phone;
    private Handler handler = new Handler();
    private Handler mHandler = new Handler();// 全局handler
    private SMSBroadcastReceiver smsReceiver;
    private View rootView = null;
    private boolean isTipNeeded = true;
    private RegisterActivity registerActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        registerActivity = (RegisterActivity) parentActivity;
        isRunning = true;
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_register_2, container, false);
            ButterKnife.bind(this, rootView);
            initView();
        } else {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.removeView(rootView);
        }


        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        smsReceiver = new SMSBroadcastReceiver();
        // 实例化过滤器并设置要过滤的广播
        IntentFilter intentFilter = new IntentFilter(ACTION);
        intentFilter.setPriority(Integer.MAX_VALUE);
        // 注册广播
        getActivity().registerReceiver(smsReceiver, intentFilter);
    }

    private void initView() {
        buttonRegisterVerificationNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String authCode = editRegisterVerificationCode.getText().toString();
                if (authCode.length() != 6) {
                    showCroupton("请正确输入6位验证码");
                    return;
                }
                String psw = editPasswords.getText().toString();
                if (psw.length() == 0) {
                    showCroupton("密码不能为空");
                    return;
                }
                if (psw.length() < 6 || psw.length() > 12) {
                    showCroupton("密码长度有误，请输入6-12位密码");
                    return;
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put(NetworkProtocol.PARAMKEY_PLATFORM_SMS_AUTH_CODE, authCode);
                map.put(NetworkProtocol.PARAMKEY_PASSWORD, psw);
                map.put(NetworkProtocol.PARAMKEY_PLATEFORM_TYPE, String.valueOf(10));
                postNetworkJson(NETWORK_TAG_REGISTER, NetworkProtocol.URL_REGISTER, map, true);
            }
        });
        smsReceiver.setOnReceivedMessageListener(new SMSBroadcastReceiver.SmsListener() {
            @Override
            public void onReceived(String content) {
                String varifyCode = getVarifyCideFromSms(content);
                editRegisterVerificationCode.setText(varifyCode);
            }
        });
        textRegisterGetVerificationCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialogUtils.showProgressDialog(true);
                // 获取短信验证码
//                getIdentificationCode(((RegisterActivity)parentActivity).getPhone());
                Map<String, String> map = new HashMap<String, String>();
                map.put(NetworkProtocol.PARAMKEY_PHONE, registerActivity.getPhone());
                postNetworkJson(NETWORK_TAG_VERIFICATION_CODE, NetworkProtocol.URL_REGISTER_VARIFICATION_CODE, map, true);

            }
        });
        textRegisterGetVerificationCode.setClickable(false);
        new Thread(new ClassCut()).start();// 开启倒计时
        showCroupton("获得验证码成功", Style.CONFIRM);

    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case NETWORK_TAG_VERIFICATION_CODE:
                if (jsonObject.isRes()) {
                    textRegisterGetVerificationCode.setClickable(false);
                    showCroupton("获得验证码成功", Style.CONFIRM);
                    new Thread(new ClassCut()).start();// 开启倒计时
                } else {
                    showCroupton(jsonObject.getDesc());
                }
                break;
            case NETWORK_TAG_REGISTER:
                if (jsonObject.isRes()) {
                    registerActivity.setPasswords(editPasswords.getText().toString());
                    registerActivity.registerDoneDialog();
                } else {
                    showCroupton(jsonObject.getDesc());
                }
                break;
            default:
                break;
        }
    }

    private String getVarifyCideFromSms(String sms) {
        if (null == sms || sms.length() == 0) {
            return "";
        }
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(sms);
        return m.replaceAll("").trim();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(smsReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isRunning = false;
        ButterKnife.unbind(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        isTipNeeded = false;
    }

    class ClassCut implements Runnable {// 倒计时逻辑子线程

        @Override
        public void run() {
            while (i > 0) {// 整个倒计时执行的循环
                if (!isRunning) {
                    return;
                }
                i--;
                mHandler.post(new Runnable() {// 通过它在UI主线程中修改显示的剩余时间
                    @Override
                    public void run() {
                        if (!isRunning) {
                            return;
                        }
                        textRegisterGetVerificationCode.setText(i + "");// 显示剩余时间
                    }
                });
                try {
                    Thread.sleep(1000);// 线程休眠一秒钟 这个就是倒计时的间隔时间
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 下面是倒计时结束逻辑
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!isRunning) {
                        return;
                    }
                    textRegisterGetVerificationCode.setText("点击获取");// 一轮倒计时结束,修改剩余时间为一分钟
                    textRegisterGetVerificationCode.setClickable(true);
                    if (isTipNeeded) {
                        showCroupton("若未收到验证码，请重新获取", Style.INFO);
                    }
                }
            });
            i = 61;// 修改倒计时剩余时间变量为60秒
        }
    }

}
