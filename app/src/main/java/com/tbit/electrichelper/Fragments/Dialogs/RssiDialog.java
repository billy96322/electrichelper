package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tbit.electrichelper.R;

/**
 * Created by Salmon on 2016/4/26 0026.
 */
public class RssiDialog extends DialogFragment {
    private static final int SEEKBAR_MIN = 50;

    private SeekBar mRssiSeekBar;
    private TextView mConfirmButton;
    private TextView mCancelButton;
    private TextView mTitle;
    private TextView mNeutralButton;

    private OnConfirmListener mConfirmListener;
    private OnCancelListener mCancelListener;

    private int mSeekValue = SEEKBAR_MIN;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_custom_rssi, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRssiSeekBar = (SeekBar) view.findViewById(R.id.slider_rssi);
        mConfirmButton = (TextView) view.findViewById(R.id.text_dialog_positive);
        mCancelButton = (TextView) view.findViewById(R.id.text_dialog_negative);
        mNeutralButton = (TextView) view.findViewById(R.id.text_dialog_neutral);
        mTitle = (TextView) view.findViewById(R.id.title);

        mTitle.setText("终端布防距离");

        mRssiSeekBar.setProgress(mSeekValue);
        mNeutralButton.setText(String.valueOf(mSeekValue + SEEKBAR_MIN));

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConfirmListener != null) {
                    mConfirmListener.onConfirm(mSeekValue);
                }
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCancelListener != null) {
                    mCancelListener.onCancel();
                } else {
                    dismiss();
                }
            }
        });

        mRssiSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSeekValue = SEEKBAR_MIN + progress;
                mNeutralButton.setText(String.valueOf(mSeekValue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        mConfirmListener = listener;
    }

    public void setOnCancelListener(OnCancelListener listener) {
        mCancelListener = listener;
    }

    public interface OnConfirmListener {
        void onConfirm(int rssi);
    }

    public interface OnCancelListener {
        void onCancel();
    }

    public void setSeekValue(int value) {
        mSeekValue = value - SEEKBAR_MIN;
    }
}
