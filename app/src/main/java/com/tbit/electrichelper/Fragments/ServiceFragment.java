package com.tbit.electrichelper.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.tbit.electrichelper.Activities.BrowserActivity;
import com.tbit.electrichelper.Activities.LoginActivity;
import com.tbit.electrichelper.Activities.ServicePointMapActivity;
import com.tbit.electrichelper.Beans.News;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Util.BitmapCache;
import com.tbit.electrichelper.Util.TbitProtocol;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ServiceFragment extends BaseFragment {

    private static final int HANDLE_SWITCH_ITEM = 0;

    @Bind(R.id.listview)
    ListView listview;
    @Bind(R.id.rl_service_point)
    LinearLayout rlServicePoint;
    @Bind(R.id.rl_service_hotline)
    LinearLayout rlServiceHotline;
    @Bind(R.id.rl_service_around)
    RelativeLayout rlServiceAround;

    private List<News> newses = new ArrayList<>();
    private NewsAdapter mAdapter;
    private Handler mHandler;
    private MyApplication application;
    public static final String TAG = ServiceFragment.class.getSimpleName();

    private int itemFlag = 0;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rl_service_around:
                    if (application.getLoginStatus() != TbitProtocol.LOGGED_OUT) {
                        if (application.hasA2Bound()) {
                            if (application.getLoginStatus() == TbitProtocol.LOGGED_IN) {
                                enterActivity(ServicePointMapActivity.class);
                            } else {
                                showCroupton(R.string.tip_only_logged_in_user_positive);
                            }
                        } else {
                            addBinding();
                        }
                    } else {
                        enterActivity(LoginActivity.class);
                    }
                    break;
                case R.id.rl_service_point: {
                    /*Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    Uri content_url = Uri.parse("http://m.yadea.com.cn/service/wangdian/");
                    intent.setData(content_url);
                    startActivity(intent);*/
                    BrowserActivity.startActivity(getActivity(), "http://m.yadea.com.cn/service/wangdian/");
                    break;
                }
                case R.id.rl_service_hotline: {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "400-600-9566"));
                    startActivity(intent);
                    break;
                }
            }
        }
    };

    public ServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        ButterKnife.bind(this, view);
        application = MyApplication.getInstance();

        initListData();
        mAdapter = new NewsAdapter(getActivity(), newses);
        listview.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BrowserActivity.startActivity(getActivity(), newses.get(position).getLinkUrl());
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isRunning) {
                    return;
                }
                super.handleMessage(msg);
                switch (msg.what) {
                    case HANDLE_SWITCH_ITEM:
                        if (itemFlag > newses.size() - 1) {
                            break;
                        }
                        listview.smoothScrollToPosition(itemFlag);
                        itemFlag++;
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendEmptyMessage(HANDLE_SWITCH_ITEM);
                            }
                        }, 1000);
                        break;
                    default:
                        break;
                }
            }
        };

        rlServiceHotline.setOnClickListener(onClickListener);
        rlServiceAround.setOnClickListener(onClickListener);
        rlServicePoint.setOnClickListener(onClickListener);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        isRunning = false;
    }

    private void initListData() {
        newses.clear();

        /*for (String url : TestImages.imageThumbUrls) {
            News news = new News();
            news.setTitle("【Big News】Tesla confirms: Model 3 will be unveiled in March");
            news.setDescribe("Since its release in 2012, the performance, ingenuity, safety, and commercial success of Tesla’s Model S");
            news.setImageUrl(url);
            newses.add(news);
        }*/

        News news1 = new News();
        news1.setLinkUrl("http://www.yadea.com.cn/service/dongtai/detail152.html");
        news1.setImageResource(R.drawable.pic_news1);
        news1.setTitle("【新闻】雅迪2015新品发布会");
        news1.setDescribe("伴随着激烈的市场竞争，消费者的消费行为日益表现出个性化，情感化和直接参与等偏好.");
        newses.add(news1);

        News news2 = new News();
        news2.setLinkUrl("http://www.yadea.com.cn/service/dongtai/detail151.html");
        news2.setImageResource(R.drawable.pic_news2);
        news2.setTitle("【新闻】雅迪售后服务活动风采展示");
        news2.setDescribe("2015年，公司启动\"雅迪——更高端的电动车\"品牌战略，在服务文化逐渐成为企业核心竞争力的大趋势下，作为雅迪授权的VIP商家.");
        newses.add(news2);

        News news3 = new News();
        news3.setLinkUrl("http://www.yadea.com.cn/service/dongtai/detail65.html");
        news3.setImageResource(R.drawable.pic_news3);
        news3.setTitle("【动态】雅迪电动车缘何成为行业唯一五星级售后认证企业");
        news3.setDescribe("随着电动车行业发展日臻成熟，产品同质化趋势在所难免，这使得消费者在选购产品时存在诸多随意性。售后服务成为企业新的购买力，成为影响消费者的一大因素。");
        newses.add(news3);

    }

    private void test(String url) {
        MyApplication application = MyApplication.getInstance();
        RequestQueue requestQueue = application.getRequestQueue();
        ImageLoader imageLoader = new ImageLoader(requestQueue, new BitmapCache());
    }

}
