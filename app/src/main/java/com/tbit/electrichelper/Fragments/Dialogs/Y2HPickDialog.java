package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.tbit.electrichelper.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Salmon on 2016/6/30 0030.
 */
public class Y2HPickDialog extends DialogFragment {
    private NumberPicker yearPicker;
    private NumberPicker monthPicker;
    private NumberPicker dayPicker;
    private NumberPicker hourPicker;
    private TextView mTextConfirm, mTextCancel;
    private int year;
    private int month;
    private int day;
    private int hour;
    private NumberPicker.OnValueChangeListener mValueChangeListener =
            new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    switch (picker.getId()) {
                        case R.id.picker_year:
                            resetMonth();
                            break;
                        case R.id.picker_month:
                            resetDay();
                            break;
                        case R.id.picker_day:
                            resetHour();
                            break;
                        case R.id.picker_hour:
                            break;
                    }
                }
            };
    private OnTimeSelectListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_time, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        yearPicker = (NumberPicker) view.findViewById(R.id.picker_year);
        monthPicker = (NumberPicker) view.findViewById(R.id.picker_month);
        dayPicker = (NumberPicker) view.findViewById(R.id.picker_day);
        hourPicker = (NumberPicker) view.findViewById(R.id.picker_hour);

        mTextCancel = (TextView) view.findViewById(R.id.text_dialog_negative);
        mTextConfirm = (TextView) view.findViewById(R.id.text_dialog_positive);

        mTextConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onTimeSelected(parseResult());
                dismiss();
            }
        });

        mTextCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onCanceled();
                dismiss();
            }
        });

        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);

        initPicker();
    }

    private void initPicker() {
        yearPicker.setOnValueChangedListener(mValueChangeListener);
        monthPicker.setOnValueChangedListener(mValueChangeListener);
        dayPicker.setOnValueChangedListener(mValueChangeListener);
        hourPicker.setOnValueChangedListener(mValueChangeListener);

        yearPicker.setMinValue(year);
        yearPicker.setMaxValue(year + 1);

        monthPicker.setMinValue(month);
        monthPicker.setMaxValue(12);

        dayPicker.setMinValue(day);
        dayPicker.setMaxValue(lastDayInMonth(year, month));

        hourPicker.setMinValue(hour);
        hourPicker.setMaxValue(23);
    }

    private int lastDayInMonth(int year, int month) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    private String parseResult() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, yearPicker.getValue());
        calendar.set(Calendar.MONTH, monthPicker.getValue() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, dayPicker.getValue());
        calendar.set(Calendar.HOUR_OF_DAY, hourPicker.getValue());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
        return format.format(calendar.getTime());
    }

    public void setOnTimeSelectListener(OnTimeSelectListener listener) {
        mListener = listener;
    }

    private void resetMonth() {
        if (year == yearPicker.getValue()) {
            monthPicker.setMinValue(month);
        } else {
            monthPicker.setMinValue(1);
        }
        resetDay();
    }

    private void resetDay() {
        int dayCount = lastDayInMonth(yearPicker.getValue(), monthPicker.getValue());
        dayPicker.setMaxValue(dayCount);
        if (year == yearPicker.getValue() &&
                month == monthPicker.getValue()) {
            dayPicker.setMinValue(day);
        } else {
            dayPicker.setMinValue(1);
        }
        resetHour();
    }

    private void resetHour() {
        if (year == yearPicker.getValue() && month == monthPicker.getValue() &&
                day == dayPicker.getValue()) {
            hourPicker.setMinValue(hour);
        } else {
            hourPicker.setMinValue(0);
        }
    }

    public interface OnTimeSelectListener {
        void onTimeSelected(String timeString);
        void onCanceled();
    }
}
