package com.tbit.electrichelper.Fragments.Register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tbit.electrichelper.Activities.RegisterActivity;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkFragment;
import com.tbit.electrichelper.Util.NetworkProtocol;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RegisterFragmentOne extends BaseNetworkFragment {
    private final int NETWORK_TAG_PHONE_SUBMIT = 0;

    @Bind(R.id.edit_register_phone_number)
    EditText editRegisterPhoneNumber;
    @Bind(R.id.button_register_phone_next)
    Button buttonRegisterPhoneNext;
    @Bind(R.id.button_register_already_registered)
    Button buttonRegisterAlreadyRegistered;

    RegisterActivity registerActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_1, container, false);
        ButterKnife.bind(this, view);
        buttonRegisterPhoneNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editRegisterPhoneNumber.getText() == null) {
                    return;
                }
                String phoneNum = editRegisterPhoneNumber.getText().toString();
                if (phoneNum.length() <= 0) {
                    showCroupton("请输入手机号");
                    return;
                }
                if (phoneNum.length() != 11) {
                    showCroupton("手机号位数为11位");
                    return;
                }
                registerActivity.setPhone(phoneNum);
                Map<String, String> map = new HashMap<String, String>();
                map.put(NetworkProtocol.PARAMKEY_PHONE, registerActivity.getPhone());
                postNetWorkWithSavingSession(NETWORK_TAG_PHONE_SUBMIT, NetworkProtocol.URL_REGISTER_VARIFICATION_CODE, map, true);

            }
        });

        return view;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        super.parseResults(jsonObject, tag);
        switch (tag) {
            case NETWORK_TAG_PHONE_SUBMIT:
                if (jsonObject.isRes()) {
                    registerActivity.toNext();
                } else {
                    showCroupton(jsonObject.getDesc());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.registerActivity = (RegisterActivity) super.parentActivity;
    }

    @Override
    public void onStart() {
        buttonRegisterAlreadyRegistered.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.finish();
            }
        });
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
