package com.tbit.electrichelper.Fragments.Register;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SMSBroadcastReceiver extends BroadcastReceiver{

	private static SmsListener mMessageListener;
	public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {
			Object[] pdus = (Object[]) intent.getExtras().get("pdus");
			for(Object pdu:pdus) {
				SmsMessage smsMessage = SmsMessage.createFromPdu((byte [])pdu);
				String sender = smsMessage.getDisplayOriginatingAddress();
				//短信内容
				String content = smsMessage.getDisplayMessageBody();
				long date = smsMessage.getTimestampMillis();
				Date tiemDate = new Date(date);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String time = simpleDateFormat.format(tiemDate);

	                /*//过滤不需要读取的短信的发送号码
	                if ("1069800040082705".equals(sender) && mMessageListener != null) {
	                    mMessageListener.onReceived(content);
	                    abortBroadcast();
	                }*/

				if (mMessageListener != null) {
					mMessageListener.onReceived(content);
					abortBroadcast();
				}
			}
		}
	}

	public interface SmsListener{
		public void onReceived(String content);
	}

	public void setOnReceivedMessageListener(SmsListener messageListener) {
		this.mMessageListener = messageListener;
	}
}
