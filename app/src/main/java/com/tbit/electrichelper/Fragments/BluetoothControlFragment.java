package com.tbit.electrichelper.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.tbit.electrichelper.Activities.LoginActivity;
import com.tbit.electrichelper.Activities.ScanActivity;
import com.tbit.electrichelper.Activities.ShareActivity;
import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.Bluetooth.Constant;
import com.tbit.electrichelper.Bluetooth.Interface.BluetoothParseServiceImpl;
import com.tbit.electrichelper.Bluetooth.Util.VibratorUtil;
import com.tbit.electrichelper.Fragments.Dialogs.EditTextDialog;
import com.tbit.electrichelper.Fragments.Dialogs.RadioGroupDialog;
import com.tbit.electrichelper.Fragments.Dialogs.RssiDialog;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Services.PacketParserService;
import com.tbit.electrichelper.Services.UartService;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;
import com.tbit.electrichelper.Views.SettingItem.SettingItem;
import com.tbit.electrichelper.Views.SettingItem.TextSettingItem;
import com.tbit.electrichelper.Views.SettingItem.ToggleSettingItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Kenny on 2016/2/22 11:02.
 * Destroyed by Salmon on 2016/5/24 17:24
 * Desc：A fragment contain many button to control the remote bluetooth.
 */
public class BluetoothControlFragment extends BaseFragment implements View.OnClickListener,
        ISimpleDialogListener {

    private static final String TAG = BluetoothControlFragment.class.getSimpleName();
    private static final int wannaRestart = 1;
    private static final int wannaUpdate = 2;
    private static final int wannaReset = 3;
    private static final int AUTO_CLOSE_DURATION = 5000;
    // 询问用户是否执行此操作
    private static final int REQUEST_ASK_DIALOG = 1001;
    // 弹出提示，询问用户是否要切换当前的蓝牙连接的设备
    private static final int REQUEST_SWITCH_DIALOG = 1002;
    private static final int REQUEST_RECONNECT_DIALOG = 1003;
    //    public static boolean isStateRefreshNeeded = true;
//    public static boolean isUIRefreshNeeded = true;
    @Bind(R.id.setting_prevention)
    ToggleSettingItem settingPrevention;
    @Bind(R.id.setting_findCar)
    ToggleSettingItem settingFindCar;
    @Bind(R.id.setting_homeMode)
    ToggleSettingItem settingHomeMode;
    @Bind(R.id.setting_silence)
    ToggleSettingItem settingSilence;
    @Bind(R.id.setting_fast_start)
    ToggleSettingItem settingFastStart;
    @Bind(R.id.setting_control_mode)
    TextSettingItem settingControlMode;
    @Bind(R.id.setting_auto_defence)
    TextSettingItem settingAutoDefence;
    @Bind(R.id.setting_defence_distance)
    TextSettingItem settingDefenceDistance;
    @Bind(R.id.setting_sensibility)
    TextSettingItem settingSensibility;
    @Bind(R.id.setting_terminal_voltage)
    TextSettingItem settingTerminalVoltage;
    @Bind(R.id.setting_wheel_radius)
    TextSettingItem settingWheelRadius;
    @Bind(R.id.setting_mileage_calibration)
    TextSettingItem settingMileageCalibration;
    @Bind(R.id.setting_query_voltage)
    TextSettingItem settingQueryVoltage;
    @Bind(R.id.setting_query_wheel_tern)
    TextSettingItem settingQueryWheelTern;
    @Bind(R.id.setting_query_temperature)
    TextSettingItem settingQueryTemperature;
    @Bind(R.id.setting_query_speed)
    TextSettingItem settingQuerySpeed;
    @Bind(R.id.setting_query_tested)
    TextSettingItem settingQueryTested;
    @Bind(R.id.setting_device_id)
    TextSettingItem settingDeviceId;
    @Bind(R.id.setting_ble_version)
    TextSettingItem settingBleVersion;
    @Bind(R.id.setting_restart)
    TextSettingItem settingRestart;
    @Bind(R.id.setting_firmware_update)
    TextSettingItem settingFirmwareUpdate;
    @Bind(R.id.setting_factory_reset)
    TextSettingItem settingFactoryReset;
    @Bind(R.id.folder_setting)
    FrameLayout folderSetting;
    @Bind(R.id.folder_query)
    FrameLayout folderQuery;
    @Bind(R.id.folder_system)
    FrameLayout folderSystem;
    @Bind(R.id.share)
    SettingItem share;
    @Bind(R.id.checkbox_folder_setting)
    CheckBox checkboxFolderSetting;
    @Bind(R.id.checkbox_folder_query)
    CheckBox checkboxFolderQuery;
    @Bind(R.id.checkbox_folder_system)
    CheckBox checkboxFolderSystem;
    @Bind(R.id.setting_device_reconnect)
    TextSettingItem settingDeviceReconnect;

    private int mCurAction;
    // 用户设置自己的仪表盘的里程数
    private EditTextDialog mMileageDialog;
    // 用户设置自己的车轮轮径
    private RadioGroupDialog wheelDialog;
    private List<RadioButton> wheelButtons;
    // 用户设置自己都电池电压
    private RadioGroupDialog voltageDialog;
    private List<RadioButton> voltageButtons;
    private RadioGroupDialog mSingleRadioDialog;
    private List<RadioButton> singleButtons;
    private RssiDialog mRssiDialog;
    private RadioGroupDialog vibDialog;
    private List<RadioButton> vibButtons;
    private BluetoothParseServiceImpl mListener;
    // handler
    private Handler mAutoCloseHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!isRunning)
                return;
            int request = msg.what;
            SettingItem item = mData.get(request);
            if (item != null && item.isProgressing()) {
                item.setProgressing(false);
            }
        }
    };
    private BroadcastReceiver cfReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constant.ACTION_DEVICE_CONN_OK)) {
                String name = intent.getStringExtra(Constant.EXTRA_DATA);
                settingDeviceId.setRightText(name);
            }
        }
    };
    private CompoundButton.OnCheckedChangeListener mOnCheckChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.checkbox_folder_setting:
                    fold(mSettingFoldItems, isChecked);
                    break;
                case R.id.checkbox_folder_query:
                    fold(mQueryFoldItems, isChecked);
                    break;
                case R.id.checkbox_folder_system:
                    fold(mSystemFoldItems, isChecked);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(cfReceiver, myFilter());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_control, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        initUIState();
    }

    private ConcurrentHashMap<Integer, SettingItem> mData = new ConcurrentHashMap<>();
    private Map<Integer, String> mStateData = new HashMap<>();

    private void init() {
        mData.put(Constant.REQUEST_HOME_MODE, settingHomeMode);
        mData.put(Constant.REQUEST_SILENCE, settingSilence);
        mData.put(Constant.REQUEST_FAST_START, settingFastStart);
        mData.put(Constant.REQUEST_CONTROL, settingControlMode);
        mData.put(Constant.REQUEST_AUTO_DEFENCE, settingAutoDefence);
        mData.put(Constant.REQUEST_RSSI, settingDefenceDistance);
        mData.put(Constant.REQUEST_SENSIBILITY, settingSensibility);
        mData.put(Constant.REQUEST_TERMINAL_VOLTAGE, settingTerminalVoltage);
        mData.put(Constant.REQUEST_WHEEL_RADIUS, settingWheelRadius);
        mData.put(Constant.REQUEST_MILEAGE_CALIBRATION, settingMileageCalibration);
        mData.put(Constant.REQUEST_QUERY_VOLTAGE, settingQueryVoltage);
        mData.put(Constant.REQUEST_QUERY_WHEEL_TERN, settingQueryWheelTern);
        mData.put(Constant.REQUEST_QUERY_TEMPERATURE, settingQueryTemperature);
        mData.put(Constant.REQUEST_QUERY_SPEED, settingQuerySpeed);
        mData.put(Constant.REQUEST_QUERY_TESTED, settingQueryTested);
        mData.put(Constant.REQUEST_RESTART, settingRestart);
        mData.put(Constant.REQUEST_UPDATE, settingFirmwareUpdate);
        mData.put(Constant.REQUEST_RESET, settingFactoryReset);

        settingDeviceReconnect.setOnClickListener(this);

        for (Map.Entry<Integer, SettingItem> entry : mData.entrySet()) {
            entry.getValue().setOnClickListener(this);
        }

        settingDeviceId.setOnClickListener(this);

        settingAutoDefence.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "textHeight: " + TbitUtil.px2dip(getActivity(), settingAutoDefence.getHeight()));
            }
        });

        settingHomeMode.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "toggleHeight: " + TbitUtil.px2dip(getActivity(), settingHomeMode.getHeight()));
            }
        });

        View.OnClickListener folderListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.folder_setting:
                        checkboxFolderSetting.performClick();
                        break;
                    case R.id.folder_query:
                        checkboxFolderQuery.performClick();
                        break;
                    case R.id.folder_system:
                        checkboxFolderSystem.performClick();
                        break;
                }
            }
        };

        folderQuery.setOnClickListener(folderListener);
        folderSetting.setOnClickListener(folderListener);
        folderSystem.setOnClickListener(folderListener);

        checkboxFolderSetting.setOnCheckedChangeListener(mOnCheckChangeListener);
        checkboxFolderQuery.setOnCheckedChangeListener(mOnCheckChangeListener);
        checkboxFolderSystem.setOnCheckedChangeListener(mOnCheckChangeListener);

        initFolders();

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (application.getLoginStatus() != TbitProtocol.LOGGED_IN) {
                    enterActivity(LoginActivity.class);
                } else {
                    if (application.hasA1Bound()) {
                        enterActivity(ShareActivity.class);
                    } else {
                        enterActivity(ScanActivity.class);
                    }
                }
            }
        });
    }

    private PacketParserService getPacketParseService() {
        return mListener.getPacketParserService();
    }

    @Override
    public void onClick(View v) {
        //暂时注释掉
        if (mListener.getUartService().getGatt() == null) {
            startActivity(new Intent(getActivity(), ScanActivity.class));
            return;
        }
        if (mListener.getUartService().getConnectionState() != UartService.STATE_CONNECTED) {
//            startActivity(new Intent(getActivity(), ScanActivity.class));
            showReConnectDialog();
            return;
        }
        if (v instanceof SettingItem && ((SettingItem) v).isProgressing()) {
            ((SettingItem) v).shake();
            return;
        }
        //暂时注释掉
        VibratorUtil.Vibrate(getActivity(), 50);
        switch (v.getId()) {
            case R.id.setting_control_mode:
                showSingleChoiceDialog("手机", "手柄", settingControlMode, 1);
                break;
            case R.id.setting_auto_defence:
                showSingleChoiceDialog("手动", "自动", settingAutoDefence, 2);
                break;
            case R.id.setting_defence_distance:
                showSettingRssiDialog();
                break;
            case R.id.setting_sensibility:
                showVibChoiceDialog(settingSensibility.getRightText());
                break;
            case R.id.setting_terminal_voltage:
                //todo 待处理
                showSettingVoltageDialog();
                break;
            case R.id.setting_wheel_radius:
                //todo 待处理
                showSettingWheelDiameterDialog();
                break;
            case R.id.setting_mileage_calibration:
                //todo 待处理
                showSettingMileageDialog();
                break;
            case R.id.setting_query_voltage:
                getPacketParseService().send(Constant.REQUEST_QUERY_VOLTAGE, Constant.COMMAND_QUERY,
                        Constant.SEND_KEY_QUERY_VOLTAGE, null);
                break;
            case R.id.setting_query_wheel_tern:
                getPacketParseService().send(Constant.REQUEST_QUERY_WHEEL_TERN, Constant.COMMAND_QUERY,
                        Constant.SEND_KEY_QUERY_WHEEL_TERN, null);
                break;
            case R.id.setting_query_temperature:
                getPacketParseService().send(Constant.REQUEST_QUERY_TEMPERATURE, Constant.COMMAND_QUERY,
                        Constant.SEND_KEY_QUERY_TEMPERATURE, null);
                break;
            case R.id.setting_query_speed:
                getPacketParseService().send(Constant.REQUEST_QUERY_SPEED, Constant.COMMAND_QUERY,
                        Constant.SEND_KEY_QUERY_SPEED, null);
                break;
            case R.id.setting_query_tested:
                getPacketParseService().send(Constant.REQUEST_QUERY_TESTED, Constant.COMMAND_QUERY,
                        Constant.SEND_KEY_QUERY_TESTED, null);
                break;
            case R.id.setting_firmware_update:
                showAskDialog(wannaRestart);
                break;
            case R.id.setting_device_id:
//                showSwitchConnDialog();
                break;
            case R.id.setting_ble_version:
                break;
            case R.id.setting_restart:
                showAskDialog(wannaRestart);
                break;
            case R.id.setting_factory_reset:
                showAskDialog(wannaRestart);
                break;
            case R.id.setting_findCar:
                setValue(Constant.REQUEST_FIND_CAR, Constant.COMMAND_SETTING, Constant.SEND_KEY_FIND,
                        new Byte[]{!settingFindCar.isCheck() ? Constant.VALUE_ON : Constant.VALUE_OFF});
                break;
            case R.id.setting_homeMode:
                setValue(Constant.REQUEST_HOME_MODE, Constant.COMMAND_SETTING, Constant.SEND_KEY_HOME_MODE,
                        new Byte[]{!settingHomeMode.isCheck() ? Constant.VALUE_ON : Constant.VALUE_OFF});
                break;
            case R.id.setting_silence:
                setValue(Constant.REQUEST_SILENCE, Constant.COMMAND_SETTING, Constant.SEND_KEY_SILENCE,
                        new Byte[]{!settingSilence.isCheck() ? Constant.VALUE_ON : Constant.VALUE_OFF});
                break;
            case R.id.setting_fast_start:
                boolean isFastStart = settingFastStart.isCheck();
                if (isFastStart) {
                    setValue(Constant.REQUEST_FAST_START, Constant.COMMAND_SETTING, Constant.SEND_KEY_FAST_START,
                            new Byte[]{Constant.VALUE_OFF});
                } else {
                    setValue(Constant.REQUEST_FAST_START, Constant.COMMAND_SETTING, Constant.SEND_KEY_FAST_START,
                            new Byte[]{Constant.VALUE_ON});
                    ((TextSettingItem) mData.get(Constant.REQUEST_AUTO_DEFENCE)).setRightText("自动");
                    mAutoCloseHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setValue(Constant.REQUEST_AUTO_DEFENCE, Constant.COMMAND_SETTING, Constant.SEND_KEY_AUTO_DEFENCE,
                                    new Byte[]{Constant.VALUE_AUTO});
                        }
                    }, 500);
                }
                break;
            case R.id.setting_device_reconnect:
                showSwitchConnDialog();
                break;
        }

    }

    private void autoClose(int request) {
        mAutoCloseHandler.sendEmptyMessageDelayed(request, AUTO_CLOSE_DURATION);
    }

    private void showAskDialog(final int action) {
        mCurAction = action;
        SimpleDialogFragment.createBuilder(getActivity(), getFragmentManager())
                .setTitle("请再次确认您的操作")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setCancelable(false)
                .setTargetFragment(this, REQUEST_ASK_DIALOG)
                .show();
    }

    private void showSettingMileageDialog() {
        if (mMileageDialog == null) {
            mMileageDialog = new EditTextDialog();
            mMileageDialog.setTitle("设置里程数");
            mMileageDialog.setCancelable(false);
            mMileageDialog.setInputType(InputType.TYPE_CLASS_NUMBER);
            mMileageDialog.setEditTextHint("请输入您的里程数");
            mMileageDialog.setEditTextListener(new EditTextDialog.EditTextListener() {
                @Override
                public void onConfirm(String editString) {
                    Log.i(TAG, "==>>" + editString);
                    int i = Integer.parseInt(editString);
                    settingMileageCalibration.setRightText(String.valueOf(i));
                    mMileageDialog.dismiss();
                }

                @Override
                public void onCancel() {
                    mMileageDialog.dismiss();
                }

                @Override
                public void onNeutral() {

                }
            });
        }
        mMileageDialog.setEditTextContent(settingMileageCalibration.getRightText());
        mMileageDialog.show(getFragmentManager(), null);
    }

    private void showSettingWheelDiameterDialog() {
        if (wheelDialog == null || wheelButtons == null) {
            wheelDialog = new RadioGroupDialog();
            wheelDialog.setTitle("请设置您的轮径(单位：寸)");
            wheelButtons = new ArrayList<>();
            AppCompatRadioButton rb1 = new AppCompatRadioButton(getActivity());
            AppCompatRadioButton rb2 = new AppCompatRadioButton(getActivity());

            rb1.setText("10");
            rb2.setText("16");

            wheelButtons.add(rb1);
            wheelButtons.add(rb2);

            wheelDialog.setOnConfirmListener(new RadioGroupDialog.OnConfirmListener() {
                @Override
                public void onConfirm(int id) {
                    String res = wheelButtons.get(id).getText().toString();
                    settingWheelRadius.setRightText(res);
                    wheelDialog.dismiss();
                }
            });

            wheelDialog.setRadioList(wheelButtons);
            wheelDialog.setCancelable(false);
        }

        int count = wheelButtons.size();
        String text = settingWheelRadius.getRightText();
        if (!TextUtils.isEmpty(text)) {
            for (int i = 0; i < count; i++) {
                RadioButton rb = wheelButtons.get(i);
                if (text.equals(rb.getText().toString())) {
                    wheelDialog.setDefaultSelect(i);
                    break;
                }
            }
        }

        if (!wheelDialog.isAdded()) {
            wheelDialog.show(getFragmentManager(), null);
        }

    }

    private void showSettingVoltageDialog() {
        if (voltageDialog == null || voltageButtons == null) {
            voltageDialog = new RadioGroupDialog();
            voltageDialog.setTitle("请设置您的电池电压");

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            AppCompatRadioButton rb12, rb36, rb48, rb60, rb64, rb72;
            rb12 = new AppCompatRadioButton(getActivity());
            rb36 = new AppCompatRadioButton(getActivity());
            rb48 = new AppCompatRadioButton(getActivity());
            rb60 = new AppCompatRadioButton(getActivity());
            rb64 = new AppCompatRadioButton(getActivity());
            rb72 = new AppCompatRadioButton(getActivity());

            rb12.setLayoutParams(params);
            rb36.setLayoutParams(params);
            rb48.setLayoutParams(params);
            rb60.setLayoutParams(params);
            rb64.setLayoutParams(params);
            rb72.setLayoutParams(params);

            rb12.setText("12V");
            rb36.setText("36V");
            rb48.setText("48V");
            rb60.setText("60V");
            rb64.setText("64V");
            rb72.setText("72V");

            voltageButtons = new ArrayList<>();
            voltageButtons.add(rb12);
            voltageButtons.add(rb36);
            voltageButtons.add(rb48);
            voltageButtons.add(rb60);
            voltageButtons.add(rb64);
            voltageButtons.add(rb72);

            voltageDialog.setRadioGroupOrientation(RadioGroup.VERTICAL);
            voltageDialog.setRadioList(voltageButtons);
            voltageDialog.setOnConfirmListener(new RadioGroupDialog.OnConfirmListener() {
                @Override
                public void onConfirm(int id) {
                    String voltageText = voltageButtons.get(id).getText().toString();
                    settingTerminalVoltage.setRightText(voltageText);
                    // todo 发送设置电压的指令
                    int voltage = Integer.valueOf(voltageText.substring(0, voltageText.lastIndexOf("V")));
                    Log.i(TAG, "--用户设置的voltage=" + voltage);
                    setValue(Constant.REQUEST_TERMINAL_VOLTAGE, Constant.COMMAND_SETTING,
                            Constant.SEND_KEY_TERMINAL_VOLTAGE, new Byte[]{(byte) voltage});
                    voltageDialog.dismiss();
                }
            });
            voltageDialog.setCancelable(false);
        }
        int count = voltageButtons.size();
        String text = settingTerminalVoltage.getRightText();
        if (!TextUtils.isEmpty(text)) {
            for (int i = 0; i < count; i++) {
                RadioButton rb = voltageButtons.get(i);
                if (rb.getText().equals(text)) {
                    rb.setChecked(true);
                    break;
                }
            }
        }
        if (!voltageDialog.isAdded()) {
            voltageDialog.show(getFragmentManager(), null);
        }
    }

    private void showSwitchConnDialog() {
        SimpleDialogFragment.createBuilder(parentActivity, getChildFragmentManager())
                .setMessage("是否要断开当前连接并重新搜索设备")
                .setPositiveButtonText(R.string.confirm)
                .setNegativeButtonText(R.string.cancel)
                .setTargetFragment(this, REQUEST_SWITCH_DIALOG)
                .setCancelable(false)
                .show();
    }

    private void showSingleChoiceDialog(final String choice1, final String choice2, final TextSettingItem item, final int type) {
        mSingleRadioDialog = new RadioGroupDialog();
        mSingleRadioDialog.setTitle("请选择");
        final AppCompatRadioButton radio1 = new AppCompatRadioButton(getActivity());
        final AppCompatRadioButton radio2 = new AppCompatRadioButton(getActivity());
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        radio1.setLayoutParams(params);
        radio1.setText(choice1);
        radio2.setLayoutParams(params);
        radio2.setText(choice2);
        singleButtons = new ArrayList<>();
        singleButtons.add(radio1);
        singleButtons.add(radio2);
        mSingleRadioDialog.setRadioList(singleButtons);
        mSingleRadioDialog.setOnConfirmListener(new RadioGroupDialog.OnConfirmListener() {
            @Override
            public void onConfirm(int id) {
                if (id == 0) {
                    item.setRightText(singleButtons.get(0).getText().toString().trim());
                    //暂时注释掉
                    if (type == 1) {
                        setValue(Constant.REQUEST_CONTROL, Constant.COMMAND_SETTING,
                                Constant.SEND_KEY_CONTROL, new Byte[]{Constant.VALUE_PHONE_CONTROL});

                    } else {
                        setValue(Constant.REQUEST_AUTO_DEFENCE, Constant.COMMAND_SETTING,
                                Constant.SEND_KEY_AUTO_DEFENCE, new Byte[]{Constant.VALUE_MANUAL});
                    }
                } else {
                    item.setRightText(singleButtons.get(1).getText().toString().trim());
                    if (type == 1) {
                        setValue(Constant.REQUEST_CONTROL, Constant.COMMAND_SETTING,
                                Constant.SEND_KEY_CONTROL, new Byte[]{Constant.VALUE_SHRANK_CONTROL});
                    } else {
                        setValue(Constant.REQUEST_AUTO_DEFENCE, Constant.COMMAND_SETTING,
                                Constant.SEND_KEY_AUTO_DEFENCE, new Byte[]{Constant.VALUE_AUTO});
                    }
                }
                mSingleRadioDialog.dismiss();
            }
        });
        if (choice1.equals(item.getRightText())) {
            singleButtons.get(0).setChecked(true);
        } else {
            singleButtons.get(1).setChecked(true);
        }

        if (!mSingleRadioDialog.isAdded()) {
            mSingleRadioDialog.show(getFragmentManager(), null);
        }
    }

    private void showReConnectDialog() {
        SimpleDialogFragment.createBuilder(parentActivity, getChildFragmentManager())
                .setMessage("当前设备已断开，系统会自动重新连接。您也可以选择手动手动断开连接并且重新搜索设备")
                .setPositiveButtonText("手动重连")
                .setNegativeButtonText(R.string.cancel)
                .setTargetFragment(this, REQUEST_RECONNECT_DIALOG)
                .show();
    }

    private void setValue(int request, byte command, byte key, Byte[] value) {
        autoClose(request);
        mData.get(request).setProgressing(true);
        getPacketParseService().send(request, command,
                key, value);
    }

    private void showSettingRssiDialog() {
        mRssiDialog = new RssiDialog();
        mRssiDialog.setCancelable(false);
        mRssiDialog.setOnConfirmListener(new RssiDialog.OnConfirmListener() {
            @Override
            public void onConfirm(int rssi) {
                showToast("value=" + rssi);
                settingDefenceDistance.setRightText(String.valueOf(rssi));
                setValue(Constant.REQUEST_RSSI, Constant.COMMAND_SETTING,
                        Constant.SEND_KEY_RSSI, new Byte[]{(byte) rssi});
                mRssiDialog.dismiss();
            }
        });
        int rssi = 50;
        try {
            rssi = Integer.valueOf(settingDefenceDistance.getRightText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        mRssiDialog.setSeekValue(rssi);
        if (!mRssiDialog.isAdded()) {
            mRssiDialog.show(getFragmentManager(), null);
        }
    }

    private void showVibChoiceDialog(final String s) {
        if (vibDialog == null && vibButtons == null) {
            vibDialog = new RadioGroupDialog();
            vibDialog.setTitle("震动灵敏度");
            vibButtons = new ArrayList<>();
            AppCompatRadioButton rb1 = new AppCompatRadioButton(getActivity());
            AppCompatRadioButton rb2 = new AppCompatRadioButton(getActivity());
            AppCompatRadioButton rb3 = new AppCompatRadioButton(getActivity());
            AppCompatRadioButton rb4 = new AppCompatRadioButton(getActivity());
            AppCompatRadioButton rb5 = new AppCompatRadioButton(getActivity());

            vibButtons.add(rb1);
            vibButtons.add(rb2);
            vibButtons.add(rb3);
            vibButtons.add(rb4);
            vibButtons.add(rb5);

            int count = vibButtons.size();
            for (int i = 0; i < count; i++) {
                RadioButton rb = vibButtons.get(i);
                rb.setText(String.valueOf(i + 1));
            }

            vibDialog.setRadioList(vibButtons);
            vibDialog.setOnConfirmListener(new RadioGroupDialog.OnConfirmListener() {
                @Override
                public void onConfirm(int id) {
                    String text = vibButtons.get(id).getText().toString();
                    Log.i(TAG, text);
                    settingSensibility.setRightText(text);
                    //暂时注释掉
                    setValue(Constant.REQUEST_SENSIBILITY, Constant.COMMAND_SETTING,
                            Constant.SEND_KEY_SENSITIVITY, new Byte[]{(byte) Integer.parseInt(text, 16)});
                    //暂时注释掉
                    vibDialog.dismiss();
                }
            });
        }

        int count = vibButtons.size();
        if (!TextUtils.isEmpty(s)) {
            for (int i = 0; i < count; i++) {
                RadioButton rb = vibButtons.get(i);
                if (s.equals(rb.getText().toString())) {
                    rb.setChecked(true);
                }
            }
        }

        if (!vibDialog.isAdded()) {
            vibDialog.show(getFragmentManager(), null);
        }
    }

    /**
     * 确保parserService初始化完成
     */

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BluetoothParseServiceImpl) {
            mListener = (BluetoothParseServiceImpl) context;
        } else {
            throw new RuntimeException("host activity must " +
                    "implement BluetoothParseServiceImpl");
        }
    }


    public void initUIState() {
        Map<Integer, String> map = readData();

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            Log.d(TAG, "initUIState: key : " + entry.getKey() + " | value: " + entry.getValue());
            int request = entry.getKey();
            SettingItem item = mData.get(request);
            if (item instanceof ToggleSettingItem) {
                ((ToggleSettingItem) item).setCheck("1".equals(entry.getValue()));
            } else if (item instanceof TextSettingItem) {
                String s = entry.getValue();
                if (!TextUtils.isEmpty(s)) {
                    ((TextSettingItem) item).setRightText(s);
                }
            }
        }
//        Object lmode_home = SharePreferenceUtil.getInstance().getData(Constant.SP_MODE_HOME, false);
//        Object lquiet_lock = SharePreferenceUtil.getInstance().getData(Constant.SP_MODE_QUIET_LOCK, false);
//        Object lfast_start = SharePreferenceUtil.getInstance().getData(Constant.SP_FAST_START, false);
//        Object lmode_control = SharePreferenceUtil.getInstance().getData(Constant.SP_MODE_CONTROL, false);
//        Object lmode_lock = SharePreferenceUtil.getInstance().getData(Constant.SP_MODE_LOCK, false);
//        Object lrssi = SharePreferenceUtil.getInstance().getData(Constant.SP_RSSI, new Integer(55));
//        Object lshock_sensitivity = SharePreferenceUtil.getInstance().getData(Constant.SP_SHOCK_SENSITIVITY, "3");
//
//        settingHomeMode.setCheck((boolean) lmode_home);
//        settingSilence.setCheck((boolean) lquiet_lock);
//        settingFastStart.setCheck((boolean) lfast_start);
//        settingDefenceDistance.setRightText(String.valueOf(lrssi));
//        settingSensibility.setRightText(String.valueOf(lshock_sensitivity));
//        settingControlMode.setRightText((boolean) lmode_control ? "手柄" : "手机");
//        settingAutoDefence.setRightText((boolean) lmode_lock ? "自动" : "手动");
    }

    private IntentFilter myFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.ACTION_DEVICE_CONN_OK);
        return filter;
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case REQUEST_ASK_DIALOG:
                if (mCurAction == wannaReset) {
                    getPacketParseService().send(Constant.REQUEST_RESET, Constant.COMMAND_FACTORY_RESET,
                            (byte) 0x01, null);
                } else if (mCurAction == wannaRestart) {
                    getPacketParseService().send(Constant.REQUEST_RESTART, Constant.COMMAND_RESTART,
                            (byte) 0x01, null);
                } else if (mCurAction == wannaUpdate) {
                    getPacketParseService().send(Constant.REQUEST_UPDATE, Constant.COMMAND_UPDATE,
                            (byte) 0x01, null);
                }
                break;
            case REQUEST_SWITCH_DIALOG:
                mListener.getUartService().disconnect();
                startActivity(new Intent(getActivity(), ScanActivity.class));
                break;
            case REQUEST_RECONNECT_DIALOG:
                mListener.getUartService().disconnect();
                startActivity(new Intent(getActivity(), ScanActivity.class));
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    // TODO: 2016/5/17 0017 显示查询得到的数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVoltageUpdate(Event.BleVoltageUpdate event) {
        settingQueryVoltage.setRightText(event.voltage + "V");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSpeedUpdate(Event.BleSpeedUpdate event) {
        settingQuerySpeed.setRightText(event.speed + "Km/h");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMileageUpdate(Event.BleMileageUpdate event) {
        settingQueryWheelTern.setRightText(String.valueOf(event.mileage));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUIUpdate(Event.BleUIUpdate event) {
        Log.d(TAG, "onUIUpdate: ");
        initUIState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnected(Event.BleConnectSucc event) {
        if (settingDeviceReconnect.getVisibility() != View.VISIBLE)
            settingDeviceReconnect.setVisibility(View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendSuccessed(Event.SendSuccess event) {
        Log.d(TAG, "onSendSuccessed: " + event.requestId);
        int sequenceId = event.requestId;
        SettingItem item = mData.get(sequenceId);
        if (item == null || !isRunning) {
            return;
        }
        if (!item.isProgressing()) {
            return;
        }
        mAutoCloseHandler.removeMessages(sequenceId);
        item.setProgressing(false);
        if (item instanceof ToggleSettingItem) {
            ToggleSettingItem toggleSettingItem = ((ToggleSettingItem) item);
            toggleSettingItem.setCheck(!toggleSettingItem.isCheck());
        } else if (item instanceof TextSettingItem) {
            item.setProgressing(false);
        }
        saveData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(parentActivity).unregisterReceiver(cfReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private List<View> mSettingFoldItems = new ArrayList<>();
    private List<View> mQueryFoldItems = new ArrayList<>();
    private List<View> mSystemFoldItems = new ArrayList<>();

    private void initFolders() {
        mSettingFoldItems.add(settingDefenceDistance);
        mSettingFoldItems.add(settingSensibility);
        mSettingFoldItems.add(settingTerminalVoltage);
        mSettingFoldItems.add(settingWheelRadius);
        mSettingFoldItems.add(settingMileageCalibration);

        mQueryFoldItems.add(settingQueryWheelTern);
        mQueryFoldItems.add(settingQuerySpeed);

        mSystemFoldItems.add(settingRestart);
        mSystemFoldItems.add(settingFirmwareUpdate);
        mSystemFoldItems.add(settingFactoryReset);
    }

    private void fold(List<View> views, boolean isOpen) {
        AlphaAnimation anim;
        if (isOpen) {
            anim = new AlphaAnimation(0f, 1f);
        } else {
            anim = new AlphaAnimation(1f, 0f);
        }
        anim.setDuration(800);
        for (View v : views) {
            v.setAnimation(anim);
            v.setVisibility(isOpen ? View.VISIBLE : View.GONE);
        }
    }

    private void saveData() {
        if (application.getCurCar().getType() == TbitProtocol.TYPE_A2)
            return;
        Map<Integer, String> toSave = readData();
        for (int i = 0; i < TbitProtocol.NEED_TO_SAVE.length; i++) {
            SettingItem item = mData.get(TbitProtocol.NEED_TO_SAVE[i]);
            if (item == null)
                continue;
            if (item instanceof ToggleSettingItem) {
                toSave.put(TbitProtocol.NEED_TO_SAVE[i], ((ToggleSettingItem) item).isCheck() ? "1" : "0");
            } else if (item instanceof TextSettingItem) {
                toSave.put(TbitProtocol.NEED_TO_SAVE[i], ((TextSettingItem) item).getRightText());
            }
        }
        application.saveA1StateData(application.getCurCar().getCarId(), toSave);
    }

    private Map<Integer, String> readData() {
        return application.readA1StateData(application.getCurCar().getCarId());
    }
}
