package com.tbit.electrichelper.Fragments.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tbit.electrichelper.R;

/**
 * Created by Salmon on 2016/6/30 0030.
 */
public class ShareDialog extends DialogFragment {
    TextView mTextPositive, mTextNegative, mContent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_share, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mTextPositive = (TextView) view.findViewById(R.id.text_dialog_positive);
        mTextNegative = (TextView) view.findViewById(R.id.text_dialog_negative);
        mContent = (TextView) view.findViewById(R.id.text_dialog_content);
    }
}
