package com.tbit.electrichelper.Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.tbit.electrichelper.Beans.News;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BitmapCache;

import java.util.List;

/**
 * Created by Salmon on 2016/1/8.
 */
public class NewsAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<News> list;
    private ImageLoader imageLoader;

    public NewsAdapter(Context context, List<News> list) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
        imageLoader = new ImageLoader(MyApplication.getInstance().getRequestQueue(), new BitmapCache());
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_news, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view
                    .findViewById(R.id.tv_title);
            viewHolder.describe = (TextView) view
                    .findViewById(R.id.tv_desc);
            viewHolder.image = (ImageView) view
                    .findViewById(R.id.image);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        // 设置手表备注名
        viewHolder.title.setText(list.get(position)
                .getTitle());

        //设置监护人编号
        viewHolder.describe.setText(list.get(position).getDescribe());

//        viewHolder.image.setDefaultImageResId(R.drawable.ic_insert_photo_orange_300_36dp);
//        viewHolder.image.setErrorImageResId(R.drawable.ic_insert_photo_grey_500_36dp);
//        viewHolder.image.setImageUrl(list.get(position).getImageUrl(), imageLoader);
        viewHolder.image.setImageResource(list.get(position).getImageResource());

        return view;
    }

    private class ViewHolder {
        TextView title;
        TextView describe;
//        NetworkImageView image;
        ImageView image;
    }

}
