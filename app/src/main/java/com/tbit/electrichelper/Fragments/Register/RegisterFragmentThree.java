package com.tbit.electrichelper.Fragments.Register;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseNetworkFragment;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RegisterFragmentThree extends BaseNetworkFragment {

    @Bind(R.id.edit_passwords)
    EditText editPasswords;
    @Bind(R.id.button_register_submit)
    Button buttonRegisterSubmit;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_3, container, false);

        createHandler();

        buttonRegisterSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String psw = editPasswords.getText().toString();
                if (psw.length() < 6 || psw.length() > 12) {
                    showCroupton("密码长度有误，请输入6-12位密码");
                    return;
                }
                doRegister(psw);
//				mListener.onArticleSelected(STEP_THREE,new Bundle());
            }
        });
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    private void createHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };
    }

    private void doRegister(final String psw) {
        new Thread(new Runnable() {

            @Override
            public void run() {

            }
        }).start();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
