package com.tbit.electrichelper.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.tbit.electrichelper.Activities.LoginActivity;
import com.tbit.electrichelper.Activities.StatusDetailActivity;
import com.tbit.electrichelper.Beans.CarStatus;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.BaseFragment;
import com.tbit.electrichelper.Util.RadarView;
import com.tbit.electrichelper.Util.TbitProtocol;
import com.tbit.electrichelper.Util.TbitUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetectFragment extends BaseFragment {

    private static final int HANDLE_REFRESH_ITEM = 0;
    private static final int HANDLE_RADAR_START = 1;
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_STATUS = "item_status";
    private static final String STRING_SCANNING = "扫描中";
    private static final String STRING_SCANED = "已扫描";
    private static final String STRING_WAITING = "待扫描";

    @Bind(R.id.radar)
    RadarView radar;
    boolean isScanning;
    @Bind(R.id.btn_detect)
    Button btnDetect;
    @Bind(R.id.listview)
    ListView listview;
    @Bind(R.id.report)
    TextView report;
    //    @Bind(R.id.tv_level)
//    TextView tvLevel;
    @Bind(R.id.tv_detect_time)
    TextView tvDetectTime;
    @Bind(R.id.ll_rank)
    LinearLayout llRank;
    @Bind(R.id.image_detect_face)
    ImageView imageDetectFace;
    @Bind(R.id.iv_rank_desc)
    ImageView ivRankDesc;
    @Bind(R.id.text_rank_mark)
    TextView textRankMark;
    @Bind(R.id.relative_detect_time)
    RelativeLayout relativeDetectTime;

    private Handler mHandler;
    private int itemFlag = 0;
    private SimpleAdapter adapter;
    private List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
    private boolean isRunning;
    public static final String TAG = DetectFragment.class.getSimpleName();

    public DetectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detect, container, false);
        ButterKnife.bind(this, view);

        itemFlag = 0;
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isRunning) {
                    return;
                }
                super.handleMessage(msg);
                switch (msg.what) {
                    case HANDLE_REFRESH_ITEM: {
                        if (itemFlag > data.size() - 1) {
                            if (itemFlag == data.size()) {
                                data.get(itemFlag - 1).put(ITEM_STATUS, STRING_SCANED);
                                adapter.notifyDataSetChanged();
                                stopScan();
                                showResult();
                            }
                            break;
                        }
                        listview.smoothScrollToPosition(itemFlag);
                        data.get(itemFlag).put(ITEM_STATUS, STRING_SCANNING);
                        if (itemFlag > 0) {
                            data.get(itemFlag - 1).put(ITEM_STATUS, STRING_SCANED);
                        }
                        adapter.notifyDataSetChanged();
                        itemFlag++;
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendEmptyMessage(HANDLE_REFRESH_ITEM);
                            }
                        }, 400);
                    }
                    break;
                    default:
                        break;
                }
            }
        };

        isScanning = false;

        btnDetect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getInstance().getLoginStatus() == TbitProtocol.LOGGED_OUT) {
                    enterActivity(LoginActivity.class);
                    return;
                }
                if (application.hasA2Bound()) {
                    if (!isScanning) {
                        start();
                        btnDetect.setText("重新检测");
                    }
                } else {
                    addBinding();
                }

            }
        });
        isRunning = true;

        adapter = new SimpleAdapter(getActivity(), data, R.layout.item_charge_record,
                new String[]{ITEM_NAME, ITEM_STATUS},
                new int[]{R.id.itemName, R.id.itemStatus});
        initData();
        listview.setAdapter(adapter);
        listview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        listview.setFooterDividersEnabled(false);
        listview.setHeaderDividersEnabled(false);

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyApplication.getInstance().getLoginStatus() != TbitProtocol.LOGGED_OUT) {
                    if (application.hasA2Bound()) {
                        if (!isScanning) {
                            enterActivity(StatusDetailActivity.class);
                        } else {
                            showCroupton("正在扫描，请稍等", Style.ALERT);
                        }
                    } else {
                        addBinding();
                    }

                } else {
                    enterActivity(LoginActivity.class);
                }

            }
        });


        return view;
    }

    private void start() {
        textRankMark.setText("");
        tvDetectTime.setText("");
        llRank.setVisibility(View.INVISIBLE);
        itemFlag = 0;
        mHandler.sendEmptyMessage(HANDLE_REFRESH_ITEM);
        isScanning = true;
        radar.startScan();
    }


    private void showResult() {
        if (llRank.getVisibility() != View.VISIBLE) {
            llRank.setVisibility(View.VISIBLE);
        }
        if (relativeDetectTime.getVisibility() != View.VISIBLE) {
            relativeDetectTime.setVisibility(View.VISIBLE);
        }
        if (report.getVisibility() != View.VISIBLE) {
            report.setVisibility(View.VISIBLE);
        }
        CarStatus status = (CarStatus) application.getCurGlobal(TbitProtocol.KEY_GLOBAL_STATUS);
        setImageAndRank(getRank(status.getStrGGPV()));
        tvDetectTime.setText(TbitUtil.getDate());
    }

    public void hideResult() {
        report.setVisibility(View.INVISIBLE);
        llRank.setVisibility(View.INVISIBLE);
        textRankMark.setText("");
        tvDetectTime.setText("");
        relativeDetectTime.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initData() {
        String[] items = new String[]{"电机缺相", "助力模式", "巡航模式",
                "控制器故障", "转把故障", "霍尔故障", "备用", "侧架状态", "三速控制器", "防飞车保护", "滑行充电"
                , "控制器保护", "刹车状态", "电机运行状态", "限速状态", "电子刹车", "倒车", "堵转保护"
                , "过流保护", "EKK备用电源", "仪表盘故障", "GPS定位", "GSM信号强度"};
        data.clear();
        for (String name : items) {
            HashMap<String, String> map = new HashMap<>();
            map.put(ITEM_NAME, name);
            map.put(ITEM_STATUS, STRING_WAITING);
            data.add(map);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        stopScan();
    }

    private void stopScan() {
        isScanning = false;
        if (radar != null)
            radar.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    private void setImageAndRank(int level) {
        switch (level) {
            case 0:
                textRankMark.setText("20分");
                imageDetectFace.setImageResource(R.drawable.image_face_40);
                ivRankDesc.setImageResource(R.drawable.image_comment_40);
                break;
            case 1:
                textRankMark.setText("20分");
                imageDetectFace.setImageResource(R.drawable.image_face_40);
                ivRankDesc.setImageResource(R.drawable.image_comment_40);
                break;
            case 2:
                textRankMark.setText("40分");
                imageDetectFace.setImageResource(R.drawable.image_face_60);
                ivRankDesc.setImageResource(R.drawable.image_comment_60);
                break;
            case 3:
                textRankMark.setText("60分");
                imageDetectFace.setImageResource(R.drawable.image_face_80);
                ivRankDesc.setImageResource(R.drawable.image_comment_80);
                break;
            case 4:
                textRankMark.setText("80分");
                imageDetectFace.setImageResource(R.drawable.image_face_100);
                ivRankDesc.setImageResource(R.drawable.image_comment_100);
                break;
            case 5:
                textRankMark.setText("100分");
                imageDetectFace.setImageResource(R.drawable.image_face_100);
                ivRankDesc.setImageResource(R.drawable.image_comment_100);
                break;
            default:
                textRankMark.setText("100分");
                imageDetectFace.setImageResource(R.drawable.image_face_100);
                ivRankDesc.setImageResource(R.drawable.image_comment_100);
                break;
        }
    }

    private int getRank(String s) {
        int result = 0;
        try {
            String[] temp = s.split("=");
            temp = temp[1].split(",");

            int battery = 0;
            if (stringToInt(temp[0]) < 2) {
                battery = 1;
            }

            result = battery + stringToInt(temp[1]) + stringToInt(temp[5]) + stringToInt(temp[6]) + stringToInt(temp[7]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 5 - result;
    }

    private int stringToInt(String s) {
        return Integer.parseInt(s);
    }

}
