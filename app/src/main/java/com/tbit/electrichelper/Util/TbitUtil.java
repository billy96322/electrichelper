package com.tbit.electrichelper.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ZoomControls;

import com.baidu.mapapi.model.LatLng;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Salmon on 2015/11/16.
 */
public class TbitUtil {

    static double DEF_2PI = 6.28318530712; // 2*PI
    static double DEF_PI180 = 0.01745329252; // PI/180.0
    static double DEF_R = 6370693.5; // radius of earth
    static double DEF_PI = 3.14159265359; // PI

    /**
     * dp转px
     */
    public static int dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * px转dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // 源图片的高度和宽度
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // 计算出实际宽高和目标宽高的比率
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // 选择宽和高中最小的比率作为inSampleSize的值，这样可以保证最终图片的宽和高
            // 一定都会大于等于目标的宽和高。
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String url,
                                                         int reqWidth, int reqHeight) {
        // 第一次解析将inJustDecodeBounds设置为true，来获取图片大小
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(url, options);
        // 调用上面定义的方法计算inSampleSize值
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // 使用获取到的inSampleSize值再次解析图片
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(url, options);
    }

    public static void hideZoomControl(ViewGroup mMapView) {

        int count = mMapView.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = mMapView.getChildAt(i);
            if (child instanceof ImageView || child instanceof ZoomControls) {
                child.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * 地图工具类
     *
     * @author Leon
     */

    public static String getDateAndTime() {
        long time = System.currentTimeMillis();//long now = android.os.SystemClock.uptimeMillis();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = new Date(time);
        String t = format.format(d1);
        return t;
    }

    public static String getDate() {
        String[] temp = getDateAndTime().split(" ");
        return temp[0];
    }

    public static Bitmap getMarkerBitmap(Context context, boolean isOnline) {
        Bitmap b = BitmapFactory.decodeResource(context.getResources(), isOnline ? R.drawable.car_marker : R.drawable.car_marker_offline);
        int newWidth = (int) (45 * context.getResources().getDisplayMetrics().density);
        int newHeight = (int) (45 * context.getResources().getDisplayMetrics().density);
        int width = b.getWidth();
        int height = b.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix m = new Matrix();
        m.postScale(scaleWidth, scaleHeight);
        b = Bitmap.createBitmap(b, 0, 0, width, height, m, true);
        return b;
    }

    public static Bitmap getMyPosMarkerBitmap(Context context) {
        Bitmap b = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_my_pos);
        int newWidth = (int) (20 * context.getResources().getDisplayMetrics().density);
        int newHeight = (int) (20 * context.getResources().getDisplayMetrics().density);
        int width = b.getWidth();
        int height = b.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix m = new Matrix();
        m.postScale(scaleWidth, scaleHeight);
        b = Bitmap.createBitmap(b, 0, 0, width, height, m, true);
        return b;
    }

    /**
     * 获取矩形对角坐标
     */
    public static LatLng[] getFitPoint(List<LatLng> points) {
        List<Double> xList = new ArrayList<Double>();
        List<Double> yList = new ArrayList<Double>();
        for (LatLng latLng : points) {
            xList.add(latLng.latitude);
            yList.add(latLng.longitude);
        }

        //java.util.Collections 是一个包装类。它包含有各种有关集合操作的静态多态方法。此类不能实例化，就像一个工具类，服务于Java的Collection框架。
        LatLng[] latlngArray = {new LatLng(Collections.min(xList), Collections.min(yList)), new LatLng(Collections.max(xList), Collections.max(yList))};

        return latlngArray;
    }

    public static LatLng[] stretchLines(LatLng[] latLngs, float multiple) {
        if (latLngs.length < 2) {
            return latLngs;
        }
        LatLng l1 = latLngs[0];
        LatLng l2 = latLngs[1];

        double x1 = l1.latitude;
        double x2 = l2.latitude;
        double y1 = l1.longitude;
        double y2 = l2.longitude;

        double xLarger = multiple * Math.abs(x1 - x2);
        double yLarger = multiple * Math.abs(y1 - y2);

        if (x1 > x2) {
            x1 += xLarger;
            x2 -= xLarger;
        } else if (x1 == x2) {
            x1 += xLarger;
            x2 += xLarger;
        } else {
            x1 -= xLarger;
            x2 += xLarger;
        }

        if (y1 > y2) {
            y1 += yLarger;
            y2 -= yLarger;
        } else if (y1 == y2) {
            y1 += yLarger;
            y2 += yLarger;
        } else {
            y1 -= yLarger;
            y2 += yLarger;
        }
        LatLng[] result = {new LatLng(x1, y1), new LatLng(x2, y2)};
        return result;
    }

    /**
     * 将yyyy-MM-dd格式的字符串
     * 转换成长整型格林尼治时间
     *
     * @param s
     * @return
     */
    public static long getTimeMilesByString(String s) {
        GregorianCalendar calendar = getGregorianCalendarByString(s);
        return calendar.getTimeInMillis();
    }

    public static GregorianCalendar getGregorianCalendarByString(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        GregorianCalendar cal = new GregorianCalendar();
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return cal;
        }
        cal.setTime(date);
        return cal;
    }

    public static boolean isWIFI() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        } else {
            return false;
        }
    }

    /**
     * 验证邮箱
     */
    public static boolean isEmailLegal(String email) {
        boolean tag = true;
        final String pattern1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        //^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$
        //^([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+.[a-zA-Z]{2,4}$
        final Pattern pattern = Pattern.compile(pattern1);
        final Matcher mat = pattern.matcher(email);
        if (!mat.find()) {
            tag = false;
        }
        return tag;
    }

    public static double GetShortDistance(LatLng start, LatLng end) {
        double ew1, ns1, ew2, ns2;
        double dx, dy, dew;
        double distance;
        // 角度转换为弧度
        ew1 = start.longitude * DEF_PI180;
        ns1 = start.latitude * DEF_PI180;
        ew2 = end.longitude * DEF_PI180;
        ns2 = end.latitude * DEF_PI180;
        // 经度差
        dew = ew1 - ew2;
        // 若跨东经和西经180 度，进行调整
        if (dew > DEF_PI)
            dew = DEF_2PI - dew;
        else if (dew < -DEF_PI)
            dew = DEF_2PI + dew;
        dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
        dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
        // 勾股定理求斜边长
        distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }

    public static String getDateStringByTimemile(long timeMile) {
        Date date = new Date(timeMile);
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        return format.format(date);
    }
}
