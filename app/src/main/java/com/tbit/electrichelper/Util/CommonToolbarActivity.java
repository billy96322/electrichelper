package com.tbit.electrichelper.Util;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbit.electrichelper.R;

/**
 * Created by Salmon on 2016/4/21 0021.
 */
public abstract class CommonToolbarActivity extends BaseNetworkActivity {
    protected Toolbar mToolbar;
    protected TextView mToolbarTitle;
    protected TextView mTextMenu;
    protected ImageView mImageMenu;
    private ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResId());

        mToolbar = (Toolbar) findViewById(R.id.toolbar_common);
        if (mToolbar != null) {
            mBack = (ImageView) mToolbar.findViewById(R.id.back);
            mToolbarTitle = (TextView) mToolbar.findViewById(R.id.title);
            mTextMenu = (TextView) mToolbar.findViewById(R.id.text_menu);
            mImageMenu = (ImageView) mToolbar.findViewById(R.id.image_menu);
            mBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            if (inflateCustomImageMenu() != 0) {
                mImageMenu.setVisibility(View.VISIBLE);
                mImageMenu.setImageResource(inflateCustomImageMenu());
                mImageMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCustomMenuClick();
                    }
                });
            } else if (!TextUtils.isEmpty(inflateCustomTextMenu())) {
                mTextMenu.setVisibility(View.VISIBLE);
                mTextMenu.setText(inflateCustomTextMenu());
                mTextMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCustomMenuClick();
                    }
                });
            } else {
                setSupportActionBar(mToolbar);
            }
        } else {
            throw new RuntimeException("have you include toolbar_common in your layout file ?");
        }
    }

    protected abstract int getLayoutResId();

    /**
     * 加载自定义文字菜单，只能加载一个文字或图片。图片优先加载。
     * 需要重写onCustomMenuClick()方法响应菜单点按事件。
     * 加载自定义菜单后，actionBar默认菜单不再生效
     * @return
     */
    protected String inflateCustomTextMenu() {
        return "";
    }

    /**
     * 加载自定义图片菜单，只能加载一个文字或图片。图片优先加载。
     * 需要重写onCustomMenuClick()方法响应菜单点按事件。
     * 加载自定义菜单后，actionBar默认菜单不再生效
     * @return
     */
    protected int inflateCustomImageMenu() {
        return 0;
    }

    /**
     * 重写此方法响应自定义菜单点按事件。
     */
    protected void onCustomMenuClick() {
    }
}
