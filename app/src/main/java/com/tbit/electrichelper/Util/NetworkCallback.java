package com.tbit.electrichelper.Util;


import com.tbit.electrichelper.Beans.TbitResponse;

/**
 * Created by Salmon on 2015/11/11.
 */
public interface NetworkCallback {

    void parseResults(TbitResponse jsonObject, int tag);

    void onFailure(int tag);

    void onProgress(long bytesWritten, long totalSize);
}