package com.tbit.electrichelper.Util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Salmon on 2015/11/11.
 */
public class ToastUtils {

    private Toast mToast;
    private Toast mLongToast;
    private Context mContext;

    public ToastUtils(Context context){
        mContext = context;
        mToast = Toast.makeText(context,"",Toast.LENGTH_SHORT);
        mLongToast = Toast.makeText(context, "", Toast.LENGTH_LONG);
    }

    public void showToast(String s){
        mToast.setText(s);
        mToast.show();
    }

    public void showLongToast(String s) {
        mLongToast.setText(s);
        mLongToast.show();
    }
}