package com.tbit.electrichelper.Util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.tbit.electrichelper.BaseApplication;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Salmon on 2015/11/11.
 */
public class NetworkUtils {

    public NetworkCallback mCallback;
    public int Method = 0;
    public int TimeoutMs = 10*1000;
    public String TAG = "Request";
    private BaseApplication mApp;
    private RequestQueue mQueue;

    public NetworkUtils(NetworkCallback callback,Context context) {
        mCallback = callback;
        mApp = MyApplication.getInstance();
        mQueue = mApp.getRequestQueue();
    }

    public ImageLoader getImageLoader() {
        ImageLoader imageLoader = new ImageLoader(mQueue, new BitmapCache()) {
            @Override
            protected Request<Bitmap> makeImageRequest(String requestUrl, int maxWidth, int maxHeight, ImageView.ScaleType scaleType, final String cacheKey) {
//                return super.makeImageRequest(requestUrl, maxWidth, maxHeight, scaleType, cacheKey);
                return new ImageRequest(requestUrl, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        onGetImageSuccess(cacheKey, response);
                    }
                }, maxWidth, maxHeight, scaleType, Bitmap.Config.RGB_565, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onGetImageError(cacheKey, error);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> map = new HashMap<>();
                        map.put("Cookie", MyApplication.getInstance().getLocalCookie());
                        return map;
                    }
                };
            }
        };
        return imageLoader;
    }

    public void LoadData(String url, final Map<String,String> params,RequestMethod method, Response.Listener listener, Response.ErrorListener errorListener){

        switch (method){
            case GET:Method = Request.Method.GET;
                break;
            case POST:Method = Request.Method.POST;
                break;
            default:break;
        }

        StringRequest request = new StringRequest(Method, url, listener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("Cookie", MyApplication.getInstance().getLocalCookie());
                return map;
            }
        };

        setRequestProperty(request);
        addRequestProperty(request);

    }


    public void loginLoadData(final int Tag,final String url, final Map<String,String> params) {
        JsonObjectPostRequest request = new JsonObjectPostRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                TbitResponse tbitResponse = new TbitResponse();
                try {
                    Log.d("YadeaNetwork", url + response.toString() + "");
                    MyApplication.getInstance().setLocalCookie(response.getString("Cookie"));
                    tbitResponse.setRes(response.getBoolean("res"));
                    if (!tbitResponse.isRes()) {
                        tbitResponse.setDesc(response.getString("desc"));
                    } else {
                        String dataString = response.getString("result");
                        if (dataString != null && dataString.length() != 0) {
                            tbitResponse.setResult(dataString);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mCallback != null) {
                    mCallback.parseResults(tbitResponse, Tag);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mCallback != null) {
                    mCallback.onFailure(Tag);
                }
            }
        }, params);
        setRequestProperty(request);
        addRequestProperty(request);
    }

    public void loadJsonData(final int Tag, final String url, final Map<String,String> params) {

        JsonPostRequest request = new JsonPostRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("YadeaNetwork", url + response.toString() + "");
                TbitResponse tbitResponse = new TbitResponse();
                try {
                    tbitResponse.setRes(response.getBoolean("res"));
                    if (!tbitResponse.isRes()) {
                        tbitResponse.setDesc(response.getString("desc"));
                    } else {
                        String dataString = response.getString("result");
                        if (dataString != null && dataString.length() != 0) {
                            tbitResponse.setResult(dataString);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mCallback != null) {
                    mCallback.parseResults(tbitResponse, Tag);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (mCallback != null) {
                    mCallback.onFailure(Tag);
                }
            }
        }, params);

        request.setSendCookie(MyApplication.getInstance().getLocalCookie());

        setRequestProperty(request);
        addRequestProperty(request);
//        loadJsonData(Tag, url, params, "res", "desc", "result");
    }

    public void loadJsonDataCommon(final int Tag, final String url, final Map<String,String> params, final String resultKey,
                                   final String describeKey, final String dataKey) {

        JsonPostRequest request = new JsonPostRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("YadeaNetwork", url + response.toString() + "");
                TbitResponse tbitResponse = new TbitResponse();
                try {
                    if (response.getInt(resultKey) == 1) {
                        tbitResponse.setRes(true);
                        String dataString = response.getString(dataKey);
                        if (dataString != null && dataString.length() != 0) {
                            tbitResponse.setResult(dataString);
                        }
                    } else {
                        tbitResponse.setDesc(response.getString(describeKey));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mCallback != null) {
                    mCallback.parseResults(tbitResponse, Tag);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mCallback != null) {
                    mCallback.onFailure(Tag);
                }
            }
        }, params);

        request.setSendCookie(MyApplication.getInstance().getLocalCookie());

        setRequestProperty(request);
        addRequestProperty(request);
    }

    private void addRequestProperty(Request request) {
        mQueue.add(request);
    }

    private void setRequestProperty(Request request) {

        request.setTag(TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(TimeoutMs, 1, 1f));
    }

    public void cancelRequest(){
        mQueue.cancelAll(TAG);
    }

    enum RequestMethod{
        GET,POST
    }
}
