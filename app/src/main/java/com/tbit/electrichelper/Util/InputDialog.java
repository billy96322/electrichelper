package com.tbit.electrichelper.Util;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.IPositiveButtonDialogListener;
import com.tbit.electrichelper.R;

/**
 * Created by Salmon on 2016/2/26.
 */
public class InputDialog extends SimpleDialogFragment {

    public static String TAG = "input";

    public static void show(AppCompatActivity activity) {
        new InputDialog().show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public int getTheme() {
        return super.getTheme();
    }

    @Override
    protected Builder build(Builder builder) {
        builder.setTitle("title")
                .setMessage("message")
                .setView(LayoutInflater.from(getActivity()).inflate(R.layout.dialog_bound, null));
        builder.setPositiveButton("I want one", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (IPositiveButtonDialogListener listener : getPositiveButtonDialogListeners()) {
                    listener.onPositiveButtonClicked(mRequestCode);
                }
                dismiss();
            }
        });
        return builder;
    }
}
