package com.tbit.electrichelper.Util;

import android.os.Environment;

import com.baidu.mapapi.model.LatLng;
import com.tbit.electrichelper.Bluetooth.Constant;

/**
 * Created by Salmon on 2015/11/13.
 */
public class TbitProtocol {

    public static final int TYPE_A2 = 0;
    public static final int TYPE_A1 = 1;

    public static final String VALUE_TBIT_COMMON_APPID = "ydgjandroid";
    public static final String VALUE_TBIT_COMMON_SECRET = "97d09986d74242428ab76395c9d464df";

    public static final String SP_NAME = "SP_TBIT_ELECT";
    public static final String SP_LOGIN_NAME = "SP_PHONE";
    public static final String SP_LOGIN_PASSWORD = "SP_PSW";
    public static final String SP_SHOW_MY_POSITION = "SP_MY_POS";
    public static final String SP_LOW_POWER_WARN = "SP_LOW_POWER";
    public static final String SP_LOW_LAST_CHOSEN_MACHINE = "SP_LAST_MACHINE";
    public static final String SP_LOW_LAST_CHOSEN_TYPE = "SP_LAST_MACHINE_TYPE";
    public static final String SP_A1_OFFLINE_DATA = "SP_A1_OFFLINE";
    public static final String SP_A1_STATE_DATA = "SP_A1_STATE";
    public static final String SP_A1_SHARE_LOCAL = "SP_A1_SHARE_LOCAL";

    public static final String KEY_GLOBAL_STATUS = "status_global";
    public static final String KEY_GLOBAL_ONLINE = "car_online_global";
    public static final String KEY_NOBELONING_PERSONAL_INFO = "personal_info_global";
    public static final String KEY_NOBELONING_PHONE = "phone_global";
    public static final String KEY_NOBELONING_PASSWORD = "password_global";
    public static final String KEY_GLOBAL_CAR_INFO = "car_info_global";
    public static final String KEY_GLOBAL_A1_ENTITY = "car_a1_global";

    public static final String ACTION_NOTHING_BIND = "action_nothing_bind";

    public static final int LOGGED_IN = 1;
    public static final int LOGGED_OUT = -1;
    public static final int GUEST = 0;

    public static final String EXPERIENCE_POS_DESC = "广东省东莞市,石大路,朗湖路,啊木通讯马蹄岗分店西北68米,嘉佳百货西南102米";
    public static final int EXPERIENCE_CARID = 315735;
    public static final String EXPERIENCE_DATA = "{\"acode\":\"\",\"carId\":315735,\"direction\":0,\"gpstime\":\"2016-01-26 18:04:30\",\"la\":22.945496,\"lat\":22.948703,\"lng\":113.861741,\"lo\":113.850258,\"mileage\":133,\"pointed\":2,\"scode\":\"s:3\",\"speed\":0,\"stopTime\":61739,\"strGGPV\":\"066:YDGJ=0,1,0,0,0,1,1,0,0,3,0,0,0,1,0,0,0,0,0,0,0,0,0,0\"}";

    public static final String EXTERNAL_STORAGE = Environment.getExternalStorageDirectory().toString();
    public static final String BAIDU_NAVI = "Navi";

    public static final LatLng DEFAULT_LATLNG = new LatLng(22.55815, 113.955027);

    public static final String BROADCAST_RECEIVER_LOGGED_IN = "receiver_logged_in";
    public static final String BROADCAST_RECEIVER_LOGGED_OUT = "receiver_logged_out";
    public static final String BROADCAST_RECEIVER_UPDATE_STATUS = "receiver_update_status";
    public static final String BROADCAST_RECEIVER_ALL_UNBOUNDED = "receiver_all_unbounded";
    public static final String BROADCAST_RECEIVER_CHANGE_CAR = "receiver_change_car";
    public static final String BROADCAST_RECEIVER_LOG_IN_RETRY = "receiver_login_retry";
    public static final String BROADCAST_RECEIVER_BIND_CHANGE = "receiver_bind_change";//添加删除绑定
    public static final String BROADCAST_RECEIVER_BLUETOOTH_DISCONNECT = "receiver_bluetooth_disconnect";

    public static final int[] CODE_TIME_SALT = new int[]{11893 ,11542 ,10280 ,12210
            ,11616 ,12023 ,10546 ,11908 ,12251 ,10104};
    public static final int[] CODE_TID_SALT = new int[]{5946, 9216, 6112, 5900, 6341,
            3870, 8893, 9523, 4538, 4865};

    public static final int[] NEED_TO_SAVE = {Constant.REQUEST_HOME_MODE, Constant.REQUEST_SILENCE,
            Constant.REQUEST_FAST_START, Constant.REQUEST_CONTROL, Constant.REQUEST_AUTO_DEFENCE,
            Constant.REQUEST_RSSI, Constant.REQUEST_SENSIBILITY, Constant.REQUEST_TERMINAL_VOLTAGE,
            Constant.REQUEST_WHEEL_RADIUS, Constant.REQUEST_MILEAGE_CALIBRATION};
}
