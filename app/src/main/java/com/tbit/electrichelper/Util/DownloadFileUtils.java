package com.tbit.electrichelper.Util;

import android.app.Activity;
import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * 文件下载类
 * Created by Salmon on 2015-11-24.
 */
public class DownloadFileUtils {


    public static void downLoadFile(final Integer tag, final Context context, String url, final NetworkCallback mCallback, File file){

        AsyncHttpClient client = MyApplication.getInstance().getAsyncHttpClient();

        client.get(url, new FileAsyncHttpResponseHandler(file) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                LogUtils.Log(((MyApplication) ((Activity) context).getApplication()).DEBUG_MODE, "asd", " status:" + statusCode);
                mCallback.onFailure(tag);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                TbitResponse tbitResponse = new TbitResponse();
                tbitResponse.setRes(true);
                tbitResponse.setResult(file.getPath());
                LogUtils.Log(((MyApplication) ((Activity) context).getApplication()).DEBUG_MODE, "asd", "path-->" + file.getPath() + " status:" + statusCode);
                mCallback.parseResults(tbitResponse, tag);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                mCallback.onProgress(bytesWritten, totalSize);
            }
        }).setTag(tag);
    }
}
