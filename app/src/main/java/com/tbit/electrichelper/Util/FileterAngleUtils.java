package com.tbit.electrichelper.Util;

import com.baidu.mapapi.model.LatLng;
import com.tbit.electrichelper.Beans.CarStatus;

import java.util.List;

public class FileterAngleUtils {
	public static void filterAngle(List<CarStatus> pas) {
		if (pas == null) {
			return;
		}
		if (pas.size() < 3)
			return;

		int i = 0;
		while (i < pas.size() - 2) {
			CarStatus p_A = pas.get(i);
			CarStatus p_B = pas.get(i + 1);
			CarStatus p_C = pas.get(i + 2);

			// 计算角度
			LatLng pStart = new LatLng(Math.abs(p_A.getLat() - p_B.getLat()),Math.abs(p_A.getLng() - p_B.getLng()) );

			LatLng pEnd = new LatLng(Math.abs(p_C.getLat() - p_B.getLat()),Math.abs(p_C.getLng() - p_B.getLng()) );

			double angle = getAngle(pStart, pEnd);
			if (angle > 0.707 && (p_B.getAcode() == null || p_B.getAcode().equals(""))) {// Math.acos(Math.cos(Math.PI / 6.0)),30度的锐角
				// 应该在此处继续判断锐角，决定过滤哪一个
				pas.remove(p_B);
			}
			i++;
		}
	}

	/**
	 * 得到角度,余弦定理
	 *
	 * @param pStart
	 * @param pEnd
	 * @return
	 */
	private static double getAngle(LatLng pStart, LatLng pEnd) {

		double x1 = pStart.latitude;
		double y1 = pStart.longitude;
		double x2 = pEnd.latitude;
		double y2 = pEnd.longitude;
		double step1 = x1 * x2 + y1 * y2;
		double step2 = Math.sqrt(Math.pow(x1, 2) + Math.pow(y1, 2)) * Math.sqrt(Math.pow(x2, 2) + Math.pow(y2, 2));
		// acrcos 0.866 30 0.707 45
		return step1 / step2;

	}

}
