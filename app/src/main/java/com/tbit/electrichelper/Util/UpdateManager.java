package com.tbit.electrichelper.Util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;

import java.io.File;

/**
 * Created by Salmon on 2016/3/9.
 */
public class UpdateManager implements NetworkCallback {
    private static final int TAG_NETWORK_CHECK_UPDATE = 0;
    private static final int TAG_NETWORK_NEW_VERSION_DESC = 1;
    private static final int TAG_NETWORK_DOWNLOAD = 2;
    private OnUpdateListener onUpdateListener;
    private Context context;

    public UpdateManager(OnUpdateListener onUpdateListener, Context context) {
        this.onUpdateListener = onUpdateListener;
        this.context = context;
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        switch (tag) {
            case TAG_NETWORK_CHECK_UPDATE:
                if (jsonObject.isRes()) {
                    int newVer = -1;
                    try {
                        newVer = Integer.parseInt(jsonObject.getResult());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (newVer != -1 && newVer > getVersionCode()) {
                        UpdateUtils.getVersionDesc(TAG_NETWORK_NEW_VERSION_DESC, NetworkProtocol.URL_APP_UPDATE_DESCRIBTION, this);
                    } else {
                        onUpdateListener.onNewVersionFound(false, "");
                    }
                }
                break;
            case TAG_NETWORK_NEW_VERSION_DESC:
                if (jsonObject.isRes()) {
                    onUpdateListener.onNewVersionFound(true, jsonObject.getResult());
                }
                break;
            case TAG_NETWORK_DOWNLOAD:
                if (jsonObject.isRes() && onUpdateListener != null) {
                    onUpdateListener.onDownloadFinished(jsonObject.getResult());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFailure(int tag) {
        if (tag == TAG_NETWORK_DOWNLOAD) {
            onUpdateListener.onDownloadFailed();
        }
    }

    @Override
    public void onProgress(long bytesWritten, long totalSize) {
        int p = (int) ((bytesWritten * 1.0 / totalSize) * 100);
        onUpdateListener.onDownloading(p);
    }

    public void checkUpdate() {
        UpdateUtils.getNewestVersion(TAG_NETWORK_CHECK_UPDATE, NetworkProtocol.URL_APP_UPDATE_VERSION, this);
    }

    public void download() {
        File appDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), context.getResources().getString(R.string.file_name));
        if (!appDir.exists()) {
            // 目录不存在 则创建
            appDir.mkdirs();
        }
        File file = new File(appDir, "yadea_update.apk");
        DownloadFileUtils.downLoadFile(TAG_NETWORK_DOWNLOAD, this.context, NetworkProtocol.URL_APP_UPDATE_DOWNLOAD, this, file);
    }

    private int getVersionCode() {
        int versionCode = 0;
        try {
            // 获取软件版本号，对应AndroidManifest.xml下android:versionCode
            versionCode = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public interface OnUpdateListener {
        void onNewVersionFound(boolean hasNewVersion, String describe);
        void onDownloading(int progress);
        void onDownloadFinished(String fileUrl);
        void onDownloadFailed();
    }

}
