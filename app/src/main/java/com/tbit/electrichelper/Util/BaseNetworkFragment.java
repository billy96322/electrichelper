package com.tbit.electrichelper.Util;

import android.os.Bundle;
import android.util.Log;

import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;

import java.util.Map;

/**
 * Created by Salmon on 2016/1/25.
 */
public abstract class BaseNetworkFragment extends BaseFragment implements NetworkCallback {

    private NetworkUtils mNetworkUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNetworkUtils = new NetworkUtils(this, parentActivity);
        Log.d("asd", this.getClass().getSimpleName() + "  onCreate");
    }


    protected void postNetworkJson(int tag, String url, Map<String, String> params, boolean showProgressbar) {
        if (showProgressbar)
            mProgressDialogUtils.showProgressDialog(true, getResources().getString(R.string.loading));
        mNetworkUtils.loadJsonData(tag, url, params);
    }

    public void postNetWorkWithSavingSession(int tag, String url, Map<String, String> params, boolean showProgressbar) {
        if (showProgressbar)
            mProgressDialogUtils.showProgressDialog(true);
        mNetworkUtils.loginLoadData(tag, url, params);
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (!isRunning) {
            return;
        }
        mProgressDialogUtils.showProgressDialog(false);
    }

    @Override
    public void onFailure(int tag) {
        if (!isRunning) {
            return;
        }
        mProgressDialogUtils.showProgressDialog(false);
        mToast.showToast(getResources().getString(R.string.internet_error));
    }

    @Override
    public void onProgress(long bytesWritten, long totalSize) {
        if (!isRunning) {
            return;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mNetworkUtils.cancelRequest();
    }
}