package com.tbit.electrichelper.Util;

import android.util.Log;

/**
 * Created by Salmon on 2015/11/11.
 */
public class LogUtils {

    public static void Log(boolean DEBUG_MODE,String TAG,String Message){
        if (DEBUG_MODE){
            Log.i(TAG, Message);
        }
    }
}
