package com.tbit.electrichelper.Util;

import android.app.Activity;
import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbit.electrichelper.MyApplication;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

/**
 * 图片上传类
 * Created by Bowen on 2015-11-05.
 */
public class UploadFileUtils {

    public interface UpLoadFileListener{
        void onUpLoadSuccess(String result);
        void onUpLoadFailure(String result);
        void onUpLoadProgress(int written, int total);
    }

    public static void upLoadFile(final Context context,RequestParams params,String url, final UpLoadFileListener mUpLoadFileListener){

        AsyncHttpClient client = MyApplication.getInstance().getAsyncHttpClient();


        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (mUpLoadFileListener != null){
                    try {
                        String s = new String(responseBody, "GB2312");
                        mUpLoadFileListener.onUpLoadSuccess(s);
                        LogUtils.Log(((MyApplication) ((Activity) context).getApplication()).DEBUG_MODE,"TAG",s);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (mUpLoadFileListener != null){
                   /* try {
                        String s = new String(responseBody, "GB2312");
                        mUpLoadFileListener.onUpLoadFailure(s);
                        LogUtils.Log(((MyApplication) ((Activity) context).getApplication()).DEBUG_MODE, "TAG", s);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }*/
//                    Log.d("LOG", "statusCode: " + statusCode);
//                    try {
//                        Log.d("LOG", "headers: " + headers==null?"null":new String(responseBody, "utf-8"));
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    error.printStackTrace();
                    String s = "";
                    mUpLoadFileListener.onUpLoadFailure(s);
                }
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                if (mUpLoadFileListener != null){
                    mUpLoadFileListener.onUpLoadProgress((int)bytesWritten,(int)totalSize);
                }
                super.onProgress(bytesWritten, totalSize);
            }
        });
    }
}
