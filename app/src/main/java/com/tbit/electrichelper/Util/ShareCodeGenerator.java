package com.tbit.electrichelper.Util;

import android.text.TextUtils;

import com.tbit.electrichelper.Beans.ShareResult;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Salmon on 2016/6/16 0016.
 * <p>
 * 密钥规则如下：
 * 长度：10位
 * 字符：纯数字
 * 示例：3563012045
 * <p>
 * |    3   |  56301  |  2045  |
 * |  FLAG  |  TIME   |  TID   |
 * <p>
 * FLAG：加盐标识位，TbitProtocol中盐数组常量的index
 * TIME：过期时间，距离2016-01-01 00:00:00 的小时数
 * TID ：设备号末三位
 * TIME 和 TID 需要加上盐
 */
public class ShareCodeGenerator {
    public static final int CODE_OK = 0;
    public static final int CODE_ERR_ILLEGAL = 1;
    public static final int CODE_ERR_EXPIRED = 2;
    private static final String LONG_TIME_DATE_STRING = "2016-01-01 00:00:00";
    private static final long TIME_MILE_OF_START_TIME = 1451577599000l;

    /**
     * 生成长期有效的密码，既是去盐之后日期为2016-01-01 00:00:00的密码
     *
     * @param targetDevice 目标设备的设备ID
     * @return 加密后的密钥
     */
    public static String gen(String targetDevice) {
        return gen(LONG_TIME_DATE_STRING, targetDevice);
    }

    /**
     * 生成有有效期的密码
     *
     * @param dateString   日期字符串，形式如：2016-01-01 00:00:00
     * @param targetDevice 目标设备ID
     * @return 加密后的密钥
     */
    public static String gen(String dateString, String targetDevice) {
        String result = "";
        long targetTimeMiles = getTimeMilesByDate(dateString);
        if (targetTimeMiles < TIME_MILE_OF_START_TIME) {
            return result;
        }
        if (TextUtils.isEmpty(targetDevice) || targetDevice.length() < 3 ||
                !TextUtils.isDigitsOnly(targetDevice)) {
            return result;
        }
        int flag = genFlag();
        int hours = mileToHours(targetTimeMiles);
        int tid = Integer.parseInt(targetDevice.substring(targetDevice.length() - 3,
                targetDevice.length()));
        int[] secret = new int[]{flag, hours, tid};
        addSalt(secret);
        result = assembly(secret);
        return result;
    }

    /**
     * 判断密钥是否合法
     *
     * @param code 已加盐的密钥
     * @return 密钥是否合法
     */
    public static boolean isCodeLegal(String code) {
        if (TextUtils.isEmpty(code)) {
            return false;
        }
        if (!isLengthLegal(code)) {
            return false;
        }
        if (!TextUtils.isDigitsOnly(code)) {
            return false;
        }
        return true;
    }

    /**
     * 判断密钥长度是否合法
     *
     * @param code 已加盐的密钥
     * @return 密钥长度是否合法
     */
    private static boolean isLengthLegal(String code) {
        return code.length() == 10;
    }

    /**
     * 解析密钥
     *
     * @param code         已加盐的密钥
     * @return 结果标志（与本类常量对比）
     */
    public static ShareResult resoleCode(String code) {
        if (!isCodeLegal(code)) {
            return new ShareResult(CODE_ERR_ILLEGAL);
        }
        int[] cutData = disassembly(code);
        wipeSalt(cutData);
        ShareResult result = genShareResult(cutData);
        if (!isExpired(cutData)) {
            result.setStatus(CODE_ERR_EXPIRED);
            return result;
        }
        result.setStatus(CODE_OK);
        return result;
    }

    /**
     * 判断密钥是否过期
     *
     * @param cutData 拆解成数组的密钥
     * @return 是否过期
     */
    private static boolean isExpired(int[] cutData) {
        int targetHourCount = cutData[1];
        int localHourCount = currentHourCount();
        if (targetHourCount > localHourCount || targetHourCount == mileToHours(TIME_MILE_OF_START_TIME)) {
            return true;
        }
        return false;
    }

//    /**
//     * 判断是否目标设备
//     *
//     * @param cutData      去盐并拆解后的数据
//     * @param targetDevice 目标设备ID
//     * @return 是否目标设备
//     */
//    private static boolean isTargetDevice(int[] cutData, String targetDevice) {
//        String part = targetDevice.substring(targetDevice.length() - 3, targetDevice.length());
//        return part.equals(String.valueOf(cutData[2]));
//    }

    /**
     * 拆解密钥
     *
     * @param code 密钥字符串
     * @return 拆解后的数组
     */
    private static int[] disassembly(String code) {
        return new int[]{
                Integer.parseInt(code.substring(0, 1)),
                Integer.parseInt(code.substring(1, 6)),
                Integer.parseInt(code.substring(6, 10))
        };
    }

    /**
     * 组装密钥
     *
     * @param data 拆解后的数组
     * @return 密钥字符串
     */
    private static String assembly(int[] data) {
        String result = "";
        for (int i : data) {
            result += i;
        }
        return result;
    }

    /**
     * 加盐
     *
     * @param origin 加盐后的密钥数组
     */
    public static void addSalt(int[] origin) {
        int flag = origin[0];
        int timeSalt = TbitProtocol.CODE_TIME_SALT[flag];
        int tidSalt = TbitProtocol.CODE_TID_SALT[flag];

        origin[1] += timeSalt;
        origin[2] += tidSalt;
    }

    /**
     * 去盐
     *
     * @param salted 未加盐的密钥数组
     */
    public static void wipeSalt(int[] salted) {
        int flag = salted[0];
        int timeSalt = TbitProtocol.CODE_TIME_SALT[flag];
        int tidSalt = TbitProtocol.CODE_TID_SALT[flag];

        salted[1] -= timeSalt;
        salted[2] -= tidSalt;
    }

    /**
     * 当前距离2016-01-01 00:00:00的小时总数
     *
     * @return
     */
    private static int currentHourCount() {
        long current = System.currentTimeMillis();
        if (current < TIME_MILE_OF_START_TIME) {
            return Integer.MAX_VALUE;
        }
        return mileToHours(current);
    }

    /**
     * 传如timeMile，自动转换成距离2016-01-01 00:00:00 的小时数
     * 注意判断时间，确保大于2016-01-01 00:00:00该时间
     *
     * @param timeMile
     * @return
     */
    private static int mileToHours(long timeMile) {
        if (timeMile < TIME_MILE_OF_START_TIME) {
            throw new RuntimeException("Are you out of your mind?");
        }
        long dif = (timeMile - TIME_MILE_OF_START_TIME);
        int hour = (int) (dif / (1000 * 60 * 60));
        return hour;
    }

    /**
     * 把类似 2016-01-01 00:00:00 这样的字符串转换成时间
     * 如果非法字符串，将返回0
     *
     * @param dateString 时间字符串，如 "2016-01-01 00:00:00"
     * @return
     */
    public static long getTimeMilesByDate(String dateString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getTimeInMillis();
    }

    /**
     * 生成结果
     *
     * @param cutData 去盐并拆解后的数据
     * @return
     */
    private static ShareResult genShareResult(int[] cutData) {
        ShareResult result = new ShareResult();
        result.setTid(String.valueOf(cutData[2]));
        long time = -1;
        if (cutData[1] == 0) {
            time = 0;
        } else if (cutData[1] > 0) {
            long temp = 1000 * 60 * 60;
            temp = temp * cutData[1];
            time = TIME_MILE_OF_START_TIME + temp;
        }
        result.setTimeMile(time);
        return result;
    }

    private static int genFlag() {
        return (int) (Math.random() * 10);
    }
}
