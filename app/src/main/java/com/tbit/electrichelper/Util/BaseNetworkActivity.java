package com.tbit.electrichelper.Util;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.R;

import java.io.File;
import java.util.Map;

/**
 * Created by Salmon on 2015/11/11.
 */
public abstract class BaseNetworkActivity extends BaseActivity implements NetworkCallback {

    private NetworkUtils mNetworkUtils;
    protected Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNetworkUtils = new NetworkUtils(this, this);
        gson = new Gson();
    }

    @Override
    public void parseResults(TbitResponse jsonObject, int tag) {
        if (!isRunning) {
            return;
        }
        application.connectionFailTime = 0;
        showProgressDialog(false);

    }

    @Override
    public void onProgress(long bytesWritten, long totalSize) {
//        Log.d("asd", ((bytesWritten/totalSize)*100) + "");
    }

    @Override
    public void onFailure(int tag) {
        if (!isRunning) {
            return;
        }
        application.connectionFailTime++;
        showProgressDialog(false);
        showToast(getResources().getString(R.string.internet_error));
    }

    public ImageLoader getImageLoader() {
        return mNetworkUtils.getImageLoader();
    }


    public void postNetWorkForLogin(int tag, String url, Map<String, String> params, boolean showProgressbar) {
        showProgressDialog(true, getResources().getString(R.string.loading));
        mNetworkUtils.loginLoadData(tag, url, params);
    }

    public void postNetworkJson(int tag, String url, Map<String, String> params, boolean showProgressbar) {
        if (showProgressbar)
            showProgressDialog(true, getResources().getString(R.string.loading));
        mNetworkUtils.loadJsonData(tag, url, params);
    }

    public void postNetworkJsonCommon(int tag, String url, Map<String, String> params, boolean showProgressbar) {
        if (showProgressbar)
            showProgressDialog(true, getResources().getString(R.string.loading));
        mNetworkUtils.loadJsonDataCommon(tag, url, params, "ret", "msg", "data");
    }


    public void downloadFile(int tag, String url, boolean showProgressbar, File file) {
        if (showProgressbar)
            showProgressDialog(true, getResources().getString(R.string.loading));
        DownloadFileUtils.downLoadFile(tag, this, url, this, file);
    }

    public void downloadFile(int tag, String url, boolean showProgressbar, int stringResource, File file) {
        if (showProgressbar)
            showProgressDialog(true, getResources().getString(stringResource));
        DownloadFileUtils.downLoadFile(tag, this, url, this, file);
    }


    public void getWithoutResult(String url) {
        mNetworkUtils.LoadData(url, null, NetworkUtils.RequestMethod.GET, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                Log.d("LOG", response==null?"null": response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNetworkUtils.cancelRequest();
    }

}
