package com.tbit.electrichelper.Util;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Salmon on 2016/3/9.
 */
public class UpdateSescribeParser extends DefaultHandler {

    private String preTag = null;
    private StringBuilder result = new StringBuilder();
    private boolean needEnter = false;

    public String getDescribe(InputStream inputStream) throws Exception {
        SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
        saxParser.parse(inputStream, this);
        return result.toString();
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void endDocument() throws SAXException {
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.preTag = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.preTag = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        log(new String(ch,start,length));
        if (preTag != null && preTag.equals("d")) {
            String date = new String(ch,start,length);
            if (needEnter) {
                result.append("\n");
            }
            result.append(date);
            needEnter = true;
        }
    }

    private void log(String s) {
        Log.d("XML", s);
    }
}
