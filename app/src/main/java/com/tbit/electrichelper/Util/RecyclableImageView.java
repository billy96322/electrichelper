package com.tbit.electrichelper.Util;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by Salmon on 2016/2/2.
 */
public class RecyclableImageView extends ImageView {
    public RecyclableImageView(Context context) {
        super(context);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setImageDrawable(null);
    }
}
