package com.tbit.electrichelper.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.igexin.sdk.PushConsts;
import com.igexin.sdk.PushManager;
import com.tbit.electrichelper.MyApplication;

/**
 * Created by Salmon on 2016/1/28.
 */
public class MyPushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d("GetuiSdkDemo", "onReceive() action=" + bundle.getInt("action"));
        switch (bundle.getInt(PushConsts.CMD_ACTION)) {

            case PushConsts.GET_MSG_DATA:
                // 获取透传数据
                // String appid = bundle.getString("appid");
                byte[] payload = bundle.getByteArray("payload");

                String taskid = bundle.getString("taskid");
                String messageid = bundle.getString("messageid");

                // smartPush第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
                boolean result = PushManager.getInstance().sendFeedbackMessage(
                        context, taskid, messageid, 90001);
                System.out.println("第三方回执接口调用" + (result ? "成功" : "失败"));

                if (payload != null) {
                    String data = new String(payload);

                    Log.d("GetuiSdkDemo", "Got Payload:" + data);

                }

                /**GPS查车透传如下*/
               /* byte[] payload = bundle.getByteArray("payload");

                String taskid = bundle.getString("taskid");
                String messageid = bundle.getString("messageid");

                // smartPush第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
                boolean result = PushManager.getInstance().sendFeedbackMessage(
                        context, taskid, messageid, 90001);
                System.out.println("第三方回执接口调用" + (result ? "成功" : "失败"));

                if (payload != null) {
                    String data = new String(payload);

                    Log.d("GetuiSdkDemo", "Got Payload:" + data);

                }*/

                /**打开activity如下*/
                //NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                /*Notification.Builder builder = new Notification.Builder(context)
                    .setTicker("this is ticker")
                    .setSmallIcon(R.drawable.icon);
                Notification notification = builder.build();*/
                /*Notification notification = new Notification(R.drawable.logo_01, "您有新的消息", System.currentTimeMillis());

                Intent notifiIntent = new Intent(context, MapActivity.class);
                notifiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pi = PendingIntent.getActivity(context, 0, notifiIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

                notification.setLatestEventInfo(context, "泰比特开咪", "您有新的消息", pi);
                manager.notify(NOTIFICATION_ID, notification);*/

                break;
            case PushConsts.GET_CLIENTID:
                // 获取ClientID(CID)
                // 第三方应用需要将CID上传到第三方服务器，并且将当前用户帐号和CID进行关联，以便日后通过用户帐号查找CID进行消息推送
                String cid = bundle.getString("clientid");
                Log.d("LOG", "获取ClientID(CID): " + cid);

                // 上传clientId的时候，需要判断是个人用户还是车队用户，个人用户上传clientId，车队用户不会上传，车队用户不推送消息
               /* MyApplication glob = (MyApplication) context
                        .getApplicationContext();
                glob.clientId = cid;*/


//                Intent broadcast = new Intent(
//                        TbitProtocol.BROADCAST_RECEIVER_PUSH_CLIENT_ID);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(broadcast);// 发送本地广播
                MyApplication.getInstance().clientId = cid;

                break;
            case PushConsts.THIRDPART_FEEDBACK:
            /*
			 * String appid = bundle.getString("appid"); String taskid =
			 * bundle.getString("taskid"); String actionid =
			 * bundle.getString("actionid"); String result =
			 * bundle.getString("result"); long timestamp =
			 * bundle.getLong("timestamp");
			 *
			 * Log.d("GetuiSdkDemo", "appid = " + appid); Log.d("GetuiSdkDemo",
			 * "taskid = " + taskid); Log.d("GetuiSdkDemo", "actionid = " +
			 * actionid); Log.d("GetuiSdkDemo", "result = " + result);
			 * Log.d("GetuiSdkDemo", "timestamp = " + timestamp);
			 */
                break;
            default:
                break;
        }
    }
}
