package com.tbit.electrichelper.Util;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.squareup.leakcanary.RefWatcher;
import com.tbit.electrichelper.BaseApplication;
import com.tbit.electrichelper.MyApplication;
import com.umeng.analytics.MobclickAgent;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Salmon on 2015/11/11.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected DialogUtils mProgressDialogUtils;
    private ToastUtils mToast;
    protected boolean isRunning;
    protected boolean visible;
    protected MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RefWatcher refWatcher = BaseApplication.getRefWatcher(this);
        refWatcher.watch(this);
        isRunning = true;
        visible = true;
        initUtils();
        application = MyApplication.getInstance();
        Log.d("asd", this.getClass().getSimpleName() + "  onCreate");
    }

    private void initUtils() {
        mProgressDialogUtils = new DialogUtils(this);
        mProgressDialogUtils.setCancelable(true);
        mToast = new ToastUtils(getApplicationContext());
    }

    public void showToast(String message){
        mToast.showToast(message);
    }

    public void showToast(int resourseId){
        showToast(getString(resourseId));
    }

    public void showLongToast(String message) {
        mToast.showLongToast(message);
    }

    public void showLongToast(int resourseId) {
        showLongToast(getString(resourseId));
    }

    public void showProgressDialog(boolean show,String message){
        mProgressDialogUtils.showProgressDialog(show, message);
    }

    public void showProgressDialog(boolean show){
        mProgressDialogUtils.showProgressDialog(show);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
        mProgressDialogUtils.dismiss();
    }

    public void enterActivity(Class<?> otherActivity) {
        BaseActivity.this.startActivity(new Intent(BaseActivity.this, otherActivity));
    }


    protected void showCrouton(String s) {
        showCrouton(s, Style.ALERT);
    }

    protected void showCrouton(int resId) {
        showCrouton(resId, Style.ALERT);
    }

    protected void showCrouton(int resId, Style style) {
        showCrouton(getString(resId), style);
    }

    protected void showCrouton(String s, Style style) {
        Crouton.cancelAllCroutons();
        Crouton.makeText(this, s, style).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        visible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        visible = false;
    }
}
