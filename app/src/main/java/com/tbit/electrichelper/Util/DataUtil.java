package com.tbit.electrichelper.Util;

import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.MyApplication;

/**
 * Created by Salmon on 2016/6/12 0012.
 */
public class DataUtil {
    public static String getMachineNoByCarId(String carId) {
        String result = "";
        CarData.CarsEntity entity = getEntityByCarId(carId);
        if (entity != null) {
            result = entity.getMachineNO();
        }
        return result;
    }

    public static CarData.CarsEntity getEntityByCarId(String carId) {
        CarData.CarsEntity result = null;
        for (CarData.CarsEntity e : MyApplication.getInstance().a2Cars) {
            if (e.getCarId().equals(carId)) {
                result = e;
            }
        }
        return result;
    }
}
