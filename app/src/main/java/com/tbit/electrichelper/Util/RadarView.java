package com.tbit.electrichelper.Util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

import com.tbit.electrichelper.R;

/**
 * 使用约定，雷达扫针的长度等于该素材矩形短的边长的一半。
 * Created by Zhang on 2016/1/10.
 */
public class RadarView extends View {

    Bitmap mBitmapRadarBackground; // 雷达扫描背景
    Bitmap mBitmapRadarScanner;           // 雷达扫针
    float percent;
    int value;// 显示的值
    float degrees;

    float ox, oy;
    float cx, cy;

    boolean isStarted;

    public RadarView(Context context) {
        this(context, null);
    }

    public RadarView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);

        isStarted = false;
    }

    private void init(AttributeSet attrs) {
        if (attrs != null && getContext() != null) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.RadarView);
            if (ta != null) {
                mBitmapRadarBackground = BitmapFactory.decodeResource(getResources(),
                        ta.getResourceId(R.styleable.RadarView_radar_background,
                                R.drawable.image_radar_background));
                mBitmapRadarScanner = BitmapFactory.decodeResource(getResources(),
                        ta.getResourceId(R.styleable.RadarView_radar_scanner, R.drawable.image_radar));
            }
            ta.recycle();
        }
    }

    public void dismiss() {
        isStarted = false;//停止线程
    }

    public void startScan() {
        if (isStarted) {
            return;
        }
        isStarted = true;//启动一个线程进行不断的旋转角度
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (isStarted) {
                    degrees = (degrees += 2) >= 360 ? 0 : degrees;
                    postInvalidate();
                    SystemClock.sleep(10);
                }
            }
        }).start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmapRadarBackground, ox, oy, null);
        canvas.save();
        canvas.clipRect(0, 0, getWidth(), (100 - value) * percent);
//        canvas.drawBitmap(bitmapGray, x, y, null);
        canvas.restore();
        if (isStarted) {
            canvas.save();
            canvas.rotate(degrees, cx, cy);
            canvas.drawBitmap(mBitmapRadarScanner, ox, oy, null);
            canvas.restore();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widMode = MeasureSpec.getMode(widthMeasureSpec);
        int heiMode = MeasureSpec.getMode(heightMeasureSpec);

        int widSize = MeasureSpec.getSize(widthMeasureSpec);
        int heiSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widMode == MeasureSpec.AT_MOST) {
            widSize = mBitmapRadarBackground.getWidth();
        }

        if (heiMode == MeasureSpec.AT_MOST) {
            heiSize = mBitmapRadarBackground.getHeight();
        }

        if (widMode != MeasureSpec.AT_MOST || heiMode != MeasureSpec.AT_MOST) {
            int smallSize = widSize < heiSize ? widSize : heiSize;
            widSize = heiSize = smallSize;
        }

        setMeasuredDimension(widSize, heiSize);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int smallSize = w < h ? w : h;
        mBitmapRadarBackground = Bitmap.createScaledBitmap(mBitmapRadarBackground, smallSize, smallSize, true);
        mBitmapRadarScanner = Bitmap.createScaledBitmap(mBitmapRadarScanner, smallSize, smallSize, true);

        cx = w/2;
        cy = h/2;

        if (w > h) {
            ox = (w-h)/2;
            oy = 0;
        } else if (w < h) {
            ox = 0;
            oy = (h-w)/2;
        } else {
            ox = oy = 0;
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public float getDegrees() {
        return degrees;
    }

    public void setDegrees(float degrees) {
        this.degrees = degrees;
    }

    public boolean isScanStarted() {
        return isStarted;
    }

    public void setStart(boolean start) {
        this.isStarted = start;
    }
}