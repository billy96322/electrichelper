package com.tbit.electrichelper.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.tbit.electrichelper.Beans.events.Event;
import com.tbit.electrichelper.MyApplication;

import org.greenrobot.eventbus.EventBus;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Salmon on 2016/1/23.
 */
public abstract class BaseFragment extends Fragment {

    protected DialogUtils mProgressDialogUtils;
    protected ToastUtils mToast;
    protected Activity parentActivity;
    protected boolean isRunning;
    protected MyApplication application;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isRunning = true;
        parentActivity = getActivity();
        application = MyApplication.getInstance();
        initUtils(parentActivity);
    }

    private void initUtils(Context context) {
        mProgressDialogUtils = new DialogUtils(context);
        mProgressDialogUtils.setCancelable(true);
        mToast = new ToastUtils(application.getApplicationContext());
    }

    public void showToast(int resourseId) {
        showToast(getString(resourseId));
    }

    public void showToast(String message){
        mToast.showToast(message);
    }

    public void showLongToast(String message) {
        mToast.showLongToast(message);
    }

    public void showLongToast(int resourseId) {
        showLongToast(getString(resourseId));
    }

    protected void showCroupton(String s, Style style) {
        try {
            Crouton.cancelAllCroutons();
            Crouton.makeText(parentActivity, s, style).show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    protected void showCroupton(String s) {
        showCroupton(s, Style.ALERT);
    }

    protected void showCroupton(int resId) {
        showCroupton(getString(resId));
    }

    protected void showCroupton(int resId, Style style) {
        showCroupton(getString(resId), style);
    }

    public void enterActivity(Class<?> otherActivity) {
        parentActivity.startActivity(new Intent(parentActivity, otherActivity));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        mProgressDialogUtils.dismiss();
    }

    protected void addBinding() {
        EventBus.getDefault().post(new Event.AddBindEvent());
    }

}