package com.tbit.electrichelper.Util;

/**
 * Created by Salmon on 2015/11/13.
 */
public class NetworkProtocol {

//    public static final String URL = "http://192.168.1.216:9000/";
//    public static final String URL_COMMON = "http://192.168.1.215:30000/";
//    public static final String URL_COMMON = "http://shop.tbit.com.cn:30000/";
//    public static final String URL = "http://shop.tbit.com.cn:11300/";

    public static final String URL_COMMON = "http://common.tbit.com.cn/";
    public static final String URL = "http://tbitgps.com/";

    /* 雅迪电动车 */

    /**注册&登录*/
    public static final String URL_REGISTER_VARIFICATION_CODE = URL + "platformUserAction!getSmsCode.do";
    public static final String URL_REGISTER = URL + "platformUserAction!register.do";
    public static final String URL_FORGOT_PASSWORD = URL + "platformUserAction!resetPassword.do";
    public static final String URL_LOGIN = URL + "platformUserAction!platformLogin.do";
    /**修改用户个人信息*/
    public static final String URL_PERSONAL_INFO_UPDATE = URL + "platformUserAction!update.do";
    /**绑定*/
    public static final String URL_BOUND_QUERY = URL + "platformCarBoundAction!getBoundCar.do";
    public static final String URL_BOUND = URL + "platformCarBoundAction!boundCar.do";
    public static final String URL_UNBOUND = URL + "platformCarBoundAction!unBound.do";
    /**保修*/
    public static final String URL_REPAIR_REPORT = URL + "repairRecordAction!add.do";
    public static final String URL_REPAIR_RECORDS = URL + "repairRecordAction!getByUserId.do";
    public static final String URL_REPAIR_IMAGEIDS = URL + "repairRecordAction!getByRepairRecordId.do";
    public static final String URL_SERVICE_OUTLET = URL + "sellPointAction!getByArea.do";

    /**获取车辆在线状态*/
    public static final String URL_ONLINE = URL + "carAction!getOnLineByUser.do";

    /**获得车辆信息*/
    public static final String URL_CAR_INFO = URL + "carAction!getCarByID.do";

    /**修改车辆信息*/
    public static final String URL_CAR_INFO_UPDATE = URL + "carAction!modify.do";

    public static final String URL_POSITION = URL + "carAction!getPositionByID.do";
    public static final String URL_GET_CAR_DATA = URL + "platformCarBoundAction!getData.do";
    public static final String URL_GET_ADDRESS_DESC = URL + "carAction!getLocation.do";
    public static final String URL_HEATBEAT = URL + "accountAction!keepAlive.do";
    public static final String URL_HISTORY = URL + "historyAction!findHistory.do";
    public static final String URL_SEND_COMMAND = URL + "carAction!sendCommand.do";

    /** 根据网点ID查询网点信息 */
    public static final String URL_GET_SELLPOINT_INFO_BY_ID = URL + "sellPointAction!getById.do";


    /**历史告警*/
    public static final String URL_HISTORY_ALERT = URL + "historyAction!getAlarmHistory.do";

    /**推送*/
    public static final String URL_PUSH_BOUND = URL + "platformAppPushAction!bound.do";
    public static final String URL_PUSH_QUERY = URL + "platformAppPushAction!getAutoPushSwitch.do";
    public static final String URL_PUSH_SET = URL + "platformAppPushAction!setAutoPushSwitch.do";

    /**消息*/
    public static final String URL_GET_MESSAGE = URL + "carAction!getCarMessage.do";


    public static final String URL_UPLOAD_IMG = URL_COMMON + "image/uploadImage.do";
    public static final String URL_GET_IMG = URL_COMMON + "image/getImage.do?imageId=%1$s&token=%2$s";

    /**升级*/
    public static final String URL_APP_UPDATE_DOWNLOAD = URL + "version/android/Ydgj.apk";
    public static final String URL_APP_UPDATE_VERSION = URL + "version/android/version_ydgj.xml";
    public static final String URL_APP_UPDATE_DESCRIBTION = URL + "version/android/Ydgj_update_explain.xml";

    /**获取Token*/
    public static final String URL_TOKEN = URL_COMMON + "appUser/getToken.do";

    /**A1绑定*/
    public static final String URL_A1_BINDING_QUERY = URL + "platformA1BoundAction!getBoundA1.do";
    public static final String URL_A1_BIND = URL + "platformA1BoundAction!boundA1.do";
    public static final String URL_A1_UNBIND = URL + "platformA1BoundAction!unBound.do";
    public static final String URL_A1_SET_REMARK = URL + "platformA1BoundAction!updateRemark.do";

    public static final String PARAMKEY_CARIDS = "carIds";
    public static final String PARAMKEY_CLIENTID = "clientId";
    public static final String PARAMKEY_LANGUAGE = "language";
    public static final String PARAMKEY_CLIENTTYPE = "clientType";
    public static final String PARAMKEY_AUTOPUSH = "autoPush";
    public static final String PARAMKEY_TYPE = "type";
    public static final String PARAMKEY_COMMAND_NAME = "commandName";
    public static final String PARAMKEY_COMMAND_VALUE = "commandValue";
    public static final String PARAMKEY_COMMAND_TYPE = "commandType";
    public static final String PARAMKEY_STARTTIME = "startTime";
    public static final String PARAMKEY_ENDTIME = "endTime";
    public static final String PARAMKEY_MAPTYPE = "mapType";
    public static final String PARAMKEY_LAT = "lat";
    public static final String PARAMKEY_LNG = "lng";
    public static final String PARAMKEY_LON = "lon";
    public static final String PARAMKEY_RADIUS = "radius";
    public static final String PARAMKEY_CARID = "carId";
    public static final String PARAMKEY_NAME = "name";
    public static final String PARAMKEY_PHONE = "phone";
    public static final String PARAMKEY_USERID = "userId";
    public static final String PARAMKEY_SELLPOINTID = "sellPointId";
    public static final String PARAMKEY_REPAIRMAN = "repairMan";
    public static final String PARAMKEY_FAULT_CODE = "faultCode";
    public static final String PARAMKEY_FAULT_DESCRIBE = "faultDescribe";
    public static final String PARAMKEY_REPAIR_RECORD_ID = "repairRecordId";
    public static final String PARAMKEY_PASSWORD = "password";
    public static final String PARAMKEY_PLATFORM_SMS_AUTH_CODE = "platformSmsAuthCode";
    public static final String PARAMKEY_LANG = "lang";
    public static final String PARAMKEY_REMARK = "remark";
    public static final String PARAMKEY_IMAGEIDS = "imageIds";
    public static final String PARAMKEY_MACHINENO = "machineNO";
    public static final String PARAMKEY_MACHINEID = "machineId";
    public static final String PARAMKEY_APPID = "appid";
    public static final String PARAMKEY_SECRET = "secret";
    public static final String PARAMKEY_TOKEN = "token";
    public static final String PARAMKEY_NO = "no";
    public static final String PARAMKEY_SIM = "sim";
    public static final String PARAMKEY_DRIVER = "driver";
    public static final String PARAMKEY_DRIVERTEL = "driverTel";
    public static final String PARAMKEY_EMAIL = "email";
    public static final String PARAMKEY_BIRTHDAY = "birthday";
    public static final String PARAMKEY_SEX = "sex";
    public static final String PARAMKEY_OPERATE_CODE = "operaCode";
    public static final String PARAMKEY_PLATEFORM_TYPE = "platformType";

}
