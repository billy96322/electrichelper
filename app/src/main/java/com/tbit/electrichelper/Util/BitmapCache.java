package com.tbit.electrichelper.Util;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by Salmon on 2015/11/19.
 */
public class BitmapCache implements ImageLoader.ImageCache {

    private LruCache<String, Bitmap> mCache;

    public BitmapCache() {
        int maxMemery = (int) Runtime.getRuntime().maxMemory();
        int maxCacheSize = maxMemery / 8;
        mCache = new LruCache<String, Bitmap>(maxCacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR1)
                    return bitmap.getByteCount()/1024;
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        };
    }

    @Override
    public Bitmap getBitmap(String url) {
        return mCache.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        mCache.put(url, bitmap /*ThumbnailUtils.extractThumbnail(bitmap, 120, 120)*/);
    }
}
