package com.tbit.electrichelper.Util;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.DataAsyncHttpResponseHandler;
import com.tbit.electrichelper.Beans.TbitResponse;
import com.tbit.electrichelper.MyApplication;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import cz.msebera.android.httpclient.Header;

/**
 * 文件下载类
 * Created by Salmon on 2015-11-24.
 */
public class UpdateUtils {


    public static void getNewestVersion(final Integer tag, String url, final NetworkCallback mCallback) {

        AsyncHttpClient client = MyApplication.getInstance().getAsyncHttpClient();

        client.get(url, new DataAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream version = new ByteArrayInputStream(responseBody);
                int result = -1;
                try {
                    SAXBuilder builder = new SAXBuilder(false);
                    Document doc = builder.build(version);
                    Element root = doc.getRootElement();
                    Element versionString = root.getChild("v");
                    result = Integer.parseInt(versionString.getValue());
                } catch (JDOMException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                TbitResponse tbitResponse = new TbitResponse();
                tbitResponse.setRes(true);
                tbitResponse.setResult(String.valueOf(result));
                mCallback.parseResults(tbitResponse, tag);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                mCallback.onFailure(tag);
            }
        }).setTag(tag);
    }

    public static void getVersionDesc(final Integer tag, String url, final NetworkCallback mCallback) {

        AsyncHttpClient client = MyApplication.getInstance().getAsyncHttpClient();

        client.get(url, new DataAsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream version = new ByteArrayInputStream(responseBody);
                String result = "";
                TbitResponse tbitResponse = new TbitResponse();
                try {
                    UpdateSescribeParser parser = new UpdateSescribeParser();
                    result = parser.getDescribe(version);
                    tbitResponse.setRes(true);
                    tbitResponse.setResult(result);
                } catch (Exception e) {
                    e.printStackTrace();
                    tbitResponse.setRes(false);
                }
                mCallback.parseResults(tbitResponse, tag);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                mCallback.onFailure(tag);
            }
        }).setTag(tag);
    }

}
