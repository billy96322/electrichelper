package com.tbit.electrichelper.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.tbit.electrichelper.R;
import com.tbit.electrichelper.Util.TbitUtil;


/**
 * Created by Kenny on 2016/2/23 18:42.
 * Desc：
 */
public class DashboardView extends View {

    private static final String TAG = DashboardView.class.getSimpleName();

    private Paint paint1; //圈圈和指标
    private Paint paint2;  //标字
    private Paint paint3;  //圆心
    private RectF r1;
    //    private Rect mileage;  //里程显示
    private Bitmap point;  //点
    private Bitmap pointer;  //指针

    private float pointerAngle = 60;
    private float mProgress = 35;
    private int mMileage = 23541;

    private int mHeight;
    private int mWidth;
    private int range;

    /**
     * @param context
     * @param attrs
     */
    public DashboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        initview();
    }

    /**
     *
     */
    private void initview() {
        // TODO Auto-generated method stub
        paint1 = new Paint();
        paint1.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        paint1.setStyle(Paint.Style.STROKE);
        paint1.setStrokeWidth(4);
        paint1.setAntiAlias(true);

        paint2 = new Paint();
        paint2.setTextSize(16);
        paint1.setStyle(Paint.Style.STROKE);
        paint2.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        paint2.setStrokeWidth(2);
        paint2.setAntiAlias(true);

        paint3 = new Paint();
        paint3.setAntiAlias(true);
        paint3.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));                    //设置画笔颜色
//        paint3.setStrokeWidth((float) 3.0);
        paint3.setStyle(Paint.Style.FILL);

//        r1 = new RectF(-200, -200, 200, 200);
//        mileage = new Rect(-80, 100, 80, 150);

        point = BitmapFactory.decodeResource(getResources(), R.drawable.centure_icon_clock);
        pointer = BitmapFactory.decodeResource(getResources(), R.drawable.pointer_icon);

    }

    /**
     * 设置指针指示
     *
     * @param progress
     */
    public void setProgress(int progress) {
        mProgress = progress;
        invalidate();
    }

    public void setMileage(int mileage) {
        mMileage = mileage;
        invalidate();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        // setMeasuredDimension(600, 300);
        Log.i(TAG, "--height=" + heightMeasureSpec + "--width=" + widthMeasureSpec);
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
        range = Math.min(mHeight, mWidth) - 30;//获取最小值
        r1 = new RectF(-range, -range, range, range);
//        mileage = new Rect(-(mHeight - 120), mHeight - 100, (mHeight - 120), mHeight - 50);
    }

    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        mWidth = TbitUtil.px2dip(getContext(), specSize);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text
            result = (int) getWidth();
            if (specMode == MeasureSpec.AT_MOST) {
                // Respect AT_MOST value if that was what is called for by
                // measureSpec
                result = Math.min(result, specSize);// 60,480
            }
        }
        Log.i(TAG, "--width=" + result);
        return specSize;
    }

    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        mHeight = TbitUtil.px2dip(getContext(), specSize);
        Log.i(TAG, "--specSize=" + specSize);

        // mAscent = (int) mPaint.ascent();
        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
            Log.i(TAG, "--EXACTLY=");
        } else {
            // Measure the text (beware: ascent is a negative number)
            result = (int) getHeight();
            Log.i(TAG, "--result=" + result);
            if (specMode == MeasureSpec.AT_MOST) {
                // Respect AT_MOST value if that was what is called for by
                // measureSpec
                Log.i(TAG, "--AT_MOST=");
                result = Math.min(result, specSize);
            }
        }
        Log.i(TAG, "--height=" + result);
        return specSize;
    }

    /*
         * (non-Javadoc)
         *
         * @see android.view.View#onDraw(android.graphics.Canvas)
         */
    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        canvas.translate(canvas.getWidth() / 2, canvas.getHeight() / 2); // 将原点移到画布中心
        canvas.drawText("km/h", -paint2.measureText("km/h", 0, "km/h".length()) / 2, -(range / 2), paint2); // km/h
//        canvas.drawRect(mileage, paint3);
        String showMileage = String.valueOf(mMileage);
        canvas.drawText(showMileage, -paint2.measureText(showMileage, 0, showMileage.length()) / 2, (range / 1.2f), paint2); // mileage

        canvas.save();
        canvas.rotate(-120, 0f, 0f);
        for (int i = 0; i < 9; i++) {
            canvas.drawLine(0, -range, 0, -(range + 10), paint1);
            canvas.drawText(i + "k", -paint2.measureText(i + "k", 0, (i + "k").length()) / 2, -(range + 20), paint2);
            canvas.rotate(30, 0f, 0f);
        }
        canvas.restore();
        canvas.drawArc(r1, 150, 240, false, paint1);
//        canvas.drawBitmap(point, -point.getWidth() / 2, -point.getHeight() / 2, paint1);
        canvas.drawCircle(0, 0, 5, paint3);
        canvas.rotate(pointerAngle + mProgress, 0f, 0f);
//		canvas.drawBitmap(pointer, -pointer.getWidth() / 2, pointer.getWidth() / 2, paint1);
        canvas.drawLine(0, 0, 0, range - 20, paint1);


    }
}
