package com.tbit.electrichelper.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.tbit.electrichelper.Activities.ScanActivity;
import com.tbit.electrichelper.R;


/**
 * Created by Kenny on 2016/2/4 13:43.
 * Desc：A view for scanning animation
 */
public class ScanView extends View {

    private Bitmap bg;
    private Bitmap arrow;
    private Bitmap btn;
    private float offset;
    private boolean isScanning;
    private MyCallback myCallback;
    private Handler mHandler;

    private static final String TAG = ScanView.class.getSimpleName();

    public interface MyCallback {
        void isScanning(boolean isScanning);
    }

    public void setMyCallback(MyCallback callback) {
        myCallback = callback;
    }

    public boolean isScanning() {
        return isScanning;
    }

    public void setIsScanning(boolean isScanning) {
        this.isScanning = isScanning;
    }


    public ScanView(Context context) {
        super(context);
        init();

    }

    public ScanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScanView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        mHandler = new Handler();
        if (bg == null && arrow == null && btn == null) {
            bg = BitmapFactory.decodeResource(getResources(), R.drawable.gplus_search_bg);
            arrow = BitmapFactory.decodeResource(getResources(), R.drawable.gplus_search_args);
            btn = BitmapFactory.decodeResource(getResources(), R.drawable.locus_round_click);
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(bg, getWidth() / 2 - bg.getWidth() / 2, getHeight() / 2 - bg.getHeight() / 2, null);

        if (isScanning) {
            Rect rect_arrow = new Rect(getWidth() / 2 - arrow.getWidth(), getHeight() / 2, getWidth() / 2, getHeight() / 2 + arrow.getHeight());
            canvas.rotate(offset, getWidth() / 2, getHeight() / 2);
            canvas.drawBitmap(arrow, null, rect_arrow, null);
            offset += 3;
        }

        canvas.drawBitmap(btn, getWidth() / 2 - btn.getWidth() / 2, getHeight() / 2 - btn.getHeight() / 2, null);

        if (isScanning)
            this.invalidate();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handleAction(event);
                return true;

        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean performClick() {
        Log.i(TAG, "--模拟点击中心动画按钮");
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                    isScanning = false;
                myCallback.isScanning(false);
                setIsScanning(false);
            }
        }, ScanActivity.SCAN_PERIOD);
        if (isScanning) {
            myCallback.isScanning(false);
            setIsScanning(false);
        } else {
            myCallback.isScanning(true);
            setIsScanning(true);
        }


        invalidate();
        return true;
    }

    public void stopScanning() {
        myCallback.isScanning(false);
        setIsScanning(false);
    }

    private void handleAction(MotionEvent event) {

        Rect rect_btn = new Rect(getWidth() / 2 - btn.getWidth() / 2, getHeight() / 2 - btn.getHeight() / 2, getWidth() / 2 + btn.getWidth() / 2, getHeight() / 2 + btn.getHeight() / 2);
        if (rect_btn.contains((int) event.getX(), (int) event.getY())) {
            Log.i(TAG, "--点击中心动画按钮");
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    isScanning = false;
                    myCallback.isScanning(false);
                    setIsScanning(false);
                }
            }, ScanActivity.SCAN_PERIOD);
            if (isScanning) {
                myCallback.isScanning(false);
                setIsScanning(false);
            } else {
                myCallback.isScanning(true);
                setIsScanning(true);
            }

            invalidate();
        }

    }
}
