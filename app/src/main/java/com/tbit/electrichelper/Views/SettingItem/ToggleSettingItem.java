package com.tbit.electrichelper.Views.SettingItem;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tbit.electrichelper.R;


/**
 * Created by Salmon on 2016/4/25 0025.
 */
public class ToggleSettingItem extends SettingItem {
    private boolean isCheck = false;
    private SwitchCompat mSwitchButton;

    public ToggleSettingItem(Context context) {
        this(context, null);
    }

    public ToggleSettingItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.ToggleSettingItem);

        if (ta != null) {
            isCheck = ta.getBoolean(R.styleable.ToggleSettingItem_toggle_check, false);
        }

        ta.recycle();

        mSwitchButton.setChecked(isCheck);
    }

    @Override
    protected View getRightView() {
        mSwitchButton = new SwitchCompat(getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mSwitchButton.setLayoutParams(params);

        return mSwitchButton;
    }

    public void setCheck(boolean isCheck) {
        hideHelpView();
        this.isCheck = isCheck;
        mSwitchButton.setChecked(isCheck);
        requestLayout();
    }

    public boolean isCheck() {
        return isCheck;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }
}
