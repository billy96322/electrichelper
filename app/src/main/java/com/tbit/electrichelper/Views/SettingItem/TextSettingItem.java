package com.tbit.electrichelper.Views.SettingItem;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tbit.electrichelper.R;


/**
 * Created by Salmon on 2016/4/25 0025.
 */
public class TextSettingItem extends SettingItem {
    private TextView mText;

    private String mTextContent;
    private int mTextSize;
    private int mTextColor;

    public TextSettingItem(Context context) {
        this(context, null);
    }

    public TextSettingItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDefaultValue();

        init(attrs);
    }

    private void setDefaultValue() {
        mTextContent = "";
        mTextSize = 0;
        mTextColor = DEFAULT_COLOR;
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.TextSettingItem);
        if (ta != null) {
            mTextContent = ta.getString(R.styleable.TextSettingItem_settingText);
            mTextSize = ta.getDimensionPixelSize(R.styleable.TextSettingItem_settingTextSize, 0);
            mTextColor = ta.getColor(R.styleable.TextSettingItem_settingTextColor, DEFAULT_COLOR);
        }

        if (mTextSize == 0) {
            mTextSize = 14;
        } else {
            mTextSize = px2dp(mTextSize);
        }

        mText.setText(mTextContent);
        mText.setTextSize(mTextSize);
        mText.setTextColor(mTextColor);

        ta.recycle();
    }

    @Override
    protected View getRightView() {
        mText = new TextView(getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mText.setLayoutParams(params);
        return mText;
    }

    public void setRightTextColor(int color) {
        mText.setTextColor(color);
    }

    public void setRightTextSize(int size) {
        mText.setTextSize(size);
    }

    public void setRightText(String content) {
        hideHelpView();
        mText.setText(content);
    }

    public String getRightText() {
        return mText.getText().toString();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }
}
