package com.tbit.electrichelper.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tbit.electrichelper.Activities.NetworkPhotoViewActivity;
import com.tbit.electrichelper.Activities.PhotoViewActivity;
import com.tbit.electrichelper.Beans.Image;
import com.tbit.electrichelper.R;

import java.io.File;
import java.util.List;

public class GalleryAdapter extends
        RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<Image> mDatas;
    private Context mContext;
    private boolean isUpload;
    private AlertDialog deleteDialog;

    public GalleryAdapter(Context context, List<Image> datats, boolean isUpload) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDatas = datats;
        this.isUpload = isUpload;
    }

    public void updateDatas(List<Image> datas) {
        mDatas = datas;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    /**
     * 创建ViewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.activity_index_gallery_item,
                viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, new ViewHolder.IMyViewHolderClicks() {
            @Override
            public void onImageClick(int position) {
                if (isUpload) {
                    PhotoViewActivity.enterActivity(mContext, mDatas.get(position).getUrl());
                } else {
                    NetworkPhotoViewActivity.startActivity(mContext, mDatas.get(position).getUrl());
                }
            }

            @Override
            public void onImageLongClick(final int position) {
                if (!isUpload) {
                    return;
                }
                showDeleteDialog(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDatas.remove(position);
                        notifyDataSetChanged();
                        deleteDialog.dismiss();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteDialog.dismiss();
                    }
                });
            }
        });

        viewHolder.mImg = (ImageView) view
                .findViewById(R.id.id_index_gallery_item_image);
        return viewHolder;
    }

    /**
     * 设置值
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
//        Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(mDatas.get(i)), 80, 80);
//        viewHolder.mImg.setImageBitmap(mDatas.get(i).getBitmap());
        if (isUpload) {
            String url = mDatas.get(i).getUrl();
            Picasso.with(mContext)
                    .load(new File(url))
                    .resize(200, 200)
                    .centerCrop()
                    .into(viewHolder.mImg);
        } else {
            String url = mDatas.get(i).getUrl();
            Log.d("asd", ">>"+url);
            Picasso.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.ic_photo_primarydark_48dp)
                    .error(R.drawable.ic_broken_image_red_400_48dp)
                    .resize(200, 200)
                    .centerCrop()
                    .into(viewHolder.mImg);
        }

    }

    private void showDeleteDialog(DialogInterface.OnClickListener positiveListener,
                                  DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.delete);
        builder.setMessage(R.string.alert_delete);
        builder.setPositiveButton(R.string.confirm, positiveListener);
        builder.setNegativeButton(R.string.cancel, negativeListener);
        deleteDialog = builder.create();
        deleteDialog.show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImg;
        IMyViewHolderClicks mListener;

        public ViewHolder(View itemLayoutView, IMyViewHolderClicks listener) {
            super(itemLayoutView);
            mListener = listener;
            mImg = (ImageView) itemLayoutView
                    .findViewById(R.id.id_index_gallery_item_image);
            mImg.setOnClickListener(this);
            mImg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onImageLongClick(getLayoutPosition());
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            mListener.onImageClick(getLayoutPosition());
        }

        public interface IMyViewHolderClicks {
            void onImageClick(int position);

            void onImageLongClick(int position);
        }
    }

}
