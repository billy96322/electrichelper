package com.tbit.electrichelper.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.tbit.electrichelper.Activities.StatusDetailActivity;
import com.tbit.electrichelper.R;

import java.util.List;
import java.util.Map;

/**
 * Created by Salmon on 2016/3/2.
 */
public class ExpandableAdapter extends BaseExpandableListAdapter {
    private List<Map<String, Object>> data;
    private LayoutInflater inflater;
    private Context context;

    public ExpandableAdapter(Context context,List<Map<String, Object>> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Map<String,Object> map = data.get(groupPosition);
        List<Map<String,String>> childList = (List<Map<String, String>>) map.get(StatusDetailActivity.ITEM_CHILD);
        return childList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Map<String,Object> map = data.get(groupPosition);
        List<Map<String,String>> childList = (List<Map<String, String>>) map.get(StatusDetailActivity.ITEM_CHILD);
        return childList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Map<String,Object> map = data.get(groupPosition);
        GroupHolder groupHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_status_detail, null);
            groupHolder = new GroupHolder();
            groupHolder.textGroupStatus = (TextView) convertView.findViewById(R.id.itemStatus);
            groupHolder.textGroupName = (TextView) convertView.findViewById(R.id.itemName);
            convertView.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }

        groupHolder.textGroupStatus.setText(String.valueOf(map.get(StatusDetailActivity.ITEM_STATUS)));
        groupHolder.textGroupName.setText(String.valueOf(map.get(StatusDetailActivity.ITEM_NAME)));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Map<String,Object> map = data.get(groupPosition);
        List<Map<String,String>> childList = (List<Map<String, String>>) map.get(StatusDetailActivity.ITEM_CHILD);
        Map<String,String> childMap = childList.get(childPosition);
        ChildHolder childHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_status_detail_child, null);
            childHolder = new ChildHolder();
            childHolder.textChildName = (TextView) convertView.findViewById(R.id.text_status_name_child);
            childHolder.textChildStatus = (TextView) convertView.findViewById(R.id.text_status_child);
            convertView.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) convertView.getTag();
        }

        childHolder.textChildName.setText(String.valueOf(childMap.get(StatusDetailActivity.ITEM_NAME)));
        childHolder.textChildStatus.setText(String.valueOf(childMap.get(StatusDetailActivity.ITEM_STATUS)));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class GroupHolder {
        TextView textGroupName;
        TextView textGroupStatus;
    }

    class ChildHolder{
        TextView textChildName;
        TextView textChildStatus;
    }
}
