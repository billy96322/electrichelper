package com.tbit.electrichelper.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tbit.electrichelper.Beans.CarData;
import com.tbit.electrichelper.MyApplication;
import com.tbit.electrichelper.R;

import java.util.List;

/**
 * Created by Salmon on 2016/3/2.
 */
public class BoundAdapter extends BaseAdapter {
    private List<CarData.CarsEntity> data;
    private Context context;
    private LayoutInflater inflater;

    public BoundAdapter(Context context, List<CarData.CarsEntity> data) {
        this.data = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_bound, null);
            holder = new ViewHolder();
            holder.textMachineNo = (TextView) convertView.findViewById(R.id.text_bound_machine_id);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (MyApplication.getInstance().getCurCar().getCarId().equals(data.get(position).getCarId())) {
            holder.textMachineNo.setTextColor(ContextCompat.getColor(context, R.color.yadea_orange));
        } else {
            holder.textMachineNo.setTextColor(ContextCompat.getColor(context, R.color.material_grey_800));
        }
        holder.textMachineNo.setText(data.get(position).getMachineNO());

        return convertView;
    }

    class ViewHolder {
        TextView textMachineNo;
    }
}
