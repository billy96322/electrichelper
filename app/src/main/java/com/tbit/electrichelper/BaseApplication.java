package com.tbit.electrichelper;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.baidu.mapapi.SDKInitializer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.MySSLSocketFactory;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tbit.electrichelper.Util.CrashHandler;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * Created by Salmon on 2015/11/11.
 */
public class BaseApplication extends Application {

    protected RequestQueue mQueue;
    protected AsyncHttpClient asyncHttpClient;
    public boolean DEBUG_MODE = true;

    public static RefWatcher getRefWatcher(Context context) {
        BaseApplication application = (BaseApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    private RefWatcher refWatcher;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // 全局异常处理
//        CrashHandler crashHandler = CrashHandler.getInstance();
//        crashHandler.init(getApplicationContext());

        refWatcher = LeakCanary.install(this);
        SDKInitializer.initialize(getApplicationContext());
    }


    public synchronized RequestQueue getRequestQueue()
    {
        if(null == mQueue){
            mQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mQueue;
    }

    public synchronized AsyncHttpClient getAsyncHttpClient() {

        if (null == asyncHttpClient) {
            asyncHttpClient = new AsyncHttpClient();
            String localCookie = MyApplication.getInstance().getLocalCookie();
            if (!localCookie.equals("")) {
                asyncHttpClient.addHeader("Cookie", localCookie);
            }

            KeyStore trustStore = null;
            try {
                trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(null, null);
                MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
                sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                asyncHttpClient.setSSLSocketFactory(sf);
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (UnrecoverableKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }

        return asyncHttpClient;
    }
}
